#ifndef TOWER_ATTACK_MAP_CONVERTOR
#define TOWER_ATTACK_MAP_CONVERTOR

#define TAMC_SYNC_MARK '$'
#define TAMC_PATH      ' '
#define TAMC_FREE      '#'
#define TAMC_EXIT      '>'
#define TAMC_COMMENT   ';'
#define TAMC_BUFFER_SIZE 1024
#define TAMC_MAX_W 50
#define TAMC_MAX_H 20

namespace TowerAttack
{
    namespace Map
    {
        /// Reads text file and converts it to map binary.
        void TxtToMap(const char * source, const char * dest);
    }
}

#endif /* TOWER_ATTACK_MAP_CONVERTOR */