#ifndef TOWER_ATTACK_MAP
#define TOWER_ATTACK_MAP

#include "Tile.h"

#include <Entities/Tower.h>
#include <Graphics.h>
#include <Geometry.h>
#include <Rendering.h>

#include <string>
#include <vector>
#include <set>

namespace TowerAttack
{
    class Game;
    namespace Maps
    {
        class Map
        {
        public:
            /// Loads new map from file.
            /*!
             * @param filepath path to file to load map from.
             */
            explicit Map(const std::string& filepath);
            /// Creates empty map of size.
            /*!
             * @param size size of the new map.
             */
            explicit Map(const Boris::Geometry::Size& size = {0, 0});
            Map(const Map& other);
            Map(Map&& other) = default;
            virtual ~Map() = default;

            Map& operator=(const Map& other);
            Map& operator=(Map&& other) noexcept;

            /// Load map from file.
            /*!
             * Loads map from file. Overwrites current map.
             * @param filepath path to file to load map from.
             */
            void Load(const std::string& filepath);
            /// Save map to file.
            /*!
             * Saves map to file.
             * @param filepath path to file to save map to.
             */
            void Save(const std::string& filepath) const;

            /// Get tile at coordinates.
            /*!
             * @param x x-coordinate
             * @param y y-coordinate
             * @return tile at coordinates.
             */
            const Tile& At(uint16_t x, uint16_t y) const;
            const Boris::Geometry::Size& Size() const;
            const std::set<unsigned int>& Entrances() const;
            uint16_t Exit() const;

            void BuildTower(const Boris::Geometry::Point& position, const Entities::Tower& tower);

            const Boris::Rendering::RectangleRenderer& Renderer() const;

        private:
            /// Converts map to texture and returns it.
            Boris::Graphics::Texture Texture() const;
            /// Evaluates map tiles.
            void Evaluate();

            Boris::Geometry::FPoint m_Position;
            Boris::Rendering::RectangleRenderer m_Renderer;
            std::vector<std::vector<Tile>> m_Map;
            Boris::Geometry::Size m_Size;
            std::set<uint32_t> m_Entrances;
            uint16_t m_Exit;

            friend class TowerAttack::Game;
        };
    }
}

#endif /* TOWER_ATTACK_MAP */