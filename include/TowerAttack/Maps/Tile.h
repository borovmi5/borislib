#ifndef TOWER_ATTACK_TILE
#define TOWER_ATTACK_TILE

#include <cstdint>

namespace TowerAttack
{
    namespace Maps
    {
        class Tile
        {
        public:
            using Type = enum class e_Tile
            {
                PATH,
                FREE,
                OCCUPIED
            };

            explicit Tile(Type tile = Type::FREE);
            Tile(const Tile& other) = default;
            Tile(Tile&& other) = default;
            virtual ~Tile() = default;

            Tile& operator=(const Tile& other) = default;
            Tile& operator=(Tile&& other) = default;

            bool operator==(const Tile& other) const;
            bool operator!=(const Tile& other) const;

            uint32_t Distance() const;
            double RiskLevel() const;

            bool IsFree() const;
            bool IsAccessible() const;

        private:
            friend class Map;

            void Distance(uint32_t dist);
            void RiskLevel(double risk);

            Type m_Type;
            uint32_t m_Distance;
            double m_RiskLevel;
        };
    }
}

#endif /* TOWER_ATTACK_TILE */