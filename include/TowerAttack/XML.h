#ifndef TOWER_ATTACK_XML
#define TOWER_ATTACK_XML

#include <string>
#include <map>

#include <libxml/tree.h>

namespace TowerAttack
{
    class XML;
    class XMLAttribute
    {
    public:
        // Implicit conversion of m_Value
        explicit operator bool() const;
        explicit operator char() const;
        explicit operator unsigned char() const;
        explicit operator const char *() const;
        operator std::string() const;
        explicit operator int() const;
        explicit operator unsigned int() const;
        explicit operator long() const;
        explicit operator unsigned long() const;
        explicit operator double() const;

    private:
        XMLAttribute(xmlAttrPtr attr);
        std::string m_Value;

        friend class XML;
    };

    class XML
    {
    public:
        /// Parse XML file.
        explicit XML(const std::string& filepath);

        /// XML structure child access
        const XML& operator[](const char * key) const;
        /// XML structure argument access
        const XMLAttribute& operator()(const char * key) const;

        const std::multimap<std::string, XML>& Childs() const;

        explicit operator bool() const;
        explicit operator char() const;
        explicit operator unsigned char() const;
        explicit operator const char *() const;
        operator std::string() const;
        explicit operator int() const;
        explicit operator long() const;
        explicit operator unsigned long() const;
        explicit operator double() const;

    private:
        /// Create XML structure from xmlNodePtr.
        XML(xmlNodePtr node);
        /// Check whether xmlNodePtr has children.
        static bool IsLeaf(xmlNodePtr node);

        std::multimap<std::string, XML> m_Childs;
        std::map<std::string, XMLAttribute> m_Attributes;
        std::string m_Value;
    };
}

#endif /* TOWER_ATTACK_XML */