#ifndef TOWER_ATTACK_SPAWN_EVENT_ARGS
#define TOWER_ATTACK_SPAWN_EVENT_ARGS

#include <Events.h>
#include <Entities/AttackerPrototype.h>

namespace TowerAttack
{
    class SpawnEventArgs : public Boris::Events::EventArgs
    {
    public:
        explicit SpawnEventArgs(const Entities::AttackerPrototype& attacker);
        SpawnEventArgs(const SpawnEventArgs& other) = default;
        SpawnEventArgs(SpawnEventArgs&& other) noexcept = default;
        ~SpawnEventArgs() override = default;

        const Entities::AttackerPrototype& Attacker() const;

    private:
        const Entities::AttackerPrototype& m_Attacker;
    };

    using SpawnEventHandler = Boris::Events::EventHandler<SpawnEventArgs>;
    using SpawnEvent = Boris::Events::Event<SpawnEventHandler >;
}

#endif /* SPAWN_EVENT_ARGS */