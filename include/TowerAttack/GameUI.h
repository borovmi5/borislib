#ifndef TOWER_ATTACK_GAME_UI
#define TOWER_ATTACK_GAME_UI

#include <Entities.h>
#include <UI.h>

#include "SpawnEventArgs.h"

namespace TowerAttack
{
    class GameUI
    {
    public:
        explicit GameUI(const Entities::AttackerSet& attackers);
        GameUI(const GameUI&) = delete;
        GameUI(GameUI&&) = delete;
        ~GameUI();

        GameUI& operator=(const GameUI&) = delete;
        GameUI& operator=(GameUI&&) = delete;

        void Resources(double value);
        void Income(int value);
        void Time(int value);
        void Goal(int value);
        void AtExit(int value);

        SpawnEvent OnSpawn;

    private:
        void Spawn(const Boris::Object& sender, const Boris::Events::EventArgs& e);

        Boris::UI::Panel * m_Top;
        Boris::UI::Panel * m_Right;
        Boris::UI::Label * m_Resources;
        Boris::UI::Label * m_ResourcesCurr;
        Boris::UI::Label * m_Income;
        Boris::UI::Label * m_TimeMinS;
        Boris::UI::Label * m_TimeCs;
        Boris::UI::Label * m_Goal;
        Boris::UI::Label * m_AtExit;

        const Entities::AttackerSet& m_Attackers;
        Boris::Events::BasicEventHandler m_SpawnHandler;
    };
}

#endif /* TOWER_ATTACK_GAME_UI */