#ifndef TOWER_ATTACK_ENTITIES
#define TOWER_ATTACK_ENTITIES

#include "Entities/ProjectilePrototype.h"
#include "Entities/TowerPrototype.h"
#include "Entities/AttackerPrototype.h"
#include "Entities/LevelPrototype.h"

#include "Entities/Projectile.h"
#include "Entities/Attacker.h"
#include "Entities/Tower.h"

namespace TowerAttack
{
    /// Game entities name space.
    namespace Entities
    {}
}

#endif /* TOWER_ATTACK_ENTITIES */