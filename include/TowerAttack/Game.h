#ifndef TOWER_ATTACK_GAME
#define TOWER_ATTACK_GAME

#include <Maps/Map.h>
#include <Entities.h>
#include <Core.h>
#include <Events.h>

#include "GameUI.h"

#include <string>
#include <map>

using namespace std;
using namespace Boris;
using namespace Boris::Events;
using namespace TowerAttack::Entities;
using namespace TowerAttack::Maps;

namespace TowerAttack
{
    class Game
    {
    public:
        Game(LevelPrototype& level);
        Game(const Game&) = delete;
        Game(Game&&) = delete;
        ~Game() = default;

        Game& operator=(const Game&) = delete;
        Game& operator=(Game&&) = delete;

        /// Binds projectile to the game.
        void AddProjectile(Projectile&& projectile);
        /// Removes projectile from the game.
        void RemoveProjectile(const Object& sender, const EventArgs& e);

        /// Binds attacker to the game.
        void AddAttacker(Attacker&& attacker);
        /// Removes attacker.
        /*!
         * Removes attacker from the game and increments
         * counter of attackers who have reached the exit.
         */
        void AttackerAtExit(const Object& sender, const EventArgs& e);
        /// Removes attacker.
        void AttackerDied(const Object& sender, const EventArgs& e);

        /// Binds tower to the game.
        void AddTower(Tower&& tower);

        const Map& GetMap() const;
        const map<unsigned int, Projectile>& GetProjectiles() const;
        const map<unsigned int, Attacker>& GetAttackers() const;

        const LevelPrototype& Level() const;

        TimePassedEvent OnUpdate;

    private:
        void Update(const Object& sender, const TimePassedEventArgs& e);
        void RenderGame(const Object& sender, const EventArgs& e);
        void Spawn(const Object& sender, const SpawnEventArgs& e);

        LevelPrototype& m_Level;

        map<unsigned int, Projectile> m_Projectiles;
        map<unsigned int, Attacker> m_Attackers;
        map<unsigned int, Tower> m_Towers;

        set<unsigned int> m_ProjectilesToDelete;
        set<unsigned int> m_AttackersToDelete;

        set<unsigned int> m_ProjectilesToAdd;

        TimePassedEventHandler m_UpdateHandler;
        BasicEventHandler m_RenderHandler;
        BasicEventHandler m_RemoveProjectileHandler;
        BasicEventHandler m_AttackerAtExitHandler;
        BasicEventHandler m_AttackerDiedHandler;
        SpawnEventHandler m_SpawnHandler;

        int m_AtExit;
        int m_RemainingTime;
        double m_Resources;

        GameUI m_UI;
    };
}

#endif /* TOWER_ATTACK_GAME */