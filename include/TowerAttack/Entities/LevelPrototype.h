#ifndef TOWER_ATTACK_LEVEL_PROTO
#define TOWER_ATTACK_LEVEL_PROTO

#include <Maps/Map.h>

#include <string>
#include <map>

namespace TowerAttack
{
    class Game;
    namespace Entities
    {
        class LevelPrototype;

        /// Map of levels.
        using LevelSet = std::map<std::string, LevelPrototype>;

        /// Parsed Level XML and Map file
        /*!
         * Serves as base for Game construction.
         */
        class LevelPrototype
        {
        public:
            explicit LevelPrototype(const std::string& filepath, const TowerSet& towers, const AttackerSet& attackers);

            const std::string& SysName() const;
            const std::string& DispName() const;
            const std::string& Description() const;
            std::pair<int, int> PlayerResources() const;
            std::pair<int, int> AIResources() const;
            int Goal() const;
            int TimeLimit() const;
            const TowerSet& Towers() const;
            const AttackerSet& Attackers() const;
            const Maps::Map& Map() const;

            static LevelSet LoadLevels(const TowerSet& towers, const AttackerSet& attackers);
        private:
            std::string m_SysName;
            std::string m_DispName;
            std::string m_Description;
            int m_PlayerResources;
            int m_PlayerIncome;
            int m_AIResources;
            int m_AIIncome;
            int m_Goal;
            int m_TimeLimit;
            TowerSet m_Towers;
            AttackerSet m_Attackers;

            Maps::Map m_Map;

            static const char * s_LevelDirectory;

            friend class TowerAttack::Game;
        };
    }
}

#endif /* TOWER_ATTACK_LEVEL_PROTO */