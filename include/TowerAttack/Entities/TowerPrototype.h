#ifndef TOWER_ATTACK_TOWER_PROTO
#define TOWER_ATTACK_TOWER_PROTO

#include <string>

#include "ProjectilePrototype.h"

namespace TowerAttack
{
    namespace Entities
    {
        class TowerPrototype;

        /// Map of towers.
        using TowerSet = std::map<std::string, TowerPrototype>;

        /// Parsed tower XML
        /*!
         * Serves as base for Tower construction.
         */
        class TowerPrototype
        {
        public:
            /// Attacker selection strategy.
            using AttackerSelection = enum class e_Strategy
            {
                FIRST, LAST, STRONGEST, WEAKEST
            };

            /// Constructor.
            /*!
             * Constructs new tower.
             * @param filepath path to the tower def file.
             * @param projectiles map of available projectiles. Must not be destructed all the time tower prototype is used.
             * removing projectiles from the map or moving them may have undefined behavior.
             */
            explicit TowerPrototype(const std::string& filepath, const ProjectileSet& projectiles);

            const std::string& SysName() const;
            const std::string& DispName() const;
            const std::string& Description() const;
            const Boris::Graphics::Color& Color() const;
            const Boris::Graphics::Texture& Texture() const;
            AttackerSelection Strategy() const;
            int ViewRange() const;
            int RateOfFire() const;
            int Range() const;
            int Burst() const;
            int Cooldown() const;
            const ProjectilePrototype& Projectile() const;
            int Speed() const;
            int Spread() const;
            int Cost() const;

            static TowerSet LoadTowers(const ProjectileSet& projectiles);

        protected:
            std::string m_SysName;
            std::string m_DispName;
            std::string m_Description;
            Boris::Graphics::Color m_Color;
            Boris::Graphics::Texture m_Texture;
            AttackerSelection m_Strategy;
            int m_ViewRange;
            int m_RateOfFire;
            int m_Range;
            int m_Burst;
            int m_BurstCooldown;
            const ProjectilePrototype * m_Projectile;
            int m_ProjectileSpeed;
            int m_ProjectileSpread;
            int m_Cost;

            static const char * s_TowerDirectory;
        };
    }
}

#endif /* TOWER_ATTACK_TOWER_PROTO */