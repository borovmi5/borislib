#ifndef TOWER_ATTACK_PROJECTILE
#define TOWER_ATTACK_PROJECTILE

#include <Core.h>
#include <Events.h>
#include <Geometry.h>
#include <Rendering.h>
#include "ProjectilePrototype.h"

namespace TowerAttack
{
    class Game;
    namespace Entities
    {
        class Projectile : public Boris::PositionedObject, public ProjectilePrototype
        {
        public:
            /// Constructor.
            /*!
             * Constructs new projectile.
             * @param parent game projectile belongs to.
             * @param position position of the projectile.
             * @param direction direction of the projectile.
             * @param speed speed of the projectile.
             * @param range max range of the projectile.
             * @param prototype prototype to build projectile from.
             */
            Projectile(const Boris::Geometry::FPoint& position, double direction, double speed, double range, const ProjectilePrototype& prototype);
            Projectile(const Projectile&) = delete;
            Projectile(Projectile&& other) noexcept;
            ~Projectile() override = default;

            Projectile& operator=(const Projectile&) = delete;
            Projectile& operator=(Projectile&&) noexcept = delete;

            unsigned int ID() const;
            double Speed() const;
            double Range() const;
            const Boris::Rendering::RectangleRenderer& Renderer() const;
            double Direction() const;
            const Boris::Geometry::FPoint& Position() const;

            /// Update projectile info.
            /*!
             * Updates projectile position and removes
             * projectile when it is out of range.
             */
            void Update(const Object& sender, const Boris::Events::TimePassedEventArgs& e);

            Boris::Events::BasicEvent OnRemove;

        private:
            using Boris::PositionedObject::Position;
            using Boris::PositionedObject::Move;
            using Boris::PositionedObject::Rotation;
            using Boris::PositionedObject::Rotate;

            unsigned int m_ID; //!< Projectile ID.
            static unsigned int s_ID; //!< Projectile ID counter.

            const double m_Speed; //!< Speed of the projectile.
            double m_Range; //!< Maximal distance from current position projectile can travel.
            Boris::Rendering::RectangleRenderer m_Renderer; //!< Renderer.
            Boris::Events::TimePassedEventHandler m_Handler; //!< Event handler for updating position.

            friend class TowerAttack::Game;
        };
    }
}

#endif /* TOWER_ATTACK_PROJECTILE */