#ifndef TOWER_ATTACK_PROJECTILE_PROTO
#define TOWER_ATTACK_PROJECTILE_PROTO

#include <Graphics.h>

#include <string>
#include <map>

namespace TowerAttack
{
    namespace Entities
    {
        class ProjectilePrototype;

        /// Map of projectiles.
        using ProjectileSet = std::map<std::string, ProjectilePrototype>;

        /// Parsed projectile XML
        /*!
         * Serves as base for Projectile construction.
         */
        class ProjectilePrototype
        {
        public:
            explicit ProjectilePrototype(const std::string& filepath);

            const std::string& SysName() const;
            const std::string& DispName() const;
            const std::string& Description() const;
            const Boris::Graphics::Color& Color() const;
            const Boris::Graphics::Texture& Texture() const;
            int Damage() const;

            static ProjectileSet LoadProjectiles();
        protected:
            std::string m_SysName;
            std::string m_DispName;
            std::string m_Description;
            Boris::Graphics::Color m_Color;
            Boris::Graphics::Texture m_Texture;
            int m_Damage;

            static const char * s_ProjectileDirectory;
        };
    }
}

#endif /* TOWER_ATTACK_PROJECTILE_PROTO */