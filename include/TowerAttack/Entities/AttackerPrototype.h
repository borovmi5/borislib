#ifndef TOWER_ATTACK_ATTACKER_PROTO
#define TOWER_ATTACK_ATTACKER_PROTO

#include <Graphics.h>

#include <string>
#include <map>

namespace TowerAttack
{
    namespace Entities
    {
        class AttackerPrototype;

        /// Map of attackers.
        using AttackerSet = std::map<std::string, AttackerPrototype>;

        /// Parsed attacker XML
        /*!
         * Serves as base for Attacker construction.
         */
        class AttackerPrototype
        {
        public:
            /// Path selection strategy.
            using PathSelection = enum class e_Strategy
            {
                SHORTEST, SAFEST, AIR
            };


            explicit AttackerPrototype(const std::string& filepath);

            const std::string& SysName() const;
            const std::string& DispName() const;
            const std::string& Description() const;
            const Boris::Graphics::Color& Color() const;
            const Boris::Graphics::Texture& Texture() const;
            PathSelection Strategy() const;
            double Speed() const;
            int Health() const;
            int MaxShield() const;
            int Regeneration() const;
            double Evasion() const;
            const std::map<std::string, double>& Resistance() const;
            int Cost() const;

            static AttackerSet LoadAttackers();
        protected:
            std::string m_SysName;
            std::string m_DispName;
            std::string m_Description;
            Boris::Graphics::Color m_Color;
            Boris::Graphics::Texture m_Texture;
            PathSelection m_Strategy;
            double m_Speed;
            int m_Health;
            int m_MaxShield;
            int m_Regeneration;
            double m_Evasion;
            std::map<std::string, double> m_Resistance;
            int m_Cost;

            static const char * s_AttackerDirectory;
        };
    }
}

#endif /* TOWER_ATTACK_ATTACKER_PROTO */