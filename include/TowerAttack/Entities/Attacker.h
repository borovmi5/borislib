#ifndef TOWER_ATTACK_ATTACKER
#define TOWER_ATTACK_ATTACKER

#include <Core.h>
#include <Events.h>
#include <Geometry.h>
#include <Rendering.h>
#include <Maps/Map.h>
#include "AttackerPrototype.h"

namespace TowerAttack
{
    class Game;
    namespace Entities
    {
        class Attacker : public Boris::PositionedObject, public AttackerPrototype
        {
        public:
            /// Constructor.
            /*!
             * Constructs new attacker.
             * @param entrance y-coord of the entrance.
             * @param prototype prototype to construct attacker from.
             * @param map reference to map.
             */
            Attacker(unsigned int entrance, const AttackerPrototype& prototype, const TowerAttack::Maps::Map& map);
            Attacker(const Attacker&) = delete;
            Attacker(Attacker&& other) noexcept;
            ~Attacker() override = default;

            Attacker& operator=(const Attacker&) = delete;
            Attacker& operator=(Attacker&&) noexcept = delete;

            unsigned int ID() const;
            /// Returns true if has health > 0.
            bool Alive() const;
            const Boris::Rendering::RectangleRenderer& Renderer() const;
            const Boris::Geometry::FPoint& Position() const;
            const Boris::Geometry::FPoint& Waypoint() const;

            int Shield() const;

            /// Deal damage to attacker based on projectile type.
            void Hit(const ProjectilePrototype& projectile);
            /// Test projectile and attacker on intersection.
            bool TryHit(const Projectile& projectile) const;

            /// Update attacker info.
            /*!
             * Updates attacker position and changes path when needed.
             */
            void Update(const Object& sender, const Boris::Events::TimePassedEventArgs& e);

            Boris::Events::BasicEvent OnExitReached;
            Boris::Events::BasicEvent OnDeath;

        protected:
            using Boris::PositionedObject::Position;
            using Boris::PositionedObject::Move;
            using Boris::PositionedObject::Rotation;
            using Boris::PositionedObject::Rotate;

            /// Set next waypoint.
            virtual void NextWaypoint();

            const TowerAttack::Maps::Map& m_Map; //!< Reference to the map.

            Boris::Geometry::FPoint m_Waypoint; //!< Next waypoint.

            unsigned int m_ID; //!< Attacker ID.
            static unsigned int s_ID; //!< Attacker ID counter;

            double m_HP; //!< Current HPs.
            double m_SP; //!< Current SPs.

            Boris::Rendering::RectangleRenderer m_Renderer; //!< Renderer.
            Boris::Rendering::RectangleRenderer m_HealthRendererBack;
            Boris::Rendering::RectangleRenderer m_HealthRendererFront;
            Boris::Rendering::RectangleRenderer m_ShieldRenderer;
            Boris::Events::TimePassedEventHandler m_Handler; //!< Event handler for updating position.

            friend class TowerAttack::Game;
        };
    }
}

#endif /* TOWER_ATTACK_ATTACKER */