#ifndef TOWER_ATTACK_TOWER
#define TOWER_ATTACK_TOWER

#include <Core.h>
#include <Events.h>
#include <Geometry.h>
#include <Rendering.h>
#include "TowerPrototype.h"

namespace TowerAttack
{
    class Game;
    namespace Entities
    {
        class Attacker;
        class Tower : public Boris::PositionedObject, public TowerPrototype
        {
        public:
            Tower(const Boris::Geometry::Point& position, const TowerPrototype& prototype, Game& game);
            Tower(const Tower&) = delete;
            Tower(Tower&& other) noexcept;
            ~Tower() override = default;

            Tower& operator=(const Tower&) = delete;
            Tower& operator=(Tower&&) noexcept = delete;

            unsigned int ID() const;

            const Boris::Rendering::RectangleRenderer& Renderer() const;
            Boris::Geometry::Point Position() const;

            /// Damage per second.
            double DPS() const;

            void Update(const Object& sender, const Boris::Events::TimePassedEventArgs& e);

        protected:
            using Boris::PositionedObject::Position;
            using Boris::PositionedObject::Move;
            using Boris::PositionedObject::Rotation;
            using Boris::PositionedObject::Rotate;

            virtual const Attacker * SelectTarget();
            void Aim(const Attacker& target);
            void Shoot();

            unsigned int m_ID;
            static unsigned int s_ID;

            Game& m_Game;

            int m_ShotCooldown; //!< Time in millis to next shot.
            int m_Magazine; //!< Remaining shots in burst.

            Boris::Rendering::RectangleRenderer m_Renderer;
            Boris::Events::TimePassedEventHandler m_Handler;

            friend class TowerAttack::Game;
        };
    }
}

#endif /* TOWER_ATTACK_TOWER */