#ifndef BORIS_OBJECT
#define BORIS_OBJECT

#include "Nullable.h"

#include <Events.h>

#include <cstddef>

namespace Boris
{
    /// Base object.
    /*!
     * Is nullable. Provides OnDestructEvent. Is needed for event system.
     */
    class Object : public Nullable
    {
    public:
        /// Default consructor
        /*!
         * Constructs new object.
         */
        Object() = default;
        /// Null constructor.
        /*!
         * Constructs new empty object.
         */
        Object(std::nullptr_t);
        /// Copy constructor.
        /*!
         * Constructs new copy of other object.
         * @param other another object to be used as data source for initialization for the object.
         */
        Object(const Object& other);
        /// Move constructor.
        /*!
         * Constructs new copy of other object using move semantics.
         * @param other another object to be used as data source for initialization for the object.
         */
        Object(Object&& other) noexcept = default;
        /// Destructor.
        /*!
         * Destructs the object. Calls OnDestruct event.
         */
        ~Object() override;

        /// Copy assignment operator.
        /*!
         * Overwrites the object with another object.
         * @param other another object to be used as data source to overwrite the object.
         * @return *this
         */
        Object& operator=(const Object& other) = default;

        Events::BasicEvent OnDestruct; //!< Event. Called by destructor.
    };
}

#endif /* BORIS_OBJECT */