#ifndef BORIS_NULLABLE
#define BORIS_NULLABLE

#include <cstddef>
#include <stdexcept>

namespace Boris
{
    /// Null object exception.
    class NullException : public std::runtime_error
    {
    public:
        NullException() : std::runtime_error("Cannot call member function of null object.") {}
    };

    /// Nullable base class
    /*!
     * Is base class for all nullable objects.
     * Provides operators to compare with null.
     * To use this class must be derived from Nullable (directly or indirectly)
     * and has Null constructor (Derived(nullptr) : Base(null) {})
     */
    class Nullable
    {
    public:
        /// Default constructor.
        /*!
         * Constructs new nullable object which is not null.
         */
        Nullable();
        /// Null constructor.
        /*!
         * Constructs new nullable object which is null.
         */
        Nullable(std::nullptr_t);
        /// Default constructor.
        /*!
         * Constructs new copy of another nullable object.
         * @param other another nullable object to be used as data source to initialize the nullable object.
         */
        Nullable(const Nullable& other) = default;
        /// Move constructor.
        /*!
         * Constructs new copy of another nullable object using move semantics.
         * @param other another nullable object to be used as data source to initialize the nullable object.
         */
        Nullable(Nullable&& other) = default;
        /// Destructor.
        /*!
         * Destructs the nullable object.
         */
        virtual ~Nullable() = default;

        /// Copy assignment.
        /*!
         * Overwrites nullable object with another nullable object.
         * @param other another nullable object to be used as data source to overwrite the nullable object.
         * @return *this
         */
        Nullable& operator=(const Nullable& other) = default;

        /// Null comparison.
        /*!
         * Compares nullable object with null.
         * @return Returns true if is null, false otherwise.
         */
        virtual bool operator==(std::nullptr_t) const;
        /// Null comparison.
        /*!
         * Compares nullable object with null.
         * @return Returns true if is not null, false otherwise.
         */
        virtual bool operator!=(std::nullptr_t) const;

        /// Null comparison.
        /*!
         * Compares nullable object with null.
         * @return Returns true if is null, false otherwise.
         */
        virtual bool IsNull();

    protected:
        bool m_Null; //!< True if object is null, false otherwise.
    };

    static const std::nullptr_t null = nullptr; //!< NULL for nullable objects.
}

#endif /* BORIS_NULLABLE */