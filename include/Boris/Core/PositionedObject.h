#ifndef BORIS_POSITIONED_OBJECT
#define BORIS_POSITIONED_OBJECT

#include "Object.h"
#include <Events.h>
#include <Geometry/Vector2.h>
#include <Geometry/FPoint.h>

namespace Boris
{
    class PositionedObject : public Object
    {
    public:
        /// Default consructor
        /*!
         * Constructs new object.
         */
        PositionedObject() = default;
        /// Null constructor.
        /*!
         * Constructs new empty object.
         */
        PositionedObject(std::nullptr_t);
        /// Constructor.
        /*!
         * Constructs new object at specified position.
         * @param position position of the object.
         */
        explicit PositionedObject(const Geometry::FPoint& position, double rotation);
        /// Copy constructor.
        /*!
         * Constructs new copy of other object.
         * @param other another object to be used as data source for initialization for the object.
         */
        PositionedObject(const PositionedObject& other) = default;
        /// Move constructor.
        /*!
         * Constructs new copy of other object using move semantics.
         * @param other another object to be used as data source for initialization for the object.
         */
        PositionedObject(PositionedObject&& other) = default;
        /// Destructor.
        /*!
         * Destructs the object. Calls OnDestruct event.
         */
        ~PositionedObject() override = default;

        /// Copy assignment operator.
        /*!
         * Overwrites the object with another object.
         * @param other another object to be used as data source to overwrite the object.
         * @return *this
         */
        PositionedObject& operator=(const PositionedObject& other) = default;

        /// Position of the object.
        /*!
         * Access position of the object.
         * @return const reference to position of the object.
         */
        const Geometry::FPoint& Position() const;
        /// Set position.
        /*!
         * Sets position to the new position
         * @param position new position
         */
        void Position(const Geometry::FPoint& position);
        /// Change position.
        /*!
         * Changes position by the difference vector.
         * @param difference difference between new position and current position.
         */
        void Move(const Geometry::Vector2& difference);

        /// Rotation of the object.
        /*!
         * Access rotation of the object.
         * @return angle of rotation of the object in radians.
         */
         double Rotation() const;
         /// Set rotation.
         /*!
          * Sets rotation to the new angle.
          * @param rotation new angle in radians.
          */
          void Rotation(double rotation);
          /// Change rotation.
          /*!
           * Changes rotation by angle.
           * @param angle angle of rotation in radians.
           */
           void Rotate(double angle);

    protected:
        Geometry::FPoint m_Position; //!< Position of the object.
        double m_Rotation; //!< Angle of rotation of the object.
    };
}

#endif /* BORIS_POSITIONED_OBJECT */