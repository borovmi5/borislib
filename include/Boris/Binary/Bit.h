#ifndef BORIS_BIT
#define BORIS_BIT

#include <cstdint>

namespace Boris
{
    namespace Binary
    {
        class Bit
        {
        public:
            /// Create new bit.
            /*!
             * Constructs new bit with value.
             * @param value whether bit is set.
             */
            Bit(bool value = false);
            /// Create new bit.
            /*!
             * Constructs new bit from value. If value is $\neq0$
             * then bit is set, otherwise bit is cleared.
             * @param value
             */
            explicit Bit(uint8_t value);
            Bit(const Bit&) = default;
            Bit(Bit&&) noexcept = default;
            ~Bit() = default;

            Bit& operator=(const Bit&) = default;
            Bit& operator=(Bit&&) = default;

            bool operator==(const Bit& other) const;
            bool operator!=(const Bit& other) const;

            /// Logical conjunction
            Bit operator&(const Bit& other) const;
            /// Logical conjunction
            Bit& operator&=(const Bit& other);
            /// Logical disjunction
            Bit operator|(const Bit& other) const;
            /// Logical disjunction
            Bit& operator|=(const Bit& other);
            /// Exclusive disjunction
            Bit operator^(const Bit& other) const;
            /// Exclusive disjunction
            Bit& operator^=(const Bit& other);

            operator bool() const;
            /// Bit to Int.
            /*!
             * @return 0x01 if bit is set, 0x00 otherwise.
             */
            explicit operator uint8_t() const;

        private:
            bool m_Value;
        };

        using bit = Bit;
    }
}

#endif /* BORIS_BIT */