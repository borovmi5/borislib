#ifndef BORIS_WORD
#define BORIS_WORD

#include "Byte.h"

#include <cstdint>
#include <array>

namespace Boris
{
    namespace Binary
    {
        class Word
        {
        public:
            /// Create new word.
            /*!
             * Constructs new word with value.
             * @param value value of word.
             */
            Word(uint16_t value = 0x0000);
            /// Create new word.
            /*!
             * Constructs new word from bit-array.
             * Lower bit indexes are lower bits.
             * @param bits bits to compose word from.
             */
            explicit Word(std::array<bit, 16> bits);
            /// Create new word.
            /*!
             * Constructs new word from byte-array.
             * Lower byte indexes are lower bytes.
             * @param bytes bytes to compose word from.
             */
            explicit Word(std::array<byte, 2> bytes);
            Word(const Word&) = default;
            Word(Word&&) noexcept = default;
            ~Word() = default;

            Word& operator=(const Word&) = default;
            Word& operator=(Word&&) = default;

            bool operator==(const Word& other) const;
            bool operator!=(const Word& other) const;

            /// Logical conjunction
            Word operator&(const Word& other) const;
            /// Logical conjunction
            Word& operator&=(const Word& other);
            /// Logical disjunction
            Word operator|(const Word& other) const;
            /// Logical disjunction
            Word& operator|=(const Word& other);
            /// Exclusive disjunction
            Word operator^(const Word& other) const;
            /// Exclusive disjunction
            Word& operator^=(const Word& other);
            /// Logical left shift
            Word operator>>(unsigned int count) const;
            /// Logical left shift
            Word& operator>>=(unsigned int count);
            /// Logical right shift
            Word operator<<(unsigned int count) const;
            /// Logical right shift
            Word& operator<<=(unsigned int count);

            operator uint16_t() const;
            /// Word to bit array
            /*!
             * @return bit array. Lower bits have lower indexes.
             */
            std::array<bit, 16> ToBits() const;
            /// Word to byte array
            /*!
             * @return byte array. Lower bytes have lower indexes.
             */
            std::array<byte, 2> ToBytes() const;

            /// Get bit at index.
            /*!
             * @param index index of bit.
             * @return bit at index.
             */
            bit GetBit(uint8_t index) const;
            /// Set bit at index.
            /*!
             * Sets bit at index to value.
             * @param index index of bit.
             * @param value new value of bit.
             * @return *this
             */
            Word& SetBit(uint8_t index, const bit& value = true);
            /// Clear bit at index.
            /*!
             * Clears bit at index.
             * @param index index of bit.
             * @return *this
             */
            Word& ClearBit(uint8_t index);
            /// Get byte at index.
            /*!
             * @param index index of bit.
             * @return byte at index.
             */
            byte GetByte(uint8_t index) const;
            /// Set byte at index.
            /*!
             * Sets byte at index to value.
             * @param index index of byte.
             * @param value new value of byte.
             * @return *this
             */
            Word& SetByte(uint8_t index, const byte& value = 0xFFu);
            /// Clear byte at index.
            /*!
             * Clears byte at index.
             * @param index index of byte.
             * @return *this
             */
            Word& ClearByte(uint8_t index);


        private:
            uint16_t m_Value;
        };

        using word = Word;
    }
}

#endif /* BORIS_WORD */