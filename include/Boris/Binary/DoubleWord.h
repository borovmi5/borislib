#ifndef BORIS_DWORD
#define BORIS_DWORD

#include "Word.h"

#include <cstdint>
#include <array>

namespace Boris
{
    namespace Binary
    {
        class DoubleWord
        {
        public:
            /// Create new double word.
            /*!
             * Constructs new double word with value.
             * @param value value of double word.
             */
            DoubleWord(uint32_t value = 0x00000000);
            /// Create new double word.
            /*!
             * Constructs new double word from bit-array.
             * Lower bit indexes are lower bits.
             * @param bits bits to compose double word from.
             */
            explicit DoubleWord(std::array<bit, 32> bits);
            /// Create new double word.
            /*!
             * Constructs new double word from byte-array.
             * Lower byte indexes are lower bytes.
             * @param bytes bytes to compose double word from.
             */
            explicit DoubleWord(std::array<byte, 4> bytes);
            /// Create new double word.
            /*!
             * Constructs new double word from word-array.
             * Lower word indexes are lower words.
             * @param words words to compose double word from.
             */
            explicit DoubleWord(std::array<word, 2> words);
            DoubleWord(const DoubleWord&) = default;
            DoubleWord(DoubleWord&&) noexcept = default;
            ~DoubleWord() = default;

            DoubleWord& operator=(const DoubleWord&) = default;
            DoubleWord& operator=(DoubleWord&&) = default;

            bool operator==(const DoubleWord& other) const;
            bool operator!=(const DoubleWord& other) const;

            /// Logical conjunction
            DoubleWord operator&(const DoubleWord& other) const;
            /// Logical conjunction
            DoubleWord& operator&=(const DoubleWord& other);
            /// Logical disjunction
            DoubleWord operator|(const DoubleWord& other) const;
            /// Logical disjunction
            DoubleWord& operator|=(const DoubleWord& other);
            /// Exclusive disjunction
            DoubleWord operator^(const DoubleWord& other) const;
            /// Exclusive disjunction
            DoubleWord& operator^=(const DoubleWord& other);
            /// Logical left shift
            DoubleWord operator>>(unsigned int count) const;
            /// Logical left shift
            DoubleWord& operator>>=(unsigned int count);
            /// Logical right shift
            DoubleWord operator<<(unsigned int count) const;
            /// Logical right shift
            DoubleWord& operator<<=(unsigned int count);

            operator uint32_t() const;
            /// Double word to bit array
            /*!
             * @return bit array. Lower bits have lower indexes.
             */
            std::array<bit, 32> ToBits() const;
            /// Double word to byte array
            /*!
             * @return byte array. Lower bytes have lower indexes.
             */
            std::array<byte, 4> ToBytes() const;
            /// Double word to word array
            /*!
             * @return word array. Lower words have lower indexes.
             */
            std::array<word, 2> ToWords() const;

            /// Get bit at index.
            /*!
             * @param index index of bit.
             * @return bit at index.
             */
            bit GetBit(uint8_t index) const;
            /// Set bit at index.
            /*!
             * Sets bit at index to value.
             * @param index index of bit.
             * @param value new value of bit.
             * @return *this
             */
            DoubleWord& SetBit(uint8_t index, const bit& value = true);
            /// Clear bit at index.
            /*!
             * Clears bit at index.
             * @param index index of bit.
             * @return *this
             */
            DoubleWord& ClearBit(uint8_t index);
            /// Get byte at index.
            /*!
             * @param index index of byte.
             * @return byte at index.
             */
            byte GetByte(uint8_t index) const;
            /// Set byte at index.
            /*!
             * Sets byte at index to value.
             * @param index index of byte.
             * @param value new value of byte.
             * @return *this
             */
            DoubleWord& SetByte(uint8_t index, const byte& value = 0xFFu);
            /// Clear byte at index.
            /*!
             * Clears byte at index.
             * @param index index of byte.
             * @return *this
             */
            DoubleWord& ClearByte(uint8_t index);
            /// Get word at index.
            /*!
             * @param index index of word.
             * @return word at index.
             */
            word GetWord(uint8_t index) const;
            /// Set word at index.
            /*!
             * Sets word at index to value.
             * @param index index of word.
             * @param value new value of word.
             * @return *this
             */
            DoubleWord& SetWord(uint8_t index, const word& value = 0xFFu);
            /// Clear word at index.
            /*!
             * Clears word at index.
             * @param index index of word.
             * @return *this
             */
            DoubleWord& ClearWord(uint8_t index);


        private:
            uint32_t m_Value;
        };

        using dword = DoubleWord;
    }
}

#endif /* BORIS_DWORD */