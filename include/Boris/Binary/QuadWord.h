#ifndef BORIS_QWORD
#define BORIS_QWORD

#include "DoubleWord.h"

#include <cstdint>
#include <array>

namespace Boris
{
    namespace Binary
    {
        class QuadWord
        {
        public:
            /// Create new double word.
            /*!
             * Constructs new quad word with value.
             * @param value value of quad word.
             */
            QuadWord(uint64_t value = 0x0000000000000000);
            /// Create new quad word.
            /*!
             * Constructs new quad word from bit-array.
             * Lower bit indexes are lower bits.
             * @param bits bits to compose quad word from.
             */
            explicit QuadWord(std::array<bit, 64> bits);
            /// Create new quad word.
            /*!
             * Constructs new quad word from byte-array.
             * Lower byte indexes are lower bytes.
             * @param bytes bytes to compose quad word from.
             */
            explicit QuadWord(std::array<byte, 8> bytes);
            /// Create new quad word.
            /*!
             * Constructs new quad word from word-array.
             * Lower word indexes are lower words.
             * @param words words to compose quad word from.
             */
            explicit QuadWord(std::array<word, 4> words);
            /// Create new quad word.
            /*!
             * Constructs new quad word from double word-array.
             * Lower double word indexes are lower double words.
             * @param dwords double words to compose quad word from.
             */
            explicit QuadWord(std::array<dword, 2> dwords);
            QuadWord(const QuadWord&) = default;
            QuadWord(QuadWord&&) noexcept = default;
            ~QuadWord() = default;

            QuadWord& operator=(const QuadWord&) = default;
            QuadWord& operator=(QuadWord&&) = default;

            bool operator==(const QuadWord& other) const;
            bool operator!=(const QuadWord& other) const;

            /// Logical conjunction
            QuadWord operator&(const QuadWord& other) const;
            /// Logical conjunction
            QuadWord& operator&=(const QuadWord& other);
            /// Logical disjunction
            QuadWord operator|(const QuadWord& other) const;
            /// Logical disjunction
            QuadWord& operator|=(const QuadWord& other);
            /// Exclusive disjunction
            QuadWord operator^(const QuadWord& other) const;
            /// Exclusive disjunction
            QuadWord& operator^=(const QuadWord& other);
            /// Logical left shift
            QuadWord operator>>(unsigned int count) const;
            /// Logical left shift
            QuadWord& operator>>=(unsigned int count);
            /// Logical right shift
            QuadWord operator<<(unsigned int count) const;
            /// Logical right shift
            QuadWord& operator<<=(unsigned int count);

            operator uint64_t() const;
            /// Quad word to bit array
            /*!
             * @return bit array. Lower bits have lower indexes.
             */
            std::array<bit, 64> ToBits() const;
            /// Quad word to byte array
            /*!
             * @return byte array. Lower bytes have lower indexes.
             */
            std::array<byte, 8> ToBytes() const;
            /// Quad word to word array
            /*!
             * @return word array. Lower words have lower indexes.
             */
            std::array<word, 4> ToWords() const;
            /// Quad word to double word array
            /*!
             * @return double word array. Lower double words have lower indexes.
             */
            std::array<dword, 2> ToDoubleWords() const;

            /// Get bit at index.
            /*!
             * @param index index of bit.
             * @return bit at index.
             */
            bit GetBit(uint8_t index) const;
            /// Set bit at index.
            /*!
             * Sets bit at index to value.
             * @param index index of bit.
             * @param value new value of bit.
             * @return *this
             */
            QuadWord& SetBit(uint8_t index, const bit& value = true);
            /// Clear bit at index.
            /*!
             * Clears bit at index.
             * @param index index of bit.
             * @return *this
             */
            QuadWord& ClearBit(uint8_t index);
            /// Get byte at index.
            /*!
             * @param index index of byte.
             * @return byte at index.
             */
            byte GetByte(uint8_t index) const;
            /// Set byte at index.
            /*!
             * Sets byte at index to value.
             * @param index index of byte.
             * @param value new value of byte.
             * @return *this
             */
            QuadWord& SetByte(uint8_t index, const byte& value = 0xFFu);
            /// Clear byte at index.
            /*!
             * Clears byte at index.
             * @param index index of byte.
             * @return *this
             */
            QuadWord& ClearByte(uint8_t index);
            /// Get word at index.
            /*!
             * @param index index of word.
             * @return word at index.
             */
            word GetWord(uint8_t index) const;
            /// Set word at index.
            /*!
             * Sets word at index to value.
             * @param index index of word.
             * @param value new value of word.
             * @return *this
             */
            QuadWord& SetWord(uint8_t index, const word& value = 0xFFu);
            /// Clear word at index.
            /*!
             * Clears word at index.
             * @param index index of word.
             * @return *this
             */
            QuadWord& ClearWord(uint8_t index);
            /// Get double word at index.
            /*!
             * @param index index of double word.
             * @return double word at index.
             */
            dword GetDoubleWord(uint8_t index) const;
            /// Set double word at index.
            /*!
             * Sets double word at index to value.
             * @param index index of double word.
             * @param value new value of double word.
             * @return *this
             */
            QuadWord& SetDoubleWord(uint8_t index, const dword& value = 0xFFu);
            /// Clear double word at index.
            /*!
             * Clears double word at index.
             * @param index index of double word.
             * @return *this
             */
            QuadWord& ClearDoubleWord(uint8_t index);


        private:
            uint64_t m_Value;
        };

        using qword = QuadWord;
    }
}

#endif /* BORIS_QWORD */