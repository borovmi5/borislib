#ifndef BORIS_BINARY_STREAM
#define BORIS_BINARY_STREAM

#include "Bit.h"
#include "Byte.h"
#include "Word.h"
#include "DoubleWord.h"
#include "QuadWord.h"

#include <fstream>
#include <cstdlib>
#include <string>

namespace Boris
{
    namespace Binary
    {
        using Endian = enum class e_Endianness
        {
            LITTLE,
            BIG
        };

        class BinaryStream : public std::fstream
        {
        public:
            BinaryStream();
            /// @private
            BinaryStream(const BinaryStream&) = delete;
            BinaryStream(BinaryStream&& other) noexcept;
            ~BinaryStream() override = default;

            /// @private
            BinaryStream& operator=(const BinaryStream& other) = delete;
            BinaryStream& operator=(BinaryStream&& other) noexcept;

            /// Open new binary file.
            /*!
             * Opens file in binary mode.
             * @param filepath path to the file.
             * @param mode io mode.
             */
            void open(const std::string& filepath, ios_base::openmode mode = ios_base::in | ios_base::out);
            /// Close binary file.
            void close();

            /// Sets input position indicator.
            /*!
             * @sa https://en.cppreference.com/w/cpp/io/basic_istream/seekg
             */
            BinaryStream& seekg(pos_type pos);
            /// Sets input position indicator.
            /*!
             * @sa https://en.cppreference.com/w/cpp/io/basic_istream/seekg
             */
            BinaryStream& seekg(off_type off, std::ios_base::seekdir dir);
            /// Sets output position indicator.
            /*!
             * @sa https://en.cppreference.com/w/cpp/io/basic_ostream/seekp
             */
            BinaryStream& seekp(pos_type pos);
            /// Sets output position indicator.
            /*!
             * @sa https://en.cppreference.com/w/cpp/io/basic_ostream/seekp
             */
            BinaryStream& seekp(off_type off, std::ios_base::seekdir dir);

            /// Get endianness.
            Endian Endianness() const;
            /// Set endianness. (Default: LITTLE)
            /*!
             * Sets endian to be used when reading/writing.
             * @param endian new endian.
             */
            void Endianness(Endian endian);

            /// Read bit.
            /*!
             * Reads bit from the stream.
             * @param force_new if true, force new byte.
             * @return next bit from the stream.
             */
            bit GetBit(bool force_new = false);
            /// Read byte.
            /*!
             * Reads byte from the stream.
             * @return next byte from the stream.
             */
            byte GetByte();
            /// Read word.
            /*!
             * Reads word from the stream.
             * @return next word from the stream.
             */
            word GetWord();
            /// Read double word.
            /*!
             * Reads double word from the stream.
             * @return next double word from the stream.
             */
            dword GetDWord();
            /// Read quad word.
            /*!
             * Reads quad word from the stream.
             * @return next quad word from the stream.
             */
            qword GetQWord();

            /// Read bit.
            /*!
             * Reads bit from the stream without removing it.
             * @param force_new if true, force new byte.
             * @return next bit from the stream.
             */
            bit PeekBit(bool force_new = false);
            /// Read byte.
            /*!
             * Reads byte from the stream without removing it.
             * @return next byte from the stream.
             */
            byte PeekByte();
            /// Read word.
            /*!
             * Reads word from the stream without removing it.
             * @return next word from the stream.
             */
            word PeekWord();
            /// Read double word.
            /*!
             * Reads double word from the stream without removing it.
             * @return next double word from the stream.
             */
            dword PeekDWord();
            /// Read double word.
            /*!
             * Reads double word from the stream without removing it.
             * @return next double word from the stream.
             */
            qword PeekQWord();

            /// Write bit.
            /*!
             * Writes bit to the stream.
             * @param b value to write.
             * @param force_new force new byte.
             */
            void WriteBit(bit b, bool force_new = false);
            /// Write byte.
            /*!
             * Writes byte to the stream.
             * @param B value to write.
             */
            void WriteByte(byte B);
            /// Write word.
            /*!
             * Writes word to the stream.
             * @param w value to write.
             */
            void WriteWord(word w);
            /// Write double word.
            /*!
             * Writes double word to the stream.
             * @param d value to write.
             */
            void WriteDWord(dword d);
            /// Write quad word.
            /*!
             * Writes quad word to the stream.
             * @param q value to write.
             */
            void WriteQWord(qword q);

            /// Compute CRC from file.
            qword Checksum();

            /// Flush byte.
            /*!
             * Flushes m_OutBuffer with unsetted bits set to 0.
             * Immediately forces new out byte for bit output.
             */
            void Flush();

        private:
            Endian m_Endian = Endian::LITTLE; //!< Endianness of the file.
            byte m_InBuffer = 0; //!< Input byte buffer for bit reading.
            byte m_OutBuffer = 0; //!< Output byte buffer for bit writing.
            int m_InPointer = 8; //!< Input pointer to buffer (index of current bit).
            int m_OutPointer = 0; //!< Output pointer to buffer (index of current bit).
            bool m_ForceNewIn = false; //!< Force new byte for next bit.
            bool m_ForceNewOut = false; //!< Force new byte for next bit.

            std::string m_FilePath; //!< Filepath to the opened file.
            std::ios_base::openmode m_Flags; //!< Flags for opened stream.
        };

        using bstream = BinaryStream;

        bstream& operator>>(bstream& stream, bit& b);
        bstream& operator>>(bstream& stream, byte& B);
        bstream& operator>>(bstream& stream, word& w);
        bstream& operator>>(bstream& stream, dword& d);
        bstream& operator>>(bstream& stream, qword& q);
        bstream& operator<<(bstream& stream, const bit& b);
        bstream& operator<<(bstream& stream, const byte& B);
        bstream& operator<<(bstream& stream, const word& w);
        bstream& operator<<(bstream& stream, const dword& d);
        bstream& operator<<(bstream& stream, const qword& q);
    }
}

#endif /* BORIS_BINARY_STREAM */