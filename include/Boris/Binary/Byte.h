#ifndef BORIS_BYTE
#define BORIS_BYTE

#include "Bit.h"

#include <cstdint>
#include <array>

namespace Boris
{
    namespace Binary
    {
        class Byte
        {
        public:
            /// Create new byte.
            /*!
             * Constructs new byte with value.
             * @param value value of byte.
             */
            Byte(uint8_t value = 0x00);
            /// Create new byte.
            /*!
             * Constructs new byte from bit-array.
             * Lower bit indexes are lower bits.
             * @param bits bits to compose byte from.
             */
            explicit Byte(std::array<bit, 8> bits);
            Byte(const Byte&) = default;
            Byte(Byte&&) noexcept = default;
            ~Byte() = default;

            Byte& operator=(const Byte&) = default;
            Byte& operator=(Byte&&) = default;

            bool operator==(const Byte& other) const;
            bool operator!=(const Byte& other) const;

            /// Logical conjunction
            Byte operator&(const Byte& other) const;
            /// Logical conjunction
            Byte& operator&=(const Byte& other);
            /// Logical disjunction
            Byte operator|(const Byte& other) const;
            /// Logical disjunction
            Byte& operator|=(const Byte& other);
            /// Exclusive disjunction
            Byte operator^(const Byte& other) const;
            /// Exclusive disjunction
            Byte& operator^=(const Byte& other);
            /// Logical left shift
            Byte operator>>(unsigned int count) const;
            /// Logical left shift
            Byte& operator>>=(unsigned int count);
            /// Logical right shift
            Byte operator<<(unsigned int count) const;
            /// Logical right shift
            Byte& operator<<=(unsigned int count);

            operator uint8_t() const;
            /// Byte to bit array
            /*!
             * @return bit array. Lower bits have lower indexes.
             */
            std::array<bit, 8> ToBits() const;

            /// Get bit at index.
            /*!
             * @param index index of bit.
             * @return bit at index.
             */
            bit GetBit(uint8_t index) const;
            /// Set bit at index.
            /*!
             * Sets bit at index to value.
             * @param index index of bit.
             * @param value new value of bit.
             * @return *this
             */
            Byte& SetBit(uint8_t index, const bit& value = true);
            /// Clear bit at index.
            /*!
             * Clears bit at index.
             * @param index index of bit.
             * @return *this
             */
            Byte& ClearBit(uint8_t index);


        private:
            uint8_t m_Value;
        };

        using byte = Byte;
    }
}

#endif /* BORIS_BYTE */