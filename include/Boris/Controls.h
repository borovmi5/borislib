#ifndef BORIS_CONTROLS
#define BORIS_CONTROLS

#include "Controls/KeyEventArgs.h"
#include "Controls/MouseEventArgs.h"
#include "Controls/Keys.h"
#include "Controls/Mouse.h"

#include <GL/freeglut.h>

namespace Boris
{
    /// Handles user input.
    /*!
     * Contains classes for handling user input via
     * keyboard and mouse.
     */
    namespace Controls
    {
        /// Keys.
        using Key = enum class e_Key : unsigned char
        {
            // Functional keys
            F1 = 0x81, F2 = 0x82, F3 = 0x83, F4 = 0x84, F5 = 0x85, F6 = 0x86,
            F7 = 0x87, F8 = 0x88, F9 = 0x89, F10 = 0x8A, F11 = 0x8B, F12 = 0x8C,

            // Alphabet
            A = 0x41, B = 0x42, C = 0x43, D = 0x44, E = 0x45, F = 0x46,
            G = 0x47, H = 0x48, I = 0x49, J = 0x4A, K = 0x4B, L = 0x4C,
            M = 0x4D, N = 0x4E, O = 0x4F, P = 0x50, Q = 0x51, R = 0x52,
            S = 0x53, T = 0x54, U = 0x55, V = 0x56, W = 0x57, X = 0x58,
            Y = 0x59, Z = 0x5A,

            // Numerics
            _0 = 0x30, _1 = 0x31, _2 = 0x32, _3 = 0x33, _4 = 0x34,
            _5 = 0x35, _6 = 0x36, _7 = 0x37, _8 = 0x38, _9 = 0x39,

            // Whitespaces
            SPACE = 0x20, TAB = 0x09, ENTER = 0x0D,

            // Modifiers
            LSHIFT = 0xF0, RSHIFT = 0xF1, LCTRL = 0xF2, RCTRL = 0xF3,
            LALT = 0xF4, RALT = 0xF5,

            // Controls
            LEFT = 0xE4, UP = 0xE5, RIGHT = 0xE6, DOWN = 0xE7,
            PAGE_UP = 0xE8, PAGE_DOWN = 0xE9, HOME = 0xEA, END = 0xEB,
            INSERT = 0xEC, NUM_LOCK = 0xED, ESC = 0x1B,

            // Deletion
            DELETE = 0x7F, BACKSPACE = 0x08,

            // Others
            BACKTICK = 0x60, DASH = 0x2D, EQUAL = 0x3D, LBRACK = 0x5B, RBRACK = 0x5D,
            SEMI = 0x3B, QUOT = 0x27, BACKSLASH = 0x5C, COMA = 0x2C, DOT = 0x2E,
            SLASH = 0x2F,

            UNKNOWN = 0x00
        };

        /// Mouse buttons.
        using MouseButton = enum class e_MouseButton : unsigned char
        {
            UNKNOWN = 0x0, LMB = 0x1, MMB = 0x2, RMB = 0x3, SCR_UP = 0x4,
            SCR_DN = 0x5, _5 = 0x6, _6 = 0x7, BACK = 0x8, FORWARD = 0x9
        };

        /// Cursors.
        using CursorType = enum class e_Cursor
        {
            DEFAULT = GLUT_CURSOR_LEFT_ARROW,
            LEFT_ARROW = GLUT_CURSOR_LEFT_ARROW,
            INVERTED = GLUT_CURSOR_RIGHT_ARROW,
            RIGHT_ARROW = GLUT_CURSOR_RIGHT_ARROW,
            TOP_LEFT = GLUT_CURSOR_TOP_LEFT_CORNER,
            TOP_RIGHT = GLUT_CURSOR_TOP_RIGHT_CORNER,
            BOTTOM_LEFT = GLUT_CURSOR_BOTTOM_LEFT_CORNER,
            BOTTOM_RIGHT = GLUT_CURSOR_BOTTOM_RIGHT_CORNER,
            UP = GLUT_CURSOR_TOP_SIDE,
            RIGHT = GLUT_CURSOR_BOTTOM_SIDE,
            DOWN = GLUT_CURSOR_BOTTOM_SIDE,
            LEFT = GLUT_CURSOR_LEFT_SIDE,
            HSCALE = GLUT_CURSOR_LEFT_RIGHT,
            LEFT_RIGHT_ARROW = GLUT_CURSOR_LEFT_RIGHT,
            VSCALE = GLUT_CURSOR_UP_DOWN,
            UP_DOWN_ARROW = GLUT_CURSOR_UP_DOWN,
            CROSS = GLUT_CURSOR_CROSSHAIR,
            CROSSHAIR = GLUT_CURSOR_CROSSHAIR,
            DESTROY = GLUT_CURSOR_DESTROY,
            INFO = GLUT_CURSOR_INFO,
            HAND = GLUT_CURSOR_INFO,
            HELP = GLUT_CURSOR_HELP,
            QUESTION_MARK = GLUT_CURSOR_HELP,
            CYCLE = GLUT_CURSOR_CYCLE,
            ROTATE = GLUT_CURSOR_CYCLE,
            WAIT = GLUT_CURSOR_WAIT,
            TEXT = GLUT_CURSOR_TEXT,
            CARET = GLUT_CURSOR_TEXT,
            SPRAY = GLUT_CURSOR_SPRAY,
            FULL = GLUT_CURSOR_FULL_CROSSHAIR,
            FULLCROSS = GLUT_CURSOR_FULL_CROSSHAIR,
            FULL_CROSSHAIR = GLUT_CURSOR_FULL_CROSSHAIR,
            NONE = GLUT_CURSOR_NONE,
            INVISIBLE = GLUT_CURSOR_NONE
        };
    }
}

#endif /* BORIS_CONTROLS */