#ifndef BORIS_COLOR
#define BORIS_COLOR

#include <Core.h>

namespace Boris
{
    namespace Graphics
    {
        /// RGBA color.
        /*!
         * Color represented by quadruple of values:
         * red channel, green channel, blue channel and alpha channel.
         *
         * Alternatively can be used hue, saturation and value.
         */
        class Color
        {
        public:
            /// Default constructor.
            /*!
             * Constructs new color. Black.
             */
            Color();
            /// Constructor.
            /*!
             * Constructs new color.
             * @param r red
             * @param g green
             * @param b blue
             * @param a alpha
             */
            Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);
            /// Constructor.
            /*!
             * Constructs new color.
             * @param r red
             * @param g green
             * @param b blue
             * @param a alpha
             */
            Color(double r, double g, double b, double a = 1.0);
            /// Constructor.
            /*!
             * Constructs new color from hexcode in format:
             *
             * #RGB | #ARGB | #RRGGBB | #AARRGGBB
             *
             * where R, G, B, A represents Red, Green, Blue and Alpha
             * channel respectively.
             * @param hexcode hexcode to convert to color.
             */
            explicit Color(const char* hexcode);
            /// Copy constructor.
            /*!
             * Constructs new copy of another color.
             * @param other another color to be used as data source to initialize the color.
             */
            Color(const Color& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another color using move semantics.
             * @param other another color to be used as data source to initialize the color.
             */
            Color(Color&& other) = default;
            /// Destructor.
            /*!
             * Destructs the color.
             */
            ~Color() = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the color with another color.
             * @param other another color to be used as data source to overwrite the color.
             */
            Color& operator=(const Color& other) = default;

            /// Color compare.
            /*!
             * Compares 2 colors and returns true if they are both exactly the same.
             * @param other another color to compare with.
             * @return true if both colors are the same, false otherwise.
             */
            bool operator==(const Color& other) const;
            /// Color compare.
            /*!
             * Compares 2 colors and returns true if they are both exactly the same.
             * @param other another color to compare with.
             * @return false if both colors are the same, true otherwise.
             */
            bool operator!=(const Color& other) const;

            /// Create color from HSV.
            /*!
             * Constructs new color from hue, saturation and value.
             * @param h hue
             * @param s saturation
             * @param v value
             * @param a alpha
             * @return new color.
             */
            static Color FromHSV(double h, double s, double v, double a = 1.0);

            /// Red channel.
            /*!
             * Gets red value of the color.
             * @return red channel value.
             */
            uint8_t Red() const;
            /// Red channel.
            /*!
             * Sets red value of the color.
             * @param r new red value <0-255>.
             * @return *this
             */
            Color& Red(uint8_t r);
            /// Red channel.
            /*!
             * Sets red value of the color.
             * @param r new red value <0-255>.
             * @return *this
             */
            Color& Red(double r);
            /// Green channel.
            /*!
             * Gets green value of the color.
             * @return green channel value.
             */
            uint8_t Green() const;
            /// Green channel.
            /*!
             * Sets green value of the color.
             * @param g new green value <0-255>.
             * @return *this
             */
            Color& Green(uint8_t g);
            /// Green channel.
            /*!
             * Sets green value of the color.
             * @param g new green value <0-255>.
             * @return *this
             */
            Color& Green(double g);
            /// Blue channel.
            /*!
             * Gets blue value of the color.
             * @return blue channel value.
             */
            uint8_t Blue() const;
            /// Blue channel.
            /*!
             * Sets blue value of the color.
             * @param b new blue value <0-255>.
             * @return *this
             */
            Color& Blue(uint8_t b);
            /// Blue channel.
            /*!
             * Sets blue value of the color.
             * @param b new blue value <0-255>.
             * @return *this
             */
            Color& Blue(double b);
            /// Alpha channel.
            /*!
             * Gets alpha value of the color.
             * @return alpha channel value.
             */
            uint8_t Alpha() const;
            /// Alpha channel.
            /*!
             * Sets alpha value of the color.
             * @param a new alpha value <0-255>.
             * @return *this
             */
            Color& Alpha(uint8_t a);
            /// Alpha channel.
            /*!
             * Sets alpha value of the color.
             * @param a new alpha value <0-255>.
             * @return *this
             */
            Color& Alpha(double a);
            /// Hue.
            /*!
             * Gets hue of the color.
             * @return hue value.
             */
            double Hue() const;
            /// Hue.
            /*!
             * Sets hue of the color.
             * @param r new hue value <0.0-360.0).
             * @return *this
             */
            Color& Hue(double h);
            /// Saturation.
            /*!
             * Gets saturation of the color.
             * @return saturation value.
             */
            double Saturation() const;
            /// Saturation.
            /*!
             * Sets saturation of the color.
             * @param r new saturation value <0.0-1.0).
             * @return *this
             */
            Color& Saturation(double s);
            /// Value.
            /*!
             * Gets value of the color.
             * @return value value.
             */
            double Value() const;
            /// Value.
            /*!
             * Sets value of the color.
             * @param r new value value <0.0-1.0).
             * @return *this
             */
            Color& Value(double v);

            /// Mixes the colors.
            /*!
             * Mixes another color to this color in the specified ratio.
             *
             * Ratio of the mix is defined as
             * \f$\frac{\Delta _{this, result}}{\Delta _{this, other}}\f$
             *
             * @param other another color to mix.
             * @param ratio ratio of the mix.
             * @return mixed color.
             */
            Color Mix(const Color& other, double ratio) const;

            /// Set RGB.
            /*!
             * Sets color from RGB.
             * Alpha is set to 255.
             * @param r red
             * @param g green
             * @param b blue
             */
            void RGB(uint8_t r, uint8_t g, uint8_t b);
            /// Set RGB.
            /*!
             * Sets color from RGB.
             * Alpha is set to 1.0.
             * @param r red
             * @param g green
             * @param b blue
             */
            void RGB(double r, double g, double b);
            /// Set RGBA.
            /*!
             * Sets color from RGBA.
             * @param r red
             * @param g green
             * @param b blue
             * @param a alpha
             */
            void RGBA(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
            /// Set RGBA.
            /*!
             * Sets color from RGBA.
             * @param r red
             * @param g green
             * @param b blue
             * @param a alpha
             */
            void RGBA(double r, double g, double b, double a);
            /// Set HSVA.
            /*!
             * Sets color from HSVA.
             * @param h hue
             * @param s saturation
             * @param v value
             * @param a alpha
             */
            void HSV(double h, double s, double v, double a = 1.0);

        private:
            uint8_t m_Red; //!< Red channel.
            uint8_t m_Green; //!< Green channel.
            uint8_t m_Blue; //!< Blue channel.
            uint8_t m_Alpha; //!< Alpha channel.
        };

        /// Converts number to color.
        Color operator""_rgb(unsigned long long rgb);
        /// Converts number to color.
        Color operator""_argb(unsigned long long argb);
        /// Converts number to color.
        Color operator""_rgb_short(unsigned long long rgb);
        /// Converts number to color.
        Color operator""_argb_short(unsigned long long argb);
    }
}

#endif /* BORIS_COLORS */