#ifndef BORIS_TEXTURE
#define BORIS_TEXTURE

#include "Image.h"
#include <Core.h>
#include <Geometry.h>

#include <string>

namespace Boris
{
    namespace Graphics
    {
        class Texture : public Object
        {
        public:
            /// Create null texture.
            Texture(std::nullptr_t = null);
            /// Create new texture from file.
            /*!
             * Creates new texture from specified file.
             * Accepts PNG and BMP formats.
             * @param texture_path path to the texture to load.
             * @param filter filter type. True - linear, false - nearest
             */
            explicit Texture(const std::string& texture_path, bool filter = false);
            /// Create new texture from image image.
            /*!
             * Creates new texture from provided image.
             * @param image source image.
             * @param filter filter type. True - linear, false - nearest
             */
            explicit Texture(const Image& image, bool filter = false);
            /// Copy constructor.
            /*!
             * Copies texture ID. Increments shared texture count.
             */
            Texture(const Texture& other);
            /// Move constructor.
            /*!
             * Copies texture ID. Increments shared texture count.
             */
            Texture(Texture&& other) noexcept;
            /// Destructor.
            /*!
             * Decrements shared texture count and deletes texture
             * when texture count is 0.
             */
            ~Texture() override;

            /// Copy assignment operator.
            /*!
             * Decrements shared texture count of current texture
             * and deletes it if is last texture pointin to it.
             * Increments shared texture count of new texture.
             */
            Texture& operator=(const Texture& other);
            /// Move assignemnt operator.
            /*!
             * Decrements shared texture count of current texture
             * and deletes it if is last texture pointin to it.
             * Increments shared texture count of new texture.
             */
            Texture& operator=(Texture&& other) noexcept;

            /// Set texture active.
            /*!
             * Sets texture as active texture.
             */
            void Apply() const;
            /// Get texture size.
            const Geometry::Size& Size() const;
            /// Get alpha channel.
            /*!
             * @return true if texture has alpha channel, false otherwise.
             */
            const bool& Alpha() const;

        private:
            unsigned int m_TextureID;
            unsigned int * m_TextureCount; // Number of instances pointing this texture.
            Geometry::Size m_Size;
            bool m_Alpha;
        };
    }
}

#endif /* BORIS_TEXTURE */