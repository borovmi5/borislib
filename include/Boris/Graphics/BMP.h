#ifndef BORIS_BMP
#define BORIS_BMP

#include <string>
#include <vector>
#include <deque>
#include <ctime>

#include <Binary.h>
#include <Geometry.h>

#include "Color.h"

namespace Boris
{
    namespace Graphics
    {
        /// BMP Reader
        /*!
         * Class for reading BMP files.
         */
        class BMP
        {
        public:
            explicit BMP(const std::string& filepath);
            BMP(const BMP&) = delete;
            BMP(BMP&&) = delete;
            ~BMP();

            BMP& operator=(const BMP&) = delete;
            BMP& operator=(BMP&&) = delete;

            /// Test file format.
            /*!
             * Reads file header and detects whether it is BMP file.
             * Checks just header. Does not guarantee the file is
             * not corrupted nor has supported format!
             * @param filepath path to the tested file.
             * @return true if file may be BMP file, false otherwise.
             */
            static bool IsBMP(const std::string& filepath);

            /// True if image has alpha channel.
            bool Alpha() const;
            /// Size of the image.
            const Geometry::Size& Size() const;
            /// Pixels per unit of the image.
            const Geometry::Size& PPU() const;
            /// Image data.
            const uint8_t * Data() const;

        private:
            using HeaderType = enum class e_Header : uint32_t
            {
                BITMAPCOREHEADER = 0xc,
                OS22XBITMAPHEADER = 0x40,
                OS22XBITMAPHEADER_SHORT = 0x10,
                BITMAPINFOHEADER = 0x28,
                BITMAPV2INFOHEADER = 0x34,
                BITMAPV3INFOHEADER = 0x38,
                BITMAPV4HEADER = 0x6c,
                BITMAPV5HEADER = 0x7c
            };
            using ColorType = enum class e_ColType
            {
                BGR,
                BGRA
            };

            using UnitType = enum class e_Unit : uint8_t
            {
                UNKNOWN = 0x00,
                METRE = 0x01
            };

            /// Read header.
            /*!
             * @param stream BMP file stream.
             * @return pixel array offset.
             */
            uint32_t ReadHeader(Binary::BinaryStream& stream);
            /// Read DIB header.
            /*!
             * @param stream BMP file stream.
             */
            void ReadDIB(Binary::BinaryStream& stream);
            /// Read color table.
            /*!
             * @param stream BMP file stream.
             */
            void ReadColorTable(Binary::BinaryStream& stream);
            /// Read image data.
            /*!
             * @param stream BMP file stream.
             */
            void ReadData(Binary::BinaryStream& stream);

            /// Read ICC profile.
            /*!
             * @param stream BMP file stream.
             */
            void ReadICC(Binary::BinaryStream& stream);

            Geometry::Size m_Size;
            uint8_t m_BitDepth;
            ColorType m_ColorType;

            Geometry::Size m_PPU;

            uint8_t * m_ImageData; //!< Image data
            unsigned int m_DataOffset; //!< Bytes already written to image data
        };
    }
}

#endif /* BORIS_BMP */