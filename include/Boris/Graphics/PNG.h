#ifndef BORIS_PNG
#define BORIS_PNG

#include <string>
#include <vector>
#include <deque>
#include <ctime>

#include <Binary.h>
#include <Geometry.h>

#include "Color.h"

namespace Boris
{
    namespace Graphics
    {
        /// PNG Reader
        /*!
         * Class for reading PNG files.
         * Currently supports 8-bit depth
         * TRUECOLOR and TRUECOLOR+ALPHA only.
         */
        class PNG
        {
        public:
            explicit PNG(const std::string& filepath);
            PNG(const PNG&) = delete;
            PNG(PNG&&) = delete;
            ~PNG();

            PNG& operator=(const PNG&) = delete;
            PNG& operator=(PNG&&) = delete;

            /// Test file format.
            /*!
             * Reads file header and detects whether it is PNG file.
             * Checks just header. Does not guarantee the file is
             * not corrupted nor has supported format!
             * @param filepath path to the tested file.
             * @return true if file may be PNG file, false otherwise.
             */
            static bool IsPNG(const std::string& filepath);

            /// True if image has alpha channel.
            bool Alpha() const;
            /// True if image is grayscale.
            bool Grayscale() const;
            /// Size of the image.
            const Geometry::Size& Size() const;
            /// Pixels per unit of the image.
            const Geometry::Size& PPU() const;
            /// True if PPU is in pixels pre metre.
            bool PPUInMetres() const;
            /// Date&time of last modification.
            time_t LastModified() const;
            /// Image data.
            const uint8_t * Data() const;

        private:
            using ColorType = enum class e_ColType : uint8_t
            {
                GRAYSCALE = 0x00,
                TRUECOLOR = 0x02,
                INDEXED = 0x03,
                GRAYSCALE_ALPHA = 0x04,
                TRUECOLOR_ALPHA = 0x06
            };

            using UnitType = enum class e_Unit : uint8_t
            {
                UNKNOWN = 0x00,
                METRE = 0x01
            };

            using FilterType = enum class e_Filter : uint8_t
            {
                NONE = 0x00,
                SUB = 0x01,
                UP = 0x02,
                AVERAGE = 0x03,
                PAETH = 0x04
            };

            using InterlaceMethod = enum class e_Interlace : uint8_t
            {
                NONE = 0x00,
                ADAM7 = 0x01
            };

            /// Initialize CRC table.
            /*!
             * Prepares CRC table for fast CRC checks.
             */
            static void InitCRC();
            /// Compute CRC for byte.
            static Binary::dword CRC(Binary::byte value, Binary::dword crc = 0);
            /// Compute CRC for word.
            static Binary::dword CRC(const Binary::word& value, Binary::dword crc = 0);
            /// Compute CRC for double word.
            static Binary::dword CRC(const Binary::dword& value, Binary::dword crc = 0);
            /// Compute CRC for quad word.
            static Binary::dword CRC(const Binary::qword& value, Binary::dword crc = 0);
            /// Read IHDR chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadIHDR(Binary::BinaryStream& stream, uint32_t length);
            /// Read PLTE chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadPLTE(Binary::BinaryStream& stream, uint32_t length);
            /// Read IDAT chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadIDAT(Binary::BinaryStream& stream, uint32_t length);
            /// Read IEND chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadIEND(Binary::BinaryStream& stream, uint32_t length);

            /// Read cHRM chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadcHRM(Binary::BinaryStream& stream, uint32_t length);
            /// Read gAMA chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadgAMA(Binary::BinaryStream& stream, uint32_t length);
            /// Read iCCP chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadiCCP(Binary::BinaryStream& stream, uint32_t length);
            /// Read sBIT chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadsBIT(Binary::BinaryStream& stream, uint32_t length);
            /// Read sRGB chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadsRGB(Binary::BinaryStream& stream, uint32_t length);
            /// Read bKGD chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadbKGD(Binary::BinaryStream& stream, uint32_t length);
            /// Read hIST chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadhIST(Binary::BinaryStream& stream, uint32_t length);
            /// Read tRNS chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadtRNS(Binary::BinaryStream& stream, uint32_t length);
            /// Read pHYs chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadpHYs(Binary::BinaryStream& stream, uint32_t length);
            /// Read sPLT chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadsPLT(Binary::BinaryStream& stream, uint32_t length);
            /// Read tIME chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadtIME(Binary::BinaryStream& stream, uint32_t length);
            /// Read iTXt chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadiTXt(Binary::BinaryStream& stream, uint32_t length);
            /// Read tEXt chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadtEXt(Binary::BinaryStream& stream, uint32_t length);
            /// Read zTXt chunk.
            /*!
             * @param stream PNG file stream.
             * @param length chunk length.
             */
            void ReadzTXt(Binary::BinaryStream& stream, uint32_t length);

            /// Decompresses the image data.
            void Decompress();
            /// Read count bits from buffer up to 64.
            Binary::qword PopBits(unsigned int count);
            /// Decompress - not compressed block
            void DecompressStatic();
            /// Build Huffman codes from symbol lengths.
            std::vector<std::vector<uint16_t>> BuildHuffman(const std::vector<uint16_t>& lengths) const;
            /// Decompress - fixed Huffman codes
            void DecompressFixed();
            /// Decompress - dynamic Huffman codes
            void DecompressDynamic();
            /// Decompress - Huffman codes
            void DecompressHuffman(const std::vector<std::vector<uint16_t>>& llCodes, const std::vector<std::vector<uint16_t>>& dCodes);
            /// Decodes next symbol from buffer.
            /*!
             * Decodes next symbol from buffer using Huffman codes generated
             * by BuildHuffman.
             * @param huffman output of BuildHuffman
             * @return decoded symbol
             * @sa PNG::BuildHuffman()
             */
            uint16_t Decode(const std::vector<std::vector<uint16_t>>& huffman);

            /// Paeth predictor
            /*!
             * Computes paeth predictor from surrounding pixels.
             * See https://www.w3.org/TR/2003/REC-PNG-20031110/#9Filters
             * @param A left pixel
             * @param B up pixel
             * @param C up-left pixel
             * @return predictor value
             */
            uint8_t Paeth(uint8_t A, uint8_t B, uint8_t C);
            /// Reconstruct decompressed image
            void Reconstruct();

            /// Flip image vertically
            void Flip();

            Geometry::Size m_Size;
            uint8_t m_BitDepth;
            ColorType m_ColorType;
            InterlaceMethod m_InterlaceMethod;

            std::vector<Color> m_Palette;
            bool m_AlphaPalette;

            Geometry::Size m_PPU;
            UnitType m_Unit;

            std::deque<Binary::byte> m_Buffer; //!< Byte buffer for inflate
            Binary::byte m_BufferTop; //!< Bit buffer for inflate
            uint8_t m_BufferTopBits; //!< Bits remaining in bit buffer

            std::time_t m_LastModified;

            // CRC table
            static Binary::dword s_CRCTable[0x100];
            static bool s_CRCInitialized;

            // Fixed Huffman codes
            static std::vector<std::vector<uint16_t>> s_FixedHuffmanLL;
            static std::vector<std::vector<uint16_t>> s_FixedHuffmanD;
            static bool s_FixedHuffmanInitialized;

            uint8_t * m_ImageData; //!< Image data
            unsigned int m_DataOffset; //!< Bytes already written to image data
            unsigned long m_DataSize; //!< Size of Image data.
        };
    }
}

#endif /* BORIS_PNG */