#ifndef BORIS_COLORS
#define BORIS_COLORS

#include "Color.h"

namespace Boris
{
    namespace Graphics
    {
        /// Web colors.
        /*!
         * Includes collection of web colors and Transparent color.
         *
         * <table>
         * <tr> <th>Category</th>           <th>Name</th>               <th>Preview</th>                        <th>R</th>  <th>G</th>  <th>B</th></tr>
		 * <tr>	<td rowspan=6>Pink</td>		<td>Pink</td>				<td style="background-color: #FFC0CB"/>	<td>FF</td>	<td>C0</td>	<td>CB</td>	</tr>
		 * <tr>								<td>LightPink</td>			<td style="background-color: #FFB6C1"/>	<td>FF</td>	<td>B6</td>	<td>C1</td>	</tr>
		 * <tr>								<td>HotPink</td>			<td style="background-color: #FF69B4"/>	<td>FF</td>	<td>69</td>	<td>B4</td>	</tr>
		 * <tr>								<td>DeepPink</td>			<td style="background-color: #FF1493"/>	<td>FF</td>	<td>14</td>	<td>93</td>	</tr>
		 * <tr>								<td>PaleVioletRed</td>		<td style="background-color: #DB7093"/>	<td>DB</td>	<td>70</td>	<td>93</td>	</tr>
		 * <tr>								<td>VioletRed</td>			<td style="background-color: #D02090"/>	<td>D0</td>	<td>20</td>	<td>90</td>	</tr>
		 * <tr>	<td rowspan=9>Red</td>		<td>LightSalmon</td>		<td style="background-color: #FFA07A"/>	<td>FF</td>	<td>A0</td>	<td>7A</td>	</tr>
		 * <tr>								<td>Salmon</td>				<td style="background-color: #FA8072"/>	<td>FA</td>	<td>80</td>	<td>72</td>	</tr>
		 * <tr>								<td>DarkSalmon</td>			<td style="background-color: #E9967A"/>	<td>E9</td>	<td>96</td>	<td>7A</td>	</tr>
		 * <tr>								<td>LightCoral</td>			<td style="background-color: #F08080"/>	<td>F0</td>	<td>80</td>	<td>80</td>	</tr>
		 * <tr>								<td>IndianRed</td>			<td style="background-color: #CD5C5C"/>	<td>CD</td>	<td>5C</td>	<td>5C</td>	</tr>
		 * <tr>								<td>Crimson</td>			<td style="background-color: #DC143C"/>	<td>DC</td>	<td>14</td>	<td>3C</td>	</tr>
		 * <tr>								<td>Firebrick</td>			<td style="background-color: #B22222"/>	<td>B2</td>	<td>22</td>	<td>22</td>	</tr>
		 * <tr>								<td>DarkRed</td>			<td style="background-color: #8B0000"/>	<td>8B</td>	<td>00</td>	<td>00</td>	</tr>
		 * <tr>								<td>Red</td>				<td style="background-color: #FF0000"/>	<td>FF</td>	<td>00</td>	<td>00</td>	</tr>
		 * <tr>	<td rowspan=5>Orange</td>	<td>OrangeRed</td>			<td style="background-color: #FF4500"/>	<td>FF</td>	<td>45</td>	<td>00</td>	</tr>
		 * <tr>								<td>Tomato</td>				<td style="background-color: #FF6347"/>	<td>FF</td>	<td>63</td>	<td>47</td>	</tr>
		 * <tr>								<td>Coral</td>				<td style="background-color: #FF7F50"/>	<td>FF</td>	<td>7F</td>	<td>50</td>	</tr>
		 * <tr>								<td>DarkOrange</td>			<td style="background-color: #FF8C00"/>	<td>FF</td>	<td>8C</td>	<td>00</td>	</tr>
		 * <tr>								<td>Orange</td>				<td style="background-color: #FFA500"/>	<td>FF</td>	<td>A5</td>	<td>00</td>	</tr>
		 * <tr>	<td rowspan=11>Yellow</td>	<td>Yellow</td>				<td style="background-color: #FFFF00"/>	<td>FF</td>	<td>FF</td>	<td>00</td>	</tr>
		 * <tr>								<td>LightYellow</td>		<td style="background-color: #FFFFE0"/>	<td>FF</td>	<td>FF</td>	<td>E0</td>	</tr>
		 * <tr>								<td>LemonChiffon</td>		<td style="background-color: #FFFACD"/>	<td>FF</td>	<td>FA</td>	<td>CD</td>	</tr>
		 * <tr>								<td>LightGoldenrod</td>		<td style="background-color: #EEDD82"/>	<td>EE</td>	<td>DD</td>	<td>82</td>	</tr>
		 * <tr>								<td>PapayaWhip</td>			<td style="background-color: #FFEFD5"/>	<td>FF</td>	<td>EF</td>	<td>D5</td>	</tr>
		 * <tr>								<td>Moccasin</td>			<td style="background-color: #FFE4B5"/>	<td>FF</td>	<td>E4</td>	<td>B5</td>	</tr>
		 * <tr>								<td>PeachPuff</td>			<td style="background-color: #FFDAB9"/>	<td>FF</td>	<td>DA</td>	<td>B9</td>	</tr>
		 * <tr>								<td>PaleGoldenrod</td>		<td style="background-color: #EEE8AA"/>	<td>EE</td>	<td>E8</td>	<td>AA</td>	</tr>
		 * <tr>								<td>Khaki</td>				<td style="background-color: #F0E68C"/>	<td>F0</td>	<td>E6</td>	<td>8C</td>	</tr>
		 * <tr>								<td>DarkKhaki</td>			<td style="background-color: #BDB76B"/>	<td>BD</td>	<td>B7</td>	<td>6B</td>	</tr>
		 * <tr>								<td>Gold</td>				<td style="background-color: #FFD700"/>	<td>FF</td>	<td>D7</td>	<td>00</td>	</tr>
		 * <tr>	<td rowspan=17>Brown</td>	<td>Cornsilk</td>			<td style="background-color: #FFF8DC"/>	<td>FF</td>	<td>F8</td>	<td>DC</td>	</tr>
		 * <tr>								<td>BlanchedAlmond</td>		<td style="background-color: #FFEBCD"/>	<td>FF</td>	<td>EB</td>	<td>CD</td>	</tr>
		 * <tr>								<td>Bisque</td>				<td style="background-color: #FFE4C4"/>	<td>FF</td>	<td>E4</td>	<td>C4</td>	</tr>
		 * <tr>								<td>NavajoWhite</td>		<td style="background-color: #FFDEAD"/>	<td>FF</td>	<td>DE</td>	<td>AD</td>	</tr>
		 * <tr>								<td>Wheat</td>				<td style="background-color: #F5DEB3"/>	<td>F5</td>	<td>DE</td>	<td>B3</td>	</tr>
		 * <tr>								<td>Burlywood</td>			<td style="background-color: #DEB887"/>	<td>DE</td>	<td>B8</td>	<td>87</td>	</tr>
		 * <tr>								<td>Tan</td>				<td style="background-color: #D2B48C"/>	<td>D2</td>	<td>B4</td>	<td>8C</td>	</tr>
		 * <tr>								<td>RosyBrown</td>			<td style="background-color: #BC8F8F"/>	<td>BC</td>	<td>8F</td>	<td>8F</td>	</tr>
		 * <tr>								<td>SandyBrown</td>			<td style="background-color: #F4A460"/>	<td>F4</td>	<td>A4</td>	<td>60</td>	</tr>
		 * <tr>								<td>Goldenrod</td>			<td style="background-color: #DAA520"/>	<td>DA</td>	<td>A5</td>	<td>20</td>	</tr>
		 * <tr>								<td>DarkGoldenrod</td>		<td style="background-color: #B8860B"/>	<td>B8</td>	<td>86</td>	<td>0B</td>	</tr>
		 * <tr>								<td>Peru</td>				<td style="background-color: #CD853F"/>	<td>CD</td>	<td>85</td>	<td>3F</td>	</tr>
		 * <tr>								<td>Chocolate</td>			<td style="background-color: #D2691E"/>	<td>D2</td>	<td>69</td>	<td>1E</td>	</tr>
		 * <tr>								<td>SaddleBrown</td>		<td style="background-color: #8B4513"/>	<td>8B</td>	<td>45</td>	<td>13</td>	</tr>
		 * <tr>								<td>Sienna</td>				<td style="background-color: #A0522D"/>	<td>A0</td>	<td>52</td>	<td>2D</td>	</tr>
		 * <tr>								<td>Brown</td>				<td style="background-color: #A52A2A"/>	<td>A5</td>	<td>2A</td>	<td>2A</td>	</tr>
		 * <tr>								<td>Maroon</td>				<td style="background-color: #B03060"/>	<td>B0</td>	<td>30</td>	<td>60</td>	</tr>
		 * <tr>	<td rowspan=20>Green</td>	<td>DarkOliveGreen</td>		<td style="background-color: #556B2F"/>	<td>55</td>	<td>6B</td>	<td>2F</td>	</tr>
		 * <tr>								<td>Olive</td>				<td style="background-color: #808000"/>	<td>80</td>	<td>80</td>	<td>00</td>	</tr>
		 * <tr>								<td>OliveDrab</td>			<td style="background-color: #6B8E23"/>	<td>6B</td>	<td>8E</td>	<td>23</td>	</tr>
		 * <tr>								<td>YellowGreen</td>		<td style="background-color: #9ACD32"/>	<td>9A</td>	<td>CD</td>	<td>32</td>	</tr>
		 * <tr>								<td>LimeGreen</td>			<td style="background-color: #32CD32"/>	<td>32</td>	<td>CD</td>	<td>32</td>	</tr>
		 * <tr>								<td>Lime</td>				<td style="background-color: #00FF00"/>	<td>00</td>	<td>FF</td>	<td>00</td>	</tr>
		 * <tr>								<td>LawnGreen</td>			<td style="background-color: #7CFC00"/>	<td>7C</td>	<td>FC</td>	<td>00</td>	</tr>
		 * <tr>								<td>Chartreuse</td>			<td style="background-color: #7FFF00"/>	<td>7F</td>	<td>FF</td>	<td>00</td>	</tr>
		 * <tr>								<td>GreenYellow</td>		<td style="background-color: #ADFF2F"/>	<td>AD</td>	<td>FF</td>	<td>2F</td>	</tr>
		 * <tr>								<td>SpringGreen</td>		<td style="background-color: #00FF7F"/>	<td>00</td>	<td>FF</td>	<td>7F</td>	</tr>
		 * <tr>								<td>MediumSpringGreen</td>	<td style="background-color: #00FA9A"/>	<td>00</td>	<td>FA</td>	<td>9A</td>	</tr>
		 * <tr>								<td>LightGreen</td>			<td style="background-color: #90EE90"/>	<td>90</td>	<td>EE</td>	<td>90</td>	</tr>
		 * <tr>								<td>PaleGreen</td>			<td style="background-color: #98FB98"/>	<td>98</td>	<td>FB</td>	<td>98</td>	</tr>
		 * <tr>								<td>DarkSeaGreen</td>		<td style="background-color: #8FBC8F"/>	<td>8F</td>	<td>BC</td>	<td>8F</td>	</tr>
		 * <tr>								<td>MediumAquamarine</td>	<td style="background-color: #66CDAA"/>	<td>66</td>	<td>CD</td>	<td>AA</td>	</tr>
		 * <tr>								<td>MediumSeaGreen</td>		<td style="background-color: #3CB371"/>	<td>3C</td>	<td>B3</td>	<td>71</td>	</tr>
		 * <tr>								<td>SeaGreen</td>			<td style="background-color: #2E8B57"/>	<td>2E</td>	<td>8B</td>	<td>57</td>	</tr>
		 * <tr>								<td>ForestGreen</td>		<td style="background-color: #228B22"/>	<td>22</td>	<td>8B</td>	<td>22</td>	</tr>
		 * <tr>								<td>Green</td>				<td style="background-color: #008000"/>	<td>00</td>	<td>80</td>	<td>00</td>	</tr>
		 * <tr>								<td>DarkGreen</td>			<td style="background-color: #006400"/>	<td>00</td>	<td>64</td>	<td>00</td>	</tr>
		 * <tr>	<td rowspan=12>Cyan</td>	<td>Aqua</td>				<td style="background-color: #00FFFF"/>	<td>00</td>	<td>FF</td>	<td>FF</td>	</tr>
		 * <tr>								<td>Cyan</td>				<td style="background-color: #00FFFF"/>	<td>00</td>	<td>FF</td>	<td>FF</td>	</tr>
		 * <tr>								<td>LightCyan</td>			<td style="background-color: #E0FFFF"/>	<td>E0</td>	<td>FF</td>	<td>FF</td>	</tr>
		 * <tr>								<td>PaleTurquoise</td>		<td style="background-color: #AFEEEE"/>	<td>AF</td>	<td>EE</td>	<td>EE</td>	</tr>
		 * <tr>								<td>Aquamarine</td>			<td style="background-color: #7FFFD4"/>	<td>7F</td>	<td>FF</td>	<td>D4</td>	</tr>
		 * <tr>								<td>Turquoise</td>			<td style="background-color: #40E0D0"/>	<td>40</td>	<td>E0</td>	<td>D0</td>	</tr>
		 * <tr>								<td>MediumTurquoise</td>	<td style="background-color: #48D1CC"/>	<td>48</td>	<td>D1</td>	<td>CC</td>	</tr>
		 * <tr>								<td>DarkTurquoise</td>		<td style="background-color: #00CED1"/>	<td>00</td>	<td>CE</td>	<td>D1</td>	</tr>
		 * <tr>								<td>LightSeaGreen</td>		<td style="background-color: #20B2AA"/>	<td>20</td>	<td>B2</td>	<td>AA</td>	</tr>
		 * <tr>								<td>CadetBlue</td>			<td style="background-color: #5F9EA0"/>	<td>5F</td>	<td>9E</td>	<td>A0</td>	</tr>
		 * <tr>								<td>DarkCyan</td>			<td style="background-color: #008B8B"/>	<td>00</td>	<td>8B</td>	<td>8B</td>	</tr>
		 * <tr>								<td>Teal</td>				<td style="background-color: #008080"/>	<td>00</td>	<td>80</td>	<td>80</td>	</tr>
		 * <tr>	<td rowspan=15>Blue</td>	<td>LightSteelBlue</td>		<td style="background-color: #B0C4DE"/>	<td>B0</td>	<td>C4</td>	<td>DE</td>	</tr>
		 * <tr>								<td>PowderBlue</td>			<td style="background-color: #B0E0E6"/>	<td>B0</td>	<td>E0</td>	<td>E6</td>	</tr>
		 * <tr>								<td>LightBlue</td>			<td style="background-color: #ADD8E6"/>	<td>AD</td>	<td>D8</td>	<td>E6</td>	</tr>
		 * <tr>								<td>SkyBlue</td>			<td style="background-color: #87CEEB"/>	<td>87</td>	<td>CE</td>	<td>EB</td>	</tr>
		 * <tr>								<td>LightSkyBlue</td>		<td style="background-color: #87CEFA"/>	<td>87</td>	<td>CE</td>	<td>FA</td>	</tr>
		 * <tr>								<td>DeepSkyBlue</td>		<td style="background-color: #00BFFF"/>	<td>00</td>	<td>BF</td>	<td>FF</td>	</tr>
		 * <tr>								<td>DodgerBlue</td>			<td style="background-color: #1E90FF"/>	<td>1E</td>	<td>90</td>	<td>FF</td>	</tr>
		 * <tr>								<td>CornflowerBlue</td>		<td style="background-color: #6495ED"/>	<td>64</td>	<td>95</td>	<td>ED</td>	</tr>
		 * <tr>								<td>SteelBlue</td>			<td style="background-color: #4682B4"/>	<td>46</td>	<td>82</td>	<td>B4</td>	</tr>
		 * <tr>								<td>RoyalBlue</td>			<td style="background-color: #4169E1"/>	<td>41</td>	<td>69</td>	<td>E1</td>	</tr>
		 * <tr>								<td>Blue</td>				<td style="background-color: #0000FF"/>	<td>00</td>	<td>00</td>	<td>FF</td>	</tr>
		 * <tr>								<td>MediumBlue</td>			<td style="background-color: #0000CD"/>	<td>00</td>	<td>00</td>	<td>CD</td>	</tr>
		 * <tr>								<td>DarkBlue</td>			<td style="background-color: #00008B"/>	<td>00</td>	<td>00</td>	<td>8B</td>	</tr>
		 * <tr>								<td>Navy</td>				<td style="background-color: #000080"/>	<td>00</td>	<td>00</td>	<td>80</td>	</tr>
		 * <tr>								<td>MidnightBlue</td>		<td style="background-color: #191970"/>	<td>19</td>	<td>19</td>	<td>70</td>	</tr>
		 * <tr>	<td rowspan=18>Magenta</td> <td>Lavender</td>			<td style="background-color: #E6E6FA"/>	<td>E6</td>	<td>E6</td>	<td>FA</td>	</tr>
		 * <tr>								<td>Thistle</td>			<td style="background-color: #D8BFD8"/>	<td>D8</td>	<td>BF</td>	<td>D8</td>	</tr>
		 * <tr>								<td>Plum</td>				<td style="background-color: #DDA0DD"/>	<td>DD</td>	<td>A0</td>	<td>DD</td>	</tr>
		 * <tr>								<td>Violet</td>				<td style="background-color: #EE82EE"/>	<td>EE</td>	<td>82</td>	<td>EE</td>	</tr>
		 * <tr>								<td>Orchid</td>				<td style="background-color: #DA70D6"/>	<td>DA</td>	<td>70</td>	<td>D6</td>	</tr>
		 * <tr>								<td>Fuchsia</td>			<td style="background-color: #FF00FF"/>	<td>FF</td>	<td>00</td>	<td>FF</td>	</tr>
		 * <tr>								<td>Magenta</td>			<td style="background-color: #FF00FF"/>	<td>FF</td>	<td>00</td>	<td>FF</td>	</tr>
		 * <tr>								<td>MediumOrchid</td>		<td style="background-color: #BA55D3"/>	<td>BA</td>	<td>55</td>	<td>D3</td>	</tr>
		 * <tr>								<td>MediumPurple</td>		<td style="background-color: #9370DB"/>	<td>93</td>	<td>70</td>	<td>DB</td>	</tr>
		 * <tr>								<td>BlueViolet</td>			<td style="background-color: #8A2BE2"/>	<td>8A</td>	<td>2B</td>	<td>E2</td>	</tr>
		 * <tr>								<td>DarkViolet</td>			<td style="background-color: #9400D3"/>	<td>94</td>	<td>00</td>	<td>D3</td>	</tr>
		 * <tr>								<td>DarkOrchid</td>			<td style="background-color: #9932CC"/>	<td>99</td>	<td>32</td>	<td>CC</td>	</tr>
		 * <tr>								<td>DarkMagenta</td>		<td style="background-color: #8B008B"/>	<td>8B</td>	<td>00</td>	<td>8B</td>	</tr>
		 * <tr>								<td>Purple</td>				<td style="background-color: #A020F0"/>	<td>A0</td>	<td>20</td>	<td>F0</td>	</tr>
		 * <tr>								<td>Indigo</td>				<td style="background-color: #4B0082"/>	<td>4B</td>	<td>00</td>	<td>82</td>	</tr>
		 * <tr>								<td>DarkSlateBlue</td>		<td style="background-color: #483D8B"/>	<td>48</td>	<td>3D</td>	<td>8B</td>	</tr>
		 * <tr>								<td>SlateBlue</td>			<td style="background-color: #6A5ACD"/>	<td>6A</td>	<td>5A</td>	<td>CD</td>	</tr>
		 * <tr>								<td>MediumSlateBlue</td>	<td style="background-color: #7B68EE"/>	<td>7B</td>	<td>68</td>	<td>EE</td>	</tr>
		 * <tr>	<td rowspan=17>White</td>	<td>White</td>				<td style="background-color: #FFFFFF"/>	<td>FF</td>	<td>FF</td>	<td>FF</td>	</tr>
		 * <tr>								<td>Snow</td>				<td style="background-color: #FFFAFA"/>	<td>FF</td>	<td>FA</td>	<td>FA</td>	</tr>
		 * <tr>								<td>Honeydew</td>			<td style="background-color: #F0FFF0"/>	<td>F0</td>	<td>FF</td>	<td>F0</td>	</tr>
		 * <tr>								<td>MintCream</td>			<td style="background-color: #F5FFFA"/>	<td>F5</td>	<td>FF</td>	<td>FA</td>	</tr>
		 * <tr>								<td>Azure</td>				<td style="background-color: #F0FFFF"/>	<td>F0</td>	<td>FF</td>	<td>FF</td>	</tr>
		 * <tr>								<td>AliceBlue</td>			<td style="background-color: #F0F8FF"/>	<td>F0</td>	<td>F8</td>	<td>FF</td>	</tr>
		 * <tr>								<td>GhostWhite</td>			<td style="background-color: #F8F8FF"/>	<td>F8</td>	<td>F8</td>	<td>FF</td>	</tr>
		 * <tr>								<td>WhiteSmoke</td>			<td style="background-color: #F5F5F5"/>	<td>F5</td>	<td>F5</td>	<td>F5</td>	</tr>
		 * <tr>								<td>Seashell</td>			<td style="background-color: #FFF5EE"/>	<td>FF</td>	<td>F5</td>	<td>EE</td>	</tr>
		 * <tr>								<td>Beige</td>				<td style="background-color: #F5F5DC"/>	<td>F5</td>	<td>F5</td>	<td>DC</td>	</tr>
		 * <tr>								<td>OldLace</td>			<td style="background-color: #FDF5E6"/>	<td>FD</td>	<td>F5</td>	<td>E6</td>	</tr>
		 * <tr>								<td>FloralWhite</td>		<td style="background-color: #FFFAF0"/>	<td>FF</td>	<td>FA</td>	<td>F0</td>	</tr>
		 * <tr>								<td>Ivory</td>				<td style="background-color: #FFFFF0"/>	<td>FF</td>	<td>FF</td>	<td>F0</td>	</tr>
		 * <tr>								<td>AntiqueWhite</td>		<td style="background-color: #FAEBD7"/>	<td>FA</td>	<td>EB</td>	<td>D7</td>	</tr>
		 * <tr>								<td>Linen</td>				<td style="background-color: #FAF0E6"/>	<td>FA</td>	<td>F0</td>	<td>E6</td>	</tr>
		 * <tr>								<td>LavenderBlush</td>		<td style="background-color: #FFF0F5"/>	<td>FF</td>	<td>F0</td>	<td>F5</td>	</tr>
		 * <tr>								<td>MistyRose</td>			<td style="background-color: #FFE4E1"/>	<td>FF</td>	<td>E4</td>	<td>E1</td>	</tr>
		 * <tr>	<td rowspan=10>Gray</td>	<td>Gainsboro</td>			<td style="background-color: #DCDCDC"/>	<td>DC</td>	<td>DC</td>	<td>DC</td>	</tr>
		 * <tr>								<td>LightGray</td>			<td style="background-color: #D3D3D3"/>	<td>D3</td>	<td>D3</td>	<td>D3</td>	</tr>
		 * <tr>								<td>Silver</td>				<td style="background-color: #C0C0C0"/>	<td>C0</td>	<td>C0</td>	<td>C0</td>	</tr>
		 * <tr>								<td>DarkGray</td>			<td style="background-color: #A9A9A9"/>	<td>A9</td>	<td>A9</td>	<td>A9</td>	</tr>
		 * <tr>								<td>Gray</td>				<td style="background-color: #808080"/>	<td>80</td>	<td>80</td>	<td>80</td>	</tr>
		 * <tr>								<td>DimGray</td>			<td style="background-color: #696969"/>	<td>69</td>	<td>69</td>	<td>69</td>	</tr>
		 * <tr>								<td>LightSlateGray</td>		<td style="background-color: #778899"/>	<td>77</td>	<td>88</td>	<td>99</td>	</tr>
		 * <tr>								<td>SlateGray</td>			<td style="background-color: #708090"/>	<td>70</td>	<td>80</td>	<td>90</td>	</tr>
		 * <tr>								<td>DarkSlateGray</td>		<td style="background-color: #2F4F4F"/>	<td>2F</td>	<td>4F</td>	<td>4F</td>	</tr>
		 * <tr>								<td>Black</td>				<td style="background-color: #000000"/>	<td>00</td>	<td>00</td>	<td>00</td>	</tr>
         * </table>
         */
        namespace Colors
        {
            /// \cond
            //X11
            //Pink
            static const Color Pink = 0xFFC0CB_rgb;
            static const Color LightPink = 0xFFB6C1_rgb;
            static const Color HotPink = 0xFF69B4_rgb;
            static const Color DeepPink = 0xFF1493_rgb;
            static const Color PaleVioletRed = 0xDB7093_rgb;
            static const Color VioletRed = 0xD02090_rgb;
            //Red
            static const Color LightSalmon = 0xFFA07A_rgb;
            static const Color Salmon = 0xFA8072_rgb;
            static const Color DarkSalmon = 0xE9967A_rgb;
            static const Color LightCoral = 0xF08080_rgb;
            static const Color IndianRed = 0xCD5C5C_rgb;
            static const Color Crimson = 0xDC143C_rgb;
            static const Color Firebrick = 0xB22222_rgb;
            static const Color DarkRed = 0x8B0000_rgb;
            static const Color Red = 0xFF0000_rgb;
            //Orange
            static const Color OrangeRed = 0xFF4500_rgb;
            static const Color Tomato = 0xFF6347_rgb;
            static const Color Coral = 0xFF7F50_rgb;
            static const Color DarkOrange = 0xFF8C00_rgb;
            static const Color Orange = 0xFFA500_rgb;
            //Yellow
            static const Color Yellow = 0xFFFF00_rgb;
            static const Color LightYellow = 0xFFFFE0_rgb;
            static const Color LemonChiffon = 0xFFFACD_rgb;
            static const Color LightGoldenrod = 0xEEDD82_rgb;
            static const Color PapayaWhip = 0xFFEFD5_rgb;
            static const Color Moccasin = 0xFFE4B5_rgb;
            static const Color PeachPuff = 0xFFDAB9_rgb;
            static const Color PaleGoldenrod = 0xEEE8AA_rgb;
            static const Color Khaki = 0xF0E68C_rgb;
            static const Color DarkKhaki = 0xBDB76B_rgb;
            static const Color Gold = 0xFFD700_rgb;
            //Brown
            static const Color Cornsilk = 0xFFF8DC_rgb;
            static const Color BlanchedAlmond = 0xFFEBCD_rgb;
            static const Color Bisque = 0xFFE4C4_rgb;
            static const Color NavajoWhite = 0xFFDEAD_rgb;
            static const Color Wheat = 0xF5DEB3_rgb;
            static const Color Burlywood = 0xDEB887_rgb;
            static const Color Tan = 0xD2B48C_rgb;
            static const Color RosyBrown = 0xBC8F8F_rgb;
            static const Color SandyBrown = 0xF4A460_rgb;
            static const Color Goldenrod = 0xDAA520_rgb;
            static const Color DarkGoldenrod = 0xB8860B_rgb;
            static const Color Peru = 0xCD853F_rgb;
            static const Color Chocolate = 0xD2691E_rgb;
            static const Color SaddleBrown = 0x8B4513_rgb;
            static const Color Sienna = 0xA0522D_rgb;
            static const Color Brown = 0xA52A2A_rgb;
            static const Color Maroon = 0xB03060_rgb;
            //Green
            static const Color DarkOliveGreen = 0x556B2F_rgb;
            static const Color Olive = 0x808000_rgb;
            static const Color OliveDrab = 0x6B8E23_rgb;
            static const Color YellowGreen = 0x9ACD32_rgb;
            static const Color LimeGreen = 0x32CD32_rgb;
            static const Color Lime = 0x00FF00_rgb;
            static const Color LawnGreen = 0x7CFC00_rgb;
            static const Color Chartreuse = 0x7FFF00_rgb;
            static const Color GreenYellow = 0xADFF2F_rgb;
            static const Color SpringGreen = 0x00FF7F_rgb;
            static const Color MediumSpringGreen = 0x00FA9A_rgb;
            static const Color LightGreen = 0x90EE90_rgb;
            static const Color PaleGreen = 0x98FB98_rgb;
            static const Color DarkSeaGreen = 0x8FBC8F_rgb;
            static const Color MediumAquamarine = 0x66CDAA_rgb;
            static const Color MediumSeaGreen = 0x3CB371_rgb;
            static const Color SeaGreen = 0x2E8B57_rgb;
            static const Color ForestGreen = 0x228B22_rgb;
            static const Color Green = 0x008000_rgb;
            static const Color DarkGreen = 0x006400_rgb;
            //Cyan
            static const Color Aqua = 0x00FFFF_rgb;
            static const Color Cyan = 0x00FFFF_rgb;
            static const Color LightCyan = 0xE0FFFF_rgb;
            static const Color PaleTurquoise = 0xAFEEEE_rgb;
            static const Color Aquamarine = 0x7FFFD4_rgb;
            static const Color Turquoise = 0x40E0D0_rgb;
            static const Color MediumTurquoise = 0x48D1CC_rgb;
            static const Color DarkTurquoise = 0x00CED1_rgb;
            static const Color LightSeaGreen = 0x20B2AA_rgb;
            static const Color CadetBlue = 0x5F9EA0_rgb;
            static const Color DarkCyan = 0x008B8B_rgb;
            static const Color Teal = 0x008080_rgb;
            //Blue
            static const Color LightSteelBlue = 0xB0C4DE_rgb;
            static const Color PowderBlue = 0xB0E0E6_rgb;
            static const Color LightBlue = 0xADD8E6_rgb;
            static const Color SkyBlue = 0x87CEEB_rgb;
            static const Color LightSkyBlue = 0x87CEFA_rgb;
            static const Color DeepSkyBlue = 0x00BFFF_rgb;
            static const Color DodgerBlue = 0x1E90FF_rgb;
            static const Color CornflowerBlue = 0x6495ED_rgb;
            static const Color SteelBlue = 0x4682B4_rgb;
            static const Color RoyalBlue = 0x4169E1_rgb;
            static const Color Blue = 0x0000FF_rgb;
            static const Color MediumBlue = 0x0000CD_rgb;
            static const Color DarkBlue = 0x00008B_rgb;
            static const Color Navy = 0x000080_rgb;
            static const Color MidnightBlue = 0x191970_rgb;
            //Magenta
            static const Color Lavender = 0xE6E6FA_rgb;
            static const Color Thistle = 0xD8BFD8_rgb;
            static const Color Plum = 0xDDA0DD_rgb;
            static const Color Violet = 0xEE82EE_rgb;
            static const Color Orchid = 0xDA70D6_rgb;
            static const Color Fuchsia = 0xFF00FF_rgb;
            static const Color Magenta = 0xFF00FF_rgb;
            static const Color MediumOrchid = 0xBA55D3_rgb;
            static const Color MediumPurple = 0x9370DB_rgb;
            static const Color BlueViolet = 0x8A2BE2_rgb;
            static const Color DarkViolet = 0x9400D3_rgb;
            static const Color DarkOrchid = 0x9932CC_rgb;
            static const Color DarkMagenta = 0x8B008B_rgb;
            static const Color Purple = 0xA020F0_rgb;
            static const Color Indigo = 0x4B0082_rgb;
            static const Color DarkSlateBlue = 0x483D8B_rgb;
            static const Color SlateBlue = 0x6A5ACD_rgb;
            static const Color MediumSlateBlue = 0x7B68EE_rgb;
            //White
            static const Color White = 0xFFFFFF_rgb;
            static const Color Snow = 0xFFFAFA_rgb;
            static const Color Honeydew = 0xF0FFF0_rgb;
            static const Color MintCream = 0xF5FFFA_rgb;
            static const Color Azure = 0xF0FFFF_rgb;
            static const Color AliceBlue = 0xF0F8FF_rgb;
            static const Color GhostWhite = 0xF8F8FF_rgb;
            static const Color WhiteSmoke = 0xF5F5F5_rgb;
            static const Color Seashell = 0xFFF5EE_rgb;
            static const Color Beige = 0xF5F5DC_rgb;
            static const Color OldLace = 0xFDF5E6_rgb;
            static const Color FloralWhite = 0xFFFAF0_rgb;
            static const Color Ivory = 0xFFFFF0_rgb;
            static const Color AntiqueWhite = 0xFAEBD7_rgb;
            static const Color Linen = 0xFAF0E6_rgb;
            static const Color LavenderBlush = 0xFFF0F5_rgb;
            static const Color MistyRose = 0xFFE4E1_rgb;
            //Gray
            static const Color Gainsboro = 0xDCDCDC_rgb;
            static const Color LightGray = 0xD3D3D3_rgb;
            static const Color Silver = 0xC0C0C0_rgb;
            static const Color DarkGray = 0xA9A9A9_rgb;
            static const Color Gray = 0x808080_rgb;
            static const Color DimGray = 0x696969_rgb;
            static const Color LightSlateGray = 0x778899_rgb;
            static const Color SlateGray = 0x708090_rgb;
            static const Color DarkSlateGray = 0x2F4F4F_rgb;
            static const Color Black = 0x000000_rgb;
            //Transparent
            static const Color Transparent = 0x00000000_argb;
            /// \endcond
        }
    }
}

#endif /* BORIS_COLORS */