#ifndef BORIS_IMAGE
#define BORIS_IMAGE

#include "Color.h"
#include "Colors.h"
#include <Geometry.h>

#include <vector>

namespace Boris
{
    namespace Graphics
    {
        class Image
        {
        public:
            Image();
            /// Constructor.
            /*!
             * Constructs new Image of specified size with background color.
             * @param size size of the image.
             * @param background default background color.
             */
            explicit Image(const Geometry::Size& size, const Color& background = Colors::White);
            Image(const Image&) = default;
            Image(Image&&) = default;
            virtual ~Image() = default;

            Image& operator=(const Image&) = delete;
            Image& operator=(Image&&) = delete;

            /// Get color of pixel.
            Color GetPixel(const Geometry::Point& pixel) const;
            /// Set color of pixel.
            void SetPixel(const Geometry::Point& pixel, const Color& color);
            /// Set color of all pixels in rectangle.
            void SetRectangle(const Geometry::Point& point, const Geometry::Size& size, const Color& color);

            /// Get image size.
            const Geometry::Size& Size() const;
            /// Resize image.
            /*!
             * If is scaling up new pixels have default background
             * color. If scaling down cropping image.
             * @param size new size of image.
             */
            void Resize(const Geometry::Size& size);

            /// Access data array in RGBA format.
            const uint8_t * Data() const;

        private:
            std::vector<uint8_t> m_ImageData;
            Geometry::Size m_Size;
            Color m_Background;
        };
    }
}

#endif /* BORIS_IMAGE */