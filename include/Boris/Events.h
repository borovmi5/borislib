#ifndef BORIS_EVENTS
#define BORIS_EVENTS

#include "Events/EventArgs.h"
#include "Events/EventHandler.h"
#include "Events/Event.h"
#include "Events/TimePassedEventArgs.h"

#include <type_traits>

namespace Boris
{
    /// Event system.
    /*!
     * Provides basic support for events.
     *
     * ## Example:
     * \include events_example.cpp
     */
    namespace Events
    {
        /// Basic event handler.
        using BasicEventHandler = EventHandler<EventArgs>;
        ///	Basic event.
        using BasicEvent = Event<BasicEventHandler>;
        /// Time passed event handler.
        using TimePassedEventHandler = EventHandler<TimePassedEventArgs>;
        /// Time passed event.
        using TimePassedEvent = Event<TimePassedEventHandler>;
        /// Registers event.
        /*!
         * Binds handler to the event.
         * @tparam E Type of event. Must be derived from Event.
         * @tparam H Type of event handler. Must be derived from EventHandler.
         * @param event event to bind handler to.
         * @param handler handler to bind to event.
         */
        template <typename E, typename H>
        void RegisterHandler(E& event, H& handler)
        {
            static_assert(std::is_base_of<EventHandler<typename H::ArgsType>, H>::value, "H must be derived from EventHandler.");
            static_assert(std::is_base_of<Event<H>, E>::value, "E must be derived from Event.");

            handler.m_Registered.push_back((typename H::EventType *)(&event));
        }
        /// Unregisters event.
        /*!
         * Unbinds handler with event.
         * @tparam E Type of event.
         * @tparam H Type of event handler.
         * @param event event to unbind handler from.
         * @param handler handler to unbind from event.
         */
        template <typename E, typename H>
        void UnregisterHandler(const E& event, H& handler)
        {
            static_assert(std::is_base_of<EventHandler<typename H::ArgsType>, H>::value, "H must be derived from EventHandler.");
            static_assert(std::is_base_of<Event<H>, E>::value, "E must be derived from Event.");

            auto registered = handler.m_Registered.begin();
            while(registered != handler.m_Registered.end())
            {
                if(*(E *)(*registered) == event)
                {
                    handler.m_Registered.erase(registered);
                    break;
                }
                ++registered;
            }
        }

        /*! \example events_example.cpp
         * This is an example of the usage of the Events.
         */
    }
}

#endif /* BORIS_EVENTS */