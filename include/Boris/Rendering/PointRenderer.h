#ifndef BORIS_POINT_RENDERER
#define BORIS_POINT_RENDERER

#include "Renderer.h"
#include <Geometry.h>
#include <Graphics.h>

namespace Boris
{
    namespace Rendering
    {
        class PointRenderer : public Renderer
        {
        public:
            /// @private
            PointRenderer() = delete;
            /// Null constructor.
            PointRenderer(std::nullptr_t);
            /// Constructor.
            /*!
             * @param position position where to render.
             * @param color color of the point.
             * @param type type of the point @sa Graphics::PointType
             * @param size size of the point
             * @param offset offset from the position
             */
            PointRenderer(const Geometry::FPoint& position, const Graphics::Color& color, const Graphics::PointType& type = Graphics::PointType::DOT, double size = 1, const Geometry::FPoint& offset = {0, 0});
            /// @private
            PointRenderer(const PointRenderer& other) = delete;
            PointRenderer(PointRenderer&& other) = default;
            ~PointRenderer() override = default;

            /// @private
            PointRenderer& operator=(const PointRenderer& other) = delete;


            void Render(const Object& sender = null, const Events::EventArgs& e = null) const override;

            double& Size();
            double Size() const;

            Graphics::PointType& Type();
            const Graphics::PointType& Type() const;

        private:
            double m_Size; //!< Size of the point.
            Graphics::PointType m_Type; //!< Type of the point.
        };
    }
}

#endif /* BORIS_POINT_RENDERER */