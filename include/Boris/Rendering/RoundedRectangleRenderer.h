#ifndef BORIS_ROUNDED_RECTANGLE_RENDERER
#define BORIS_ROUNDED_RECTANGLE_RENDERER

#include "Renderer.h"
#include <Geometry.h>
#include <Graphics.h>

namespace Boris
{
    namespace Rendering
    {
        /// Rectangle renderer.
        class RoundedRectangleRenderer : public Renderer
        {
        public:
            /// @private
            RoundedRectangleRenderer() = delete;
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param size size of the rectangle.
             * @param radius corner radius.
             * @param offset offset from the position.
             */
            RoundedRectangleRenderer(const Geometry::FPoint& position, const Geometry::FSize& size, double radius, const Geometry::FPoint& offset = {0, 0});
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param size size of the rectangle.
             * @param radius corner radius.
             * @param color color of the rectangle.
             * @param offset offset from the position.
             */
            RoundedRectangleRenderer(const Geometry::FPoint& position, const Geometry::FSize& size, double radius,
                              const Graphics::Color& color, const Geometry::FPoint& offset = {0, 0});
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param size size of the rectangle.
             * @param radius corner radius.
             * @param color color of the rectangle.
             * @param outline color of the outline of rectangle.
             * @param outline_width width of the outline of rectangle.
             * @param type style of the outline of rectangle.
             * @param offset offset from the position.
             */
            RoundedRectangleRenderer(const Geometry::FPoint& position, const Geometry::FSize& size, double radius,
                              const Graphics::Color& color, const Graphics::Color& outline, double outline_width, const Graphics::LineType& type = Graphics::LineType::SOLID, const Geometry::FPoint& offset = {0, 0});
            /// Null constructor.
            RoundedRectangleRenderer(std::nullptr_t);
            /// @private
            RoundedRectangleRenderer(const RoundedRectangleRenderer& other) = delete;
            /// Move constructor.
            RoundedRectangleRenderer(RoundedRectangleRenderer&& other) = default;
            /// Destructor.
            ~RoundedRectangleRenderer() override = default;

            /// @private
            RoundedRectangleRenderer& operator=(const RoundedRectangleRenderer& other) = delete;

            void Render(const Object& sender = null, const Events::EventArgs& e = null) const override;

            using Renderer::Offset;
            void Offset(const Geometry::FPoint& offset) override;
            /// Size of the rectangle.
            const Geometry::FSize& Size() const;
            /// Size of the rectangle.
            Geometry::FSize& Size();
            /// Corner radius.
            double Radius() const;
            /// Corner radius.
            void Radius(double radius);

        private:
            Geometry::RoundedRectangle m_RoundedRectangle; //!< Rounded rectangle to render.
        };
    }
}

#endif /* BORIS_RECTANGLE_RENDERER */