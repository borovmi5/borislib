#ifndef BORIS_RENDERER
#define BORIS_RENDERER

#include <Core.h>
#include <Events.h>
#include <Geometry.h>
#include <Graphics.h>

namespace Boris
{
    namespace Rendering
    {
        /// Base renderer.
        /*!
         * Handles rendering on screen.
         */
        class Renderer : public Object
        {
        public:
            /// @private
            Renderer() = delete;
            /// Constructor.
            /*!
             * Constructs new renderer with specified position and offset.
             * @param position const reference to position, where to render.
             * @param offset offset from that position.
             * @param innersize size of the inner space.
             * @param rotation rotation of the object.
             */
            Renderer(const Geometry::FPoint& position, const Geometry::FPoint& offset = {}, const Geometry::FSize& innersize = {}, double rotation = 0);
            /// Null constructor.
            /*!
             * Constructs new null renderer.
             */
            Renderer(std::nullptr_t);
            /// @private
            Renderer(const Renderer& other) = delete;
            /// Move constructor.
            /*!
             * Constructs new copy of another renderer using move semantics.
             * @param other another renderer to be used as data source to initialize the renderer.
             */
            Renderer(Renderer&& other) noexcept;
            /// Destructor.
            /*!
             * Destructs the renderer.
             */
            ~Renderer() override = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the renderer with another renderer.
             * @param other another renderer to be used as data source to overwrite the renderer.
             * @return *this
             */
            Renderer& operator=(const Renderer& other) = delete;

            /// Executes rendering.
            virtual void Render(const Object& sender = null, const Events::EventArgs& e = null) const = 0;

            const Geometry::FPoint& Position() const;
            void Position(const Geometry::FPoint& position);

            /// Offset from position.
            /*!
             * Access offset from the position.
             * @return offset from position.
             */
            const Geometry::FPoint& Offset() const;
            /// Offset from position.
            /*!
             * Set offset from the position.
             * @param offset new offset from position.
             */
            virtual void Offset(const Geometry::FPoint& offset);

            /// Color.
            /*!
             * Access render color of planes.
             * @return const reference to plane render color.
             */
            const Graphics::Color& Color() const;
            /// Color.
            /*!
             * Set render color of planes.
             * @param color new color of planes.
             */
            void Color(const Graphics::Color& color);

            /// Texture.
            /*!
             * Access bound texture.
             * @return const reference to the texture.
             */
             const Graphics::Texture& Texture() const;
             /// Texture
             /*!
              * Set texture.
              * @param texture new texture.
              */
              void Texture(const Graphics::Texture& texture);
              /// Texture.
              /*!
               * Clear texture.
               */
               void ClearTexture();

            /// Line color.
            /*!
             * Access render color of lines.
             * @return const reference to line render color.
             */
            const Graphics::Color& LineColor() const;
            /// Line color.
            /*!
             * Set render color of lines.
             * @param color new color of lines.
             */
            void LineColor(const Graphics::Color& color);

            /// Line width.
            /*!
             * Access render width of lines.
             * @return render width of lines.
             */
            double LineWidth() const;
            /// Line width.
            /*!
             * Set render width of lines.
             * @param width new width of lines.
             */
            void LineWidth(double width);

            /// Line style.
            /*!
             * Access style of lines.
             * @return const reference to style of lines.
             */
            const Graphics::LineType& LineType() const;
            /// Line style.
            /*!
             * Set style of lines.
             * @param type new line style.
             */
            void LineType(const Graphics::LineType& type);

            /// Getter
            const Geometry::FSize& InnerSize() const;
            /// Access
            Geometry::FSize& InnerSize();
            /// Getter
            const Geometry::FPoint& InnerOffset() const;
            /// Access
            Geometry::FPoint& InnerOffset();

            /// Getter
            const double& Rotation() const;
            /// Setter
            void Rotation(double rotation);

            /// Set parent renderer.
            /*!
             * Sets parent renderer. Coordinate system for this
             * renderer has [0,0] on the position of the parent renderer.
             * This renderer renders every time, parent renderer does.
             * @param renderer new parent renderer.
             */
            void SetParent(Renderer& renderer);
            /// Clears parent.
            void UnsetParent();


            Events::BasicEvent OnRender; //!< Triggered on render.

        protected:
            Events::BasicEventHandler m_RenderHandler; //!< Handles OnRender of parent renderer.

            Geometry::FPoint m_Position; //!< Reference to position.

            Geometry::FPoint m_Offset; //!< Offset from the position.

            Graphics::Color m_Color; //!< Color of planes.
            Graphics::Texture m_Texture; //!< Texture.
            Graphics::Color m_OutlineColor; //!< Color of lines.
            Graphics::LineType m_OutlineType; //!< Style of lines.
            double m_OutlineWidth; //!< Width of lines.

            Geometry::FSize m_InnerSize; //!< Size of inner space. [0, 0] means not changed.
            Geometry::FPoint m_InnerOffset; //!< Offset of inner space.
            double m_Rotation; //!< Rotation of object in degrees.
        };
    }
}

#endif /* BORIS_RENDERER */