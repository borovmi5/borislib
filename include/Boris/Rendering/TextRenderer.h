#ifndef BORIS_TEXT_RENDERER
#define BORIS_TEXT_RENDERER

#include "Renderer.h"
#include <Geometry.h>
#include <Graphics.h>

#include <string>

namespace Boris
{
    namespace Rendering
    {
        /// Text renderer.
        class TextRenderer : public Renderer
        {
        public:
            /// @private
            TextRenderer() = delete;
            /// Null constructor.
            TextRenderer(std::nullptr_t);
            /// Constructor.
            /*!
             * Constructs new text renderer.
             * @param position reference to position where to render.
             * @param text reference to text to render.
             * @param offset offset from the point.
             */
            TextRenderer(const Geometry::FPoint& position, const std::string& text, const Geometry::FPoint& offset = {});
            /// Constructor.
            /*!
             * Constructs new text renderer.
             * @param position reference to position where to render.
             * @param text reference to text to render.
             * @param color color of the text.
             * @param size font size.
             * @param mono whether to use monospace font.
             * @param bold whether the font is bold.
             * @param offset offset from the point.
             */
            TextRenderer(const Geometry::FPoint& position, const std::string& text, const Graphics::Color& color, double size = 16, bool bold = false, bool mono = false, const Geometry::FPoint& offset = {});
            /// @private
            TextRenderer(const TextRenderer& other) = delete;
            /// Move constructor.
            TextRenderer(TextRenderer&& other) = default;
            /// Destructor.
            ~TextRenderer() override = default;

            /// @private
            TextRenderer& operator=(const TextRenderer& other) = delete;

            void Render(const Object& sender = null, const Events::EventArgs& e = null) const override;

            /// Text.
            /*!
             * Access rendered text.
             * @return const reference to rendered text.
             */
            const std::string& Text() const;
            /// Setter.
            std::string& Text();

            /// Getter.
            const double& Size() const;
            /// Access.
            double& Size();

            /// Getter.
            const bool& Mono() const;
            /// Access.
            bool& Mono();

            /// Getter.
            const bool& Bold() const;
            /// Access.
            bool& Bold();

            /// Get string width in pixels.
            double Width() const;

        private:
            std::string m_Text; //!< Reference to the text.
            double m_Size; //!< Font size.
            bool m_Mono; //!< If true use monospace.
            bool m_Bold; //!< If true font is bold.
        };
    }
}

#endif /* BORIS_TEXT_RENDERER */