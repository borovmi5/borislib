#ifndef BORIS_CIRCLE_RENDERER
#define BORIS_CIRCLE_RENDERER

#include "Renderer.h"
#include <Geometry.h>
#include <Graphics.h>

namespace Boris
{
    namespace Rendering
    {
        /// Circle renderer.
        class CircleRenderer : public Renderer
        {
        public:
            /// @private
            CircleRenderer() = delete;
            /// Constructor.
            /*!
             * Constructs new circle renderer.
             * @param position reference where to render.
             * @param radius radius of the circle.
             * @param offset offset from the point.
             */
            CircleRenderer(const Geometry::FPoint& position, double radius, const Geometry::FPoint& offset = {0, 0});
            /// Constructor.
            /*!
             * Constructs new circle renderer.
             * @param position reference where to render.
             * @param radius radius of the circle.
             * @param color color of the circle.
             * @param offset offset from the point.
             */
            CircleRenderer(const Geometry::FPoint& position, double radius, const Graphics::Color& color, const Geometry::FPoint& offset = {0, 0});
            /// Constructor.
            /*!
             * Constructs new circle renderer.
             * @param position reference where to render.
             * @param radius radius of the circle.
             * @param color color of the circle.
             * @param outline color of the outline of circle.
             * @param outline_width width of the outline of circle.
             * @param offset offset from the point.
             */
            CircleRenderer(const Geometry::FPoint& position, double radius, const Graphics::Color& color, const Graphics::Color& outline, double outline_width, const Graphics::LineType& type = Graphics::LineType::SOLID, const Geometry::FPoint& offset = {0, 0});
            /// Null constructor.
            CircleRenderer(std::nullptr_t);
            /// @private
            CircleRenderer(const CircleRenderer& other) = delete;
            /// Move constructor.
            CircleRenderer(CircleRenderer&& other) = default;
            /// Destructor.
            ~CircleRenderer() override = default;

            /// @private
            CircleRenderer& operator=(const CircleRenderer& other) = delete;

            void Render(const Object& sender = null, const Events::EventArgs& e = null) const override;

            using Renderer::Offset;
            void Offset(const Geometry::FPoint& offset) override;
            /// Radius.
            /*!
             * Get radius of the circle.
             * @return radius of the circle.
             */
            double Radius();
            /// Radius.
            /*!
             * Set radius of the circle.
             * @param radius new radius of the circle.
             */
            void Radius(double radius);

        private:
            Geometry::Circle m_Circle; //!< Circle to render.
        };
    }
}

#endif /* BORIS_RECTANGLE_RENDERER */