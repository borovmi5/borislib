#ifndef BORIS_POLYGON_RENDERER
#define BORIS_POLYGON_RENDERER

#include "Renderer.h"

#include <Geometry.h>
#include <Graphics.h>

namespace Boris
{
    namespace Rendering
    {
        /// Polygon renderer
        class PolygonRenderer : public Renderer
        {
        public:
            /// @private
            PolygonRenderer() = delete;
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param poly polygon to render.
             * @param offset offset from the position.
             */
            PolygonRenderer(const Geometry::FPoint& position, Geometry::Polygon poly, const Geometry::FPoint& offset = {});
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param poly polygon to render.
             * @param color color of the polygon.
             * @param offset offset from the position.
             */
            PolygonRenderer(const Geometry::FPoint& position, Geometry::Polygon poly,
                            const Graphics::Color& color, const Geometry::FPoint& offset = {});
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param poly polygon to render.
             * @param color color of the polygon.
             * @param outline color of the outline of the polygon.
             * @param outline_width width of the outline of the polygon.
             * @param offset offset from the position.
             */
            PolygonRenderer(const Geometry::FPoint& position, Geometry::Polygon poly, const Graphics::Color& color,
                            const Graphics::Color& outline, double outline_width, const Geometry::FPoint& offset = {});
            /// Null constructor.
            PolygonRenderer(std::nullptr_t);
            ///	@private
            PolygonRenderer(const PolygonRenderer& other) = delete;
            /// Move constructor.
            /*!
             * Constructs new copy of another PolygonRenderer using move semantics.
             * @param other another PolygonRenderer to be used as source to initialize the PolygonRenderer.
             */
            PolygonRenderer(PolygonRenderer&& other) noexcept = default;
            /// Destructor.
            /*!	Destructs the PolygonRenderer.
             */
            ~PolygonRenderer() override = default;

            /// @private
            PolygonRenderer& operator=(const PolygonRenderer& other) = delete;

            void Render(const Object& sender = null, const Events::EventArgs& e = null) const override;

            using Renderer::Offset;
            void Offset(const Geometry::FPoint& offset) override;

        private:
            Geometry::Polygon m_Polygon;
        };
    }
}

#endif /* BORIS_POLYGON_RENDERER */