#ifndef BORIS_LINE_RENDERER
#define BORIS_LINE_RENDERER

#include "Renderer.h"
#include <Geometry.h>

namespace Boris
{
    namespace Rendering
    {
        /// Line renderer.
        class LineRenderer : public Renderer
        {
        public:
            /// @private
            LineRenderer() = delete;
            /// Null constructor.
            LineRenderer(std::nullptr_t);
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param p1 first point of the line.
             * @param p2 second point of the line.
             * @param offset offset from the point.
             */
            LineRenderer(const Geometry::FPoint& position, const Geometry::FPoint& p1, const Geometry::FPoint& p2, const Geometry::FPoint& offset = {0, 0});
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param p1 first point of the line.
             * @param p2 second point of the line.
             * @param color color of the line.
             * @param width width of the line.
             * @param type style of the line.
             * @param offset offset from the point.
             */
            LineRenderer(const Geometry::FPoint& position, const Geometry::FPoint& p1, const Geometry::FPoint& p2, const Graphics::Color& color, double width, const Graphics::LineType& type, const Geometry::FPoint& offset = {0, 0});
            /// @private
            LineRenderer(const LineRenderer& other) = delete;
            /// Move constructor.
            LineRenderer(LineRenderer&& other) = default;
            /// Destructor.
            ~LineRenderer() override = default;

            /// @private
            LineRenderer& operator=(const LineRenderer& other) = delete;

            void Render(const Object& sender = null, const Events::EventArgs& e = null) const override;

            using Renderer::Offset;
            void Offset(const Geometry::FPoint& offset) override;

            /// First point.
            /*!
             * Get first point of the line.
             * @return first point.
             */
            Geometry::FPoint P1() const;
            /// First point.
            /*!
             * Set first point of the line.
             * @param point new first point.
             */
            void P1(const Geometry::FPoint& point);

            /// Second point.
            /*!
             * Get second point of the line.
             * @return second point.
             */
            Geometry::FPoint P2() const;
            /// Second point.
            /*!
             * Set second point of the line.
             * @param point new second point.
             */
            void P2(const Geometry::FPoint& point);

        private:
            Geometry::Line m_Line; //!< Line to render.
        };
    }
}

#endif /* BORIS_LINE_RENDERER */