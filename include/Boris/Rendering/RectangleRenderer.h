#ifndef BORIS_RECTANGLE_RENDERER
#define BORIS_RECTANGLE_RENDERER

#include "Renderer.h"
#include <Geometry.h>
#include <Graphics.h>

namespace Boris
{
    namespace Rendering
    {
        /// Rectangle renderer.
        class RectangleRenderer : public Renderer
        {
        public:
            /// @private
            RectangleRenderer() = delete;
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param size size of the rectangle.
             * @param offset offset from the position.
             */
            RectangleRenderer(const Geometry::FPoint& position, const Geometry::FSize& size, const Geometry::FPoint& offset = {0, 0});
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param size size of the rectangle.
             * @param color color of the rectangle.
             * @param offset offset from the position.
             */
            RectangleRenderer(const Geometry::FPoint& position, const Geometry::FSize& size,
                              const Graphics::Color& color, const Geometry::FPoint& offset = {0, 0});
            /// Constructor.
            /*!
             * Constructs new renderer.
             * @param position reference to position where to render.
             * @param size size of the rectangle.
             * @param color color of the rectangle.
             * @param outline color of the outline of rectangle.
             * @param outline_width width of the outline of rectangle.
             * @param type style of the outline of rectangle.
             * @param offset offset from the position.
             */
            RectangleRenderer(const Geometry::FPoint& position, const Geometry::FSize& size,
                              const Graphics::Color& color, const Graphics::Color& outline, double outline_width, const Graphics::LineType& type = Graphics::LineType::SOLID, const Geometry::FPoint& offset = {0, 0});
            /// Null constructor.
            RectangleRenderer(std::nullptr_t);
            /// @private
            RectangleRenderer(const RectangleRenderer& other) = delete;
            /// Move constructor.
            RectangleRenderer(RectangleRenderer&& other) = default;
            /// Destructor.
            ~RectangleRenderer() override = default;

            /// @private
            RectangleRenderer& operator=(const RectangleRenderer& other) = delete;

            void Render(const Object& sender = null, const Events::EventArgs& e = null) const override;

            using Renderer::Offset;
            void Offset(const Geometry::FPoint& offset) override;
            /// Size of the rectangle.
            /*!
             * Access size of the rectangle.
             * @return const reference to the size of the rectangle.
             */
            const Geometry::FSize& Size() const;
            /// Size of the rectangle.
            /*!
             * Set size of the rectangle.
             * @param size new size of the rectangle.
             */
            virtual void Size(const Geometry::FSize& size);

        private:
            Geometry::Rectangle m_Rectangle; //!< Rectangle to render.
        };
    }
}

#endif /* BORIS_RECTANGLE_RENDERER */