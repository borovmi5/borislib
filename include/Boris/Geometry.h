#ifndef BORIS_GEOMETRY
#define BORIS_GEOMETRY

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif /* M_PI */

#include "Geometry/Matrix.h"
#include "Geometry/Vector.h"
#include "Geometry/Vector2.h"
#include "Geometry/Vector3.h"
#include "Geometry/Point.h"
#include "Geometry/FPoint.h"
#include "Geometry/Size.h"
#include "Geometry/FSize.h"
#include "Geometry/Shape.h"
#include "Geometry/Line.h"
#include "Geometry/Rectangle.h"
#include "Geometry/Circle.h"
#include "Geometry/Polygon.h"
#include "Geometry/RoundedRectangle.h"

#include <GL/freeglut.h>

namespace Boris
{
    /// Basic geometry elements and functions.
    /*!
     * Provides implementation of basic geometry elements.
     *
     * Provides matrices, vectors, points, lines, rectangles etc.
     */
    namespace Geometry
    {
        /// Constructs unit matrix
        /*!
         * Constructs matrix \f$\mathbb{E}\in T^{n,n}\f$.
         *
         * \f$(\forall i\in\hat n)(\forall j\in\hat n)\f$
         * \f$\mathbb{E}_{ij}=\left\{\begin{matrix} 0,\ i\neq j \\ 1,\ i=j\end{matrix}\right.\f$
         * @tparam N Size of matrix.
         * @tparam T Type of elements of matrix.
         * @param _1 Unit of T
         * @return Unit matrix.
         */
        template <unsigned int N, typename T = double>
        static Matrix<N, N, T> UnitMatrix(const T& _1 = 1);

        /// Radians.
        /*!
         * Angle defined by this number is in radians.
         * Does nothing. But may be used together with
         * _deg and _grad to prevent confusion.
         * @param angle angle in radians.
         * @return angle in radians.
         */
        constexpr double operator""_rad(long double angle);
        /// Degrees.
        /*!
         * Angle defined by this number is in degrees.
         * Converts to radians.
         * @param angle angle in degrees.
         * @return angle in radians.
         */
        constexpr double operator""_deg(long double angle);
        /// Grads.
        /*!
         * Angle defined by this number is in grads.
         * Converts to radians.
         * @param angle angle in grads.
         * @return angle in radians.
         */
        constexpr double operator""_grad(long double angle);

        template <unsigned int N, typename T>
        static Matrix<N, N, T> UnitMatrix(const T& _1)
        {
            Matrix<N, N, T> result;
            for(unsigned int j = 0; j < N; ++j)
            {
                result.At(j, j) = _1;
            }
            return result;
        }

        void glVertex(const Vector2& vector);
        void glUV(const Vector2& vector);

        constexpr double operator""_rad(long double angle)
        {
            return angle;
        }

        constexpr double operator""_deg(long double angle)
        {
            return angle / 180. * M_PI;
        }

        constexpr double operator""_grad(long double angle)
        {
            return angle / 200. * M_PI;
        }
    }
}

#endif /* BORIS_GEOMETRY */