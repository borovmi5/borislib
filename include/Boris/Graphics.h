#ifndef BORIS_GRAPHICS
#define BORIS_GRAPHICS

#include "Graphics/Color.h"
#include "Graphics/Colors.h"
#include "Graphics/Texture.h"
#include "Graphics/PNG.h"
#include "Graphics/BMP.h"
#include "Graphics/Image.h"

#include <GL/freeglut.h>

namespace Boris
{
    /// Basic graphics.
    /*!
     * Includes classes and functions for styling and graphics.
     */
    namespace Graphics
    {
        /// Render color.
        /*!
         * Sets openGL render color to specified color.
         * @param color color to set for rendering.
         */
        void glColor(const Color& color);

        /// Type of line.
        using LineType = enum class e_LineType : uint16_t
        {
            SOLID = 0xFFFF,     // ________________
            LONGDASH = 0xFCFC,  // _____   _____
            TWODASH = 0xF6F6,   // ____ __ ____ __
            DOTDASH = 0xE4E4,   // ___  _  ___  _
            DASHED = 0xF0F0,    // ____    ____
            DOTTED = 0x8888,    // _   _   _   _
            BLANK = 0x0000      //
        };

        /// Type of point.
        using PointType = enum class e_PointType
        {
            DOT, CROSS, SQUARE, CIRCLE
        };
    }
}

#endif /* BORIS_GRAPHICS */