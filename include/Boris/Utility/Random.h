#ifndef BORIS_RANDOM
#define BORIS_RANDOM

#include <random>

namespace Boris
{
    namespace Utility
    {
        /// Random generator.
        /*!
         * Provides seeded random generator for integer and real numbers.
         *
         * Enabled types:
         * | TYPE |
         * | --- |
         * | char |
         * | int8_t |
         * | uint8_t |
         * | int16_t |
         * | uint16_t |
         * | int32_t |
         * | uint32_t |
         * | int64_t |
         * | uint64_t |
         * | float |
         * | double |
         */
        class Random
        {
        public:
            /// Default constructor.
            /*!
             * Constructs new time-seeded random generator.
             */
            Random();
            /// Constructor.
            /*!
             * Constructs new user-seeded random generator.
             * @param seed seed for generator.
             */
            Random(uint64_t seed);
            /// Copy constructor.
            /*!
             * Constructs new copy of another random generator.
             * @param other another random generator to be used as data source to initialize the random generator.
             */
            Random(const Random& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another random generator using move semantics.
             * @param other another random generator to be used as data source to initialize the random generator.
             */
            Random(Random&& other) = default;
            /// Destructor.
            /*!
             * Destructs the random generator.
             */
            ~Random() = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the random generator with another random generator.
             * @param other another random generator to be used as data source to overwrite the random generator.
             */
            Random& operator=(const Random& other) = default;

            /// Next random value
            /*!
             * Return next random value from whole range of generator for integers, or number
             * in range from 0 to 1 for real numbers.
             *
             * @tparam T Numeric type of generated value.
             * @return random value.
             */
            template <typename T>
            T Next();
            /// Next random value
            /*!
             * Return next random value from the specified range.
             * @tparam T Numeric type of generated value.
             * @param min lower bound of generator.
             * @param max upper bound of generator.
             * @return random value from specified range.
             */
            template <typename T>
            T Next(T min, T max);

        private:
            std::mt19937_64 m_Generator;
            std::uniform_int_distribution<uint64_t> m_Int;
            std::uniform_real_distribution<double> m_Real;
        };

        /// \cond
        template <> char Random::Next<char>();
        template <> char Random::Next<char>(char, char);

        template <> int8_t Random::Next<int8_t>();
        template <> int8_t Random::Next<int8_t>(int8_t, int8_t);
        template <> uint8_t Random::Next<uint8_t>();
        template <> uint8_t Random::Next<uint8_t>(uint8_t, uint8_t);

        template <> int16_t Random::Next<int16_t>();
        template <> int16_t Random::Next<int16_t>(int16_t, int16_t);
        template <> uint16_t Random::Next<uint16_t>();
        template <> uint16_t Random::Next<uint16_t>(uint16_t, uint16_t);

        template <> int32_t Random::Next<int32_t>();
        template <> int32_t Random::Next<int32_t>(int32_t, int32_t);
        template <> uint32_t Random::Next<uint32_t>();
        template <> uint32_t Random::Next<uint32_t>(uint32_t, uint32_t);

        template <> int64_t Random::Next<int64_t>();
        template <> int64_t Random::Next<int64_t>(int64_t, int64_t);
        template <> uint64_t Random::Next<uint64_t>();
        template <> uint64_t Random::Next<uint64_t>(uint64_t, uint64_t);

        template <> float Random::Next<float>();
        template <> float Random::Next<float>(float, float);

        template <> double Random::Next<double>();
        template <> double Random::Next<double>(double, double);
        /// \endcond
    }
}

#endif /* BORIS_RANDOM */