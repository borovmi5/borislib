#ifndef BORIS_MULTIARRAY
#define BORIS_MULTIARRAY

#include <initializer_list>
#include <tuple>

namespace Boris
{
    namespace Utility
    {
        /// Multidimensional array.
        /*!
         * Multidimensional array that acts similarly to std::array, but supports
         * multiple dimensions.
         * @tparam T The type of the elements. T must be CopyAssignable and CopyConstructible.
         * @tparam Size Size of first dimension.
         * @tparam Other Sizes of other dimensions.
         */
        template <typename T, unsigned int Size, unsigned int ... Other>
        class MultiArray
        {
        public:
            /// Default constructor
            /*!
             * Constructs an array with elements with default value.
             */
            MultiArray();
            /// Constructor.
            /*!
             * Constructs an array with elements with value value.
             * @param value the value to initialize elements of the array with.
             */
            explicit MultiArray(const T& value);
            /// Constructor.
            /*!
             * Constructs an array with the contents of the initializer list values.
             * @param values initializer list to initialize the elements of the array with.
             */
            MultiArray(const std::initializer_list<T>& values);
            /// Copy constructor.
            /*!
             * Constructs an array with the copy of contents of other.
             * @param other another array to be used as source to initialize the elements of the array with.
             */
            MultiArray(const MultiArray<T, Size, Other...>& other);
            /// Move constructor.
            /*!
             * Constructs an array with the copy of contents of other using move semantics.
             * @param other another array to be used as source to initialize the elements of the array with.
             */
            MultiArray(MultiArray<T, Size, Other...>&& other) noexcept;
            /// Destructor.
            /*!
             * Destructs the array. The destructors of the elements are called and the used storage is freed.
             * Note, that if the elements are pointers, the pointed-to objects are not destroyed.
             */
            virtual ~MultiArray() = default;

            /// Copy assignment operator.
            /*!
             * Overwrites every element of the array with the corresponding element of another array.
             * @param other another array to be used as source to overwrite the elements of the array.
             * @return *this
             */
            MultiArray<T, Size, Other...>& operator=(const MultiArray<T, Size, Other...>& other);

            /// Fills the array with value.
            /*!
             * Overwrites every element of the array with the value.
             * @param value new value of all elements.
             */
            void Fill(const T& value);

            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified location index, with bounds checking.
             * If index is not within the range of the array, an exception of type std::out_of_range is thrown.
             * @param index index of the element to return.
             * @return reference to the requested element.
             */
            T& At(unsigned int index);
            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified location index, with bounds checking.
             * If index is not within the range of the array, an exception of type std::out_of_range is thrown.
             * @param index index of the element to return.
             * @return const reference to the requested element.
             */
            const T& At(unsigned int index) const;

            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified location index, with bounds checking.
             * If index is not within the range of the array, an exception of type std::out_of_range is thrown.
             * @param index index of the element to return.
             * @return reference to the requested element.
             */
            T& operator[](unsigned int index);
            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified location index, with bounds checking.
             * If index is not within the range of the array, an exception of type std::out_of_range is thrown.
             * @param index index of the element to return.
             * @return const reference to the requested element.
             */
            const T& operator[](unsigned int index) const;
        private:
            T m_Array[Size]; //!< Stored element.
        };

        /// Multidimensional array.
        /*!
         * Multidimensional array that acts similarly to std::array, but supports
         * multiple dimensions.
         * @tparam T The type of the elements. T must be CopyAssignable and CopyConstructible.
         * @tparam Size Size of first dimension.
         * @tparam Next Size of second dimension.
         * @tparam Other Sizes of other dimensions.
         */
        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        class MultiArray<T, Size, Next, Other...>
        {
        public:
            /// Default constructor
            /*!
             * Constructs an array with elements with default value.
             */
            MultiArray();
            /// Constructor.
            /*!
             * Constructs an array with elements with value value.
             * @param value the value to initialize elements of the array with.
             */
            explicit MultiArray(const T& value);
            /// Constructor.
            /*!
             * Constructs an array with the contents of the initializer list values.
             * @param values initializer list to initialize the elements of the array with.
             */
            MultiArray(const std::initializer_list<MultiArray<T, Next, Other...>>& values);
            /// Copy constructor.
            /*!
             * Constructs an array with the copy of contents of other.
             * @param other another array to be used as source to initialize the elements of the array with.
             */
            MultiArray(const MultiArray<T, Size, Next, Other...>& other);
            /// Move constructor.
            /*!
             * Constructs an array with the copy of contents of other using move semantics.
             * @param other another array to be used as source to initialize the elements of the array with.
             */
            MultiArray(MultiArray<T, Size, Next, Other...>&& other) noexcept;
            /// Destructor.
            /*!
             * Destructs the array. The destructors of the elements are called and the used storage is freed.
             * Note, that if the elements are pointers, the pointed-to objects are not destroyed.
             */
            virtual ~MultiArray() = default;

            /// Copy assignment operator.
            /*!
             * Overwrites every element of the array with the corresponding element of another array.
             * @param other another array to be used as source to overwrite the elements of the array.
             * @return *this
             */
            MultiArray<T, Size, Next, Other...>& operator=(const MultiArray<T, Size, Next, Other...>& other);

            /// Fills the array with value.
            /*!
             * Overwrites every element of the array with the value.
             * @param value new value of all elements.
             */
            void Fill(const T& value);

            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified location index, with bounds checking.
             * If index is not within the range of the array, an exception of type std::out_of_range is thrown.
             * @param index index of the element to return.
             * @return reference to the requested element.
             */
            MultiArray<T, Next, Other...>& At(unsigned int index);
            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified location index, with bounds checking.
             * If index is not within the range of the array, an exception of type std::out_of_range is thrown.
             * @param index index of the element to return.
             * @return const reference to the requested element.
             */
            const MultiArray<T, Next, Other...>& At(unsigned int index) const;

            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified location index, with bounds checking.
             * If index is not within the range of the array, an exception of type std::out_of_range is thrown.
             * @param index index of the element to return.
             * @return reference to the requested element.
             */
            MultiArray<T, Next, Other...>& operator[](unsigned int index);
            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified location index, with bounds checking.
             * If index is not within the range of the array, an exception of type std::out_of_range is thrown.
             * @param index index of the element to return.
             * @return const reference to the requested element.
             */
            const MultiArray<T, Next, Other...>& operator[](unsigned int index) const;
        private:
            MultiArray<T, Next, Other...> m_Array[Size]; //!< Stored elements.
        };



        //Implementation
        template <typename T, unsigned int Size, unsigned int ... Other>
        MultiArray<T, Size, Other...>::MultiArray() :
            m_Array()
        {}

        template <typename T, unsigned Size, unsigned int ... Other>
        MultiArray<T, Size, Other...>::MultiArray(const T& value) :
            m_Array()
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i] = value;
        }

        template <typename T, unsigned int Size, unsigned int ... Other>
        MultiArray<T, Size, Other...>::MultiArray(const std::initializer_list<T>& values) :
            m_Array()
        {
            int i = 0;
            for(const auto& value : values)
                m_Array[i++] = value;
        }

        template <typename T, unsigned int Size, unsigned int ... Other>
        MultiArray<T, Size, Other...>::MultiArray(const MultiArray<T, Size, Other...>& other) :
            m_Array()
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i] = other.m_Array[i];
        }

        template <typename T, unsigned int Size, unsigned int ... Other>
        MultiArray<T, Size, Other...>::MultiArray(MultiArray<T, Size, Other...>&& other) noexcept :
            m_Array()
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i] = std::move(other.m_Array[i]);
        }

        template <typename T, unsigned int Size, unsigned int ... Other>
        MultiArray<T, Size, Other...>& MultiArray<T, Size, Other...>::operator=(const MultiArray<T, Size, Other...>& other)
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i] = other.m_Array[i];
            return *this;
        }

        template <typename T, unsigned int Size, unsigned int ... Other>
        void MultiArray<T, Size, Other...>::Fill(const T& value)
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i] = value;
        }

        template <typename T, unsigned int Size, unsigned int ... Other>
        T& MultiArray<T, Size, Other...>::At(unsigned int index)
        {
            if(index >= Size)
                throw std::out_of_range("Index out of range.");
            return m_Array[index];
        }

        template <typename T, unsigned int Size, unsigned int ... Other>
        const T& MultiArray<T, Size, Other...>::At(unsigned int index) const
        {
            if(index >= Size)
                throw std::out_of_range("Index out of range.");
            return m_Array[index];
        }

        template <typename T, unsigned int Size, unsigned int ... Other>
        T& MultiArray<T, Size, Other...>::operator[](unsigned int index)
        {
            if(index >= Size)
                throw std::out_of_range("Index out of range.");
            return m_Array[index];
        }

        template <typename T, unsigned int Size, unsigned int ... Other>
        const T& MultiArray<T, Size, Other...>::operator[](unsigned int index) const
        {
            if(index >= Size)
                throw std::out_of_range("Index out of range.");
            return m_Array[index];
        }



        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        MultiArray<T, Size, Next, Other...>::MultiArray() :
            m_Array()
        {}

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        MultiArray<T, Size, Next, Other...>::MultiArray(const T& value) :
            m_Array()
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i].Fill(value);
        }

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        MultiArray<T, Size, Next, Other...>::MultiArray(const std::initializer_list<MultiArray<T, Next, Other...>>& values) :
            m_Array()
        {
            int i = 0;
            for(const auto& value : values)
                m_Array[i++] = value;
        }

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        MultiArray<T, Size, Next, Other...>::MultiArray(const MultiArray<T, Size, Next, Other...>& other) :
            m_Array()
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i] = other.m_Array[i];
        }

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        MultiArray<T, Size, Next, Other...>::MultiArray(MultiArray<T, Size, Next, Other...>&& other) noexcept :
            m_Array()
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i] = std::move(other.m_Array[i]);
        }

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        MultiArray<T, Size, Next, Other...>& MultiArray<T, Size, Next, Other...>::operator=(const MultiArray<T, Size, Next, Other...>& other)
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i] = other.m_Array[i];
            return *this;
        }

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        void MultiArray<T, Size, Next, Other...>::Fill(const T& value)
        {
            for(int i = 0; i < Size; ++i)
                m_Array[i].Fill(value);
        }

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        MultiArray<T, Next, Other...>& MultiArray<T, Size, Next, Other...>::At(unsigned int index)
        {
            if(index >= Size)
                throw std::out_of_range("Index out of range.");
            return m_Array[index];
        }

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        const MultiArray<T, Next, Other...>& MultiArray<T, Size, Next, Other...>::At(unsigned int index) const
        {
            if(index >= Size)
                throw std::out_of_range("Index out of range.");
            return m_Array[index];
        }

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        MultiArray<T, Next, Other...>& MultiArray<T, Size, Next, Other...>::operator[](unsigned int index)
        {
            if(index >= Size)
                throw std::out_of_range("Index out of range.");
            return m_Array[index];
        }

        template <typename T, unsigned int Size, unsigned int Next, unsigned int ... Other>
        const MultiArray<T, Next, Other...>& MultiArray<T, Size, Next, Other...>::operator[](unsigned int index) const
        {
            if(index >= Size)
                throw std::out_of_range("Index out of range.");
            return m_Array[index];
        }
    }
}

#endif /* BORIS_MULTIARRAY */