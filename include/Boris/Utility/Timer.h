#ifndef BORIS_TIMER
#define BORIS_TIMER

#include <Core/Object.h>
#include <Events.h>
#include <chrono>
#include <future>

namespace Boris
{
    namespace Utility
    {
        /// Timer & interval.
        /*!
         * Is nullable.
         * Basic timer implementation.
         *
         * If set to TIMER, once started triggers event OnTimeElapsed
         * after waiting requested time.
         * If set to INTERVAL, once started triggers event OnTimeElapsed
         * after waiting requested time and then automatically restarts.
         *
         * To stop the timer before time limit use Stop().
         *
         * Stop() has no effect if the timer is not started.
         * Start() has no effect if the timer is started.
         * Restart() can be used to start the timer.
         */
        class Timer : public Object
        {
        public:
            /// Type of the timer: TIMER, INTERVAL.
            using TimerType = enum class e_TimerType : bool
            {
                INTERVAL, TIMER
            };

            /// @private
            Timer() = delete;
            /// Null constructor.
            /*!
             * Constructs new null timer.
             */
            Timer(std::nullptr_t);
            /// Constructor.
            /*!
             * Constructs new timer with specified requested time
             * and type.
             * @param msec number of milliseconds before OnTimeElapsed event triggers.
             * @param type type of the timer: TIMER, INTERVAL.
             */
            Timer(unsigned int msec, TimerType type);
            /// Copy constructor.
            /*!
             * Constructs new copy of another timer.
             * @param other another timer to be used as data source to initialize the timer.
             */
            Timer(const Timer& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another timer using move semantics.
             * @param other another timer to be used as data source to initialize the timer.
             */
            Timer(Timer&& other) = default;
            /// Destructor.
            /*!
             * Destructs the timer,
             */
            ~Timer() = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the timer with another timer.
             * @param other another timer to be used as data source to overwrite the timer.
             * @return *this
             */
            Timer& operator=(const Timer& other) = default;

            /// Requested time.
            /*!
             * Access the requested time of the timer.
             * @return requested time in milliseconds.
             */
            unsigned int Milliseconds() const;
            /// Requested time.
            /*!
             * Access the requested time of the timer.
             * @return requested time reference in milliseconds.
             */
            unsigned int& Milliseconds();
            /// Remaining time.
            /*!
             * Returns remaining time.
             * @return requested time in milliseconds.
             */
            unsigned int Remaining() const;
            /// Type of timer.
            /*!
             * Acces type of the timer.
             * @return type of the timer.
             */
            TimerType Type() const;
            /// Type of timer.
            /*!
             * Acces type of the timer.
             * @return type of the timer reference.
             */
            TimerType& Type();

            /// Starts the timer.
            void Start();
            /// Restarts the timer.
            /*!
             * If not started already, just starts it.
             */
            void Restart();
            /// Stops the timer.
            void Stop();

            /// Requested time elapsed.
            /*!
             * Triggers when requested time elapses.
             */
            Events::BasicEvent OnTimeElapsed;

        private:
            /// Wait loop.
            /*!
             * Cancellable wait loop.
             */
            void Wait();

            TimerType m_Type; //!< Type of the timer.
            std::future<void> m_WaitThread; //!< Thread of the timer.
            unsigned int m_Milliseconds; //!< Requested timer.
            std::chrono::time_point<std::chrono::high_resolution_clock> m_Start; //!< Start time of the last cycle.
            bool m_Stop; //!< Stop request.
            bool m_Started; //!< Holds whether timer is started.
        };
    }
}

#endif /* BORIS_TIMER */