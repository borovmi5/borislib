#ifndef BORIS_RENDERING
#define BORIS_RENDERING

#include "Rendering/Renderer.h"
#include "Rendering/RectangleRenderer.h"
#include "Rendering/CircleRenderer.h"
#include "Rendering/LineRenderer.h"
#include "Rendering/TextRenderer.h"
#include "Rendering/PointRenderer.h"
#include "Rendering/RoundedRectangleRenderer.h"
#include "Rendering/PolygonRenderer.h"

namespace Boris
{
    namespace Rendering
    {
        const Graphics::Color s_DefaultColor = Graphics::Colors::AliceBlue;
        const Graphics::Color s_DefaultOutlineColor = Graphics::Colors::DarkSlateGray;
        const Graphics::LineType s_DefaultOutlineType = Graphics::LineType::SOLID;
        const double s_DefaultOutlineWidth = 1.;

        const unsigned int s_CircleSlices = 32;
    }
}

#endif /* BORIS_RENDERING */