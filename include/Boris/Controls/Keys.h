#ifndef BORIS_KEYS
#define BORIS_KEYS

#include "KeyEventArgs.h"

#include <Events.h>
#include <UI/Application.h>

namespace Boris
{
    namespace Events
    {
        /// Handler for KeyEvent.
        using KeyEventHandler = EventHandler<KeyEventArgs>;
        /// Key-related event.
        using KeyEvent = Event<KeyEventHandler>;
    }
    namespace Controls
    {
        /// Key input handler.
        /*!
         * Handles key input.
         *
         * To use must be initialized by calling BindGLUT().
         * After that throws events every time key is pressed
         * or released.
         */
        class Keys
        {
        public:
            /// @private
            Keys() = delete;

            /// Check key state.
            /*!
             * Checks state of specific key.
             * @param key key to check.
             * @return true if key is down, false otherwise.
             */
            static bool IsDown(Key key);
            /// Check key state.
            /*!
             * Checks state of specific key.
             * @param key key to check.
             * @return true if key is up, false otherwise.
             */
            static bool IsUp(Key key);

            static Events::KeyEvent OnKeyDown; //!< Key down event.
            static Events::KeyEvent OnKeyUp; //!< Key up event.
            static Events::KeyEvent  OnKeyPress; //!< Key press event. (Character, space, backspace or enter).

        private:
            /// @private
            static void KeyDown(unsigned char c, int x, int y); // glutKeyboardFunc() callback
            /// @private
            static void KeyUp(unsigned char c, int x, int y); // glutKeyboardUpFunc() callback
            /// @private
            static void SpecialDown(int c, int x, int y); // glutSpecialFunc() callback
            /// @private
            static void SpecialUp(int c, int x, int y); // glutSpecialUpFunc() callback

            static bool s_Keys[UINT8_MAX]; //!< Key states.

            friend class UI::Application;
        };
    }
}

#endif /* BORIS_KEYS */