#ifndef BORIS_KEY_EVENT_ARGS
#define BORIS_KEY_EVENT_ARGS

#include <Events/EventArgs.h>

namespace Boris
{
    namespace Controls
    {
        using Key = enum class e_Key : unsigned char;
    }
    namespace Events
    {
        /// Keyboard event arguments.
        /*!
         * Is nullable.
         * Event arguments used by KeyEvent informing about which key was pressed.
         * Also tracks modifiers.
         */
        class KeyEventArgs : public EventArgs
        {
        public:
            /// @private
            KeyEventArgs() = delete;
            /// Null constructor.
            /*!
             * Constructs new null KeyEventArgs.
             */
            KeyEventArgs(std::nullptr_t);
            /// Constructor.
            /*!
             * Constructs new KeyEventArgs with specified KeyChar, KeyCode
             * and Modifiers.
             *
             * Modifiers are coded as 3-bit number where:
             *
             * | Key | Code |
             * | --- | --- |
             * | SHIFT | XX1 |
             * | CONTROL | X1X |
             * | ALT | 1XX |
             * @param keyChar character representing pressed key
             * (including modification by modifier keys).
             * @param keyCode pressed Key.
             * @param modifiers pressed modifier keys.
             */
            KeyEventArgs(unsigned char keyChar, Controls::Key keyCode, int modifiers);
            /// Copy constructor.
            /*!
             * Constructs new copy of another key event args.
             * @param other another key event args to be used as data source to initialize the key event args.
             */
            KeyEventArgs(const KeyEventArgs& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another key event args using move semantics.
             * @param other another key event args to be used as data source to initialize the key event args.
             */
            KeyEventArgs(KeyEventArgs&& other) = default;
            /// Destructor.
            /*!
             * Destructs the KeyEventArgs.
             */
            ~KeyEventArgs() override = default;

            /// @private
            KeyEventArgs& operator=(const KeyEventArgs& other) = delete;

            /// Key code.
            /*!
             * Pressed key expressed as Controls::Keys::Key.
             * @return pressed key code.
             */
            Controls::Key Key() const;
            /// Key char.
            /*!
             * Pressed key expressed as resulting ASCII character.
             * @return resulting ASCII character.
             */
            char KeyChar() const;
            /// Control modifier.
            /*!
             * Returns whether control was held during key press.
             * @return whether control was held during key press.               *
             */
            bool Control() const;
            /// Shift modifier.
            /*!
            * Returns whether shift was held during key press.
            * @return whether shift was held during key press.               *
            */
            bool Shift() const;
            /// Alt modifier.
            /*!
             * Returns whether alt was held during key press.
             * @return whether alt was held during key press.               *
             */
            bool Alt() const;

        private:
            const unsigned char m_Char; //!< Pressed key char.
            const Controls::Key m_Key; //!< Pressed key.
            const int m_Modifiers; //!< Pressed modifiers.
        };
    }
}

#endif /* BORIS_KEY_EVENT_ARGS */