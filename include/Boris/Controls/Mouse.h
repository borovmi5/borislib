#ifndef BORIS_MOUSE
#define BORIS_MOUSE

#include "MouseEventArgs.h"

#include <Events.h>
#include <Utility/Timer.h>
#include <UI/Application.h>

namespace Boris
{
    namespace Events
    {
        /// Handler for MouseEvent.
        using MouseEventHandler = EventHandler<MouseEventArgs>;
        /// Mouse-related event.
        using MouseEvent = Event<MouseEventHandler>;
    }

    namespace Controls
    {
        using CursorType = enum class e_Cursor;

        /// Mouse input handler.
        /*!
         * Handles mouse input.
         *
         * To use must be initialized by calling BindGLUT();
         * After that throws events every time button is pressed or released
         * or mouse moves.
         *
         * ## Events
         * | Event | Description |
         * | --- | --- |
         * | OnMouseDown | Called when mouse button is pressed. (Ignores scroll up and scroll down) |
         * | OnMouseUp | Called when mouse button is released. (Ignores scroll up and scroll down) |
         * | OnMouseClick | Called when mouse button is released. (Ignores scroll up and scroll down) |
         * | OnMouseDoubleClick | Called when mouse button is for the second time after less then 500 ms. (Ignores scroll up and scroll down) |
         * | OnMouseEnter | Called when mouse enters window. |
         * | OnMouseMove | Called when mouse moves over window. |
         * | OnMouseHover | Called when stops motion over window. |
         * | OnMouseLeave | Called when mouse leaves window. |
         * | OnScrollUp | Called when scroll up is released. |
         * | OnScrollDown | Called when scroll down is released. |
         * | OnMouseDrag | Called when mouse moves over window while LMB is pressed. |
         * | OnMouseDrop | Called when OnMouseDrag was triggered and LMB is released. |
         *
         * While clicking events are triggered in the following order:
         * -# OnMouseDown
         * -# OnMouseClick
         * -# OnMouseUp
         * -# OnMouseDown
         * -# OnMouseDoubleClick
         * -# OnMouseUp
         */
        class Mouse
        {
        public:
            /// @private
            Mouse() = delete;

            /// Check button state.
            /*!
             * Checks state of specific mouse button.
             * @param button button to check.
             * @return true if button is down, false otherwise.
             */
            static bool IsPressed(MouseButton button);
            /// Check button state.
            /*!
             * Checks state of specific mouse button.
             * @param button button to check.
             * @return true if button is up, false otherwise.
             */
            static bool IsReleased(MouseButton button);

            static void SetCursor(CursorType cursor);

            static Events::MouseEvent OnMouseDown; //!< @sa Mouse
            static Events::MouseEvent OnMouseUp; //!< @sa Mouse
            static Events::MouseEvent OnMouseClick; //!< @sa Mouse
            static Events::MouseEvent OnMouseDoubleClick; //!< @sa Mouse
            static Events::BasicEvent OnMouseEnter; //!< @sa Mouse
            static Events::MouseEvent OnMouseMove; //!< @sa Mouse
            static Events::MouseEvent OnMouseHover; //!< @sa Mouse
            static Events::BasicEvent OnMouseLeave; //!< @sa Mouse
            static Events::MouseEvent OnScrollUp; //!< @sa Mouse
            static Events::MouseEvent OnScrollDown; //!< @sa Mouse
            static Events::MouseEvent OnMouseDrag; //!< @sa Mouse
            static Events::MouseEvent OnMouseDrop; //!< @sa Mouse

        private:
            /// Initializes the timer for mouse hover.
            static void InitTimer();
            /// @private
            static void MouseButton(int button, int state, int x, int y); // glutMouseFunc() callback
            /// @private
            static void MouseMove(int x, int y); // glutMotionFunct() and glutPassiveMotionFunc() callback
            /// @private
            static void MouseEntry(int state); // glutEntryFunc() callback
            /// @private
            static void MouseHover(const Object& sender, const Events::EventArgs& e); // Hover callback.

            static Geometry::Point s_Position; //!< Last position of the mouse.
            static bool s_Buttons[10]; //!< Button states.
            static Controls::MouseButton s_Last; //!< Last button pressed.
            static int s_Time; //!< Last button press GLUT_ELAPSED_TIME.
            static bool s_DoubleClicked; //!< Holds whether last click was click or double click.
            static bool s_Drag; //!< Holds whether is dragging currently.
            static Utility::Timer s_Timer; //!< Timer for hover control.
            static Events::BasicEventHandler  s_TimerHandler; //!< Handler for the timer.

            friend class UI::Application;
        };
    }
}

#endif /* BORIS_MOUSE */