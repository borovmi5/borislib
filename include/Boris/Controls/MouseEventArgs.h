#ifndef BORIS_MOUSE_EVENT_ARGS
#define BORIS_MOUSE_EVENT_ARGS

#include <Events/EventArgs.h>
#include <Geometry/Point.h>

namespace Boris
{
    namespace Controls
    {
        using MouseButton = enum class e_MouseButton : unsigned char;
    }
    namespace Events
    {
        /// Mouse event arguments.
        /*!
         * Is nullable.
         * Event arguments used by MouseEvent informing about mouse
         * position and pressed mouse buttons.
         */
        class MouseEventArgs : public EventArgs
        {
        public:
            /// @private
            MouseEventArgs() = delete;
            /// Null constructor.
            /*!
             * Constructs new null MouseEventArgs.
             */
            MouseEventArgs(std::nullptr_t);
            /// Constructor
            /*!
             * Constructs new MouseEventArgs with specified mouse position
             * and no button information.
             * @param pos position of the mouse cursor.
             */
            explicit MouseEventArgs(const Geometry::Point& pos);
            /// Constructor
            /*!
             * Constructs new MouseEventArgs with specified mouse position
             * and button state.
             * @param pos position of the mouse cursor.
             * @param button button associated with the event.
             * @param state state of the mouse button, true = pressed, false = released.
             */
            MouseEventArgs(const Geometry::Point& pos, const Controls::MouseButton& button, bool state);
            /// Copy constructor.
            /*!
             * Constructs new copy of another mouse event args.
             * @param other another mouse event args to be used as data source to initialize the mouse event args.
             */
            MouseEventArgs(const MouseEventArgs& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another mouse event args using move semantics.
             * @param other another mouse event args to be used as data source to initialize the mouse event args.
             */
            MouseEventArgs(MouseEventArgs&& other) = default;
            /// Destructor.
            /*!
             * Destructs the MouseEventArgs.
             */
            ~MouseEventArgs() override = default;

            /// @private
            MouseEventArgs& operator=(const MouseEventArgs& other) = delete;

            /// Position.
            /*!
             * Point representing position of the mouse.
             * @return position of the mouse.
             */
            const Geometry::Point& Position() const;
            /// Mouse button info.
            /*!
             * Button associated with the event and its state.
             * @return true pair, where first is MouseButton and second is its state, where true = pressed, false = released.
             */
            std::pair<Controls::MouseButton, bool> Button() const;

        private:
            const Geometry::Point m_Position; //!< Position of the mouse.
            const Controls::MouseButton m_Button; //!< Mouse button changed. (If the event is not associated with mouse button use UNKNOWN.)
            const bool m_State; //!< State of the mouse button.
        };
    }
}

#endif /* BORIS_MOUSE_EVENT_ARGS */