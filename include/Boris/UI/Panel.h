#ifndef BORIS_PANEL
#define BORIS_PANEL

#include "Control.h"

namespace Boris
{
    namespace UI
    {
        class Panel : public Control
        {
        public:
            explicit Panel(const Geometry::Size& size, const Indentation& margin = {});
            Panel(nullptr_t);
            Panel(Panel&& other) noexcept;
            ~Panel() override = default;

            void Color(const Graphics::Color& color);
            const Graphics::Color& Color() const;

            void BorderColor(const Graphics::Color& color);
            const Graphics::Color& BorderColor() const;

            void BorderStyle(const Graphics::LineType& style);
            const Graphics::LineType& BorderStyle() const;

            void BorderThickness(double thickness);
            double BorderThickness() const;

        protected:
            void Offset(const Geometry::Point& offset) override;
            Geometry::Point Offset() const override;
            void RealSize(const Geometry::Size& size) override;
            Geometry::Size RealSize() const override;
            void Position(const Geometry::Point& position) override;
            Geometry::Point Position() const override;
            Rendering::Renderer& Renderer() override;

            Rendering::RectangleRenderer m_Renderer;

            friend class Control;
            friend class Application;
        };

        template <> Panel& Control::AddControl<Panel>(Panel&& control);
        template <> Panel& Application::AddControl<Panel>(Panel&& control);
    }
}

#endif /* BORIS_PANEL */