#ifndef BORIS_APPLICATION
#define BORIS_APPLICATION

#include <Geometry.h>
#include <Rendering.h>
#include "Control.h"
#include <string>
#include <list>
#include <thread>

namespace Boris
{
    namespace UI
    {
        /// Application controller.
        class Application
        {
        public:
            /// @private
            Application() = delete;

            /// Starts application and registers callbacks.
            /*!
             * Registers all callbacks used and creates primary window.
             * @param name application name.
             */
            static void Init(std::string name);
            /// Waits until GLUT ended.
            static void Start();
            /// Stops application and closes all windows.
            /*!
             * Tries to clean after itself :)
             * @param exit_code exit code.
             */
            static void End(int exit_code = 0);
            /// Cleans up after application.
            static void CleanUp();

            static const std::string& Title();
            static void Title(const std::string& title);

            static Geometry::Size Size();
            static void Size(const Geometry::Size& size);

            static Geometry::Point Position();
            static void Position(const Geometry::Point& position);

            static bool Visible();
            static void Visible(bool visible);

            static bool Fullscreen();
            static void Fullscreen(bool fullscreen);

            template <typename C>
            static C& AddControl(C&& control);
            static Control RemoveControl(const Control& control);
            static const std::list<Control *>& Controls();
            static Control& GetControl(unsigned int ID);

            static Events::BasicEvent OnDisplay;
            static Events::TimePassedEvent OnIdle;
            static Events::BasicEvent OnResize;

        private:
            static void display(); //!< GLUT display callback.
            static void overlay_display(); //!< GLUT overlay display callback.
            static void idle(); //!< GLUT idle callback.
            static void resize(int width, int height); //!< GLUT reshape callback.
            static void visibility(int state); //!< GLUT visibility callback.

            static std::string s_AppName; //!< Name of the application.
            static int s_AppWin; //!< App window ID.
            static bool s_WindowVisible; //!< App window visible.
            static bool s_Fullscreen; //!< App window in fullscreen mode.

            static int s_LastTime; //!< Hodls last GlutGet(GLUT_ELAPSED_TIME) result.

            static Rendering::PointRenderer s_UIRenderer; //!< Top-level controls base renderer.
            static std::list<Control *> s_Controls; //!< Top-level controls.

            static bool s_Running;
        };
    }
}

#endif /* BORIS_APPLICATION */