#ifndef BORIS_BUTTON
#define BORIS_BUTTON

#include <Controls.h>
#include <Graphics.h>

#include "Label.h"

namespace Boris
{
    namespace UI
    {
        class Button : public Control
        {
        public:
            explicit Button(const Geometry::Size& size, const std::string& text, const Indentation& margin = {});
            Button(nullptr_t);
            Button(Button&& other) noexcept;
            ~Button() override = default;

            void Color(const Graphics::Color& color);
            const Graphics::Color& Color() const;

            void HoverColor(const Graphics::Color& color);
            const Graphics::Color& HoverColor() const;

            void ClickColor(const Graphics::Color& color);
            const Graphics::Color& ClickColor() const;

            void BorderColor(const Graphics::Color& color);
            const Graphics::Color& BorderColor() const;

            void BorderStyle(const Graphics::LineType& style);
            const Graphics::LineType& BorderStyle() const;

            void BorderThickness(double thickness);
            double BorderThickness() const;

            void ContentColor(const Graphics::Color& color);
            const Graphics::Color& ContentColor() const;

            void Bold(bool bold);
            bool Bold() const;

            void Mono(bool mono);
            bool Mono() const;

            void Content(const std::string& text);
            const std::string& Content() const;

            void FontSize(double size);
            double FontSize() const;

            void ContentVAlign(VerticalAlignment alignment);
            VerticalAlignment ContentVAlign() const;

            void ContentHAlign(HorizontalAlignment alignment);
            HorizontalAlignment ContentHAlign() const;

            void CornerRadius(double r);
            double CornerRadius() const;

            void Value(const std::string& val);
            const std::string& Value() const;

            Events::BasicEvent OnClick;

        private:
            void Offset(const Geometry::Point& offset) override;
            Geometry::Point Offset() const override;
            void RealSize(const Geometry::Size& size) override;
            Geometry::Size RealSize() const override;
            void Position(const Geometry::Point& position) override;
            Geometry::Point Position() const override;
            Rendering::Renderer& Renderer() override;

            void MouseMove(const Object& sender, const Events::MouseEventArgs& e);
            void MouseClick(const Object& sender, const Events::MouseEventArgs& e);
            void MouseUp(const Object& sender, const Events::MouseEventArgs& e);

            Rendering::RoundedRectangleRenderer m_Renderer;
            Label * m_Label;

            std::string m_Value;

            Graphics::Color m_NormalColor;
            Graphics::Color m_HoverColor;
            Graphics::Color m_ClickColor;

            Events::MouseEventHandler m_MouseMove;
            Events::MouseEventHandler m_MouseClick;
            Events::MouseEventHandler m_MouseUp;

            friend class Control;
            friend class Application;
        };

        template <> Button& Control::AddControl<Button>(Button&& control);
        template <> Button& Application::AddControl<Button>(Button&& control);
    }
}

#endif /* BORIS_BUTTON */