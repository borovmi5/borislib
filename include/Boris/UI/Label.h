#ifndef BORIS_LABEL
#define BORIS_LABEL

namespace Boris
{
    namespace UI
    {
        class Label : public Control
        {
        public:
            Label(const std::string& content, double size, const Indentation& margin = {});
            Label(nullptr_t);
            Label(Label&& other) noexcept;
            ~Label() override = default;

            void Color(const Graphics::Color& color);
            const Graphics::Color& Color() const;

            void Bold(bool bold);
            bool Bold() const;

            void Mono(bool mono);
            bool Mono() const;

            void Content(const std::string& text);
            const std::string& Content() const;

            void FontSize(double size);
            double FontSize() const;

        private:
            void Offset(const Geometry::Point& offset) override;
            Geometry::Point Offset() const override;
            void RealSize(const Geometry::Size& size) override;
            Geometry::Size RealSize() const override;
            void Position(const Geometry::Point& position) override;
            Geometry::Point Position() const override;
            Rendering::Renderer& Renderer() override;

            Rendering::TextRenderer m_Renderer;

            friend class Control;
            friend class Application;
        };

        template <> Label& Control::AddControl<Label>(Label&& control);
        template <> Label& Application::AddControl<Label>(Label&& control);
    }
}

#endif /* BORIS_LABEL */