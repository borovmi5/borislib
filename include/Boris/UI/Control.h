#ifndef BORIS_CONTROL
#define BORIS_CONTROL

#include <Core.h>
#include <Geometry.h>
#include <Graphics.h>
#include <Events.h>
#include <Rendering.h>

#include <list>
#include <Rendering/Renderer.h>

namespace Boris
{
    namespace UI
    {
        class Application;
        class Panel;
        class Control : public Object
        {
        public:
            using HorizontalAlignment = enum class e_HAlign
            {
                LEFT,
                CENTER,
                RIGHT,
                STRETCH
            };
            using VerticalAlignment = enum class e_VAlign
            {
                TOP,
                MIDDLE,
                BOTTOM,
                STRETCH
            };
            using Indentation = struct st_Indent
            {
                int Top;
                int Right;
                int Bottom;
                int Left;
            };

        protected:
            Control();
            Control(nullptr_t);
        public:
            Control(const Control& other) = delete;
            Control(Control&& other) noexcept;
            ~Control() override;

            Control& operator=(const Control& other) = delete;
            Control& operator=(Control&& other) = delete;

            bool operator==(const Control& other) const;
            bool operator!=(const Control& other) const;

            unsigned int ID() const;

            void Size(const Geometry::Size& size);
            Geometry::Size Size() const;

            void HAlign(HorizontalAlignment alignment);
            HorizontalAlignment HAlign() const;
            void VAlign(VerticalAlignment alignment);
            VerticalAlignment VAlign() const;

            void Margin(const Indentation& margin);
            const Indentation& Margin() const;
            void Padding(const Indentation& padding);
            const Indentation& Padding() const;

            /// Moves control to new parent.
            template <typename C>
            C& AddControl(C&& control);
            /// Removes control from parent.
            Control RemoveControl(const Control& control);
            const std::list<Control *>& Controls() const;
            Control& GetControl(unsigned int ID);
            const Control& GetControl(unsigned int ID) const;

            const Control * Parent() const;

            const Rendering::Renderer& Renderer() const { return *m_DummyRenderer; };

            Events::BasicEvent OnReshape;

            virtual Geometry::Point Offset() const { return {}; };
            virtual Geometry::Size RealSize() const { return {}; };
            virtual Geometry::Point Position() const { return {}; } ;

        protected:
            virtual void Offset(const Geometry::Point& offset) {};
            virtual void RealSize(const Geometry::Size& size) {};
            virtual void Position(const Geometry::Point& position) {};
            virtual Rendering::Renderer& Renderer() { return *m_DummyRenderer; } ;
            /// Updates control position, size and offset.
            void Reshape(const Object& sender, const Events::EventArgs& e);

            unsigned int m_ID;
            static unsigned int s_ID;

            Geometry::Point m_Position;
            Geometry::Size m_Size;

            HorizontalAlignment m_HAlign;
            VerticalAlignment m_VAlign;

            Indentation m_Margin;
            Indentation m_Padding;

            std::list<Control *> m_Controls;

            Control * m_Parent;

            Events::BasicEventHandler m_ReshapeHandler;

            Rendering::Renderer * m_DummyRenderer;

            friend class Application;
        };
    }
}

#endif /* BORIS_CONTROL */