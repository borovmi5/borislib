#ifndef BORIS_UTILITY
#define BORIS_UTILITY

#include "Utility/MultiArray.h"
#include "Utility/Random.h"
#include "Utility/Timer.h"

namespace Boris
{
    /// Various utility classes.
    /*!
     * This namespace contains various classes with wide variety
     * of usages.
     */
    namespace Utility
    {}
}

#endif /* BORIS_UTILITY */