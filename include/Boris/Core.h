#ifndef BORIS_CORE
#define BORIS_CORE

#include "Core/Nullable.h"
#include "Core/Object.h"
#include "Core/PositionedObject.h"

#include <cassert>

/// Collection of libraries for GLUT.
/*!
 * Includes various collection of libraries made for GLUT.
 */
namespace Boris
{
    /// STL-clamp
    /*!
     * Included here, for use with older C++ standards, because
     * it was introduced in C++17.
     * For detailed documentation see
     * <a href="https://en.cppreference.com/w/cpp/algorithm/clamp">documentation</a>.
     */
    template<class T, class Compare>
    constexpr const T& clamp( const T& v, const T& lo, const T& hi, Compare comp )
    {
        return assert( !comp(hi, lo) ),
                comp(v, lo) ? lo : comp(hi, v) ? hi : v;
    }
    /// STL-clamp
    /*!
     * Included here, for use with older C++ standards, because
     * it was introduced in C++17.
     * For detailed documentation see
     * <a href="https://en.cppreference.com/w/cpp/algorithm/clamp">documentation</a>.
     */
    template<class T>
    constexpr const T& clamp( const T& v, const T& lo, const T& hi )
    {
        return clamp( v, lo, hi, std::less<>() );
    }
    /// Converts hex-char to byte
    /*!
     * Converts char to byte.
     * @param c char to convert.
     * @return 0-15 for 0-9a-fA-F
     */
    constexpr const uint8_t hex_to_byte(const char c)
    {
        return assert((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') || (c >= '0' && c <= '9')),
            c > 'a' ? uint8_t(c - 'a' + 10) : c > 'A' ? uint8_t(c - 'A' + 10) : uint8_t(c - '0');
    }
    /// Converts hex-char to byte
    /*!
     * Converts char to byte.
     * @param c two chars to convert.
     * @return 0-255 for [0-9a-fA-F]{2}
     */
    constexpr const uint8_t hex_to_byte(const char c[2])
    {
        return assert((
                    (c[0] >= 'a' && c[0] <= 'f')
                    || (c[0] >= 'A' && c[0] <= 'F')
                    || (c[0] >= '0' && c[0] <= '9'))
                && ((c[1] >= 'a' && c[0] <= 'f')
                    || (c[1] >= 'A' && c[1] <= 'F')
                    || (c[1] >= '0' && c[1] <= '9'))),
            ((c[0] > 'a' ? c[0] - 'a' + 10u : c[0] > 'A' ? uint8_t(c[0] - 'A' + 10) : uint8_t(c[0] - '0')) << 4u)
            + (c[1] > 'a' ? uint8_t(c[1] - 'a' + 10) : c[1] > 'A' ? uint8_t(c[1] - 'A' + 10) : uint8_t(c[1] - '0'));
    }
}

#endif /* BORIS_CORE */