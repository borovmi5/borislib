#ifndef BORIS_EVENT
#define BORIS_EVENT

#include "EventHandler.h"

#include <type_traits>
#include <stdexcept>

namespace Boris
{
    namespace Events
    {
        /// Basic event.
        /*!
         * Stores event handlers and calls them all once called.
         * Multiple registration of the same handler results in multiple calls of that
         * handler when event is called.
         * @tparam Handler Event handler type. Must be derived from EventHandler.
         */
        template <typename Handler>
        class Event
        {
            static_assert(std::is_base_of<EventHandler<typename Handler::ArgsType>, Handler>::value, "Handler must be derived from EventHandler.");
        public:
            /// Associated handler type.
            using HandlerType = Handler;

            /// Default constructor.
            /*!
             * Constructs new empty event.
             */
            Event();
            /// Copy constructor.
            /*!
             * Constructs new copy of another event.
             * @param other another event to be used as data source to initialize the event.
             */
            Event(const Event& other);
            /// Move constructor.
            /*!
             * Constructs new copy of another event using move semantics.
             * @param other another event to be used as data source to initialize the event.
             */
            Event(Event&& other) noexcept;
            /// Destructor.
            /*!
             * Destructs the event handler. Unregisters all event handlers registered to it.
             */
            virtual ~Event();

            /// Copy assignment operator.
            /*!
             * Overwrites the event with another event.
             * @param other another event to be used as data source to overwrite the event.
             * @return *this
             */
            Event& operator=(const Event& other);

            /// EventHandler registration.
            /*!
             * Registers event handler to event.
             * @param handler event handler to register to the event.
             * @return *this
             */
            virtual Event& operator+=(Handler& handler);
            /// EventHandler unregistration.
            /*!
             * Unregisters event handler from event.
             * @param handler event handler to unregister from the event.
             * @return *this
             */
            virtual Event& operator-=(Handler& handler);

            /// Event call.
            /*!
             * Calls all event handlers registered to the event.
             * @param sender object that triggered the event.
             * @param e event arguments associated with the event.
             */
            virtual void operator()(const Object& sender, const typename Handler::ArgsType& e) const;

            /// Event comparison
            /*!
             * Compares two events by ID (not by registered handlers).
             * @param other another event to compare the event with.
             * @return true if both events have the same event ID, false otherwise.
             */
            virtual bool operator==(const Event& other);
            /// Event comparison
            /*!
             * Compares two events by ID (not by registered handlers).
             * @param other another event to compare the event with.
             * @return false if both events have the same event ID, true otherwise.
             */
            virtual bool operator!=(const Event& other);

            /// Event ID
            /*!
             * Returns event id.
             * @return event id.
             */
            virtual uint64_t ID() const;

        protected:
            uint64_t m_EventID; //!< Unique event ID.
            static uint64_t s_EventCtr; //!< Global event counter.
            std::vector<Handler *> m_Handlers; //!< Registered event handlers.
        };

        template <typename Handler>
        Event<Handler>::Event() :
            m_EventID(++s_EventCtr)
        {}

        template <typename Handler>
        Event<Handler>::Event(const Event<Handler>& other) :
            m_EventID(++s_EventCtr)
        {
            for(auto handler : other.m_Handlers)
                *this += *handler;
        }

        template <typename Handler>
        Event<Handler>::Event(Boris::Events::Event<Handler>&& other) noexcept :
            m_EventID(std::move(other.m_EventID)),
            m_Handlers()
        {
            auto subs = other.m_Handlers;
            for(auto& s : subs)
                other -= *s;
            for(auto& s : subs)
                *this += *s;
        }

        template <typename Handler>
        Event<Handler>::~Event()
        {
            for(auto handler : m_Handlers)
                UnregisterHandler(*this, *handler);
        }

        template <typename Handler>
        Event<Handler>& Event<Handler>::operator=(const Event<Handler>& other)
        {
            if(other.m_EventID == m_EventID)
                return *this;
            for(auto handler : m_Handlers)
                UnregisterHandler(*this, *handler);
            m_Handlers.clear();
            for(auto handler : other.m_Handlers)
                *this += *handler;
            return *this;
        }

        template <typename Handler>
        Event<Handler>& Event<Handler>::operator+=(Handler& handler)
        {
            if(handler.Empty())
                throw std::runtime_error("Trying to subscribe event with empty event handler.");
            m_Handlers.push_back(&handler);
            RegisterHandler(*this, handler);
            return *this;
        }

        template <typename Handler>
        Event<Handler>& Event<Handler>::operator-=(Handler& handler)
        {
            if(handler.Empty())
                throw std::runtime_error("Trying to unsubscribe event with empty event handler.");
            auto registered = m_Handlers.begin();
            while(registered != m_Handlers.end())
            {
                if(**registered == handler)
                {
                    m_Handlers.erase(registered);
                    UnregisterHandler(*this, handler);
                    break;
                }
                ++registered;
            }
            return *this;
        }

        template <typename Handler>
        void Event<Handler>::operator()(const Object& sender, const typename Handler::ArgsType& e) const
        {
            for(auto handler : m_Handlers)
                if(!handler->Empty())
                    (*handler)(sender, e);
        }

        template <typename Handler>
        bool Event<Handler>::operator==(const Event& other)
        {
            return m_EventID == other.m_EventID;
        }

        template <typename Handler>
        bool Event<Handler>::operator!=(const Event& other)
        {
            return m_EventID != other.m_EventID;
        }

        template <typename Handler>
        uint64_t Event<Handler>::ID() const
        {
            return m_EventID;
        }

        template <typename Handler>
        uint64_t Event<Handler>::s_EventCtr = 0;
    }
}

#endif /* BORIS_EVENT */