#ifndef BORIS_EVENT_HANDLER
#define BORIS_EVENT_HANDLER

#include "EventArgs.h"

#include <functional>
#include <typeinfo>
#include <type_traits>
#include <vector>

namespace Boris
{
    class Object;
    namespace Events
    {
        template <typename Handler> class Event;

        /// Event handler
        /*!
         * Wraps function to be used to handle event. Function must be in form
         * void function_name(const Object& sender, const EventArgs& e).
         * @tparam Args Event arguments. Must be derived from EventArgs.
         */
        template <typename Args>
        class EventHandler
        {
            static_assert(std::is_base_of<EventArgs, Args>::value, "Args must be derived from EventArgs.");
        public:
            /// Associated function type.
            using function = std::function<void(const Object&, const Args&)>;
            /// Associated event type.
            using EventType = Event<EventHandler<Args>>;
            /// Associated args type.
            using ArgsType = Args;

            /// Default constructor.
            /*!
             * Constructs empty event handler without any bound function.
             */
            EventHandler();
            /// Constructor.
            /*!
             * Constructs new event handler with function f bound to it.
             *
             * This can be used for static functions only! For member functions see
             * #EventHandler(F&& f, I&& instance).
             * @param f function to bind to this event handler.
             */
            explicit EventHandler(const function& f);
            /// Constructor.
            /*!
             * Constructs new event handler with function f of instance instance bound to it.
             *
             * This has to be used for member functions! For static functions see
             * #EventHandler(const function& f).
             * @tparam F Function type of bound function.
             * @tparam I Instance type.
             * @param f function to bind to this event handler.
             * @param instance instance to which the function belongs.
             */
            template <typename F, typename I>
            explicit EventHandler(F&& f, I&& instance);
            /// Copy constructor.
            /*!
             * Constructs new copy of another event handler.
             *
             * This copy is independent from the other! It cannot be used to unsubscribe events of
             * other and is not subscribed to any events!
             * @param other another event handler to be used as data source to initialize the event handler.
             */
            EventHandler(const EventHandler& other);
            /// Move constructor.
            /*!
             * Constructs new copy of another event handler using move semantics.
             * @param other another event handler to be used as data source to initialize the event handler.
             */
            EventHandler(EventHandler&& other) noexcept;
            /// Destructor.
            /*!
             * Destructs the event handler and unregisters it from all events.
             */
            virtual ~EventHandler();

            /// Copy assignment operator.
            /*!
             * Overwrites the event handler with another event handler.
             * @param other another event handler to be used as data source to overwrite the event handler.
             * @return *this
             */
            EventHandler& operator=(const EventHandler& other);

            /// Function assignment.
            /*!
             * Assigns function to empty event handler.
             *
             * This cannot be used to change already bound function! This can be only used on
             * empty event handler!
             * @param f function to bind to this event handler.
             * @return *this
             */
            EventHandler& operator=(const function& f);

            /// Calls event handler.
            /*!
             * Calls function bound to the event handler with arguments sender and e.
             * @param sender object that is source of event trigger.
             * @param e event args bound to the event.
             */
            virtual void operator()(const Object& sender, const Args& e) const;

            /// Compares two event handlers.
            /*!
             * Returns true only if other is exactly the same event handler as this.
             * Compares event handlers by ID, NOT by function!
             * @param other another event handler to compare with this
             * @return true if other is the same instance as this, false otherwise.
             */
            virtual bool operator==(const EventHandler& other) const;
            /// Compares two event handlers.
            /*!
             * Returns false only if other is exactly the same event handler as this.
             * Compares event handlers by ID, NOT by function!
             * @param other another event handler to compare with this
             * @return false if other is the same instance as this, true otherwise.
             */
            virtual bool operator!=(const EventHandler& other) const;
            /// Checks whether event handler is empty.
            /*!
             * Returns true if function was not assigned yet.
             * @return true if function was not assigned yet, false otherwise.
             */
            virtual bool operator==(std::nullptr_t) const;
            /// Checks whether event handler is empty.
            /*!
             * Returns false if function was not assigned yet.
             * @return false if function was not assigned yet, true otherwise.
             */
            virtual bool operator!=(std::nullptr_t) const;

            /// Event Handler ID
            /*!
             * Returns event handler id.
             * @return event handler id.
             */
            virtual uint64_t ID() const;
            /// Checks whether event handler is empty.
            /*!
             * Returns true if function was not assigned yet.
             * @return true if function was not assigned yet, false otherwise.
             */
            virtual bool Empty() const;
            /// Unsubs all events.
            virtual void Clear();

        protected:
            uint64_t m_HandlerID; //!< Unique event handler ID.
            static uint64_t s_HandlerCtr; //!< Global event handler counter.
            function m_F; //!< Bound function.
            std::vector<EventType *> m_Registered; //!< Registered events.

            template <typename E, typename H>
            friend void RegisterHandler(E& event, H& handler);
            template <typename E, typename H>
            friend void UnregisterHandler(const E& event, H& handler);
        };

        template <typename Args>
        EventHandler<Args>::EventHandler() :
            m_HandlerID(0),
            m_F(nullptr)
        {}

        template <typename Args>
        EventHandler<Args>::EventHandler(const EventHandler& other) :
            m_HandlerID(++s_HandlerCtr),
            m_F(other.m_F)
        {}

        template <typename Args>
        EventHandler<Args>::EventHandler(const function& f) :
            m_HandlerID(++s_HandlerCtr),
            m_F(f)
        {}

        template <typename Args>
        template <typename F, typename I>
        EventHandler<Args>::EventHandler(F&& f, I&& instance) :
            m_HandlerID(++s_HandlerCtr),
            m_F(std::bind(f, instance, std::placeholders::_1, std::placeholders::_2))
        {}

        template <typename Args>
        EventHandler<Args>::EventHandler(EventHandler&& other) noexcept :
            m_HandlerID(std::move(other.m_HandlerID)),
            m_F(),
            m_Registered()
        {
            auto reg = other.m_Registered;
            for(auto& r : reg)
                *r -= other;
            m_F = std::move(other.m_F);
            for(auto& r : reg)
                *r += *this;
        }

        template <typename Args>
        EventHandler<Args>::~EventHandler()
        {
            while(!m_Registered.empty())
                *m_Registered.at(0) -= *this;
        }

        template <typename Args>
        EventHandler<Args>& EventHandler<Args>::operator=(const EventHandler<Args>& other)
        {
            if(other.m_HandlerID == m_HandlerID)
                return *this;
            while(!m_Registered.empty())
                *m_Registered.at(0) -= *this;
            for(auto event : other.m_Registered)
                *event += *this;
            return *this;
        }

        template <typename Args>
        EventHandler<Args>& EventHandler<Args>::operator=(const function& f)
        {
            if(m_HandlerID == 0)
                m_HandlerID = ++s_HandlerCtr;
            m_F = f;
            return *this;
        }

        template <typename Args>
        void EventHandler<Args>::operator()(const Object& sender, const Args& e) const
        {
            m_F(sender, e);
        }

        template <typename Args>
        bool EventHandler<Args>::operator==(const EventHandler<Args>& other) const
        {
            return m_HandlerID == other.m_HandlerID;
        }

        template <typename Args>
        bool EventHandler<Args>::operator!=(const EventHandler<Args>& other) const
        {
            return m_HandlerID != other.m_HandlerID;
        }

        template <typename Args>
        bool EventHandler<Args>::operator==(std::nullptr_t) const
        {
            return m_F == nullptr;
        }

        template <typename Args>
        bool EventHandler<Args>::operator!=(std::nullptr_t) const
        {
            return m_F != nullptr;
        }

        template <typename Args>
        uint64_t EventHandler<Args>::ID() const
        {
            return m_HandlerID;
        }

        template <typename Args>
        bool EventHandler<Args>::Empty() const
        {
            return m_F == nullptr;
        }

        template <typename Args>
        void EventHandler<Args>::Clear()
        {
            while(!m_Registered.empty())
                *m_Registered.at(0) -= *this;
        }

        template <typename Args>
        uint64_t EventHandler<Args>::s_HandlerCtr = 0;
    }
}

#endif /* BORIS_EVENT_HANDLER */
