#ifndef BORIS_TIME_PASSED_EVENT_ARGS
#define BORIS_TIME_PASSED_EVENT_ARGS

#include "EventArgs.h"

namespace Boris
{
    namespace Events
    {
        /// Time passed event arguments.
        /*!
         * Is nullable.
         * Event arguments with passed time in milliseconds.
         */
        class TimePassedEventArgs : public EventArgs
        {
        public:
            explicit TimePassedEventArgs(unsigned int millis) : EventArgs(), m_Milliseconds(millis) {};
            TimePassedEventArgs(std::nullptr_t) : EventArgs(null), m_Milliseconds() {};
            TimePassedEventArgs(const TimePassedEventArgs&) = default;
            TimePassedEventArgs(TimePassedEventArgs&&) noexcept = default;
            ~TimePassedEventArgs() override = default;

            TimePassedEventArgs& operator=(const TimePassedEventArgs&) = default;

            unsigned int Millis() const { return m_Milliseconds; };

        private:
            unsigned int m_Milliseconds;
        };
    }
}

#endif /* BORIS_TIME_PASSED_EVENT_ARGS */