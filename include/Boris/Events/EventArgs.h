#ifndef BORIS_EVENT_ARGS
#define BORIS_EVENT_ARGS

#include <Core/Nullable.h>

namespace Boris
{
    namespace Events
    {
        /// Event arguments.
        /*!
         * Is nullable.
         * Basic event arguments. This has not any data. Every event argument class must
         * be derived from this.
         */
        class EventArgs : Nullable
        {
        public:
            /// Default constructor.
            /*!
             * Constructs new EventArgs.
             */
            EventArgs() = default;
            /// Null constructor.
            /*!
             * Constructs new null EventArgs.
             */
            EventArgs(std::nullptr_t) : Nullable(null) {};
            /// Copy constructor.
            /*!
             * Constructs new copy of another event args.
             * @param other another event args to be used as data source to initialize the event args.
             */
            EventArgs(const EventArgs& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another event args using move semantics.
             * @param other another event args to be used as data source to initialize the event args.
             */
            EventArgs(EventArgs&& other) noexcept = default;
            /// Destructor.
            /*!
             * Destructs EventArgs.
             */
            ~EventArgs() override = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the event args with another event args.
             * @param other another event args to be used as data source to overwrite the event args.
             * @return *this
             */
            EventArgs& operator=(const EventArgs& other) = default;
        };
    }
}

#endif /* BORIS_EVENT_ARGS */