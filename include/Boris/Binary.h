#ifndef BORIS_BINARY
#define BORIS_BINARY

#include "Binary/Bit.h"
#include "Binary/Byte.h"
#include "Binary/Word.h"
#include "Binary/DoubleWord.h"
#include "Binary/QuadWord.h"
#include "Binary/BinaryStream.h"

namespace Boris
{
    /// Includes classes or binary processing.
    namespace Binary
    {}
}

#endif /* BORIS_BINARY */