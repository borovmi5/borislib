#ifndef BORIS_VECTOR
#define BORIS_VECTOR

#include "Matrix.h"
#include <Utility/MultiArray.h>

#include <iostream>

namespace Boris
{
    namespace Geometry
    {
        /// N-Vector
        /*!
         * Vector of N elements that supports standard vector operations.
         * @tparam N Number of elements of vector.
         * @tparam T Type of elements. T must be CopyAssignable, CopyConstructible and
         * must have operators +, +=, -, -=, *, *=, /, /= defined.
         */
        template <unsigned int N, typename T = double>
        class Vector : public Matrix<N, 1, T>
        {
        public:
            /// Default construtor.
            /*!
             * Constructs an n-vector with every element being 0.
             */
            Vector() = default;
            /// Constructor.
            /*!
             * Constructs an n-vector with elements assigned to value of
             * corresponding elements of array.
             * @param array array to be used as source to initialize the elements of vector with.
             */
            explicit Vector(const T array [N]);
            /// Constructor.
            /*!
             * Constructs an n-vector with elements assigned to value of
             * corresponding elements of array.
             * @param array array to be used as source to initialize the elements of vector with.
             */
            explicit Vector(const Utility::MultiArray<T, N>& array);
            /// Constructor.
            /*!
             * Constructs an n-vector with elements assigned to value of
             * corresponding elements of n,1-matrix.
             * @param matrix matrix to be used as source to initialize the elements of vector with.
             */
            Vector(const Matrix<N, 1, T>& matrix);
            /// Constructor.
            /*!
             * Constructs an n-vector with the contents of the initializer list values.
             * @param values initializer list to initialize the elements of the vector with.
             */
            Vector(const std::initializer_list<T>& values);
            /// Copy constructor.
            /*!
             * Constructs an n-vector with the copy of contents of other.
             * @param other another vector to be used as source to initialize the elements of vector.
             */
            Vector(const Vector& other) = default;
            /// Move constructor.
            /*!
             * Constructs an n-vector with the copy of contents of other using move semantics.
             * @param other another vector to be used as source to initialize the elements of vector.
             */
            Vector(Vector&& other) noexcept = default;
            /// Destructor.
            /*!
             * Destructs the vector.
             */
            ~Vector() override = default;

            /// Copy assignment operator.
            /*!
             * Overwrites every element of the vector with the corresponding element of another vector.
             * @param other another vector to be used as source to overwrite the elements of the vector.
             * @return *this
             */
            Vector& operator=(const Vector& other) = default;

            /// Vector sum
            /*!
             * Constructs new vector that is sum of the vector and other vector.
             *
             * Vector sum:
             *
             * \f$\vec{u},\vec{v},\vec{w}\in T^{n}\f$
             *
             * \f$\vec{w}=\vec{u}+\vec{v}\rightarrow\f$
             * \f$(\forall i\in\hat n)\f$
             * \f$\vec{w}_i=\vec{u}_i+\vec{v}_i\f$
             * @param other another vector to add to the vector.
             * @return new vector that is sum of the vector and other vector.
             */
            Vector operator+(const Vector& other) const;

            /// Vector sum
            /*!
             * Adds another vector to the vector.
             *
             * Vector sum:
             *
             * \f$\vec{u},\vec{v},\vec{w}\in T^{n}\f$
             *
             * \f$\vec{w}=\vec{u}+\vec{v}\rightarrow\f$
             * \f$(\forall i\in\hat n)\f$
             * \f$\vec{w}_i=\vec{u}_i+\vec{v}_i\f$
             * @param other another vector to add to the vector.
             * @return *this
             */
            Vector& operator+=(const Vector& other);
            /// Vector substraction
            /*!
             * Construcst new vector that is difference of the vector and other vector.
             *
             * Vector substraction:
             *
             * \f$\vec{u},\vec{v},\vec{w}\in T^{n}\f$
             *
             * \f$\vec{w}=\vec{u}-\vec{v}\rightarrow\f$
             * \f$(\forall i\in\hat n)\f$
             * \f$\vec{w}_i=\vec{u}_i-\vec{v}_i\f$
             * @param other another vector to substract from the vector.
             * @return new vector that is difference of the vector and other vector.
             */
            Vector operator-(const Vector& other) const;
            /// Vector substraction
            /*!
             * Substracts another vector from the vector.
             *
             * Vector substraction:
             *
             * \f$\vec{u},\vec{v},\vec{w}\in T^{n}\f$
             *
             * \f$\vec{w}=\vec{u}-\vec{v}\rightarrow\f$
             * \f$(\forall i\in\hat n)\f$
             * \f$\vec{w}_i=\vec{u}_i-\vec{v}_i\f$
             * @param other another vector to substract from the vector.
             * @return new vector that is difference of the vector and other vector.
             */
            Vector& operator-=(const Vector& other);
            /// Scalar multiplication
            /*!
             * Constructs new vector whose elements are products of the vector elements and scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u},\vec{v}\in T^{n},\ c\in T\f$
             *
             * \f$\vec{v}=\vec{u}\cdot c\rightarrow\f$
             * \f$(\forall i\in\hat n)\f$
             * \f$\vec{v}_i=\vec{u}_i\cdot c\f$
             * @param c scalar multiplying vector.
             * @return new vector that is product of the vector and scalar.
             */
            Vector operator*(const T& c) const;
            /// Scalar multiplication
            /*!
             * Constructs new vector whose elements are products of the vector elements and scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u},\vec{v}\in T^{n},\ c\in T\f$
             *
             * \f$\vec{v}=c\cdot\vec{u}\rightarrow\f$
             * \f$(\forall i\in\hat n)\f$
             * \f$\vec{v}_i=c\cdot\vec{u}_i\f$
             * @param c scalar multiplying vector.
             * @param vector vector multiplied by scalar.
             * @tparam _N Number of elements of vector.
             * @tparam _T Type of elements of vector.
             * @return new vector that is product of the vector and scalar.
             */
            template <unsigned int _N, typename _T>
            friend Vector<_N, _T> operator*(const _T& c, const Vector<_N, _T>& vector);
            /// Scalar multiplication
            /*!
             * Multiplies vector with scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u},\vec{v}\in T^{n},\ c\in T\f$
             *
             * \f$\vec{v}=\vec{u}\cdot c\rightarrow\f$
             * \f$(\forall i\in\hat n)\f$
             * \f$\vec{v}_i=\vec{u}_i\cdot c\f$
             * @param c scalar multiplying vector.
             * @return *this
             */
            Vector& operator*=(const T& c);

            /// Vector scalar multiplication
            /*!
             * Multiplies vector with other vector.
             *
             * Vector scalar multiplication:
             *
             * \f$\vec{u},\vec{v}\in T^{n},\ s\in T^{m,o}\f$
             *
             * \f$ s=\sum_{i\in\hat n}u_i\cdot v_i\f$
             * @param other another vector to multiply the vector with.
             * @return result of scalar multiplication.
             */
            T operator*(const Vector& other) const;
            /// Scalar division.
            /*!
             * Constructs new vector whose elements are quotients of the vector elements and scalar.
             *
             * Scalar division:
             *
             * \f$\vec{u},\vec{v}\in T^{n},\ c\in T\f$
             *
             * \f$\vec{v}=\vec{u}\div c\rightarrow\f$
             * \f$(\forall i\in\hat n)\f$
             * \f$\vec{v}_i=\vec{u}_i\div c\f$
             * @param c scalar dividing vector.
             * @return new vector that is product of the vector and scalar.
             */
            Vector operator/(const T& c) const;
            /// Scalar division.
            /*!
             * Divides vector with scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u},\vec{v}\in T^{n},\ c\in T\f$
             *
             * \f$\vec{v}=\vec{u}\div c\rightarrow\f$
             * \f$(\forall i\in\hat n)\f$
             * \f$\vec{v}_i=\vec{u}_i\div c\f$
             * @param c scalar dividing vector.
             * @return *this
             */
            Vector& operator/=(const T& c);

            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified index.
             * If index is not within the range of the matrix, an exception of type std::out_of_range is thrown.
             * @param i index of the element to return.
             * @return reference to the requested element.
             */
            T& At(unsigned int i);
            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified index.
             * If index is not within the range of the matrix, an exception of type std::out_of_range is thrown.
             * @param i index of the element to return.
             * @return const reference to the requested element.
             */
            const T& At(unsigned int i) const;
        private:
            using Matrix<N, 1, T>::Det;
        };

        template <unsigned int N, typename T>
        Vector<N, T>::Vector(const T array [N]) :
                Matrix<N, 1, T>()
        {
            for(unsigned int i = 0; i < N; ++i)
                this->m_Matrix[i][0] = array[i];
        }

        template <unsigned int N, typename T>
        Vector<N, T>::Vector(const Utility::MultiArray<T, N>& array) :
            Matrix<N, 1, T>()
        {
            for(unsigned int i = 0; i < N; ++i)
                this->m_Matrix[i][0] = array[i];
        }

        template <unsigned int N, typename T>
        Vector<N, T>::Vector(const Matrix<N, 1, T>& matrix) :
                Matrix<N, 1, T>(matrix)
        {}

        template <unsigned int N, typename T>
        Vector<N, T>::Vector(const std::initializer_list<T>& values) :
            Matrix<N, 1, T>()
        {
            unsigned int i = 0;
            for(const auto& value : values)
                this->m_Matrix[i++][0] = value;
        }

        template <unsigned int N, typename T>
        Vector<N, T> Vector<N, T>::operator+ (const Vector<N, T> & other) const
        {
            Vector res = *this;
            res += other;
            return res;
        }

        template <unsigned int N, typename T>
        Vector<N, T>& Vector<N, T>::operator+= (const Vector<N, T> & other)
        {
            for(unsigned int i = 0; i < N; i++)
                this->m_Matrix[i][0] += other.m_Matrix[i][0];
            return *this;
        }

        template <unsigned int N, typename T>
        Vector<N, T> Vector<N, T>::operator- (const Vector<N, T> & other) const
        {
            Vector res = *this;
            res -= other;
            return res;
        }

        template <unsigned int N, typename T>
        Vector<N, T> & Vector<N, T>::operator-= (const Vector<N, T> & other)
        {
            for(unsigned int i = 0; i < N; i++)
                this->m_Matrix[i][0] -= other.m_Matrix[i][0];
            return *this;
        }

        template <unsigned int N, typename T>
        Vector<N, T> Vector<N, T>::operator* (const T& c) const
        {
            Vector res = *this;
            res *= c;
            return res;
        }

        /// Scalar multiplication
        /*!
         * Constructs new vector whose elements are products of the vector elements and scalar.
         *
         * Scalar multiplication:
         *
         * \f$\vec{u},\vec{v}\in T^{n},\ c\in T\f$
         *
         * \f$\vec{v}=c\cdot\vec{u}\rightarrow\f$
         * \f$(\forall i\in\hat n)\f$
         * \f$\vec{v}_i=c\cdot\vec{u}_i\f$
         * @param c scalar multiplying vector.
         * @param vector vector multiplied by scalar.
         * @tparam N Number of elements of vector.
         * @tparam T Type of elements of vector.
         * @return new vector that is product of the vector and scalar.
         */
        template <unsigned int N, typename T>
        Vector<N, T> operator* (const T& c, const Vector<N, T>& vector)
        {
            Vector<N, T> res = vector;
            res *= c;
            return res;
        }

        template <unsigned int N, typename T>
        Vector<N, T> & Vector<N, T>::operator*= (const T& c)
        {
            for(unsigned int i = 0; i < N; i++)
                    this->m_Matrix[i][0] *= c;
            return *this;
        }

        template <unsigned int N, typename T>
        T Vector<N, T>::operator* (const Vector<N, T> & other) const
        {
            T res;
            for(unsigned int i = 0; i < N; i++)
                res += At(i) * other.At(i);
            return res;
        }

        template <unsigned int N, typename T>
        Vector<N, T> Vector<N, T>::operator/ (const T& c) const
        {
            Vector<N, T> res = *this;
            res /= c;
            return res;
        }

        template <unsigned int N, typename T>
        Vector<N, T>& Vector<N, T>::operator/= (const T& c)
        {
            for(unsigned int i = 0; i < N; i++)
                this->m_Matrix[i][0] /= c;
            return *this;
        }

        template <unsigned int N, typename T>
        T& Vector<N, T>::At(unsigned int i)
        {
            return this->m_Matrix[i][0];
        }

        template <unsigned int N, typename T>
        const T& Vector<N, T>::At(unsigned int i) const
        {
            return this->m_Matrix[i][0];
        }
    }
}

#endif /* BORIS_MATRIX_CPP */