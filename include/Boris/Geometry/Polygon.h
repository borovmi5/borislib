#ifndef BORIS_POLYGON
#define BORIS_POLYGON

#include "Shape.h"
#include "FPoint.h"

#include <vector>
#include <initializer_list>

namespace Boris
{
    namespace Geometry
    {
        /// Polygon shape.
        /*!
         * Polygon defined by its points.
         */
        class Polygon : public Shape
        {
        public:
            /// @private
            Polygon() = delete;
            /// Null constructor.
            /*!
             * Constructs new null polygon.
             */
            Polygon(std::nullptr_t);
            /// Constructor.
            /*!
             * Constructs new polygon from given list of points.
             * @param points points of the polygon.
             */
            Polygon(const std::vector<FPoint>& points);
            /// Constructor.
            /*!
             * Constructs new polygon from given list of points.
             * @param points points of the polygon.
             */
            Polygon(std::initializer_list<FPoint> points);
            /// Copy constructor.
            /*!
             * Constructs new copy of another polygon.
             * @param other another polygon to be used as data source to initialize the polygon.
             */
            Polygon(const Polygon& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another polygon using move semantics.
             * @param other another polygon to be used as data source to initialize the polygon.
             */
            Polygon(Polygon&& other) = default;
            /// Destructor.
            /*!
             * Destructs the polygon.
             */
            ~Polygon() override = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the polygon with copy of another polygon.
             * @param other another polygon to be used as data source to overwrite the polygon.
             * @return *this
             */
            Polygon& operator=(const Polygon& other) = default;

            /// Polygon compare.
            /*!
             * Compares two polygons.
             * @param other another polygon to compare.
             * @return true if both polygons have the same points, false otherwise.
             */
            bool operator==(const Polygon& other) const;
            /// Polygon compare.
            /*!
             * Compares two polygons.
             * @param other another polygon to compare.
             * @return false if both polygons have the same points, true otherwise.
             */
            bool operator!=(const Polygon& other) const;

            /// Get shape-defining points.
            /*!
             * Returns points, that are defining the shape.
             * @return list of points defining shape.
             */
            std::vector<FPoint> Points() const override;
            /// Access shape-defining points.
            /*!
             * Returns points, that are defining the shape.
             * @return list of points defining shape.
             */
            std::vector<FPoint>& Points();

            /// Access point at index
            const FPoint& operator[](unsigned int index) const;
            /// Access point at index
            FPoint& operator[](unsigned int index);

        private:
            /// Points-intersect test.
            /*!
             * Tests shape-point intersection on each point.
             * @param points list of points to test.
             * @return true if at least one of the points is within the shape, false otherwise.
             */
            bool IntersectionTest(const std::vector<FPoint>& points) const override;

            std::vector<FPoint> m_Points; //!< Points of the polygon.
        };
    }
}

#endif /* BORIS_POLYGON */