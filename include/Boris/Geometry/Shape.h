#ifndef BORIS_SHAPE
#define BORIS_SHAPE

#include <Core.h>

#include "FPoint.h"
#include "Vector2.h"

#include <vector>

namespace Boris
{
    namespace Geometry
    {
        /// Shape base class.
        /*!
         * Base class for geometric shapes.
         */
        class Shape : public Object
        {
        public:
            /// Default constructor.
            /*!
             * Constructs new shape.
             */
            Shape() = default;
            /// Null constructor.
            /*!
             * Constructs new null shape.
             */
            Shape(std::nullptr_t) : Object(null) {}
            /// Copy constructor.
            /*!
             * Constructs new copy of another shape.
             * @param other another shape to be used as data source to initialize the shape.
             */
            Shape(const Shape& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another shape using move semantics.
             * @param other another shape to be used as data source to initialize the shape.
             */
            Shape(Shape&& other) = default;
            /// Destructor.
            /*!
             * Destructs the shape.
             */
            ~Shape() override = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the shape with copy of another shape.
             * @param other another shape to be used as data source to overwrite the shape.
             * @return *this
             */
            Shape& operator=(const Shape& other) = default;

            /// Shape compare.
            /*!
             * Dummy. Does nothing 🙃
             * @param other another shape
             * @return false
             */
            bool operator==(const Shape& other) { return false; };
            /// Shape compare.
            /*!
             * Dummy. Does nothing 🙃
             * @param other another shape
             * @return false
             */
            bool operator!=(const Shape& other) { return true; };

            /// Get shape-defining points.
            /*!
             * Returns point, which are defining the shape.
             * @return list of points defining shape.
             */
            virtual std::vector<FPoint> Points() const = 0;

            /// Shape-intersect test.
            /*!
             * Tests shape-shape intersection and returns true if they intersect each other.
             * @param other another shape to test with this shape.
             * @return true if shapes intersect, false otherwise.
             */
            bool Intersects(const Shape& other) const
            {
                return IntersectionTest(other.Points()) || other.IntersectionTest(Points());
            }

            /// Point-intersect test.
            /*!
             * Tests shape-point intersection.
             * @param point point to test.
             * @return true if the point is within the shape, false otherwise.
             */
            bool Intersects(const FPoint& point) const
            {
                return IntersectionTest({point});
            }

        private:
            /// Points-intersect test.
            /*!
             * Tests shape-point intersection on each point.
             * @param points list of points to test.
             * @return true if at least one of the points is within the shape, false otherwise.
             */
            virtual bool IntersectionTest(const std::vector<FPoint>& points) const = 0;
        };
    }
}

#endif /* BORIS_SHAPE */