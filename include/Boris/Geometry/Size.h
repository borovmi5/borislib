#ifndef BORIS_SIZE
#define BORIS_SIZE

#include "Vector2.h"

namespace Boris
{
    namespace Geometry
    {
        class FSize;

        /// 2D Integer size.
        /*!
         * Simple size with width and height.
         */
        class Size
        {
        public:
            /// Default constructor.
            /*!
             * Constructs new size with dimensions [0; 0].
             */
            Size();
            /// Constructor.
            /*!
             * Constructs new size with specified dimensions.
             * @param width width of size.
             * @param height height of size.
             */
            Size(unsigned int width, unsigned int height);
            /// Copy constructor.
            /*!
             * Constructs new copy of another size.
             * @param other another size to be used as data source to initialize the size.
             */
            Size(const Size& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another size using move semantics.
             * @param other another size to be used as data source to initialize the size.
             */
            Size(Size&& other) = default;
            /// Destructor.
            /*!
             * Destructs the size.
             */
            ~Size() = default;

            /// Copy assignment operator
            /*!
             * Overwrites the point with another size.
             * @param other another size to be used as data source to overwrite the size.
             * @return *this
             */
            Size& operator=(const Size& other) = default;

            /// Size comparison.
            /*!
             * Compares two sizes and returns true when both have the same dimensions.
             * @param other another size to compare the size with.
             * @return true if both sizes have the same dimensions, false otherwise.
             */
            bool operator==(const Size& other) const;
            /// Size comparison.
            /*!
             * Compares two sizes and returns true when both have different dimensions.
             * @param other another size to compare the size with.
             * @return false if both sizes have the same dimensions, true otherwise.
             */
            bool operator!=(const Size& other) const;

            /// Area of the dimensions
            /*!
             * Returns area covered by dimensions.
             * @return covered area.
             */
            unsigned int Area() const;

            /// Resize.
            /*!
             * Changes size dimensions to specified values.
             * @param width new width of the size.
             * @param height new height of the size.
             * @return
             */
            Size& Resize(unsigned int width, unsigned int height);

            /// Width.
            /*!
             * Access to the width of the size.
             * @return const reference to the width.
             */
            unsigned int Width() const;
            /// Set width.
            /*!
             * Sets the width of the size to the value width.
             * @param width new width of the size.
             */
            void Width(unsigned int width);

            /// Height.
            /*!
             * Access to the height of the size.
             * @return const reference to the height.
             */
            unsigned int Height() const;
            /// Set height.
            /*!
             * Sets the height of the size to the value height.
             * @param height new height of the size.
             */
            void Height(unsigned int height);

            /// Conversion to Vector2.
            /*!
             * Converts the size to new Vector2.
             * @return Vector2 with the same dimensions as the size.
             */
            explicit operator Vector2() const;

            explicit operator FSize() const;

        private:
            unsigned int m_Width; //!< Width of the point.
            unsigned int m_Height; //!< Height of the point.
        };
    }
}

#endif /* BORIS_SIZE */