#ifndef BORIS_CIRCLE
#define BORIS_CIRCLE

#include "Shape.h"
#include "FPoint.h"

#include <vector>

namespace Boris
{
    namespace Geometry
    {
        /// Circle shape.
        /*!
         * Circle defined by its center and radius.
         */
        class Circle : public Shape
        {
        public:
            /// @private
            Circle() = delete;
            /// Null constructor.
            /*!
             * Constructs new null circle.
             */
            Circle(std::nullptr_t);
            /// Constructor.
            /*!
             * Constructs new circle with given center and radius.
             * @param center center of the circle.
             * @param radius radius of the circle.
             */
            Circle(FPoint center, double radius);
            /// Copy constructor.
            /*!
             * Constructs new copy of another circle.
             * @param other another circle to be used as data source to initialize the circle.
             */
            Circle(const Circle& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another circle using move semantics.
             * @param other another circle to be used as data source to initialize the circle.
             */
            Circle(Circle&& other) = default;
            /// Destructor.
            /*!
             * Destructs the circle.
             */
            ~Circle() override = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the circle with copy of another circle.
             * @param other another circle to be used as data source to overwrite the circle.
             * @return *this
             */
            Circle& operator=(const Circle& other) = default;

            /// Circle compare.
            /*!
             * Compares two circle.
             * @param other another circle to compare.
             * @return true if both circles have the same radius and center, false otherwise.
             */
            bool operator==(const Circle& other) const;
            /// Circle compare.
            /*!
             * Compares two circle.
             * @param other another circle to compare.
             * @return false if both circles have the same radius and center, true otherwise.
             */
            bool operator!=(const Circle& other) const;

            /// Circle center.
            /*!
             * Access circle center.
             * @return reference to circle center.
             */
            FPoint& Center();
            /// Circle center.
            /*!
             * Access circle center.
             * @return const reference to circle center.
             */
            const FPoint& Center() const;

            /// Circle radius.
            /*!
             * Access circle radius.
             * @return reference to circle radius.
             */
            double& Radius();
            /// Circle radius.
            /*!
             * Access circle radius.
             * @return const reference to circle radius.
             */
            const double& Radius() const;

            /// Circle area.
            /*!
             * Computes area covered by the circle.
             * @return circle area.
             */
            double Area();

            /// Get shape-defining points.
            /*!
             * Returns points, that are defining the shape.
             * For circle shape-defining points are:
             *
             * \f$\left\{C,[C_x+r,C_y],
             * [C_x-r,C_y],[C_x,C_y+r],
             * [C_x,C_y-r]\right\}\f$
             * @return list of points defining shape.
             */
            std::vector<FPoint> Points() const override;

        private:
            /// Points-intersect test.
            /*!
             * Tests shape-point intersection on each point.
             * @param points list of points to test.
             * @return true if at least one of the points is within the shape, false otherwise.
             */
            bool IntersectionTest(const std::vector<FPoint>& points) const override;

            FPoint m_Center; //!< Center of the circle.
            double m_Radius; //!< Radius of the circle.
        };
    }
}

#endif /* BORIS_CIRCLE */