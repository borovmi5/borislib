#ifndef BORIS_MATRIX
#define BORIS_MATRIX

#include <Utility/MultiArray.h>

#include <iostream>

namespace Boris
{
    namespace Geometry
    {
        /// M,N-Matrix
        /*!
         * Matrix of M rows and N columns that supports standard matrix operations.
         * @tparam M Number of rows of matrix.
         * @tparam N Number of columns of matrix.
         * @tparam T Type of elements. T must be CopyAssignable, CopyConstructible and
         * must have operators +, +=, -, -=, *, *=, /, /= defined.
         */
        template <unsigned int M, unsigned int N, typename T = double>
        class Matrix
        {
        public:
            /// Default constructor.
            /*!
             * Constructs an m,n-matrix with every element being 0.
             */
            Matrix() = default;
            /// Constructor.
            /*!
             * Constructs an m,n-matrix with elements assigned to value of
             * corresponding elements of array.
             * @param array array to be used as source to initialize the elements of matrix with.
             */
            explicit Matrix(const Utility::MultiArray<T, M, N>& array);
            /// Constructor.
            /*!
             * Constructs an m,n-matrix with the contents of the initializer list values.
             * @param values initializer list to initialize the elements of the matrix with.
             */
            Matrix(const std::initializer_list<Utility::MultiArray<T, N>>& values);
            /// Copy constructor.
            /*!
             * Constructs an m,n-matrix with the copy of contents of other.
             * @param other another matrix to be used as source to initialize the elements of matrix.
             */
            Matrix(const Matrix& other) = default;
            /// Move constructor.
            /*!
             * Constructs an m,n-matrix with the copy of contents of other using move semantics.
             * @param other another matrix to be used as source to initialize the elements of matrix.
             */
            Matrix(Matrix&& other) noexcept = default;
            /// Destructor.
            /*!
             * Destructs the matrix.
             */
            virtual ~Matrix() = default;

            /// Copy assignment operator.
            /*!
             * Overwrites every element of the matrix with the corresponding element of another matrix.
             * @param other another matrix to be used as source to overwrite the elements of the matrix.
             * @return *this
             */
            Matrix& operator=(const Matrix& other) = default;

            /// Matrix sum
            /*!
             * Constructs new matrix which is sum of the matrix and other matrix.
             *
             * Matrix sum:
             *
             * \f$\mathbb{A},\mathbb{B},\mathbb{D}\in T^{m,n}\f$
             *
             * \f$\mathbb{D}=\mathbb{A}+\mathbb{B}\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{D}_{ij}=\mathbb{A}_{ij}+\mathbb{B}_{ij}\f$
             * @param other another matrix to add to the matrix.
             * @return new matrix that is sum of the matrix and other matrix.
             */
            Matrix operator+(const Matrix& other) const;
            /// Matrix sum
            /*!
             * Adds another matrix to the matrix.
             *
             * Matrix sum:
             *
             * \f$\mathbb{A},\mathbb{B},\mathbb{D}\in T^{m,n}\f$
             *
             * \f$\mathbb{D}=\mathbb{A}+\mathbb{B}\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{D}_{ij}=\mathbb{A}_{ij}+\mathbb{B}_{ij}\f$
             * @param other another matrix to add to the matrix.
             * @return *this
             */
            Matrix& operator+=(const Matrix& other);
            /// Matrix substraction
            /*!
             * Construcst new matrix which is difference of the matrix and other matrix.
             *
             * Matrix substraction:
             *
             * \f$\mathbb{A},\mathbb{B},\mathbb{D}\in T^{m,n}\f$
             *
             * \f$\mathbb{D}=\mathbb{A}-\mathbb{B}\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{D}_{ij}=\mathbb{A}_{ij}-\mathbb{B}_{ij}\f$
             * @param other another matrix to substract from the matrix.
             * @return new matrix that is difference of the matrix and other matrix.
             */
            Matrix operator-(const Matrix& other) const;
            /// Matrix substraction
            /*!
             * Substracts another matrix to the matrix.
             *
             * Matrix substraction:
             *
             * \f$\mathbb{A},\mathbb{B},\mathbb{D}\in T^{m,n}\f$
             *
             * \f$\mathbb{D}=\mathbb{A}-\mathbb{B}\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{D}_{ij}=\mathbb{A}_{ij}-\mathbb{B}_{ij}\f$
             * @param other another matrix to substract from the matrix.
             * @return *this
             */
            Matrix& operator-=(const Matrix& other);
            /// Scalar multiplication
            /*!
             * Constructs new matrix whose elements are products of the matrix elements and scalar.
             *
             * Scalar multiplication:
             *
             * \f$\mathbb{A},\mathbb{D}\in T^{m,n},\ c\in T\f$
             *
             * \f$\mathbb{D}=\mathbb{A}\cdot c\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{D}_{ij}=\mathbb{A}_{ij}\cdot c\f$
             * @param c scalar multiplying matrix.
             * @return new matrix that is product of the matrix and scalar.
             */
            Matrix operator*(const T& c) const;
            /// Scalar multiplication
            /*!
             * Constructs new matrix whose elements are products of the matrix elements and scalar.
             *
             * Scalar multiplication:
             *
             * \f$\mathbb{A},\mathbb{D}\in T^{m,n},\ c\in T\f$
             *
             * \f$\mathbb{D}=c\cdot\mathbb{A}\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{D}_{ij}=c\cdot\mathbb{A}_{ij}\f$
             * @param c scalar multiplying matrix.
             * @param matrix matrix multiplied by scalar.
             * @tparam _M Number of rows of matrix.
             * @tparam _N Number of columns of matrix.
             * @tparam _T Type of elements of matrix.
             * @return new matrix that is product of the matrix and scalar.
             */
            template <unsigned int _M, unsigned int _N, typename _T>
            friend Matrix<_M, _N, _T> operator*(const _T& c, const Matrix<_M, _N, _T>& matrix);
            /// Scalar multiplication
            /*!
             * Multiplies matrix with scalar.
             *
             * Scalar multiplication:
             *
             * \f$\mathbb{A},\mathbb{D}\in T^{m,n},\ c\in T\f$
             *
             * \f$\mathbb{D}=\mathbb{A}\cdot c\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{D}_{ij}=\mathbb{A}_{ij}\cdot c\f$
             * @param c scalar multiplying matrix.
             * @return *this
             */
            Matrix& operator*=(const T& c);
            /// Matrix multiplication
            /*!
             * Multiplies matrix with other matrix.
             *
             * Matrix multiplication:
             *
             * \f$\mathbb{A}\in T^{m,n},\ \mathbb{B}\in T^{n,o},\ \mathbb{D}\in T^{m,o}\f$
             *
             * \f$\mathbb{D}=\mathbb{A}\cdot\mathbb{B}\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat o)\f$
             * \f$\mathbb{D}_{ij}=\sum_{k\in\hat n}{\mathbb{A}_{ik}\cdot\mathbb{B}_{kj}}\f$
             * @param other another matrix to multiply the matrix with.
             * @tparam M Number of rows of the matrix.
             * @tparam N Number of columns of the matrix. Number of rows of other matrix.
             * @tparam O Number of columns of other matrix.
             * @return new m,o-matrix which is product of m,n-matrix and n,o-matrix.
             */
            template <unsigned int O>
            Matrix<M, O, T> operator*(const Matrix<N, O, T>& other) const;
            /// Scalar division.
            /*!
             * Constructs new matrix whose elements are quotients of the matrix elements and scalar.
             *
             * Scalar division:
             *
             * \f$\mathbb{A},\mathbb{D}\in T^{m,n},\ c\in T\f$
             *
             * \f$\mathbb{D}=\mathbb{A}\div c\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{D}_{ij}=\mathbb{A}_{ij}\div c\f$
             * @param c scalar dividing matrix.
             * @return new matrix that is product of the matrix and scalar.
             */
            Matrix operator/(const T& c) const;
            /// Scalar division.
            /*!
             * Divides matrix with scalar.
             *
             * Scalar division:
             *
             * \f$\mathbb{A},\mathbb{D}\in T^{m,n},\ c\in T\f$
             *
             * \f$\mathbb{D}=\mathbb{A}\cdot c\rightarrow\f$
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{D}_{ij}=\mathbb{A}_{ij}\cdot c\f$
             * @param c scalar dividing matrix.
             * @return *this
             */
            Matrix& operator/=(const T& c);

            /// Compares matrix with other matrix.
            /*!
             * @param other another matrix to compare with.
             * @return true if all elements of both matrices are equal. Otherwise false.
             */
            bool operator==(const Matrix& other);
            /// Compares matrix with other matrix.
            /*!
             * @param other another matrix to compare with.
             * @return false if all elements of both matrices are equal. Other wise true.
             */
            bool operator!=(const Matrix& other);

            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified row and column.
             * If index is not within the range of the matrix, an exception of type std::out_of_range is thrown.
             * @param i row of the element to return.
             * @param j column of the element to return.
             * @return reference to the requested element.
             */
            T& At(unsigned int i, unsigned int j);
            /// Access specified element with bounds checking.
            /*!
             * Returns a reference to the element at specified row and column.
             * If index is not within the range of the matrix, an exception of type std::out_of_range is thrown.
             * @param i row of the element to return.
             * @param j column of the element to return.
             * @return const reference to the requested element.
             */
            const T& At(unsigned int i, unsigned int j) const;

            /// Computes determinant.
            /*!
             * Computes determinant of matrix.
             * @return determinant.
             */
            T Det() const;

            /// Constructs transposed matrix.
            /*!
             * Matrix transposition:
             * \f$\mathbb{A}\in T^{m,n},\ \mathbb{A}^T\in T^{n,m}\f$
             *
             * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
             * \f$\mathbb{A}^T_{ij}=\mathbb{A}_{ji}\f$
             * @return Transposed matrix.
             */
            Matrix<N, M, T> Transpose();
        protected:
            Utility::MultiArray<T, M, N> m_Matrix; //!< Stored matrix.
        };

        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T>::Matrix(const Utility::MultiArray<T, M, N>& array) :
                m_Matrix(array)
        {}

        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T>::Matrix(const std::initializer_list<Utility::MultiArray<T, N>>& values) :
                m_Matrix()
        {
            unsigned int i = 0;
            for(const auto& value : values)
                m_Matrix[i++] = value;
        }

        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T> Matrix<M, N, T>::operator+ (const Matrix<M, N, T> & other) const
        {
            Matrix res = *this;
            res += other;
            return res;
        }

        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T>& Matrix<M, N, T>::operator+= (const Matrix<M, N, T> & other)
        {
            for(unsigned int i = 0; i < M; i++)
                for(unsigned int j = 0; j < N; j++)
                    m_Matrix[i][j] += other.m_Matrix[i][j];
            return *this;
        }

        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T> Matrix<M, N, T>::operator- (const Matrix<M, N, T> & other) const
        {
            Matrix res = *this;
            res -= other;
            return res;
        }

        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T> & Matrix<M, N, T>::operator-= (const Matrix<M, N, T> & other)
        {
            for(unsigned int i = 0; i < M; i++)
                for(unsigned int j = 0; j < N; j++)
                    m_Matrix[i][j] -= other.m_Matrix[i][j];
            return *this;
        }

        template <unsigned int M, unsigned int N, typename T>
        template <unsigned int O>
        Matrix<M, O, T> Matrix<M, N, T>::operator* (const Matrix<N, O, T> & other) const
        {
            Matrix<M, O, T> res;
            for(unsigned int i = 0; i < M; i++)
                for(unsigned int j = 0; j < O; j++)
                {
                    res.At(i, j) = 0;
                    for(unsigned int k = 0; k < N; k++)
                        res.At(i, j) += At(i, k) * other.At(k, j);
                }
            return res;
        }
        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T> Matrix<M, N, T>::operator* (const T& c) const
        {
            Matrix<M, N, T> res = *this;
            res *= c;
            return res;
        }

        /// Scalar multiplication
        /*!
         * Constructs new matrix whose elements are products of the matrix elements and scalar.
         *
         * Scalar multiplication:
         *
         * \f$\mathbb{A},\mathbb{D}\in T^{m,n},\ c\in T\f$
         *
         * \f$\mathbb{D}=c\cdot\mathbb{A}\rightarrow\f$
         * \f$(\forall i\in\hat m)(\forall j\in\hat n)\f$
         * \f$\mathbb{D}_{ij}=c\cdot\mathbb{A}_{ij}\f$
         * @param c scalar multiplying matrix.
         * @param matrix matrix multiplied by scalar.
         * @tparam M Number of rows of matrix.
         * @tparam N Number of columns of matrix.
         * @tparam T Type of elements of matrix.
         * @return new matrix that is product of the matrix and scalar.
         */
        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T> operator* (const T& c, const Matrix<M, N, T> & matrix)
        {
            Matrix<M, N, T> res = matrix;
            res *= c;
            return res;
        }

        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T> & Matrix<M, N, T>::operator*= (const T& c)
        {
            for(unsigned int i = 0; i < M; i++)
                for(unsigned int j = 0; j < N; j++)
                    m_Matrix[i][j] *= c;
            return *this;
        }

        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T> Matrix<M, N, T>::operator/ (const T& c) const
        {
            Matrix<M, N, T> res = *this;
            res /= c;
            return res;
        }

        template <unsigned int M, unsigned int N, typename T>
        Matrix<M, N, T> & Matrix<M, N, T>::operator/= (const T& c)
        {
            for(unsigned int i = 0; i < M; i++)
                for(unsigned int j = 0; j < N; j++)
                    m_Matrix[i][j] /= c;
            return *this;
        }

        template <unsigned int M, unsigned int N, typename T>
        bool Matrix<M, N, T>::operator== (const Matrix & other)
        {
            for(unsigned int i = 0; i < M; i++)
                for(unsigned int j = 0; j < N; j++)
                    if(m_Matrix[i][j] != other.m_Matrix[i][j])
                        return false;
            return true;
        }

        template <unsigned int M, unsigned int N, typename T>
        bool Matrix<M, N, T>::operator!= (const Matrix & other)
        {
            for(unsigned int i = 0; i < M; i++)
                for(unsigned int j = 0; j < N; j++)
                    if(m_Matrix[i][j] != other.m_Matrix[i][j])
                        return true;
            return false;
        }

        template <unsigned int M, unsigned int N, typename T>
        T& Matrix<M, N, T>::At(unsigned int i, unsigned int j)
        {
            return m_Matrix[i][j];
        }

        template <unsigned int M, unsigned int N, typename T>
        const T& Matrix<M, N, T>::At(unsigned int i, unsigned int j) const
        {
            return m_Matrix[i][j];
        }

        template <unsigned int M, unsigned int N, typename T>
        T Matrix<M, N, T>::Det() const
        {
            static_assert(M == N, "Determinant can be only defined for square matrices!");
            Utility::MultiArray<T, M, N> temp(m_Matrix);
            T result = T();
            for(unsigned int j = 0; j < N; ++j)
            {
                unsigned int i;
                for(i = j; i < M && temp[i][j] == T(); ++i);
                if(i != j)
                {
                    if(i >= M)
                        return T();
                    for(unsigned int k = j; k < N; ++k)
                        temp[j][k] += temp[i][k];
                }
                for(i = j + 1; i < M; ++i)
                {
                    if(temp[i][j] != T())
                    {
                        T inv = temp[i][j] / temp[j][j];
                        for(unsigned int k = j; k < N; ++k)
                            temp[i][k] -= temp[j][k] * inv;
                    }
                }
                if(j == 0)
                    result = temp[j][j];
                else
                    result *= temp[j][j];
            }
            return result;
        }

        template <unsigned int M, unsigned int N, typename T>
        Matrix<N, M, T> Matrix<M, N, T>::Transpose()
        {
            Matrix<N, M, T> transposed;
            for(int i = 0; i < M; ++i)
                for(int j = 0; j < N; ++j)
                    transposed.At(i, j) = At(j, i);
            return transposed;
        }
    }
}

#endif /* BORIS_MATRIX */