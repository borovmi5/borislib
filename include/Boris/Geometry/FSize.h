#ifndef BORIS_FSIZE
#define BORIS_FSIZE

#include "Vector2.h"

namespace Boris
{
    namespace Geometry
    {
        class Size;

        /// 2D Real size.
        /*!
         * Simple size with width and height.
         */
        class FSize
        {
        public:
            /// Default constructor.
            /*!
             * Constructs new size with dimensions [0; 0].
             */
            FSize();
            /// Constructor.
            /*!
             * Constructs new size with specified dimensions.
             * @param width width of size.
             * @param height height of size.
             */
            FSize(double width, double height);
            /// Copy constructor.
            /*!
             * Constructs new copy of another size.
             * @param other another size to be used as data source to initialize the size.
             */
            FSize(const FSize& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another size using move semantics.
             * @param other another size to be used as data source to initialize the size.
             */
            FSize(FSize&& other) = default;
            /// Destructor.
            /*!
             * Destructs the size.
             */
            ~FSize() = default;

            /// Copy assignment operator
            /*!
             * Overwrites the point with another size.
             * @param other another size to be used as data source to overwrite the size.
             * @return *this
             */
            FSize& operator=(const FSize& other) = default;

            /// Size comparison.
            /*!
             * Compares two sizes and returns true when both have the same dimensions.
             * @param other another size to compare the size with.
             * @return true if both sizes have the same dimensions, false otherwise.
             */
            bool operator==(const FSize& other) const;
            /// Size comparison.
            /*!
             * Compares two sizes and returns true when both have different dimensions.
             * @param other another size to compare the size with.
             * @return false if both sizes have the same dimensions, true otherwise.
             */
            bool operator!=(const FSize& other) const;

            /// Area of the dimensions
            /*!
             * Returns area covered by dimensions.
             * @return covered area.
             */
            double Area() const;

            /// Resize.
            /*!
             * Changes size dimensions to specified values.
             * @param width new width of the size.
             * @param height new height of the size.
             * @return
             */
            FSize& Resize(double width, double height);

            /// Width.
            /*!
             * Access to the width of the size.
             * @return const reference to the width.
             */
            double Width() const;
            /// Set width.
            /*!
             * Sets the width of the size to the value width.
             * @param width new width of the size.
             */
            void Width(double width);

            /// Height.
            /*!
             * Access to the height of the size.
             * @return const reference to the height.
             */
            double Height() const;
            /// Set height.
            /*!
             * Sets the height of the size to the value height.
             * @param height new height of the size.
             */
            void Height(double height);

            /// Conversion to Vector2.
            /*!
             * Converts the size to new Vector2.
             * @return Vector2 with the same dimensions as the size.
             */
            operator Vector2() const;

            explicit operator Size() const;

        private:
            double m_Width; //!< Width of the point.
            double m_Height; //!< Height of the point.
        };
    }
}

#endif /* BORIS_FSIZE */