#ifndef BORIS_FPOINT
#define BORIS_FPOINT

#include "Vector2.h"

namespace Boris
{
    namespace Geometry
    {
        class Point;

        /// 2D Real point.
        /*!
         * Simple point with coordinates X and Y.
         */
        class FPoint
        {
        public:
            /// Default constructor.
            /*!
             * Constructs new point at coords [0; 0].
             */
            FPoint();
            /// Constructor.
            /*!
             * Constructs new point with the same coordinates as vector.
             * @param vector vector to be used as data source to initialize the point.
             */
            FPoint(const Vector2& vector);
            /// Constructor.
            /*!
             * Constructs new point at specified coords.
             * @param x x-coord of point.
             * @param y y-coord of point.
             */
            FPoint(double x, double y);
            /// Copy constructor.
            /*!
             * Constructs new copy of another point.
             * @param other another point to be used as data source to initialize the point.
             */
            FPoint(const FPoint& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another point using move semantics.
             * @param other another point to be used as data source to initialize the point.
             */
            FPoint(FPoint&& other) = default;
            /// Destructor.
            /*!
             * Destructs the point.
             */
            ~FPoint() = default;

            /// Copy assignment operator
            /*!
             * Overwrites the point with another point.
             * @param other another point to be used as data source to overwrite the point.
             * @return *this
             */
            FPoint& operator=(const FPoint& other) = default;

            /// Point comparison.
            /*!
             * Compares two points and returns true when both have the same coords.
             * @param other another point to compare the point with.
             * @return true if both points have the same coords, false otherwise.
             */
            bool operator==(const FPoint& other) const;
            /// Point comparison.
            /*!
             * Compares two points and returns true when both have differrent coords.
             * @param other another point to compare the point with.
             * @return false if both points have the same coords, true otherwise.
             */
            bool operator!=(const FPoint& other) const;

            /// Point difference.
            /*!
             * Returns Vector2 that:
             *
             * \f$Points a=[a_x;a_y],\ b=[b_x;n_y]
             *
             * \vec{v}=a.Difference(b)=(a_x - b_x; a_y - b_y)\f$
             * @param other another point to which count the difference.
             * @return difference of two points.
             */
            Vector2 Difference(const FPoint& other);
            /// Points distance.
            /*!
             * Returns distance to another point.
             * @param other another point to which count the distance.
             * @return distance of two points.
             */
            double Distance(const FPoint& other);

            /// Relocate point.
            /*!
             * Changes point coords to specified values.
             *
             * \f$If\ a=[a_x;a_y]\ then\ a.MoveTo(b_x, b_y)=[b_x;b_y]\f$
             * @param x new x-coord of the point.
             * @param y new y-coord of the point.
             * @return *this
             */
            FPoint& MoveTo(double x, double y);
            /// Relocate point.
            /*!
             * Changes point coords by specified values.
             *
             * \f$If\ a=[a_x;a_y]\ then\ a.MoveTo(b_x, b_y)=[a_x + b_x; a_y + b_y]\f$
             * @param x x-coord increment.
             * @param y y-coord increment.
             * @return *this
             */
            FPoint& MoveBy(double x, double y);

            /// X-coord.
            /*!
             * Access to the x-coord of the point.
             * @return reference to the x-coord.
             */
            double& X();
            /// X-coord.
            /*!
             * Access to the x-coord of the point.
             * @return const reference to the x-coord.
             */
            double X() const;
            /// Set x-coord.
            /*!
             * Sets the x-coord of the point to the value x.
             * @param x new x-coord of the point.
             */
            void X(double x);

            /// Y-coord.
            /*!
             * Access to the y-coord of the point.
             * @return reference to the y-coord.
             */
            double& Y();
            /// Y-coord.
            /*!
             * Access to the y-coord of the point.
             * @return const reference to the y-coord.
             */
            double Y() const;
            /// Set y-coord
            /*!
             * Sets the y-coord of the point to the value y.
             * @param y new y-coord of the point.
             */
            void Y(double y);

            /// Conversion to Vector2.
            /*!
             * Converts the point to new Vector2.
             * @return Vector2 with the same coordinates as the point.
             */
            operator Vector2() const;

            explicit operator Point() const;

        private:
            double m_X; //!< X-coord of the point.
            double m_Y; //!< Y-coord of the point.
        };
    }
}

#endif /* BORIS_FPOINT */