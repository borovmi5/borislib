#ifndef BORIS_VECTOR2
#define BORIS_VECTOR2

#include "Vector.h"

namespace Boris
{
    namespace Geometry
    {
        /// 2D vector
        /*!
         * Implementation of 2D vector. Has 2 double coordinates X and Y.
         */
        class Vector2 : public Vector<2, double>
        {
        public:
            /// Default constructor.
            /*!
             * Constructs new vector (0; 0).
             */
            Vector2() = default;
            /// Constructor.
            /*!
             * Constructs new vector (x; y).
             * @param x 1st coordinate of vector.
             * @param y 2nd coordinate of vector.
             */
            Vector2(double x, double y);
            /// Constructor.
            /*!
             * Constructs new vector from other vector.
             * @param vector vector to be used as source for initialize coordinates.
             */
            Vector2(const Vector<2, double>& vector);
            /// Constructor.
            /*!
             * Constructs new vector from 2,1-matrix
             * @param matrix 2,1-matrix to be used as source to initialize coordinates.
             */
            Vector2(const Matrix<2, 1, double>& matrix);
            /// Copy constructor.
            /*!
             * Constructs new vector with the copy of coordinates of other vector.
             * @param other another vector to be used as source to initialize coordinates.
             */
            Vector2(const Vector2& other) = default;
            /// Move constructor.
            /*!
             * Constructs new vector with the copy of coordinates of other vector using move semantics.
             * @param other another vector to be used as source to initialize coordinates.
             */
            Vector2(Vector2&& other) = default;
            /// Destructor.
            /*!
             * Destructs the vector.
             */
            ~Vector2() override = default;

            /// Constructs new vector.
            /*!
             * Constructs new vector defined by absolute value and direction <0; 2PI)
             * @param abs absolute value of vector.
             * @param dir direction of vector.
             * @return new vector.
             */
            static Vector2 FromAbsDir(double abs, double dir);

            /// Copy assignment operator.
            /*!
             * Replaces coordinates of the vector with coordinates of other vector.
             * @param other another vector to be used as source to overwrite coordinate of the vector.
             * @return *this
             */
            Vector2& operator=(const Vector2& other) = default;

            /// Vector sum
            /*!
             * Constructs new vector that is sum of the vector and other vector.
             *
             * Vector sum:
             *
             * \f$\vec{u}=(u_x,u_y),\vec{v}=(v_x,v_y)\in T^{2}\f$
             *
             * \f$\vec{u}+\vec{v}=(u_x+v_x,u_y+v_y)\f$
             * @param other another vector to add to the vector.
             * @return new vector that is sum of the vector and other vector.
             */
            Vector2 operator+(const Vector2& other) const;
            /// Vector sum
            /*!
             * Adds another vector to the vector.
             *
             * Vector sum:
             *
             * \f$\vec{u}=(u_x,u_y),\vec{v}=(v_x,v_y)\in T^{2}\f$
             *
             * \f$\vec{u}+\vec{v}=(u_x+v_x,u_y+v_y)\f$
             * @param other another vector to add to the vector.
             * @return *this
             */
            Vector2& operator+=(const Vector2& other);
            /// Vector substraction
            /*!
             * Construcst new vector that is difference of the vector and other vector.
             *
             * Vector substraction:
             *
             * \f$\vec{u}=(u_x,u_y),\vec{v}=(v_x,v_y)\in T^{2}\f$
             *
             * \f$\vec{u}-\vec{v}=(u_x-v_x,u_y-v_y)\f$
             * @param other another vector to substract from the vector.
             * @return new vector that is difference of the vector and other vector.
             */
            Vector2 operator-(const Vector2& other) const;
            /// Vector substraction
            /*!
             * Substracts another vector from the vector.
             *
             * Vector substraction:
             *
             * \f$\vec{u}=(u_x,u_y),\vec{v}=(v_x,v_y)\in T^{2}\f$
             *
             * \f$\vec{u}-\vec{v}=(u_x-v_x,u_y-v_y)\f$
             * @param other another vector to substract from the vector.
             * @return new vector that is difference of the vector and other vector.
             */
            Vector2& operator-=(const Vector2& other);
            /// Scalar multiplication
            /*!
             * Constructs new vector whose elements are products of the vector elements and scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u}=(u_x,u_y)\in T^{2},\ c\in T\f$
             *
             * \f$\vec{u}\cdot c=(u_x\cdot c,u_y\cdot c)\f$
             * @param c scalar multiplying vector.
             * @return new vector that is product of the vector and scalar.
             */
            Vector2 operator*(double c) const;
            /// Scalar multiplication
            /*!
             * Constructs new vector whose elements are products of the vector elements and scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u}=(u_x,u_y)\in T^{2},\ c\in T\f$
             *
             * \f$c\cdot\vec{u}=(c\cdot u_x,c\cdot u_y)\f$
             * @param c scalar multiplying vector.
             * @param vector vector multiplied by scalar.
             * @return new vector that is product of the vector and scalar.
             */
            friend Vector2 operator*(double c, const Vector2& vector);
            /// Matrix multiplication
            /*!
             * Constructs new vector transformed by 2,2-matrix.
             * @param matrix transformation matrix.
             * @param vector vector to transform.
             * @return new vector that is vector transformed by matrix.
             */
            friend Vector2 operator*(const Matrix<2, 2>& matrix, const Vector2& vector);
            /// Scalar multiplication
            /*!
             * Multiplies vector with scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u}=(u_x,u_y)\in T^{2},\ c\in T\f$
             *
             * \f$\vec{u}\cdot c=(u_x\cdot c,u_y\cdot c)\f$
             * @param c scalar multiplying vector.
             * @return *this
             */
            Vector2& operator*=(double c);
            /// Scalar division.
            /*!
             * Constructs new vector whose elements are quotients of the vector elements and scalar.
             *
             * Scalar division:
             *
             * \f$\vec{u}=(u_x,u_y)\in T^{2},\ c\in T\f$
             *
             * \f$\vec{u}\div c=(u_x\div c,u_y\div c)\f$
             * @param c scalar dividing vector.
             * @return new vector that is product of the vector and scalar.
             */
            Vector2 operator/(double c) const;
            /// Scalar division.
            /*!
             * Divides vector with scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u}=(u_x,u_y)\in T^{2},\ c\in T\f$
             *
             * \f$\vec{u}\div c=(u_x\div c,u_y\div c)\f$
             * @param c scalar dividing vector.
             * @return *this
             */
            Vector2& operator/=(double c);

            /// Absolute value.
            /*!
             * Computes absolute value of vector defined as:
             *
             * \f$\vec{u}(u_x,u_y)\in T^2\f$
             *
             * \f$\left|\vec{u}\right|=\sqrt{u^2_x+u^2_y}\f$
             * @return absolute value of vector.
             */
            double Abs() const;
            /// Direction.
            /*!
             * Computes direction of vector defined as:
             * \f$\vec{u}(u_x,u_y)\in T^2\f$
             *
             * \f$
             * dir\ \vec{u}=arctg2(u_y,u_x)=
             * \left\{\begin{matrix}
             * arctg\frac{u_y}{u_x}&if\ x>0,\\
             * arctg\frac{u_y}{u_x}+\pi&if\ x<0\land y\geq 0,\\
             * arctg\frac{u_y}{u_x}-\pi&if\ x<0\land y<0,\\
             * \frac{\pi}{2}&if\ x=0\land y>0,\\
             * -\frac{\pi}{2}&if\ x=0\land y<0,\\
             * undefined&if\ x=0\land y=0\\
             * \end{matrix}\right.
             * \f$
             * @return
             */
            double Dir() const;
            /// X coordinate.
            /*!
             * Access to X coordinate of vector.
             * @return reference to X coordinate.
             */
            double& X();
            /// Y coordinate.
            /*!
             * Access to Y coordinate of vector.
             * @return reference to Y coordinate.
             */
            double& Y();
            /// X coordinate.
            /*!
             * Access to X coordinate of vector.
             * @return X coordinate.
             */
            double X() const;
            /// Y coordinate.
            /*!
             * Access to Y coordinate of vector.
             * @return Y coordinate.
             */
            double Y() const;
            /// Set X coordinate.
            /*!
             * Sets X coordinate to value x.
             * @param x new value of X coordinate.
             */
            void X(double x);
            /// Set Y coordinate.
            /*!
             * Sets Y coordinate to value x.
             * @param y new value of Y coordinate.
             */
            void Y(double y);

        private:
            using Vector<2, double>::At;
        };
    }
}

#endif /* BORIS_VECTOR2 */