#ifndef BORIS_LINE
#define BORIS_LINE

#include "Shape.h"
#include "FPoint.h"

#include <vector>

namespace Boris
{
    namespace Geometry
    {
        /// Line.
        /*!
         * Line defined by its two points.
         */
        class Line : public Shape
        {
        public:
            /// @private
            Line() = delete;
            /// Null constructor.
            /*!
             * Constructs new null line.
             */
            Line(std::nullptr_t);
            /// Constructor.
            /*!
             * Constructs new line with given points.
             * @param p1 first point.
             * @param p2 second point.
             */
            Line(FPoint p1, FPoint p2);
            /// Constructor.
            /*!
             * Constructs new line with given point and second point computed
             * from distance from first point and direction to the second point.
             * @param p1 first point.
             * @param length line length.
             * @param direction direction from first to the second point.
             */
            Line(FPoint p1, double length, double direction);
            /// Copy constructor.
            /*!
             * Constructs new copy of another line.
             * @param other another line to be used as data source to initialize the line.
             */
            Line(const Line& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another line using move semantics.
             * @param other another line to be used as data source to initialize the line.
             */
            Line(Line&& other) = default;
            /// Destructor.
            /*!
             * Destructs the line.
             */
            ~Line() override = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the line with copy of another line.
             * @param other another line to be used as data source to overwrite the line.
             * @return *this
             */
            Line& operator=(const Line& other) = default;

            /// Line compare.
            /*!
             * Compares two lines.
             * @param other another line to compare.
             * @return true if both line have the same points (order is not important), false otherwise.
             */
            bool operator==(const Line& other) const;
            /// Line compare.
            /*!
             * Compares two lines.
             * @param other another line to compare.
             * @return false if both line have the same points (order is not important), true otherwise.
             */
            bool operator!=(const Line& other) const;

            /// First point.
            /*!
             * Access first point of the line.
             * @return reference to first point.
             */
            FPoint& P1();
            /// First point.
            /*!
             * Access first point of the line.
             * @return const reference to first point.
             */
            const FPoint& P1() const;

            /// Second point.
            /*!
             * Access second point of the line.
             * @return reference to second point.
             */
            FPoint& P2();
            /// Second point.
            /*!
             * Access second point of the line.
             * @return const reference to second point.
             */
            const FPoint& P2() const;

            /// Line length.
            /*!
             * Get length of the line.
             * @return distance between first and second point.
             */
            double Length() const;
            /// Line direction.
            /*!
             * Get direction of the line.
             * @return direction from the first point to the second.
             */
            double Direction() const;

            /// Get shape-defining points.
            /*!
             * Returns points, that are defining the shape.
             * For rectangle shape-defining points are start and end point of the line.
             * @return list of points defining shape.
             */
            std::vector<FPoint> Points() const override;

        private:
            /// Points-intersect test.
            /*!
             * Tests shape-point intersection on each point.
             * @param points list of points to test.
             * @return true if at least one of the points is within the shape, false otherwise.
             */
            bool IntersectionTest(const std::vector<FPoint>& points) const override;

            FPoint m_P1; //!< Start/First point.
            FPoint m_P2; //!< End/Second point.
        };
    }
}

#endif /* BORIS_LINE */