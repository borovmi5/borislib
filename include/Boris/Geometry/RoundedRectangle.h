#ifndef BORIS_ROUNDED_RECTANGLE
#define BORIS_ROUNDED_RECTANGLE

#include "Shape.h"
#include "FPoint.h"
#include "FSize.h"

#include <vector>

namespace Boris
{
    namespace Geometry
    {
        /// Rectangle shape.
        /*!
         * Rectangle with rounded corners defined by its upper-left corner and size and radius of corners.
         * Corner radius is maximally half of the minimum from height and width.
         */
        class RoundedRectangle : public Shape
        {
        public:
            /// @private
            RoundedRectangle() = delete;
            /// Null constructor.
            /*!
             * Constructs new null rectangle.
             */
            RoundedRectangle(std::nullptr_t);
            /// Constructor.
            /*!
             * Constructs new rectangle with given upper-left point, size and radius.
             * @param point upper-left point of the rectangle.
             * @param size size of the rectangle.
             * @param radius radius of the corners of the rectangle.
             */
            RoundedRectangle(FPoint point, FSize size, double radius);
            /// Copy constructor.
            /*!
             * Constructs new copy of another rectangle.
             * @param other another rectangle to be used as data source to initialize the rectangle.
             */
            RoundedRectangle(const RoundedRectangle& other) = default;
            /// Move constructor.
            /*!
             * Constructs new copy of another rectangle using move semantics.
             * @param other another rectangle to be used as data source to initialize the rectangle.
             */
            RoundedRectangle(RoundedRectangle&& other) = default;
            /// Destructor.
            /*!
             * Destructs the rectangle.
             */
            ~RoundedRectangle() override = default;

            /// Copy assignment operator.
            /*!
             * Overwrites the rectangle with copy of another rectangle.
             * @param other another rectangle to be used as data source to overwrite the rectangle.
             * @return *this
             */
            RoundedRectangle& operator=(const RoundedRectangle& other) = default;

            /// Rectangle compare.
            /*!
             * Compares two rectangles.
             * @param other another rectangle to compare.
             * @return true if both rectangles have the same upper-left point and size, false otherwise.
             */
            bool operator==(const RoundedRectangle& other) const;
            /// Rectangle compare.
            /*!
             * Compares two rectangles.
             * @param other another rectangle to compare.
             * @return false if both rectangles have the same upper-left point and size, true otherwise.
             */
            bool operator!=(const RoundedRectangle& other) const;

            /// Upper-left point.
            /*!
             * Access upper-left point of the rectangle.
             * @return reference to upper-left point.
             */
            FPoint& Point();
            /// Upper-left point.
            /*!
             * Access upper-left point of the rectangle.
             * @return const reference to upper-left point.
             */
            const FPoint& Point() const;

            /// Upper-left point.
            /*!
             * Access upper-left point of the rectangle.
             * @return const reference to upper-left point.
             */
            const FPoint& UpperLeft() const;
            /// Upper-right point.
            /*!
             * Gets upper-right point of the rectangle.
             * @return upper-right point.
             */
            FPoint UpperRight() const;
            /// Lower-left point.
            /*!
             * Gets lower-left point of the rectangle.
             * @return lower-left point.
             */
            FPoint LowerLeft() const;
            /// Lower-right point.
            /*!
             * Gets lower-right point of the rectangle.
             * @return lower-right point.
             */
            FPoint LowerRight() const;

            /// Rectangle size.
            /*!
             * Access size of the rectangle.
             * @return reference to size of the rectangle.
             */
            FSize& Size();
            /// Rectangle size.
            /*!
             * Access size of the rectangle.
             * @return const reference to size of the rectangle.
             */
            const FSize& Size() const;

            /// Rectangle corner radius.
            /*!
             * Sets corner radius of the rectangle.
             * @param radius new corner radius.
             */
            void Radius(double radius);
            /// Rectangle corner radius.
            /*!
             * Gets corner radius of the rectangle.
             * @return corner radius of the rectangle.
             */
            double Radius() const;
            /// Rectangle corner radius.
            /*!
             * Gets corner radius of the rectangle. Returned value
             * is not set value but real value (limited by rectangle size).
             * @return corner radius of the rectangle.
             */
            double ActualRadius() const;

            /// Rectangle area.
            /*!
             * Computes area covered by the rectangle.
             * @return rectangle area.
             */
            double Area();

            /// Get shape-defining points.
            /*!
             * Returns points, that are defining the shape.
             * For rectangle shape-defining points are points that are at intersections
             * of corner circles and rectangle itself.
             * @return list of points defining shape.
             */
            std::vector<FPoint> Points() const override;

        private:
            /// Points-intersect test.
            /*!
             * Tests shape-point intersection on each point.
             * @param points list of points to test.
             * @return true if at least one of the points is within the shape, false otherwise.
             */
            bool IntersectionTest(const std::vector<FPoint>& points) const override;

            FPoint m_Point; //!< Upper-left point of the rectangle.
            FSize m_Size; //!< Size of the rectangle.
            double m_Radius; //!< Corner radius of the rectangle.
        };
    }
}

#endif /* BORIS_RECTANGLE */