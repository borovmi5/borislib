#ifndef BORIS_VECTOR3
#define BORIS_VECTOR3

#include "Vector.h"
#include "Vector2.h"

namespace Boris
{
    namespace Geometry
    {
        /// 3D vector
        /*!
         * Implementation of 3D vector. Has 3 double coordinates X, Y and Z.
         */
        class Vector3 : public Vector<3, double>
        {
        public:
            /// Default constructor.
            /*!
             * Constructs new vector (0; 0; 0).
             */
            Vector3() = default;
            /// Constructor.
            /*!
             * Constructs new vector (x; y; z).
             * @param x 1st coordinate of vector.
             * @param y 2nd coordinate of vector.
             * @param z 3rd coordinate of vector.
             */
            Vector3(double x, double y, double z);
            /// Constructor.
            /*!
             * Constructs new vector from other vector.
             * @param vector vector to be used as source for initialize coordinates.
             */
            Vector3(const Vector<3, double>& vector);
            /// Constructor.
            /*!
             * Constructs new vector from 3,1-matrix
             * @param matrix 3,1-matrix to be used as source to initialize coordinates.
             */
            Vector3(const Matrix<3, 1, double>& matrix);
            /// Copy constructor.
            /*!
             * Constructs new vector with the copy of coordinates of other vector.
             * @param other another vector to be used as source to initialize coordinates.
             */
            Vector3(const Vector3& other) = default;
            /// Move constructor.
            /*!
             * Constructs new vector with the copy of coordinates of other vector using move semantics.
             * @param other another vector to be used as source to initialize coordinates.
             */
            Vector3(Vector3&& other) = default;
            /// Destructor.
            /*!
             * Destructs the vector.
             */
            ~Vector3() override = default;

            /// Constructs new vector.
            /*!
             * Constructs new vector defined by absolute value and direction <0; 2PI)
             * @param abs absolute value of vector.
             * @param dir directions of vector (dir.X X-Y, dir.Y X-Z).
             * @return new vector.
             */
            static Vector3 FromAbsDir(double abs, Vector2 dir);

            /// Copy assignment operator.
            /*!
             * Replaces coordinates of the vector with coordinates of other vector.
             * @param other another vector to be used as source to overwrite coordinate of the vector.
             * @return *this
             */
            Vector3& operator=(const Vector3& other) = default;

            /// Vector sum
            /*!
             * Constructs new vector that is sum of the vector and other vector.
             *
             * Vector sum:
             *
             * \f$\vec{u}=(u_x,u_y,u_z),\vec{v}=(v_x,v_y,v_z)\in T^{3}\f$
             *
             * \f$\vec{u}+\vec{v}=(u_x+v_x,u_y+v_y,u_z+v_z)\f$
             * @param other another vector to add to the vector.
             * @return new vector that is sum of the vector and other vector.
             */
            Vector3 operator+(const Vector3& other) const;
            /// Vector sum
            /*!
             * Adds another vector to the vector.
             *
             * Vector sum:
             *
             * \f$\vec{u}=(u_x,u_y,u_z),\vec{v}=(v_x,v_y,v_z)\in T^{3}\f$
             *
             * \f$\vec{u}+\vec{v}=(u_x+v_x,u_y+v_y,u_z+v_z)\f$
             * @param other another vector to add to the vector.
             * @return *this
             */
            Vector3& operator+=(const Vector3& other);
            /// Vector substraction
            /*!
             * Construcst new vector that is difference of the vector and other vector.
             *
             * Vector substraction:
             *
             * \f$\vec{u}=(u_x,u_y,u_z),\vec{v}=(v_x,v_y,v_z)\in T^{3}\f$
             *
             * \f$\vec{u}-\vec{v}=(u_x-v_x,u_y-v_y,u_z-v_z)\f$
             * @param other another vector to substract from the vector.
             * @return new vector that is difference of the vector and other vector.
             */
            Vector3 operator-(const Vector3& other) const;
            /// Vector substraction
            /*!
             * Substracts another vector from the vector.
             *
             * Vector substraction:
             *
             * \f$\vec{u}=(u_x,u_y,u_z),\vec{v}=(v_x,v_y,v_z)\in T^{3}\f$
             *
             * \f$\vec{u}-\vec{v}=(u_x-v_x,u_y-v_y,u_z-v_z)\f$
             * @param other another vector to substract from the vector.
             * @return new vector that is difference of the vector and other vector.
             */
            Vector3& operator-=(const Vector3& other);
            /// Scalar multiplication
            /*!
             * Constructs new vector whose elements are products of the vector elements and scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u}=(u_x,u_y,u_z)\in T^{3},\ c\in T\f$
             *
             * \f$\vec{u}\cdot c=(u_x\cdot c,u_y\cdot c, u_z\cdot c)\f$
             * @param c scalar multiplying vector.
             * @return new vector that is product of the vector and scalar.
             */
            Vector3 operator*(double c) const;
            /// Scalar multiplication
            /*!
             * Constructs new vector whose elements are products of the vector elements and scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u}=(u_x,u_y,u_z)\in T^{3},\ c\in T\f$
             *
             * \f$c\cdot\vec{u}=(c\cdot u_x,c\cdot u_y,c\cdot u_z)\f$
             * @param c scalar multiplying vector.
             * @param vector vector multiplied by scalar.
             * @return new vector that is product of the vector and scalar.
             */
            friend Vector3 operator*(double c, const Vector3& vector);
            /// Matrix multiplication
            /*!
             * Constructs new vector transformed by 3,3-matrix.
             * @param matrix transformation matrix.
             * @param vector vector to transform.
             * @return new vector that is vector transformed by matrix.
             */
            friend Vector3 operator*(const Matrix<3, 3>& matrix, const Vector3& vector);
            /// Scalar multiplication
            /*!
             * Multiplies vector with scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u}=(u_x,u_y,u_z)\in T^{3},\ c\in T\f$
             *
             * \f$\vec{u}\cdot c=(u_x\cdot c,u_y\cdot c,u_z\cdot c)\f$
             * @param c scalar multiplying vector.
             * @return *this
             */
            Vector3& operator*=(double c);
            /// Scalar division.
            /*!
             * Constructs new vector whose elements are quotients of the vector elements and scalar.
             *
             * Scalar division:
             *
             * \f$\vec{u}=(u_x,u_y,u_z)\in T^{3},\ c\in T\f$
             *
             * \f$\vec{u}\div c=(u_x\div c,u_y\div c,u_z\div c)\f$
             * @param c scalar dividing vector.
             * @return new vector that is product of the vector and scalar.
             */
            Vector3 operator/(double c) const;
            /// Scalar division.
            /*!
             * Divides vector with scalar.
             *
             * Scalar multiplication:
             *
             * \f$\vec{u}=(u_x,u_y,u_z)\in T^{3},\ c\in T\f$
             *
             * \f$\vec{u}\div c=(u_x\div c,u_y\div c,u_z\div c)\f$
             * @param c scalar dividing vector.
             * @return *this
             */
            Vector3& operator/=(double c);

            /// Absolute value.
            /*!
             * Computes absolute value of vector defined as:
             *
             * \f$\vec{u}(u_x,u_y,u_z)\in T^3\f$
             *
             * \f$\left|\vec{u}\right|=\sqrt{u^2_x+u^2_y+u^2_z}\f$
             * @return absolute value of vector.
             */
            double Abs() const;
            /// Direction.
            /*!
             * Computes direction of vector defined as:
             * \f$\vec{u}(u_x,u_y,u_z)\in T^3\f$
             *
             * \f$
             * dir_{xy}\ \vec{u}=arctg2(u_y,u_x)=
             * \left\{\begin{matrix}
             * arctg\frac{u_y}{u_x}&if\ x>0,\\
             * arctg\frac{u_y}{u_x}+\pi&if\ x<0\land y\geq 0,\\
             * arctg\frac{u_y}{u_x}-\pi&if\ x<0\land y<0,\\
             * \frac{\pi}{2}&if\ x=0\land y>0,\\
             * -\frac{\pi}{2}&if\ x=0\land y<0,\\
             * undefined&if\ x=0\land y=0\\
             * \end{matrix}\right.
             * \f$
             *
             * \f$
             * dir_{xz}\ \vec{u}=arctg2(u_z,u_x)=
             * \left\{\begin{matrix}
             * arctg\frac{u_z}{u_x}&if\ x>0,\\
             * arctg\frac{u_z}{u_x}+\pi&if\ x<0\land z\geq 0,\\
             * arctg\frac{u_z}{u_x}-\pi&if\ x<0\land z<0,\\
             * \frac{\pi}{2}&if\ x=0\land z>0,\\
             * -\frac{\pi}{2}&if\ x=0\land z<0,\\
             * undefined&if\ x=0\land z=0\\
             * \end{matrix}\right.
             * \f$
             * @return
             */
            Vector2 Dir() const;
            /// X coordinate.
            /*!
             * Access to X coordinate of vector.
             * @return reference to X coordinate.
             */
            double& X();
            /// Y coordinate.
            /*!
             * Access to Y coordinate of vector.
             * @return reference to Y coordinate.
             */
            double& Y();
            /// Z coordinate.
            /*!
             * Access to Z coordinate of vector.
             * @return reference to Z coordinate.
             */
            double& Z();
            /// X coordinate.
            /*!
             * Access to X coordinate of vector.
             * @return X coordinate.
             */
            double X() const;
            /// Y coordinate.
            /*!
             * Access to Y coordinate of vector.
             * @return Y coordinate.
             */
            double Y() const;
            /// Z coordinate.
            /*!
             * Access to Z coordinate of vector.
             * @return Z coordinate.
             */
            double Z() const;
            /// Set X coordinate.
            /*!
             * Sets X coordinate to value x.
             * @param x new value of X coordinate.
             */
            void X(double x);
            /// Set Y coordinate.
            /*!
             * Sets Y coordinate to value x.
             * @param y new value of Y coordinate.
             */
            void Y(double y);
            /// Set Z coordinate.
            /*!
             * Sets Z coordinate to value x.
             * @param z new value of Z coordinate.
             */
            void Z(double z);

        private:
            using Vector<3, double>::At;
        };
    }
}

#endif /* BORIS_VECTOR3 */