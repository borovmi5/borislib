#include <GameUI.h>
#include <Graphics.h>

#include <Entities.h>

#include <sstream>
#include <iomanip>
#include <cmath>

using namespace std;
using namespace Boris;
using namespace Boris::Graphics;
using namespace Boris::UI;
using namespace TowerAttack::Entities;

namespace TowerAttack
{
    GameUI::GameUI(const AttackerSet& attackers) :
        m_SpawnHandler(&GameUI::Spawn, this),
        m_Attackers(attackers)
    {
        Panel spawn_menu({120, 0});
        Panel top_bar({0, 50});
        Panel resources_panel({150, 30});
        Panel time_panel({150, 30});
        Panel goal_panel({150, 30});

        Label goal_label({"GOAL: ", 8});
        Label goal_done({"", 14});
        Label goal_needed({""}, 10);
        Label time_label({"TIME LIMIT: ", 8});
        Label time_min_s({""}, 14);
        Label time_cs({""}, 10);
        Label p_resources({""}, 14);
        Label p_income({""}, 10);
        Label resources({"cr."}, 10);;

        // Spawn menu
        int i = 0;
        for(const auto& att : attackers)
        {
            ostringstream oss;
            oss << att.second.DispName() << endl
                << "cost: " << att.second.Cost() << " cr.";
            Button b({0, 38}, oss.str());
            b.Margin({ .Top = 43 * i });
            b.Padding({ .Top = 5, .Right = 5, .Bottom = 5, .Left = 5 });
            b.FontSize(10);
            b.ContentVAlign(Control::VerticalAlignment::TOP);
            b.ContentHAlign(Control::HorizontalAlignment::LEFT);
            b.HAlign(Control::HorizontalAlignment::STRETCH);
            b.Value(att.second.SysName());
            b.OnClick += m_SpawnHandler;

            spawn_menu.AddControl(move(b));
            ++i;
        }

        spawn_menu.VAlign(Control::VerticalAlignment::STRETCH);
        spawn_menu.HAlign(Control::HorizontalAlignment::RIGHT);
        spawn_menu.Padding({ .Top = 10, .Right = 10, .Bottom = 10, .Left = 10 });
        spawn_menu.Margin({ .Top = 50 });
        spawn_menu.Color(Colors::Gainsboro);
        spawn_menu.BorderColor(Colors::Transparent);
        m_Right = &Application::AddControl(move(spawn_menu));

        // Top bar
        // Time panel
        time_label.VAlign(Control::VerticalAlignment::MIDDLE);
        time_label.HAlign(Control::HorizontalAlignment::LEFT);
        time_label.Bold(true);

        time_min_s.VAlign(Control::VerticalAlignment::BOTTOM);
        time_min_s.HAlign(Control::HorizontalAlignment::LEFT);
        time_min_s.Margin({ .Left = int(time_label.Size().Width() + 5) });

        time_cs.VAlign(Control::VerticalAlignment::BOTTOM);
        time_cs.HAlign(Control::HorizontalAlignment::LEFT);
        time_cs.Margin({ .Left = time_min_s.Margin().Left + int(time_min_s.Size().Width()) });

        time_panel.AddControl(move(time_label));
        m_TimeMinS = &time_panel.AddControl(move(time_min_s));
        m_TimeCs = &time_panel.AddControl(move(time_cs));
        time_panel.VAlign(Control::VerticalAlignment::MIDDLE);
        time_panel.HAlign(Control::HorizontalAlignment::LEFT);
        time_panel.Color(Colors::Gainsboro);
        time_panel.BorderColor(Colors::Transparent);
        time_panel.Padding({ .Top = 8, .Right = 10, .Bottom = 8, .Left = 10 });

        // Goal panel
        goal_label.VAlign(Control::VerticalAlignment::MIDDLE);
        goal_label.HAlign(Control::HorizontalAlignment::LEFT);
        goal_label.Bold(true);

        goal_done.VAlign(Control::VerticalAlignment::BOTTOM);
        goal_done.HAlign(Control::HorizontalAlignment::LEFT);
        goal_done.Margin({ .Left = int(goal_label.Size().Width()) + 5 });

        goal_needed.VAlign(Control::VerticalAlignment::BOTTOM);
        goal_needed.HAlign(Control::HorizontalAlignment::LEFT);
        goal_needed.Margin({ .Left = goal_done.Margin().Left + int(goal_done.Size().Width()) });

        goal_panel.AddControl(move(goal_label));
        m_AtExit = &goal_panel.AddControl(move(goal_done));
        m_Goal = &goal_panel.AddControl(move(goal_needed));
        goal_panel.VAlign(Control::VerticalAlignment::MIDDLE);
        goal_panel.HAlign(Control::HorizontalAlignment::LEFT);
        goal_panel.Color(Colors::Gainsboro);
        goal_panel.BorderColor(Colors::Transparent);
        goal_panel.Padding({ .Top = 8, .Right = 10, .Bottom = 8, .Left = 10 });
        goal_panel.Margin({ .Left = int(time_panel.Size().Width()) + 10 });

        // Resources panel
        p_resources.VAlign(Control::VerticalAlignment::MIDDLE);
        p_resources.HAlign(Control::HorizontalAlignment::LEFT);

        resources.VAlign(Control::VerticalAlignment::MIDDLE);
        resources.HAlign(Control::HorizontalAlignment::LEFT);
        resources.Margin({ .Left = int(p_resources.Size().Width()) + 5 });

        p_income.VAlign(Control::VerticalAlignment::MIDDLE);
        p_income.HAlign(Control::HorizontalAlignment::LEFT);
        p_income.Margin({ .Left = resources.Margin().Left + int(resources.Size().Width()) + 5 });

        m_Resources = &resources_panel.AddControl(move(p_resources));
        m_ResourcesCurr = &resources_panel.AddControl(move(resources));
        m_Income = &resources_panel.AddControl(move(p_income));
        resources_panel.VAlign(Control::VerticalAlignment::MIDDLE);
        resources_panel.HAlign(Control::HorizontalAlignment::RIGHT);
        resources_panel.Color(Colors::Gainsboro);
        resources_panel.BorderColor(Colors::Transparent);
        resources_panel.Padding({ .Top = 8, .Right = 10, .Bottom = 8, .Left = 10 });

        top_bar.AddControl(move(time_panel));
        top_bar.AddControl(move(goal_panel));
        top_bar.AddControl(move(resources_panel));
        top_bar.VAlign(Control::VerticalAlignment::TOP);
        top_bar.HAlign(Control::HorizontalAlignment::STRETCH);
        top_bar.Padding({ .Top = 10, .Right = 10, .Bottom = 10, .Left = 10 });
        top_bar.BorderThickness(2);
        m_Top = &Application::AddControl(move(top_bar));
    }

    GameUI::~GameUI()
    {
        Application::RemoveControl(*m_Top);
        Application::RemoveControl(*m_Right);
    }

    void GameUI::Resources(double value)
    {
        ostringstream oss;
        oss << setprecision(0) << fixed << floor(value);
        m_Resources->Content(oss.str());

        m_ResourcesCurr->Margin({ .Left = int(m_Resources->Size().Width()) + 5 });
        m_Income->Margin({ .Left = m_ResourcesCurr->Margin().Left + int(m_ResourcesCurr->Size().Width()) + 5 });
    }

    void GameUI::Income(int value)
    {
        ostringstream oss;
        oss << "+" << setprecision(0) << fixed << value << "/min";
        m_Income->Content(oss.str());
    }

    void GameUI::Time(int value)
    {
        ostringstream oss;
        int mins = value / 60000;
        int secs = (value % 60000) / 1000;
        int csecs = (value % 1000) / 10;

        oss << mins << ":" << setw(2) << setfill('0') << secs;
        m_TimeMinS->Content(oss.str());

        oss.str("");
        oss << "." << setw(2) << setfill('0') << csecs;
        m_TimeCs->Content(oss.str());

        m_TimeCs->Margin({ .Left = m_TimeMinS->Margin().Left + int(m_TimeMinS->Size().Width()) });
    }

    void GameUI::Goal(int value)
    {
        ostringstream oss;
        oss << "/" << value;

        m_Goal->Content(oss.str());
    }

    void GameUI::AtExit(int value)
    {
        ostringstream oss;
        oss << value;

        m_AtExit->Content(oss.str());

        m_Goal->Margin({ .Left = m_AtExit->Margin().Left + int(m_AtExit->Size().Width()) });
    }

    void GameUI::Spawn(const Boris::Object& sender, const Boris::Events::EventArgs& e)
    {
        OnSpawn(null, SpawnEventArgs(m_Attackers.at(dynamic_cast<const Button&>(sender).Value())));
    }
}