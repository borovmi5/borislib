#include <Game.h>
#include <UI/Application.h>
#include <Graphics.h>

#include <UI.h>

#include <unistd.h>

#include <Utility.h>

using namespace Boris;
using namespace Boris::Graphics;
using namespace Boris::UI;

using namespace Boris::Utility;

namespace TowerAttack
{
    Game::Game(LevelPrototype& level) :
        m_Level(level),
        m_UpdateHandler(&Game::Update, this),
        m_RenderHandler(&Game::RenderGame, this),
        m_RemoveProjectileHandler(&Game::RemoveProjectile, this),
        m_AttackerAtExitHandler(&Game::AttackerAtExit, this),
        m_AttackerDiedHandler(&Game::AttackerDied, this),
        m_SpawnHandler(&Game::Spawn, this),
        m_AtExit(),
        m_RemainingTime(m_Level.TimeLimit()),
        m_Resources(m_Level.PlayerResources().first),
        m_UI(m_Level.Attackers())
    {
        Application::OnDisplay += m_RenderHandler;
        Application::OnIdle += m_UpdateHandler;

        m_UI.Income(m_Level.PlayerResources().second);
        m_UI.Goal(m_Level.Goal());
        m_UI.AtExit(m_AtExit);
        m_UI.Time(m_RemainingTime);
        m_UI.Resources(m_Resources);
        m_UI.OnSpawn += m_SpawnHandler;
    }

    void Game::AddProjectile(Projectile&& projectile)
    {
        projectile.m_Renderer.SetParent(m_Level.m_Map.m_Renderer);
        projectile.OnRemove += m_RemoveProjectileHandler;
        m_ProjectilesToAdd.insert(projectile.ID());
        m_Projectiles.insert({ projectile.ID(), move(projectile) });
    }

    void Game::RemoveProjectile(const Object& sender, const EventArgs& e)
    {
        m_ProjectilesToDelete.insert(dynamic_cast<const Projectile&>(sender).ID());
    }

    void Game::AddAttacker(Attacker&& attacker)
    {
        attacker.m_Renderer.SetParent(m_Level.m_Map.m_Renderer);
        OnUpdate += attacker.m_Handler;
        attacker.OnExitReached += m_AttackerAtExitHandler;
        attacker.OnDeath += m_AttackerDiedHandler;
        m_Attackers.insert({ attacker.ID(), move(attacker) });
    }

    void Game::AttackerAtExit(const Object& sender, const EventArgs& e)
    {
        m_AtExit++;
        m_UI.AtExit(m_AtExit);
        m_AttackersToDelete.insert(dynamic_cast<const Attacker&>(sender).ID());
    }

    void Game::AttackerDied(const Object& sender, const EventArgs& e)
    {
        m_AttackersToDelete.insert(dynamic_cast<const Attacker&>(sender).ID());
    }

    void Game::AddTower(Tower&& tower)
    {
        const Tile& tile = m_Level.m_Map.At(tower.Position().X(), tower.Position().Y());
        if(!tile.IsFree())
            throw runtime_error("Tile already occupied");
        m_Level.m_Map.BuildTower(tower.Position(), tower);
        OnUpdate += tower.m_Handler;
        tower.m_Renderer.SetParent(m_Level.m_Map.m_Renderer);
        m_Towers.insert({ tower.ID(), move(tower) });
    }

    const Map& Game::GetMap() const
    {
        return m_Level.m_Map;
    }

    const map<unsigned int, Projectile>& Game::GetProjectiles() const
    {
        return m_Projectiles;
    }

    const map<unsigned int, Attacker>& Game::GetAttackers() const
    {
        return m_Attackers;
    }

    const LevelPrototype& Game::Level() const
    {
        return m_Level;
    }

    void Game::Update(const Object& sender, const TimePassedEventArgs& e)
    {
        m_RemainingTime = m_RemainingTime > e.Millis() ? m_RemainingTime - e.Millis() : 0;
        m_UI.Time(m_RemainingTime);

        m_Resources += m_Level.PlayerResources().second / 60000. * e.Millis();
        m_UI.Resources(m_Resources);

        for(auto id : m_ProjectilesToAdd)
            OnUpdate += m_Projectiles.at(id).m_Handler;
        m_ProjectilesToAdd.clear();

        for(auto& projectile : m_Projectiles)
        {
            for(auto& attacker : m_Attackers)
            {
                if(attacker.second.TryHit(projectile.second))
                {
                    attacker.second.Hit(projectile.second);
                    RemoveProjectile(projectile.second, null);
                }
            }
        }

        for(auto id : m_ProjectilesToDelete)
            m_Projectiles.erase(id);
        m_ProjectilesToDelete.clear();

        for(auto id : m_AttackersToDelete)
            m_Attackers.erase(id);
        m_AttackersToDelete.clear();

        OnUpdate(null, e);
    }

    void Game::RenderGame(const Object& sender, const EventArgs& e)
    {
        glPushMatrix();
            // UI
        glPopMatrix();

        glPushMatrix();
        glScaled(Application::Size().Width() - 120, Application::Size().Height() - 50, 1);

        // Draw lawn green background;
        glBegin(GL_TRIANGLE_FAN);
            glColor(Colors::ForestGreen);
            glVertex2d(0, 0);
            glVertex2d(1, 0);
            glVertex2d(1, 1);
            glVertex2d(0, 1);
        glEnd();

        double wRatio = m_Level.m_Map.Size().Width() / (Application::Size().Width() - 120.);
        double hRatio = m_Level.m_Map.Size().Height() / (Application::Size().Height() - 50.);
        double maxRatio = max(wRatio, hRatio);
        wRatio /= maxRatio;
        hRatio /= maxRatio;

        glTranslated(0.5 - wRatio / 2., 0.5 - hRatio / 2., 0);
        glScaled(wRatio, hRatio, 1);

        m_Level.m_Map.Renderer().Render(null, null);

        glPopMatrix();
    }

    void Game::Spawn(const Object& sender, const TowerAttack::SpawnEventArgs& e)
    {
        if(m_Resources < e.Attacker().Cost())
            return;

        m_Resources -= e.Attacker().Cost();

        Random r;
        auto ent = m_Level.m_Map.Entrances().begin();
        for(int i = r.Next(0ul, m_Level.m_Map.Entrances().size()); i; --i)
            ++ent;

        AddAttacker(Attacker(m_Level.m_Map.Size().Height() - 1- *ent, e.Attacker(), m_Level.m_Map));
    }
}