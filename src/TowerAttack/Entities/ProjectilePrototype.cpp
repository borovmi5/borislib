#include <Entities.h>
#include <Graphics.h>
#include <XML.h>

#include <string>

#include <iostream>

#include <libxml/xmlreader.h>

#include <dirent.h>

using namespace std;
using namespace Boris;
using namespace Boris::Graphics;

namespace TowerAttack
{
    namespace Entities
    {
        ProjectilePrototype::ProjectilePrototype(const string& filepath)
        {
            XML prototype(filepath);

            m_SysName = prototype["SYS_NAME"];
            try
            {
                m_DispName = prototype["DISP_NAME"];
            }
            catch (...)
            {
                cerr << "WARN: Projectile " << m_SysName << " defined in " << filepath << " missing display name." << endl;
            }
            try
            {
                m_Description = prototype["DESC"];
            }
            catch (...)
            {
                cerr << "WARN: Projectile " << m_SysName << " defined in " << filepath << " missing description." << endl;
            }
            m_Damage = int(prototype["DAMAGE"]);
            m_Color = Graphics::Color((const char *)(prototype["COLOR"]));
            try
            {
                m_Texture = Graphics::Texture(prototype["TEXTURE"]);
            }
            catch (...)
            {
                m_Texture = null;
                cerr << "WARN: Projectile " << m_SysName << " defined in " << filepath << " has not valid texture." << endl;
            }
        }

        const string& ProjectilePrototype::SysName() const
        {
            return m_SysName;
        }

        const string& ProjectilePrototype::DispName() const
        {
            return m_DispName.empty() ? m_SysName : m_DispName;
        }

        const string& ProjectilePrototype::Description() const
        {
            return m_Description;
        }

        const Color& ProjectilePrototype::Color() const
        {
            return m_Color;
        }

        const Texture& ProjectilePrototype::Texture() const
        {
            return m_Texture;
        }

        int ProjectilePrototype::Damage() const
        {
            return m_Damage;
        }

        map<string, ProjectilePrototype> ProjectilePrototype::LoadProjectiles()
        {
            DIR * dir = opendir(s_ProjectileDirectory);
            if(!dir)
                throw runtime_error("Can't open projectile directory.");

            map<string, ProjectilePrototype> result;

            for(auto * file = readdir(dir); file; file = readdir(dir))
            {
                if(file->d_type != DT_REG && file->d_type != DT_UNKNOWN)
                    continue;
                try
                {
                    ProjectilePrototype tmp(string(s_ProjectileDirectory) + '/' + file->d_name);
                    auto res = result.insert({ tmp.SysName(), move(tmp) });
                    if(!res.second)
                        cerr << "WARN: Attacker " << res.first->first << " has multimple definition." << endl;
                }
                catch (exception& e)
                {
                    cerr << "WARN: Projectile " << string(s_ProjectileDirectory) + '/' + file->d_name << " not valid. Skipping. " << endl;
                    cerr << e.what() << endl;
                }
            }

            closedir(dir);

            return result;
        }

        const char * ProjectilePrototype::s_ProjectileDirectory = "defs/projectiles";
    }
}