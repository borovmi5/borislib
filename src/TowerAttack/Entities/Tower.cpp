#include <Entities.h>
#include <Game.h>

#include <cmath>

#include <limits>

using namespace std;
using namespace Boris;
using namespace Boris::Events;
using namespace Boris::Geometry;
using namespace Boris::Graphics;
using namespace Boris::Rendering;

namespace TowerAttack
{
    namespace Entities
    {
        Tower::Tower(const Boris::Geometry::Point& position, const TowerAttack::Entities::TowerPrototype& prototype, Game& game) :
            PositionedObject(FPoint(position), 0),
            TowerPrototype(prototype),
            m_ID(++s_ID),
            m_Game(game),
            m_ShotCooldown(),
            m_Magazine(m_Burst),
            m_Renderer(FPoint(m_Position), { 0, 0 }, m_Color, { 0, 0 }),
            m_Handler(&Tower::Update, this)
        {
            if(m_Texture != null)
            {
                unsigned int maxDim = max(m_Texture.Size().Width(), m_Texture.Size().Height());
                double wRatio = double(m_Texture.Size().Width()) / maxDim;
                double hRatio = double(m_Texture.Size().Height()) / maxDim;
                double wSize = 0.8 * wRatio;
                double hSize = 0.8 * hRatio;
                m_Renderer.Size({ wSize, hSize });
                m_Renderer.Offset({ -wSize / 2, -hSize / 2 });
                m_Renderer.Texture(m_Texture);
            }
            else
            {
                m_Renderer.Size({ 0.8, 0.8 });
                m_Renderer.Offset({ -0.4, -0.4 });
            }
        }

        Tower::Tower(Tower&& other) noexcept :
            PositionedObject(move(other)),
            TowerPrototype(move(other)),
            m_ID(other.m_ID),
            m_Game(other.m_Game),
            m_ShotCooldown(other.m_ShotCooldown),
            m_Magazine(other.m_Magazine),
            m_Renderer(move(other.m_Renderer)),
            m_Handler(move(other.m_Handler))
        {
            m_Handler = bind(&Tower::Update, this, placeholders::_1, placeholders::_2);
        }

        unsigned int Tower::ID() const
        {
            return m_ID;
        }

        const RectangleRenderer& Tower::Renderer() const
        {
            return m_Renderer;
        }

        Point Tower::Position() const
        {
            return Point(m_Position);
        }

        double Tower::DPS() const
        {
            double Dmg = m_Projectile->Damage();
            double ROF = m_RateOfFire;
            int B = max(m_Burst, 0);
            double CD = max(m_BurstCooldown, 0);

            double cycle = B ? 60. * B / ROF + CD : 60. / ROF;
            double CPS = 1 / cycle;

            return B ? CPS * B * Dmg : CPS * Dmg;
        }

        void Tower::Update(const Object& sender, const TimePassedEventArgs& e)
        {
            const Attacker * target = SelectTarget();
            if(target)
            {
                Aim(*target);
                m_ShotCooldown -= e.Millis();
                if(m_ShotCooldown <= 0)
                {
                    Shoot();
                    --m_Magazine;
                    if(m_Magazine <= 0 && m_Burst > 0)
                    {
                        m_Magazine = m_Burst;
                        m_ShotCooldown = m_BurstCooldown * 1000;
                    }
                    else if(m_Burst <= 0)
                    {
                        m_Magazine = 1;
                        m_ShotCooldown = 60000 / m_RateOfFire;
                    }
                    else
                    {
                        m_ShotCooldown = 60000 / m_RateOfFire;
                    }
                }
            }
        }

        const Attacker * Tower::SelectTarget()
        {
            double min = numeric_limits<double>::max();
            const Attacker * best = nullptr;
            for(const auto& attacker : m_Game.GetAttackers())
            {
                if((Vector2(attacker.second.Position()) - m_Position).Abs() <= m_ViewRange)
                {
                    double dist;
                    if(attacker.second.Strategy() == Attacker::PathSelection::AIR)
                        dist = (Vector2(m_Game.GetMap().Size().Width() - 1, m_Game.GetMap().Size().Height() - 1 - m_Game.GetMap().Exit()) - attacker.second.Position()).Abs();
                    else
                    {
                        Point pos = Point(attacker.second.Position());
                        dist = m_Game.GetMap().At(pos.X(), m_Game.GetMap().Size().Height() - 1 - pos.Y()).Distance();
                    }
                    if(dist < min)
                    {
                        min = dist;
                        best = &attacker.second;
                    }
                }
            }
            return best;
        }

        void Tower::Aim(const Attacker& target)
        {
            Vector2 t_vec = Vector2(target.Waypoint()) - target.Position();
            double t_dir = t_vec.Dir();
            double bow = abs((Vector2(m_Position) - target.Position()).Dir() - t_dir);
            double deflection = asin(double(target.Speed()) / m_ProjectileSpeed * sin(bow));

            double angle_to_target = (Vector2(target.Position()) - m_Position).Dir();

            double y_coord;
            if(abs(t_vec.X()) > numeric_limits<double>::epsilon())
                y_coord = (target.Position().X() / t_vec.X() * t_vec.Y() + m_Position.X() * t_vec.Y() - target.Position().Y()) / t_vec.X();
            else if(t_vec.Y() < 0)
                y_coord = numeric_limits<double>::min();
            else
                y_coord = numeric_limits<double>::max();

            if((target.Position().X() < m_Position.X() && ((y_coord < m_Position.Y() && t_vec.X() <= 0) || (y_coord >= m_Position.Y() && t_vec.X() >= 0)))
            || (target.Position().X() >= m_Position.X() && ((y_coord < m_Position.Y() && t_vec.X() >= 0) || (y_coord >= m_Position.Y() && t_vec.X() <= 0))))
                angle_to_target += deflection;
            else
                angle_to_target -= deflection;

            m_Rotation = angle_to_target - M_PI / 2;
            m_Renderer.Rotation(m_Rotation);
        }

        void Tower::Shoot()
        {
            m_Game.AddProjectile(move(Entities::Projectile(m_Position, m_Rotation, m_ProjectileSpeed, m_Range, *m_Projectile)));
        }

        unsigned int Tower::s_ID = 0;
    }
}