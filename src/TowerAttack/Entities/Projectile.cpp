#include <Entities.h>
#include <Core.h>
#include <Events.h>
#include <Geometry.h>
#include <Graphics.h>

using namespace std;
using namespace Boris;
using namespace Boris::Events;
using namespace Boris::Geometry;
using namespace Boris::Graphics;
using namespace Boris::Rendering;

namespace TowerAttack
{
    namespace Entities
    {
        Projectile::Projectile(const FPoint& position, double direction, double speed, double range, const ProjectilePrototype& prototype) :
            PositionedObject(position, direction),
            ProjectilePrototype(prototype),
            m_ID(s_ID++),
            m_Speed(speed),
            m_Range(range),
            m_Renderer(m_Position, {0.1, 0.5}, m_Color, { -0.05, -0.25 }),
            m_Handler(&Projectile::Update, this)
        {
            m_Renderer.Rotation(m_Rotation);
            m_Renderer.Texture(m_Texture);
        }

        Projectile::Projectile(Projectile&& other) noexcept :
            PositionedObject(move(other)),
            ProjectilePrototype(move(other)),
            m_ID(other.m_ID),
            m_Speed(other.m_Speed),
            m_Range(other.m_Range),
            m_Renderer(move(other.m_Renderer)),
            m_Handler(move(other.m_Handler)),
            OnRemove(move(other.OnRemove))
        {
            m_Handler = bind(&Projectile::Update, this, placeholders::_1, placeholders::_2);
        }

        unsigned int Projectile::ID() const
        {
            return m_ID;
        }

        double Projectile::Speed() const
        {
            return m_Speed;
        }

        double Projectile::Range() const
        {
            return m_Range;
        }

        const RectangleRenderer& Projectile::Renderer() const
        {
            return m_Renderer;
        }

        double Projectile::Direction() const
        {
            return m_Rotation;
        }

        const FPoint& Projectile::Position() const
        {
            return m_Position;
        }

        void Projectile::Update(const Object& sender, const TimePassedEventArgs& e)
        {
            double abs = (double(m_Speed) * e.Millis()) / 1e3;
            Move(Vector2::FromAbsDir(abs, m_Rotation + M_PI / 2.));
            m_Renderer.Position(m_Position);
            m_Range -= abs;

            if(m_Range < 0)
                OnRemove(*this, null);
        }

        unsigned int Projectile::s_ID = 0;
    }
}