#include <Entities.h>
#include <Graphics.h>
#include <XML.h>

#include <string>
#include <map>
#include <iostream>

#include <dirent.h>

using namespace std;
using namespace Boris;
using namespace Boris::Graphics;

namespace TowerAttack
{
    namespace Entities
    {
        TowerPrototype::TowerPrototype(const string& filepath, const ProjectileSet& projectiles) :
            m_Projectile(nullptr)
        {
            XML prototype(filepath);

            m_SysName = prototype["SYS_NAME"];
            try
            {
                m_DispName = prototype["DISP_NAME"];
            }
            catch (...)
            {
                cerr << "WARN: Tower " << m_SysName << " defined in " << filepath << " missing display name." << endl;
            }
            try
            {
                m_Description = prototype["DESC"];
            }
            catch (...)
            {
                cerr << "WARN: Tower " << m_SysName << " defined in " << filepath << " missing description." << endl;
            }

            string strategy = prototype["STRATEGY"]("AI");
            if(strategy == "FIRST")
                m_Strategy = AttackerSelection::FIRST;
            else if(strategy == "LAST")
                m_Strategy = AttackerSelection::LAST;
            else if(strategy == "STRONGEST")
                m_Strategy = AttackerSelection::STRONGEST;
            else
                m_Strategy = AttackerSelection::WEAKEST;

            m_ViewRange = int(prototype["VIEW_RANGE"]);
            m_RateOfFire = int(prototype["GUN"]["RATE_OF_FIRE"]);

            try
            {
                m_Range = int(prototype["GUN"]["RANGE"]);
            }
            catch (...)
            {
                cerr << "WARN: Tower " << m_SysName << " defined in " << filepath << " missing gun range." << endl;
                m_Range = m_ViewRange;
            }
            try
            {
                m_Burst = int(prototype["GUN"]["BURST"]);
                m_BurstCooldown = int(prototype["GUN"]["BURST_COOLDOWN"]);
                if(m_Burst < 0 || m_BurstCooldown < 0)
                    throw exception();
            }
            catch (...)
            {
                cerr << "WARN: Tower " << m_SysName << " defined in " << filepath << " missing burst." << endl;
                m_Burst = 0;
                m_BurstCooldown = 0;
            }
            try
            {
                m_Projectile = &projectiles.at(prototype["GUN"]["PROJECTILE"]["SYS_NAME"]);
            }
            catch (...)
            {
                throw runtime_error("Non-existent projectile.");
            }

            m_ProjectileSpeed = int(prototype["GUN"]["PROJECTILE"]["SPEED"]);
            m_ProjectileSpread = int(prototype["GUN"]["PROJECTILE"]["SPREAD"]);
            m_Cost = int(prototype["COST"]);
            m_Color = Graphics::Color((const char *)(prototype["COLOR"]));
            try
            {
                m_Texture = Graphics::Texture(prototype["TEXTURE"]);
            }
            catch (...)
            {
                m_Texture = null;
                cerr << "WARN: Tower " << m_SysName << " defined in " << filepath << " missing texture." << endl;
            }
        }

        const string& TowerPrototype::SysName() const
        {
            return m_SysName;
        }

        const string& TowerPrototype::DispName() const
        {
            return m_DispName;
        }

        const string& TowerPrototype::Description() const
        {
            return m_Description;
        }

        const Graphics::Color& TowerPrototype::Color() const
        {
            return m_Color;
        }

        const Graphics::Texture& TowerPrototype::Texture() const
        {
            return m_Texture;
        }

        TowerPrototype::AttackerSelection TowerPrototype::Strategy() const
        {
            return m_Strategy;
        }

        int TowerPrototype::ViewRange() const
        {
            return m_ViewRange;
        }

        int TowerPrototype::RateOfFire() const
        {
            return m_RateOfFire;
        }

        int TowerPrototype::Range() const
        {
            return m_Range;
        }

        int TowerPrototype::Burst() const
        {
            return m_Burst;
        }

        int TowerPrototype::Cooldown() const
        {
            return m_BurstCooldown;
        }

        const ProjectilePrototype& TowerPrototype::Projectile() const
        {
            return *m_Projectile;
        }

        int TowerPrototype::Speed() const
        {
            return m_ProjectileSpeed;
        }

        int TowerPrototype::Spread() const
        {
            return m_ProjectileSpread;
        }

        int TowerPrototype::Cost() const
        {
            return m_Cost;
        }

        TowerSet TowerPrototype::LoadTowers(const ProjectileSet& projectiles)
        {
            DIR * dir = opendir(s_TowerDirectory);
            if(!dir)
                throw runtime_error("Can't open tower directory.");

            TowerSet result;

            for(auto * file = readdir(dir); file; file = readdir(dir))
            {
                if(file->d_type != DT_REG && file->d_type != DT_UNKNOWN)
                    continue;
                try
                {
                    TowerPrototype tmp(string(s_TowerDirectory) + '/' + file->d_name, projectiles);
                    auto res = result.insert({ tmp.SysName(), move(tmp) });
                    if(!res.second)
                        cerr << "WARN: Tower " << res.first->first << " has multiple definition." << endl;
                }
                catch (exception& e)
                {
                    cerr << "WARN: Tower " << string(s_TowerDirectory) + '/' + file->d_name << " not valid. Skipping. " << endl;
                    cerr << e.what() << endl;
                }
            }

            return result;
        }

        const char * TowerPrototype::s_TowerDirectory = "defs/towers";
    }
}