#include <Entities.h>
#include <Graphics.h>
#include <XML.h>

#include <string>

#include <iostream>

#include <dirent.h>

using namespace std;
using namespace Boris;
using namespace Boris::Graphics;

namespace TowerAttack
{
    namespace Entities
    {
        AttackerPrototype::AttackerPrototype(const string& filepath)
        {
            XML prototype(filepath);

            m_SysName = prototype["SYS_NAME"];
            try
            {
                m_DispName = prototype["DISP_NAME"];
            }
            catch (...)
            {
                cerr << "WARN: Attacker " << m_SysName << " defined in " << filepath << " missing display name." << endl;
            }
            try
            {
                m_Description = prototype["DESC"];
            }
            catch (...)
            {
                cerr << "WARN: Attacker " << m_SysName << " defined in " << filepath << " missing description." << endl;
            }

            string strategy = prototype["STRATEGY"]("PATH");
            if(strategy == "SHORTEST")
                m_Strategy = PathSelection::SHORTEST;
            else if(strategy == "SAFEST")
                m_Strategy = PathSelection::SAFEST;
            else
                m_Strategy = PathSelection::AIR;

            m_Speed = double(prototype["SPEED"]);

            m_Health = int(prototype["HEALTH"]["HP"]);

            try
            {
                m_MaxShield = int(prototype["HEALTH"]["SP"]);
                m_Regeneration = int(prototype["HEALTH"]["SP"]);
            }
            catch (...)
            {
                m_MaxShield = 0;
                m_Regeneration = 0;
                cerr << "WARN: Attacker " << m_SysName << " defined in " << filepath << " has not shield defined." << endl;
            }
            try
            {
                m_Evasion = double(prototype["EVASION_CHANCE"]);
            }
            catch (...)
            {
                m_Evasion = 0;
                cerr << "WARN: Attacker " << m_SysName << " defined in " << filepath << " has not evasion chance defined." << endl;
            }
            try
            {
                for(const auto& projectile : prototype["RESISTANCE"].Childs())
                {
                    auto res = m_Resistance.insert({projectile.second["SYS_NAME"], double(projectile.second["VALUE"])});
                    if(!res.second)
                        cerr << "WARN: " << m_SysName << " has multiple resistance definition for " << string(projectile.second["SYS_NAME"]) << endl;
                }
            }
            catch (...)
            {
                cerr << "WARN: Attacker " << m_SysName << " defined in " << filepath << " has not resistance defined." << endl;
            }

            m_Cost = int(prototype["COST"]);

            m_Color = Graphics::Color((const char *)(prototype["COLOR"]));
            try
            {
                m_Texture = Graphics::Texture(prototype["TEXTURE"]);
            }
            catch (...)
            {
                m_Texture = null;
                cerr << "WARN: Attacker " << m_SysName << " defined in " << filepath << " has not valid texture." << endl;
            }
        }

        const string& AttackerPrototype::SysName() const
        {
            return m_SysName;
        }

        const string& AttackerPrototype::DispName() const
        {
            return m_DispName.empty() ? m_SysName : m_DispName;
        }

        const string& AttackerPrototype::Description() const
        {
            return m_Description;
        }

        const Color& AttackerPrototype::Color() const
        {
            return m_Color;
        }

        const Texture& AttackerPrototype::Texture() const
        {
            return m_Texture;
        }

        AttackerPrototype::PathSelection AttackerPrototype::Strategy() const
        {
            return m_Strategy;
        }

        double AttackerPrototype::Speed() const
        {
            return m_Speed;
        }

        int AttackerPrototype::Health() const
        {
            return m_Health;
        }

        int AttackerPrototype::MaxShield() const
        {
            return m_MaxShield;
        }

        int AttackerPrototype::Regeneration() const
        {
            return m_Regeneration;
        }

        double AttackerPrototype::Evasion() const
        {
            return m_Evasion;
        }

        const map<string, double>& AttackerPrototype::Resistance() const
        {
            return m_Resistance;
        }

        int AttackerPrototype::Cost() const
        {
            return m_Cost;
        }

        AttackerSet AttackerPrototype::LoadAttackers()
        {
            DIR * dir = opendir(s_AttackerDirectory);
            if(!dir)
                throw runtime_error("Can't open attacker directory.");

            AttackerSet result;

            for(auto * file = readdir(dir); file; file = readdir(dir))
            {
                if(file->d_type != DT_REG && file->d_type != DT_UNKNOWN)
                    continue;
                try
                {
                    AttackerPrototype tmp(string(s_AttackerDirectory) + '/' + file->d_name);
                    auto res = result.insert({ tmp.SysName(), move(tmp) });
                    if(!res.second)
                        cerr << "WARN: Attacker " << res.first->first << " has multiple definition." << endl;
                }
                catch (exception& e)
                {
                    cerr << "WARN: Attacker " << string(s_AttackerDirectory) + '/' + file->d_name << " not valid. Skipping. " << endl;
                    cerr << e.what() << endl;
                }
            }

            closedir(dir);

            return result;
        }

        const char * AttackerPrototype::s_AttackerDirectory = "defs/attackers";
    }
}