#include <Entities.h>
#include <Core.h>
#include <Events.h>
#include <Geometry.h>
#include <Graphics.h>

#include <limits>
#include <cmath>

using namespace std;
using namespace Boris;
using namespace Boris::Events;
using namespace Boris::Geometry;
using namespace Boris::Graphics;
using namespace Boris::Rendering;
using namespace TowerAttack::Maps;

namespace TowerAttack
{
    namespace Entities
    {
        Attacker::Attacker(unsigned int entrance, const AttackerPrototype& prototype, const Map& map) :
            PositionedObject({ -1, double(entrance) }, 0),
            AttackerPrototype(prototype),
            m_Map(map),
            m_Waypoint(0, double(entrance)),
            m_ID(s_ID++),
            m_HP(m_Health),
            m_SP(m_MaxShield),
            m_Renderer(m_Position, { 0, 0 }, m_Color, { 0, 0 }),
            m_HealthRendererBack({ 0, 0.45 }, { 0.8, 0.2 }, Colors::Red, { -0.4, 0 }),
            m_HealthRendererFront({ 0, 0.45 }, { 0.8, 0.2 }, Colors::Lime, { -0.4, 0 }),
            m_ShieldRenderer({ 0, 0.45 }, { 0.8, 0.1 }, Colors::Blue, { -0.4, 0 }),
            m_Handler(&Attacker::Update, this)
        {
            if(m_Texture != null)
            {
                unsigned int maxDim = max(m_Texture.Size().Width(), m_Texture.Size().Height());
                double wRatio = double(m_Texture.Size().Width()) / maxDim;
                double hRatio = double(m_Texture.Size().Height()) / maxDim;
                double wSize = 0.8 * wRatio;
                double hSize = 0.8 * hRatio;
                m_Renderer.Size({ wSize, hSize });
                m_Renderer.Offset({ -wSize / 2, -hSize / 2 });
                m_Renderer.Texture(m_Texture);
            }
            else
            {
                m_Renderer.Size( { 0.8, 0.8 } );
                m_Renderer.Offset( { -0.4, -0.4 } );
            }
            m_HealthRendererBack.SetParent(m_Renderer);
            m_HealthRendererFront.SetParent(m_Renderer);
            m_ShieldRenderer.SetParent(m_Renderer);

            if(m_MaxShield == 0)
                m_ShieldRenderer.Size({});
        }

        Attacker::Attacker(Attacker&& other) noexcept :
            PositionedObject(move(other)),
            AttackerPrototype(move(other)),
            m_Map(other.m_Map),
            m_Waypoint(other.m_Waypoint),
            m_ID(other.m_ID),
            m_HP(m_Health),
            m_SP(m_MaxShield),
            m_Renderer(move(other.m_Renderer)),
            m_HealthRendererBack(move(other.m_HealthRendererBack)),
            m_HealthRendererFront(move(other.m_HealthRendererFront)),
            m_ShieldRenderer(move(other.m_ShieldRenderer)),
            m_Handler(move(other.m_Handler)),
            OnExitReached(move(other.OnExitReached)),
            OnDeath(move(other.OnDeath))
        {
            m_Handler = bind(&Attacker::Update, this, placeholders::_1, placeholders::_2);
        }

        unsigned int Attacker::ID() const
        {
            return m_ID;
        }

        bool Attacker::Alive() const
        {
            return m_HP > 0;
        }

        const RectangleRenderer& Attacker::Renderer() const
        {
            return m_Renderer;
        }

        const FPoint& Attacker::Position() const
        {
            return m_Position;
        }

        const FPoint& Attacker::Waypoint() const
        {
            return m_Waypoint;
        }

        int Attacker::Shield() const
        {
            return int(round(m_SP));
        }

        void Attacker::Hit(const ProjectilePrototype& projectile)
        {
            double val = 1;
            if(m_Resistance.find(projectile.SysName()) != m_Resistance.end())
                val = 1 - clamp(m_Resistance.at(projectile.SysName()), 0., 1.);
            double dmg = projectile.Damage() * val;

            m_SP -= dmg;

            if(m_SP < 0)
            {
                dmg = -m_SP;
                m_SP = 0;
            }
            else
                dmg = 0;

            m_HP -= dmg;

            if(!Alive())
                OnDeath(*this, null);

            m_HealthRendererFront.Size({
               max(m_HP / m_Health * 0.8, 0.),
               0.2
            });

            if(m_MaxShield == 0)
                m_ShieldRenderer.Size({});
            else
            {
                m_ShieldRenderer.Size({
                    max(m_SP / m_MaxShield * 0.8, 0.),
                    0.1
                });
            }
        }

        bool Attacker::TryHit(const Projectile& projectile) const
        {
            Rectangle att(m_Renderer.Offset(), m_Renderer.Size());
            Rectangle pj(projectile.Renderer().Offset(), projectile.Renderer().Size());
            Matrix<2, 2> rot = {
                    { cos(projectile.Direction() * M_PI / 180), -sin(projectile.Direction() * M_PI / 180) },
                    { sin(projectile.Direction() * M_PI / 180), cos(projectile.Direction() * M_PI / 180) }
            };

            vector<Vector2> pts = {
                    rot * Vector2(pj.UpperLeft()),
                    rot * Vector2(pj.UpperRight()),
                    rot * Vector2(pj.LowerRight()),
                    rot * Vector2(pj.LowerLeft())
            };

            pts.push_back((pts[0] + pts[1]) / 2);
            pts.push_back((pts[1] + pts[2]) / 2);
            pts.push_back((pts[2] + pts[3]) / 2);
            pts.push_back((pts[3] + pts[0]) / 2);

            for(auto& pt : pts)
                pt += Vector2(m_Position) - projectile.Position();

            for(auto& pt : pts)
                if(att.Intersects(pt))
                    return true;

            return false;
        }

        void Attacker::Update(const Object& sender, const TimePassedEventArgs& e)
        {
            // Compute distance traveled.
            double distance = m_Speed * e.Millis() / 1000.;

            while (distance > 0)
            {
                // Get distance to the waypoint
                Vector2 diff = Vector2(m_Waypoint) - m_Position;
                Move(Vector2::FromAbsDir(min(distance, diff.Abs()), diff.Dir()));
                distance -= min(distance, diff.Abs());
                if(distance > 0)
                {
                    if(m_Position.X() >= m_Map.Size().Width())
                    {
                        OnExitReached(*this, null);
                        return;
                    }
                    else
                        NextWaypoint();
                }
            }
            m_Renderer.Position(m_Position);

            m_SP = min(
                m_SP + m_Regeneration * e.Millis() / 1000.,
                double(m_MaxShield)
            );

            if(m_MaxShield == 0)
                m_ShieldRenderer.Size({});
            else
            {
                m_ShieldRenderer.Size({
                   max(m_SP / m_MaxShield * 0.8, 0.),
                   0.1
                });
            }
        }

        void Attacker::NextWaypoint()
        {
            Point pos = { int(round(m_Position.X())), int(m_Map.Size().Height() - 1 - round(m_Position.Y())) };

            enum Direction {
                UP = 0,
                RIGHT = 1,
                DOWN = 2,
                LEFT = 3
            };

            array<Point, 4> dirs = {
                    Point(0, -1),
                    Point(1, 0),
                    Point(0, 1),
                    Point(-1, 0)
            };

            if(m_Strategy == PathSelection::AIR)
            {
                if(pos.X() < 0)
                    m_Waypoint = FPoint(0, pos.Y());
                else if(pos.X() < m_Map.Size().Width() - 1)
                    m_Waypoint = FPoint(m_Map.Size().Width() - 1, m_Map.Size().Height() - 1 - m_Map.Exit());
                else
                    m_Waypoint = FPoint(m_Map.Size().Width(), m_Map.Size().Height() - 1 - m_Map.Exit());
            }
            else
            {
                if(pos.X() >= 0
                   && pos.Y() >= 0
                   && pos.X() < m_Map.Size().Width()
                   && pos.Y() < m_Map.Size().Height()
                   && m_Map.At(pos.X(), pos.Y()).Distance() == 0)
                {
                    m_Waypoint = FPoint(m_Map.Size().Width(), m_Map.Size().Height() - 1 - pos.Y());
                    return;
                }

                unsigned int min = numeric_limits<unsigned int>::max();
                int dir = 0;

                for(int d = UP; d <= LEFT; ++d)
                {
                    int x = pos.X() + dirs[d].X();
                    int y = pos.Y() - dirs[d].Y();
                    if(x >= 0
                       && y >= 0
                       && x < m_Map.Size().Width()
                       && y < m_Map.Size().Height()
                       && m_Map.At(x, y).IsAccessible()
                       && m_Map.At(x, y).Distance() < min)
                    {
                        dir = d;
                        min = m_Map.At(x, y).Distance();
                    }
                }

                m_Waypoint = Vector2(m_Position) + Vector2(dirs[dir]);
            }
        }

        unsigned int Attacker::s_ID = 0;
    }
}