#include <Entities.h>
#include <XML.h>

#include <string>
#include <iostream>

#include <dirent.h>

using namespace std;
using namespace Boris;
using namespace TowerAttack::Maps;

namespace TowerAttack
{
    namespace Entities
    {
        LevelPrototype::LevelPrototype(const string& filepath, const TowerSet& towers, const AttackerSet& attackers)
        {
            if(filepath.substr(filepath.size() - 4, 4) != ".xml")
                throw runtime_error("Not a definition file.");
            XML prototype(filepath);

            m_SysName = prototype["SYS_NAME"];
            try
            {
                m_DispName = prototype["DISP_NAME"];
            }
            catch (...)
            {
                cerr << "WARN: Level " << m_SysName << " defined in " << filepath << " missing display name." << endl;
            }
            try
            {
                m_Description = prototype["DESC"];
            }
            catch (...)
            {
                cerr << "WARN: Level " << m_SysName << " defined in " << filepath << " missing description." << endl;
            }

            m_PlayerResources = int(prototype["RESOURCES"]["PLAYER_INIT"]);
            m_PlayerIncome = int(prototype["RESOURCES"]["PLAYER_INCOME"]);
            m_AIResources = int(prototype["RESOURCES"]["AI_INIT"]);
            m_AIIncome = int(prototype["RESOURCES"]["AI_INCOME"]);

            m_Goal = int(prototype["GOAL"]);
            m_TimeLimit = int(prototype["TIME_LIMIT"]) * 1000;

            bool towerFilterInclusive = false;
            bool towerFilterExists = true;
            try
            {
                towerFilterInclusive = string(prototype["FILTERS"]["TOWERS"]("TYPE")) == "INCLUSIVE";
            }
            catch (...)
            {
                towerFilterExists = false;
                cerr << "WARN: Level " << m_SysName << " defined in " << filepath << " missing tower filters." << endl;
            }

            bool attackerFilterInclusive = false;
            bool attackerFilterExists = true;
            try
            {
                attackerFilterInclusive = string(prototype["FILTERS"]["ATTACKERS"]("TYPE")) == "INCLUSIVE";
            }
            catch (...)
            {
                attackerFilterExists = false;
                cerr << "WARN: Level " << m_SysName << " defined in " << filepath << " missing attacker filters." << endl;
            }

            m_Towers = towerFilterInclusive ? TowerSet() : towers;
            m_Attackers = attackerFilterInclusive ? AttackerSet() : attackers;

            if(towerFilterExists)
            {
                for(const auto& tow : prototype["FILTERS"]["TOWERS"].Childs())
                {
                    if(towers.find(tow.second) != towers.end())
                    {
                        if(towerFilterInclusive)
                            m_Towers.insert({ tow.second, towers.at(tow.second) });
                        else
                            m_Towers.erase(tow.second);
                    }
                }
            }

            if(attackerFilterExists)
            {
                for(const auto& att : prototype["FILTERS"]["ATTACKERS"].Childs())
                {
                    if(attackers.find(att.second) != attackers.end())
                    {
                        if(attackerFilterInclusive)
                            m_Attackers.insert({ att.second, attackers.at(att.second) });
                        else
                            m_Attackers.erase(att.second);
                    }
                }
            }

            m_Map.Load(filepath.substr(0, filepath.size() - 3) + "map");
        }

        const string& LevelPrototype::SysName() const
        {
            return m_SysName;
        }

        const string& LevelPrototype::DispName() const
        {
            return m_DispName;
        }

        const string& LevelPrototype::Description() const
        {
            return m_Description;
        }

        pair<int, int> LevelPrototype::PlayerResources() const
        {
            return { m_PlayerResources, m_PlayerIncome };
        }

        pair<int, int> LevelPrototype::AIResources() const
        {
            return { m_AIResources, m_AIIncome };
        }

        int LevelPrototype::Goal() const
        {
            return m_Goal;
        }

        int LevelPrototype::TimeLimit() const
        {
            return m_TimeLimit;
        }

        const TowerSet& LevelPrototype::Towers() const
        {
            return m_Towers;
        }

        const AttackerSet& LevelPrototype::Attackers() const
        {
            return m_Attackers;
        }

        const Maps::Map& LevelPrototype::Map() const
        {
            return m_Map;
        }

        LevelSet LevelPrototype::LoadLevels(const TowerSet& towers, const AttackerSet& attackers)
        {
            DIR * dir = opendir(s_LevelDirectory);
            if(!dir)
                throw runtime_error("Can't open level directory.");

            LevelSet result;

            for(auto * file = readdir(dir); file; file = readdir(dir))
            {
                if(file->d_type != DT_REG && file->d_type != DT_UNKNOWN)
                    continue;
                if(string(file->d_name).substr(string(file->d_name).size() - 4, 4) != ".xml")
                    continue;
                try
                {
                    LevelPrototype tmp(string(s_LevelDirectory) + '/' + file->d_name, towers, attackers);
                    auto res = result.insert({ tmp.SysName(), move(tmp) });
                    if(!res.second)
                        cerr << "WARN: Level " << res.first->first << " has multiple definition." << endl;
                }
                catch (exception& e)
                {
                    cerr << "WARN: Tower " << string(s_LevelDirectory) + '/' + file->d_name << " not valid. Skipping. " << endl;
                    cerr << e.what() << endl;
                }
            }

            closedir(dir);

            return result;
        }

        const char * LevelPrototype::s_LevelDirectory = "defs/levels";
    }
}