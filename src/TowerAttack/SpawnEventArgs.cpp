#include <SpawnEventArgs.h>

using namespace std;
using namespace Boris;
using namespace Boris::Events;
using namespace TowerAttack::Entities;

namespace TowerAttack
{
    SpawnEventArgs::SpawnEventArgs(const AttackerPrototype& attacker) :
        EventArgs(),
        m_Attacker(attacker)
    {}

    const AttackerPrototype& SpawnEventArgs::Attacker() const
    {
        return m_Attacker;
    }
}