#include <Maps/Tile.h>

#include <limits>

using namespace std;

namespace TowerAttack
{
    namespace Maps
    {
        Tile::Tile(Type tile) :
            m_Type(tile),
            m_Distance(numeric_limits<uint32_t>::max()),
            m_RiskLevel(0)
        {}

        bool Tile::operator==(const Tile& other) const
        {
            return this == &other;
        }

        bool Tile::operator!=(const Tile& other) const
        {
            return this != &other;
        }

        uint32_t Tile::Distance() const
        {
            return m_Distance;
        }

        void Tile::Distance(uint32_t dist)
        {
            m_Distance = dist;
        }

        double Tile::RiskLevel() const
        {
            return m_RiskLevel;
        }

        void Tile::RiskLevel(double risk)
        {
            m_RiskLevel = risk;
        }

        bool Tile::IsFree() const
        {
            return m_Type == Type::FREE;
        }

        bool Tile::IsAccessible() const
        {
            return m_Type == Type::PATH;
        }
    }
}