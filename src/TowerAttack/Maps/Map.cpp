#include <Maps/Map.h>

#include <Binary.h>

#include <limits>
#include <queue>
#include <cmath>

#include <iomanip>

using namespace std;
using namespace Boris;
using namespace Boris::Binary;
using namespace Boris::Geometry;
using namespace Boris::Graphics;
using namespace Boris::Rendering;
using namespace TowerAttack::Entities;

namespace TowerAttack
{
    namespace Maps
    {
        Map::Map(const string& filepath) :
            m_Position(),
            m_Renderer(m_Position, {1, 1}, 0xfff_rgb_short),
            m_Map(),
            m_Size(),
            m_Entrances(),
            m_Exit()
        {
            Load(filepath);
        }

        Map::Map(const class Size& size) :
            m_Position(),
            m_Renderer(m_Position, {1, 1}, 0xfff_rgb_short),
            m_Map(),
            m_Size(size),
            m_Entrances(),
            m_Exit()
        {
            m_Map.resize(size.Width());
            for(auto& col : m_Map)
                col.resize(size.Height());
            m_Renderer.Texture(Texture());
            m_Renderer.InnerSize() = FSize(m_Size.Width(), m_Size.Height());
        }

        Map::Map(const Map& other) :
            m_Position(other.m_Position),
            m_Renderer(m_Position, {1, 1}, 0xfff_rgb_short),
            m_Map(),
            m_Size(other.m_Size),
            m_Entrances(other.m_Entrances),
            m_Exit(other.m_Exit)
        {
            for(auto& row : other.m_Map)
                m_Map.push_back(row);
            m_Renderer.Texture(Texture());
            m_Renderer.InnerSize() = FSize(m_Size.Width(), m_Size.Height());
        }

        Map& Map::operator=(const Map& other)
        {
            if(this == &other)
                return *this;
            m_Position = other.m_Position;
            m_Map.clear();
            for(auto& row : other.m_Map)
                m_Map.push_back(row);
            m_Size = other.m_Size;
            m_Entrances = other.m_Entrances;
            m_Exit = other.m_Exit;
            m_Renderer.Texture(Texture());
            m_Renderer.InnerSize() = FSize(m_Size.Width(), m_Size.Height());
            return *this;
        }

        Map& Map::operator=(Map&& other) noexcept
        {
            if(this == &other)
                return *this;
            m_Position = other.m_Position;
            m_Map = move(other.m_Map);
            m_Size = other.m_Size;
            m_Entrances = move(other.m_Entrances);
            m_Exit = other.m_Exit;
            m_Renderer.Texture(Texture());
            m_Renderer.InnerSize() = FSize(m_Size.Width(), m_Size.Height());
            return *this;
        }

        void Map::Load(const std::string& filepath)
        {
            m_Map.clear();
            m_Entrances.clear();

            BinaryStream file;
            file.open(filepath, ios_base::in);
            if(!file.is_open())
                throw ios_base::failure("Cannot open map file for read!");
            if(file.Checksum())
                throw runtime_error("Map file is corrupted!");

            bit in_bit;
            word in_word;
            dword in_dword;

            // TAMP header
            file >> in_dword;
            if(in_dword != dword(0x504d4154))
                throw runtime_error("Map file corrupted!");

            // Reserve
            file >> in_dword;

            // Width
            file >> in_word;
            m_Size.Width(in_word ? uint32_t(in_word) : numeric_limits<uint16_t>::max() + 1);
            m_Map.resize(m_Size.Width());

            // Height
            file >> in_word;
            m_Size.Height(in_word ? uint32_t(in_word) : numeric_limits<uint16_t>::max() + 1);
            for(auto& col : m_Map)
                col.resize(m_Size.Height());

            // Entrances count
            file >> in_word;
            uint32_t entrances_count = in_word ? uint32_t(in_word) : numeric_limits<uint16_t>::max() + 1;

            // Exit y-coordinate
            file >> in_word;
            m_Exit = in_word;

            // Entrances y-coordinates
            for(; entrances_count; --entrances_count)
            {
                file >> in_word;
                m_Entrances.insert(in_word ? uint32_t(in_word) : numeric_limits<uint16_t>::max() + 1);
            }

            // Tiles
            for(int y = 0; y < m_Size.Height(); ++y)
            {
                for(int x = 0; x < m_Size.Width(); ++x)
                {
                    file >> in_bit;
                    m_Map[x][y] = Tile(in_bit ? Tile::Type::PATH : Tile::Type::FREE);
                }

                if(!file.good())
                    throw runtime_error("Map file corrupted!");
            }

            file.close();

            Evaluate();

            m_Renderer.Texture(Texture());
            m_Renderer.InnerSize() = FSize(m_Size.Width() + 0.5, m_Size.Height() + 0.5);
            m_Renderer.InnerOffset() = { 0.75 / m_Renderer.InnerSize().Width(), 0.75 / m_Renderer.InnerSize().Height() };
        }

        void Map::Save(const std::string& filepath) const
        {
            BinaryStream file;
            file.open(filepath, ios_base::in | ios_base::out);
            if(!file.is_open())
                throw ios_base::failure("Cannot open map file for write!");

            bit in_bit;
            word in_word;

            // TAMP header
            file << dword(0x504d4154);

            // Reserve
            file << dword();

            // Width
            file << word(m_Size.Width());

            // Height
            file << word(m_Size.Height());

            // Entrances count
            file << word(m_Entrances.size());

            // Exit y-coordinate
            file << word(m_Exit);

            // Entrances y-coordinates
            for(word w : m_Entrances)
                file << w;

            // Tiles
            for(int y = 0; y < m_Size.Height(); ++y)
            {
                for(int x = 0; x < m_Size.Width(); ++x)
                    file << bit(m_Map[x][y].IsAccessible());

                if(!file.good())
                    throw runtime_error("Write error!");
            }

            file.Flush();

            // Trailer
            int pos = file.tellg() % 8;

            if(pos)
                for(pos = 8 - pos; pos; --pos)
                    file << byte();

            // Write CRC
            qword check = file.Checksum();
            file << check;
            file.close();
        }

        const Tile& Map::At(uint16_t x, uint16_t y) const
        {
            return m_Map[x][y];
        }

        Texture Map::Texture() const
        {
            Image img({ m_Size.Width() * 4 + 2, m_Size.Height() * 4 + 2 }, Colors::ForestGreen);
            for(int y = 0; y < m_Size.Height(); ++y)
                for(int x = 0; x < m_Size.Width(); ++x)
                    if(m_Map[x][y].IsAccessible())
                        img.SetRectangle({x * 4 + 1, y * 4 + 1}, {4, 4}, Colors::Bisque);

            img.SetRectangle(Point(m_Size.Width() * 4 + 1, m_Exit * 4 + 1), {1, 4}, Colors::Red);
            for(const auto& e : m_Entrances)
                img.SetRectangle(Point(0, e * 4 + 1), {1, 4}, Colors::Lime);
            return Boris::Graphics::Texture(img);
        }

        const Geometry::Size& Map::Size() const
        {
            return m_Size;
        }

        const set<unsigned int>& Map::Entrances() const
        {
            return m_Entrances;
        }

        uint16_t Map::Exit() const
        {
            return m_Exit;
        }

        void Map::BuildTower(const Point& position, const Tower& tower)
        {
            if(position.X() < 0
               || position.X() >= m_Size.Width()
               || position.Y() < 0
               || position.Y() >= m_Size.Height())
                throw range_error("Tile out of map boundaries.");

            m_Map[position.X()][position.Y()].m_Type = Tile::Type::OCCUPIED;

            for(int y = tower.ViewRange(); y >= 0; --y)
            {
                for(int x = int(round(sin(acos((y - 0.5) / tower.ViewRange())) * tower.ViewRange())); x >= 0; --x)
                {
                    Tile * ul = nullptr;
                    Tile * ur = nullptr;
                    Tile * ll = nullptr;
                    Tile * lr = nullptr;

                    if(position.X() >= x)
                    {
                        if(position.Y() >= y)
                            ul = &(m_Map[position.X() - x][position.Y() - y]);
                        if(position.Y() + y < m_Size.Height())
                            ll = &(m_Map[position.X() - x][position.Y() + y]);
                    }
                    if(position.X() + x < m_Size.Width())
                    {
                        if(position.Y() >= y)
                            ur = &m_Map[position.X() + x][position.Y() - y];
                        if(position.Y() + y < m_Size.Height())
                            lr = &m_Map[position.X() + x][position.Y() + y];
                    }

                    if(x == 0 && y == 0)
                    {
                        ul->RiskLevel(ul->RiskLevel() + tower.DPS());
                    }
                    else if(x <= int(round(sin(acos(y / tower.ViewRange())) * tower.ViewRange())) && y <= int(round(sin(acos(x / tower.ViewRange())) * tower.ViewRange())))
                    {
                        if(ul) ul->RiskLevel(ul->RiskLevel() + tower.DPS());
                        if(x != 0 && ur) ur->RiskLevel(ur->RiskLevel() + tower.DPS());
                        if(y != 0 && ll) ll->RiskLevel(ll->RiskLevel() + tower.DPS());
                        if(x != 0 && y != 0 && lr) lr->RiskLevel(lr->RiskLevel() + tower.DPS());
                    }
                    else
                    {
                        if(ul) ul->RiskLevel(ul->RiskLevel() + tower.DPS() * .5);
                        if(x != 0 && ur) ur->RiskLevel(ur->RiskLevel() + tower.DPS() * .5);
                        if(y != 0 && ll) ll->RiskLevel(ll->RiskLevel() + tower.DPS() * .5);
                        if(x != 0 && y != 0 && lr) lr->RiskLevel(lr->RiskLevel() + tower.DPS() * .5);
                    }
                }
            }
        }

        const RectangleRenderer& Map::Renderer() const
        {
            return m_Renderer;
        }

        void Map::Evaluate()
        {
            // BFS evaluation
            vector<vector<bool>> fresh(m_Size.Width(), vector<bool>(m_Size.Height()));
            for(int y = 0; y < m_Size.Height(); ++y)
                for(int x = 0; x < m_Size.Width(); ++x)
                {
                    fresh[x][y] = m_Map[x][y].IsAccessible();
                    m_Map[x][y].Distance(numeric_limits<uint32_t>::max());
                }

            queue<Point> tileQueue;
            fresh[m_Size.Width() - 1][m_Exit] = false;
            m_Map[m_Size.Width() - 1][m_Exit].Distance(0);
            tileQueue.push(Point(m_Size.Width() - 1, m_Exit));

            while(!tileQueue.empty())
            {
                Point cur = tileQueue.front();
                tileQueue.pop();

                //up
                if(cur.Y() && fresh[cur.X()][cur.Y() - 1])
                {
                    fresh[cur.X()][cur.Y() - 1] = false;
                    m_Map[cur.X()][cur.Y() - 1].Distance(m_Map[cur.X()][cur.Y()].Distance() + 1);
                    tileQueue.push({cur.X(), cur.Y() - 1});
                }

                //right
                if(cur.X() < m_Size.Width() - 1 && fresh[cur.X() + 1][cur.Y() - 1])
                {
                    fresh[cur.X() + 1][cur.Y()] = false;
                    m_Map[cur.X() + 1][cur.Y()].Distance(m_Map[cur.X()][cur.Y()].Distance() + 1);
                    tileQueue.push({cur.X() + 1, cur.Y()});
                }

                //down
                if(cur.Y() < m_Size.Height() - 1 && fresh[cur.X()][cur.Y() + 1])
                {
                    fresh[cur.X()][cur.Y() + 1] = false;
                    m_Map[cur.X()][cur.Y() + 1].Distance(m_Map[cur.X()][cur.Y()].Distance() + 1);
                    tileQueue.push({cur.X(), cur.Y() + 1});
                }

                //left
                if(cur.X() && fresh[cur.X() - 1][cur.Y()])
                {
                    fresh[cur.X() - 1][cur.Y()] = false;
                    m_Map[cur.X() - 1][cur.Y()].Distance(m_Map[cur.X()][cur.Y()].Distance() + 1);
                    tileQueue.push({cur.X() - 1, cur.Y()});
                }
            }

            vector<uint32_t> invalidEntrance;
            for(auto ent : m_Entrances)
            {
                if(m_Map[0][ent].Distance() == numeric_limits<uint32_t>::max())
                {
                    invalidEntrance.push_back(ent);
                    cerr << "WARN: Map entrance [0;" << ent << "] is not valid, because it is not reachable from exit. Removing..." << endl;
                }
            }
            for(auto ent : invalidEntrance)
                m_Entrances.erase(ent);

            if(m_Entrances.empty())
                throw runtime_error("Map is not valid. No valid entrances.");
        }
    }
}