#include <XML.h>

#include <libxml/tree.h>
#include <libxml/parser.h>

#include <algorithm>
#include <cmath>

using namespace std;

namespace TowerAttack
{
    XMLAttribute::XMLAttribute(xmlAttrPtr attr)
    {
        if(!attr->children)
            return;

        m_Value = (char *)(attr->children->content);
    }

    XMLAttribute::operator bool() const
    {
        string tmp = m_Value;
        transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);
        if(m_Value.empty() || m_Value == "0" || tmp == "false")
            return false;
        if(m_Value == "1" || tmp == "true")
            return true;
        throw runtime_error("Value not of Boolean type.");
    }

    XMLAttribute::operator char() const
    {
        if(m_Value.empty())
            return 0;
        else
            return m_Value[0];
    }

    XMLAttribute::operator unsigned char() const
    {
        if(m_Value.empty())
            return 0;
        else
            return m_Value[0];
    }

    XMLAttribute::operator const char * () const
    {
        return m_Value.data();
    }

    XMLAttribute::operator std::string() const
    {
        return m_Value;
    }

    XMLAttribute::operator int() const
    {
        size_t idx;
        int result = stoi(m_Value, &idx, 0);
        if(idx != m_Value.size())
            throw runtime_error("Value NaN.");
        return result;
    }

    XMLAttribute::operator long() const
    {
        size_t idx;
        long result = stol(m_Value, &idx, 0);
        if(idx != m_Value.size())
            throw runtime_error("Value NaN.");
        return result;
    }

    XMLAttribute::operator unsigned long() const
    {
        size_t idx;
        unsigned long result = stoul(m_Value, &idx, 0);
        if(idx != m_Value.size())
            throw runtime_error("Value NaN.");
        return result;
    }

    XMLAttribute::operator double() const
    {
        size_t idx;
        double result = stod(m_Value, &idx);
        if(idx != m_Value.size())
            throw runtime_error("Value NaN.");
        return result;
    }

    XML::XML(const string& filepath)
    {
        xmlDocPtr file = nullptr;
        xmlParserCtxtPtr parser = nullptr;

        try
        {
            parser = xmlNewParserCtxt();
            if(!parser)
                throw runtime_error("Could not allocate memory");

            file = xmlCtxtReadFile(parser, filepath.c_str(), nullptr, XML_PARSE_DTDVALID);
            if(!file)
                throw runtime_error("Could not read the file.");

            if(!parser->valid)
                throw runtime_error("File not valid.");


        }
        catch (exception& e)
        {
            xmlFreeDoc(file);
            xmlFreeParserCtxt(parser);
            xmlCleanupParser();
            throw runtime_error(string("XML: Parse error. ") + e.what());
        }

        xmlNodePtr node = xmlDocGetRootElement(file);

        if(!IsLeaf(node))
        {
            for(xmlNodePtr child = node->children; child; child = child->next)
                if(child->type == XML_ELEMENT_NODE)
                    m_Childs.insert({ (char *)( child->name ), child });
        }
        else if(node->children)
            m_Value = (char *)(node->children->content);

        for(xmlAttrPtr attr = node->properties; attr; attr = attr->next)
            if(attr->type == XML_ATTRIBUTE_NODE)
                m_Attributes.insert({ (char*)(attr->name), attr });

        xmlFreeDoc(file);
        xmlFreeParserCtxt(parser);
        xmlCleanupParser();
    }

    XML::XML(xmlNodePtr node)
    {
        if(!IsLeaf(node))
        {
            for(xmlNodePtr child = node->children; child; child = child->next)
                if(child->type == XML_ELEMENT_NODE)
                    m_Childs.insert({ (char *)( child->name ), child });

        }
        else if(node->children)
            m_Value = (char *)(node->children->content);

        for(xmlAttrPtr attr = node->properties; attr; attr = attr->next)
            if(attr->type == XML_ATTRIBUTE_NODE)
                m_Attributes.insert({ (char*)(attr->name), attr });
    }

    bool XML::IsLeaf(xmlNodePtr node)
    {
        for(xmlNodePtr child = node->children; child; child = child->next)
            if(child->type == XML_ELEMENT_NODE)
                return false;
        return true;
    }

    const XML& XML::operator[](const char * key) const
    {
        auto res = m_Childs.find(key);
        if(res == m_Childs.end())
            throw runtime_error("Element does not exist.");
        return res->second;
    }

    const XMLAttribute& XML::operator()(const char * key) const
    {
        auto res = m_Attributes.find(key);
        if(res == m_Attributes.end())
            throw runtime_error("Attribute does not exist.");
        return res->second;
    }

    const multimap<string, XML>& XML::Childs() const
    {
        return m_Childs;
    }

    XML::operator bool() const
    {
        string tmp = m_Value;
        transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);
        if(m_Value.empty() || m_Value == "0" || tmp == "false")
            return false;
        if(m_Value == "1" || tmp == "true")
            return true;
        throw runtime_error("Value not of Boolean type.");
    }

    XML::operator char() const
    {
        if(m_Value.empty())
            return 0;
        else
            return m_Value[0];
    }

    XML::operator unsigned char() const
    {
        if(m_Value.empty())
            return 0;
        else
            return m_Value[0];
    }

    XML::operator const char * () const
    {
        return m_Value.data();
    }

    XML::operator std::string() const
    {
        return m_Value;
    }

    XML::operator int() const
    {
        size_t idx;
        int result = stoi(m_Value, &idx, 0);
        if(idx != m_Value.size())
            throw runtime_error("Value NaN.");
        return result;
    }

    XML::operator long() const
    {
        size_t idx;
        long result = stol(m_Value, &idx, 0);
        if(idx != m_Value.size())
            throw runtime_error("Value NaN.");
        return result;
    }

    XML::operator unsigned long() const
    {
        size_t idx;
        unsigned long result = stoul(m_Value, &idx, 0);
        if(idx != m_Value.size())
            throw runtime_error("Value NaN.");
        return result;
    }

    XML::operator double() const
    {
        size_t idx;
        double result = stod(m_Value, &idx);
        if(idx != m_Value.size())
            throw runtime_error("Value NaN.");
        return result;
    }
}