#include <UI.h>

using namespace std;
using namespace Boris;
using namespace Boris::Controls;
using namespace Boris::Events;
using namespace Boris::Geometry;
using namespace Boris::Graphics;
using namespace Boris::Rendering;

namespace Boris
{
    namespace UI
    {
        Button::Button(const class Size& size, const string& text, const Indentation& margin) :
            Control(),
            m_Renderer({}, FSize(size), 0, Colors::GhostWhite, Colors::Gainsboro, 1, LineType::SOLID),
            m_NormalColor(Colors::GhostWhite),
            m_HoverColor(Colors::AliceBlue),
            m_ClickColor(Colors::PowderBlue),
            m_MouseMove(&Button::MouseMove, this),
            m_MouseClick(&Button::MouseMove, this),
            m_MouseUp(&Button::MouseUp, this)
        {
            m_Size = size;
            m_Margin = margin;
            Label label(text, 14);
            label.VAlign(VerticalAlignment::MIDDLE);
            label.HAlign(HorizontalAlignment::CENTER);
            m_Label = &AddControl(move(label));
            Mouse::OnMouseMove += m_MouseMove;
            Mouse::OnMouseClick += m_MouseClick;
            Mouse::OnMouseDoubleClick += m_MouseClick;
            Mouse::OnMouseUp += m_MouseUp;
        }

        Button::Button(nullptr_t) :
            Control(null),
            m_Renderer(null),
            m_Label(nullptr),
            m_NormalColor(),
            m_HoverColor(),
            m_ClickColor()
        {}

        Button::Button(Boris::UI::Button&& other) noexcept :
            Control(move(other)),
            m_Renderer(move(other.m_Renderer)),
            m_Label(other.m_Label),
            m_Value(move(other.m_Value)),
            m_NormalColor(other.m_NormalColor),
            m_HoverColor(other.m_HoverColor),
            m_ClickColor(other.m_ClickColor),
            m_MouseMove(move(other.m_MouseMove)),
            m_MouseClick(move(other.m_MouseClick)),
            m_MouseUp(move(other.m_MouseUp)),
            OnClick(move(other.OnClick))
        {
            m_MouseMove = bind(&Button::MouseMove, this, placeholders::_1, placeholders::_2);
            m_MouseClick = bind(&Button::MouseClick, this, placeholders::_1, placeholders::_2);
            m_MouseUp = bind(&Button::MouseUp, this, placeholders::_1, placeholders::_2);
        }

        void Button::Color(const class Color& color)
        {
            if(m_Null)
                throw NullException();
            m_NormalColor = color;
        }

        const Color& Button::Color() const
        {
            if(m_Null)
                throw NullException();
            return m_NormalColor;
        }

        void Button::HoverColor(const class Color& color)
        {
            if(m_Null)
                throw NullException();
            m_HoverColor = color;
        }

        const Color& Button::HoverColor() const
        {
            if(m_Null)
                throw NullException();
            return m_HoverColor;
        }

        void Button::ClickColor(const class Color& color)
        {
            if(m_Null)
                throw NullException();
            m_ClickColor = color;
        }

        const Color& Button::ClickColor() const
        {
            if(m_Null)
                throw NullException();
            return m_ClickColor;
        }

        void Button::BorderColor(const class Color& color)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.LineColor(color);
        }

        const Color& Button::BorderColor() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.LineColor();
        }

        void Button::BorderStyle(const LineType& style)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.LineType(style);
        }

        const LineType& Button::BorderStyle() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.LineType();
        }

        void Button::BorderThickness(double thickness)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.LineWidth(thickness);
        }

        double Button::BorderThickness() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.LineWidth();
        }

        void Button::ContentColor(const class Color& color)
        {
            if(m_Null)
                throw NullException();
            m_Label->Color(color);
        }

        const Color& Button::ContentColor() const
        {
            if(m_Null)
                throw NullException();
            return m_Label->Color();
        }

        void Button::Bold(bool bold)
        {
            if(m_Null)
                throw NullException();
            m_Label->Bold(bold);
        }

        bool Button::Bold() const
        {
            if(m_Null)
                throw NullException();
            return m_Label->Bold();
        }

        void Button::Mono(bool mono)
        {
            if(m_Null)
                throw NullException();
            m_Label->Mono(mono);
        }

        bool Button::Mono() const
        {
            if(m_Null)
                throw NullException();
            return m_Label->Mono();
        }

        void Button::Content(const string& text)
        {
            if(m_Null)
                throw NullException();
            m_Label->Content(text);
        }

        const string& Button::Content() const
        {
            if(m_Null)
                throw NullException();
            return m_Label->Content();
        }

        void Button::FontSize(double size)
        {
            if(m_Null)
                throw NullException();
            m_Label->FontSize(size);
        }

        double Button::FontSize() const
        {
            if(m_Null)
                throw NullException();
            return m_Label->FontSize();
        }

        void Button::ContentVAlign(VerticalAlignment alignment)
        {
            if(m_Null)
                throw NullException();
            m_Label->VAlign(alignment);
        }

        Control::VerticalAlignment Button::ContentVAlign() const
        {
            if(m_Null)
                throw NullException();
            return m_Label->VAlign();
        }

        void Button::ContentHAlign(HorizontalAlignment alignment)
        {
            if(m_Null)
                throw NullException();
            m_Label->HAlign(alignment);
        }

        Control::HorizontalAlignment Button::ContentHAlign() const
        {
            if(m_Null)
                throw NullException();
            return m_Label->HAlign();
        }

        void Button::CornerRadius(double r)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Radius(r);
        }

        double Button::CornerRadius() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.Radius();
        }

        void Button::Offset(const Point& offset)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Offset(FPoint(offset));
        }

        Point Button::Offset() const
        {
            if(m_Null)
                throw NullException();
            return Point(m_Renderer.Offset());
        }

        void Button::RealSize(const class Size& size)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Size() = FSize(size);
        }

        Geometry::Size Button::RealSize() const
        {
            if(m_Null)
                throw NullException();
            return Geometry::Size(m_Renderer.Size());
        }

        void Button::Position(const Point& position)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Position(FPoint(position));
        }

        Point Button::Position() const
        {
            if(m_Null)
                throw NullException();
            return Point(m_Renderer.Position());
        }

        Renderer& Button::Renderer()
        {
            if(m_Null)
                throw NullException();
            return m_Renderer;
        }

        void Button::Value(const string& val)
        {
            m_Value = val;
        }

        const string& Button::Value() const
        {
            return m_Value;
        }

        void Button::MouseMove(const Object& sender, const MouseEventArgs& e)
        {
            Vector2 pos = Vector2(e.Position());
            for(const Control * c = m_Parent; c; c = c->Parent())
                pos -= Vector2(c->Position());

            RoundedRectangle rect(Vector2(m_Renderer.Position()) + m_Renderer.Offset(), m_Renderer.Size(), m_Renderer.Radius());
            if(rect.Intersects(pos))
                m_Renderer.Color(m_HoverColor);
            else
                m_Renderer.Color(m_NormalColor);
        }

        void Button::MouseClick(const Object& sender, const MouseEventArgs& e)
        {
            if(e.Button().first != MouseButton::LMB)
                return;
            Vector2 pos = Vector2(e.Position());
            for(const Control * c = m_Parent; c; c = c->Parent())
                pos -= Vector2(c->Position());

            RoundedRectangle rect(Vector2(m_Renderer.Position()) + m_Renderer.Offset(), m_Renderer.Size(), m_Renderer.Radius());
            if(rect.Intersects(pos))
            {
                m_Renderer.Color(m_ClickColor);
                OnClick(*this, null);
            }
        }

        void Button::MouseUp(const Object& sender, const MouseEventArgs& e)
        {
            if(e.Button().first != MouseButton::LMB)
                return;
            Vector2 pos = Vector2(e.Position());
            for(const Control * c = m_Parent; c; c = c->Parent())
                pos -= Vector2(c->Position());

            RoundedRectangle rect(Vector2(m_Renderer.Position()) + m_Renderer.Offset(), m_Renderer.Size(), m_Renderer.Radius());
            if(rect.Intersects(pos))
                m_Renderer.Color(m_HoverColor);
            else
                m_Renderer.Color(m_NormalColor);
        }

        template <>
        Button& Control::AddControl<Button>(Button&& control)
        {
            if(m_Null)
                throw NullException();
            control.Renderer().SetParent(Renderer());
            control.m_Parent = this;
            control.m_ReshapeHandler.Clear();
            OnReshape += control.m_ReshapeHandler;
            control.Reshape(*this, null);
            auto * ptr = new Button(move(control));
            m_Controls.push_back(ptr);
            return *ptr;
        }

        template <>
        Button& Application::AddControl<Button>(Button&& control)
        {
            control.Renderer().SetParent(s_UIRenderer);
            control.m_Parent = nullptr;
            control.m_ReshapeHandler.Clear();
            OnResize += control.m_ReshapeHandler;
            control.Reshape(null, null);
            auto * ptr = new Button(move(control));
            s_Controls.push_back(ptr);
            return *ptr;
        }
    }
}