#include <UI.h>

using namespace std;
using namespace Boris;
using namespace Boris::Geometry;
using namespace Boris::Graphics;
using namespace Boris::Rendering;

namespace Boris
{
    namespace UI
    {
        Label::Label(const string& content, double size, const Indentation& margin) :
            Control(),
            m_Renderer({}, content, Colors::Black)
        {
            m_Renderer.Size() = size;
            m_Size = { (unsigned int)(m_Renderer.Width()), (unsigned int)(size) };
            m_Margin = margin;

            Reshape(null, null);
        }

        Label::Label(nullptr_t) :
            Control(null),
            m_Renderer({})
        {}

        Label::Label(Label&& other) noexcept :
            Control(move(other)),
            m_Renderer(move(other.m_Renderer))
        {}

        void Label::Color(const Graphics::Color& color)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Color(color);
        }

        const Graphics::Color& Label::Color() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.Color();
        }

        void Label::Bold(bool bold)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Bold() = bold;
        }

        bool Label::Bold() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.Bold();
        }

        void Label::Mono(bool mono)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Mono() = mono;
        }

        bool Label::Mono() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.Mono();
        }

        void Label::Content(const string& text)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Text() = text;
        }

        const string& Label::Content() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.Text();
        }

        void Label::FontSize(double size)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Size() = size;
            m_Size = { (unsigned int)(m_Renderer.Width()), (unsigned int)(size) };
        }

        double Label::FontSize() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.Size();
        }

        void Label::Offset(const Point& offset)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Offset(FPoint(offset));
        }

        Point Label::Offset() const
        {
            if(m_Null)
                throw NullException();
            return Point(m_Renderer.Offset());
        }

        void Label::RealSize(const class Size& size)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Size() = size.Height();
        }

        Geometry::Size Label::RealSize() const
        {
            if(m_Null)
                throw NullException();
            return { (unsigned int)(m_Renderer.Width()), (unsigned int)(m_Renderer.Size()) };
        }

        void Label::Position(const class Point& position)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Position(FPoint(position));
        }

        Point Label::Position() const
        {
            if(m_Null)
                throw NullException();
            return Point(m_Renderer.Position());
        }

        Rendering::Renderer& Label::Renderer()
        {
            return m_Renderer;
        }

        template <>
        Label& Control::AddControl<Label>(Label&& control)
        {
            if(m_Null)
                throw NullException();
            control.Renderer().SetParent(Renderer());
            control.m_Parent = this;
            control.m_ReshapeHandler.Clear();
            OnReshape += control.m_ReshapeHandler;
            control.Reshape(*this, null);
            auto * ptr = new Label(move(control));
            m_Controls.push_back(ptr);
            return *ptr;
        }

        template <>
        Label& Application::AddControl<Label>(Label&& control)
        {
            control.Renderer().SetParent(s_UIRenderer);
            control.m_Parent = nullptr;
            control.m_ReshapeHandler.Clear();
            OnResize += control.m_ReshapeHandler;
            control.Reshape(null, null);
            auto * ptr = new Label(move(control));
            s_Controls.push_back(ptr);
            return *ptr;
        }
    }
}