#include <UI.h>
#include <Geometry.h>
#include <Graphics.h>
#include <Events.h>

#include <list>

using namespace std;
using namespace Boris::Events;
using namespace Boris::Geometry;
using namespace Boris::Graphics;

namespace Boris
{
    namespace UI
    {
        Control::Control() :
            Object(),
            m_ID(++s_ID),
            m_Position(),
            m_Size(),
            m_HAlign(HorizontalAlignment::LEFT),
            m_VAlign(VerticalAlignment::TOP),
            m_Margin(),
            m_Padding(),
            m_Controls(),
            m_Parent(nullptr),
            m_ReshapeHandler(&Control::Reshape, this)
        {}

        Control::Control(nullptr_t) :
            Object(null),
            m_ID(),
            m_Position(),
            m_Size(),
            m_HAlign(),
            m_VAlign(),
            m_Margin(),
            m_Padding(),
            m_Controls(),
            m_Parent(nullptr),
            m_ReshapeHandler()
        {}

        Control::Control(Control&& other) noexcept :
            Object(move(other)),
            m_ID(other.m_ID),
            m_Position(other.m_Position),
            m_Size(other.m_Size),
            m_HAlign(other.m_HAlign),
            m_VAlign(other.m_VAlign),
            m_Margin(other.m_Margin),
            m_Padding(other.m_Padding),
            m_Controls(move(other.m_Controls)),
            m_Parent(other.m_Parent),
            m_ReshapeHandler(move(other.m_ReshapeHandler)),
            OnReshape(move(other.OnReshape))
        {
            m_ReshapeHandler = bind(&Control::Reshape, this, placeholders::_1, placeholders::_2);
            for(auto it : m_Controls)
                it->m_Parent = this;
        }

        Control::~Control()
        {
            while(!m_Controls.empty())
            {
                delete m_Controls.front();
                m_Controls.pop_front();
            }
        }

        bool Control::operator==(const Control& other) const
        {
            return m_ID == other.m_ID;
        }

        bool Control::operator!=(const Control& other) const
        {
            return m_ID != other.m_ID;
        }

        unsigned int Control::ID() const
        {
            return m_ID;
        }

        void Control::Size(const class Size& size)
        {
            if(m_Null)
                throw NullException();
            m_Size = size;
            Reshape(null, null);
        }

        Geometry::Size Control::Size() const
        {
            if(m_Null)
                throw NullException();
            return RealSize();
        }

        void Control::HAlign(HorizontalAlignment alignment)
        {
            if(m_Null)
                throw NullException();
            m_HAlign = alignment;
            Reshape(null, null);
        }

        Control::HorizontalAlignment Control::HAlign() const
        {
            if(m_Null)
                throw NullException();
            return m_HAlign;
        }

        void Control::VAlign(VerticalAlignment alignment)
        {
            if(m_Null)
                throw NullException();
            m_VAlign = alignment;
            Reshape(null, null);
        }

        Control::VerticalAlignment Control::VAlign() const
        {
            if(m_Null)
                throw NullException();
            return m_VAlign;
        }

        void Control::Margin(const Indentation& margin)
        {
            if(m_Null)
                throw NullException();
            m_Margin = margin;
            Reshape(null, null);
        }

        const Control::Indentation& Control::Margin() const
        {
            if(m_Null)
                throw NullException();
            return m_Margin;
        }

        void Control::Padding(const Indentation& padding)
        {
            if(m_Null)
                throw NullException();
            m_Padding = padding;
            Reshape(null, null);
        }

        const Control::Indentation& Control::Padding() const
        {
            if(m_Null)
                throw NullException();
            return m_Padding;
        }

        template <typename C>
        C& Control::AddControl(C&& control)
        {
            if(m_Null)
                throw NullException();
            control.Renderer().SetParent(Renderer());
            control.m_Parent = this;
            control.m_ReshapeHandler.Clear();
            OnReshape += control.m_ReshapeHandler;
            control.Reshape(null, null);
            C * ptr = new C(forward(control));
            m_Controls.push_back(ptr);
            return *ptr;
        }

        Control Control::RemoveControl(const Control& control)
        {
            if(m_Null)
                throw NullException();
            for(auto it = m_Controls.begin(); it != m_Controls.end(); ++it)
            {
                if(**it == control)
                {
                    Control c = move(**it);
                    delete *it;
                    m_Controls.erase(it);
                    c.m_Parent = nullptr;
                    c.m_ReshapeHandler.Clear();
                    c.Renderer().UnsetParent();
                    return move(c);
                }
            }
            return null;
        }

        const list<Control *>& Control::Controls() const
        {
            if(m_Null)
                throw NullException();
            return m_Controls;
        }

        Control& Control::GetControl(unsigned int ID)
        {
            if(m_Null)
                throw NullException();
            for(auto& control : m_Controls)
                if(control->m_ID == ID)
                    return *control;

            throw runtime_error("No such control.");
        }

        const Control& Control::GetControl(unsigned int ID) const
        {
            if(m_Null)
                throw NullException();
            for(const auto& control : m_Controls)
                if(control->m_ID == ID)
                    return *control;

            throw runtime_error("No such control.");
        }

        const Control * Control::Parent() const
        {
            return m_Parent;
        }

        void Control::Reshape(const Object& sender, const EventArgs& e)
        {
            Point basePoint =
                    m_Parent ?
                    Point(m_Parent->Offset()) :
                    Point();
            Indentation parentPadding =
                    m_Parent ?
                    m_Parent->m_Padding :
                    Indentation();
            class Size parentSize =
                    m_Parent ?
                    m_Parent->Size() :
                    Application::Size();
            Point offset = {};

            basePoint.X() += m_Margin.Left - m_Margin.Right;
            basePoint.Y() += m_Margin.Bottom - m_Margin.Top;

            if(parentPadding.Left + parentPadding.Right > parentSize.Width())
                parentSize.Width(0);
            else
                parentSize.Width(parentSize.Width() - (parentPadding.Left + parentPadding.Right));

            if(parentPadding.Top + parentPadding.Bottom > parentSize.Height())
                parentSize.Height(0);
            else
                parentSize.Height(parentSize.Height() - (parentPadding.Top + parentPadding.Bottom));


            switch(m_VAlign)
            {
                case VerticalAlignment::TOP:
                    basePoint.Y() += parentPadding.Bottom + parentSize.Height();
                    offset.Y() = -int(m_Size.Height());
                    break;
                case VerticalAlignment::MIDDLE:
                    basePoint.Y() += parentPadding.Bottom + parentSize.Height() / 2;
                    offset.Y() = -int(m_Size.Height()) / 2;
                    break;
                case VerticalAlignment::BOTTOM:
                case VerticalAlignment::STRETCH:
                    basePoint.Y() += parentPadding.Bottom;
                    offset.Y() = 0;
                    break;
            }
            switch(m_HAlign)
            {
                case HorizontalAlignment::LEFT:
                case HorizontalAlignment::STRETCH:
                    basePoint.X() += parentPadding.Left;
                    offset.X() = 0;
                    break;
                case HorizontalAlignment::CENTER:
                    basePoint.X() += parentPadding.Left + parentSize.Width() / 2;
                    offset.X() = -int(m_Size.Width()) / 2;
                    break;
                case HorizontalAlignment::RIGHT:
                    basePoint.X() += parentPadding.Left + parentSize.Width();
                    offset.X() = -int(m_Size.Width());
                    break;
            }

            m_Position = basePoint;
            Position(m_Position);
            Offset(offset);

            RealSize(
                {
                    m_HAlign == HorizontalAlignment::STRETCH ?
                    parentSize.Width() :
                    m_Size.Width(),
                    m_VAlign == VerticalAlignment::STRETCH ?
                    parentSize.Height() :
                    m_Size.Height()
                }
            );

            OnReshape(*this, e);
        }

        unsigned int Control::s_ID = 0;
    }
}