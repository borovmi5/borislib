#include <UI.h>

using namespace std;
using namespace Boris::Events;
using namespace Boris::Geometry;
using namespace Boris::Graphics;

namespace Boris
{
    namespace UI
    {
        Panel::Panel(const class Size& size, const Indentation& margin) :
            Control(),
            m_Renderer({}, FSize(size), Colors::GhostWhite, Colors::Gainsboro, 1, LineType::SOLID)
        {
            m_Size = size;
            m_Margin = margin;

            Reshape(null, null);
        }

        Panel::Panel(nullptr_t) :
            Control(null),
            m_Renderer({})
        {}

        Panel::Panel(Panel&& other) noexcept :
            Control(move(other)),
            m_Renderer(move(other.m_Renderer))
        {}

        void Panel::Color(const class Color& color)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Color(color);
        }

        const Graphics::Color& Panel::Color() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.Color();
        }

        void Panel::BorderColor(const class Color& color)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.LineColor(color);
        }

        const Graphics::Color& Panel::BorderColor() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.LineColor();
        }

        void Panel::BorderStyle(const LineType& style)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.LineType(style);
        }

        const LineType& Panel::BorderStyle() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.LineType();
        }

        void Panel::BorderThickness(double thickness)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.LineWidth(thickness);
        }

        double Panel::BorderThickness() const
        {
            if(m_Null)
                throw NullException();
            return m_Renderer.LineWidth();
        }

        void Panel::Offset(const Point& offset)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Offset(FPoint(offset));
        }

        Point Panel::Offset() const
        {
            if(m_Null)
                throw NullException();
            return Point(m_Renderer.Offset());
        }

        void Panel::RealSize(const class Size& size)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Size(FSize(size));
        }

        Geometry::Size Panel::RealSize() const
        {
            if(m_Null)
                throw NullException();
            return { (unsigned int)(m_Renderer.Size().Width()), (unsigned int)(m_Renderer.Size().Height()) };
        }

        void Panel::Position(const Point& position)
        {
            if(m_Null)
                throw NullException();
            m_Renderer.Position(FPoint(position));
        }

        Point Panel::Position() const
        {
            if(m_Null)
                throw NullException();
            return Point(m_Renderer.Position());
        }

        Rendering::Renderer& Panel::Renderer()
        {
            if(m_Null)
                throw NullException();
            return m_Renderer;
        }

        template <>
        Panel& Control::AddControl<Panel>(Panel&& control)
        {
            if(m_Null)
                throw NullException();
            control.Renderer().SetParent(Renderer());
            control.m_Parent = this;
            control.m_ReshapeHandler.Clear();
            OnReshape += control.m_ReshapeHandler;
            control.Reshape(*this, null);
            auto * ptr = new Panel(move(control));
            m_Controls.push_back(ptr);
            return *ptr;
        }

        template <>
        Panel& Application::AddControl<Panel>(Panel&& control)
        {
            control.Renderer().SetParent(s_UIRenderer);
            control.m_Parent = nullptr;
            control.m_ReshapeHandler.Clear();
            OnResize += control.m_ReshapeHandler;
            control.Reshape(null, null);
            auto * ptr = new Panel(move(control));
            s_Controls.push_back(ptr);
            return *ptr;
        }
    }
}