#include <Controls.h>
#include <Graphics.h>
#include <UI.h>

#include <GL/freeglut.h>

using namespace std;
using namespace Boris::Controls;
using namespace Boris::Graphics;
using namespace Boris::Events;

namespace Boris
{
    namespace UI
    {
        void Application::Init(string name = "App")
        {
            s_AppName = move(name);

            // Init display
            int tmp = 0;
            glutInit(&tmp, nullptr);
            glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
            glutInitWindowPosition(-1, -1);
            glutInitWindowSize(800, 600);
            glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
            s_AppWin = glutCreateWindow(s_AppName.c_str());
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            s_WindowVisible = true;
            s_Fullscreen = false;
            s_LastTime = glutGet(GLUT_ELAPSED_TIME);
            glutDisplayFunc(display);
            glutOverlayDisplayFunc(overlay_display);
            glutIdleFunc(idle);
            glutReshapeFunc(resize);
            glutVisibilityFunc(visibility);


            // Init keyboard
            glutKeyboardFunc(Keys::KeyDown);
            glutKeyboardUpFunc(Keys::KeyUp);
            glutSpecialFunc(Keys::SpecialDown);
            glutSpecialUpFunc(Keys::SpecialUp);
            glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

            // Init mouse
            glutMouseFunc(Mouse::MouseButton);
            glutMotionFunc(Mouse::MouseMove);
            glutPassiveMotionFunc(Mouse::MouseMove);
            glutEntryFunc(Mouse::MouseEntry);
            Mouse::InitTimer();
        }

        void Application::Start()
        {
            s_Running = true;
            glutMainLoop();
            s_Running = false;
        }

        void Application::End(int exit_code)
        {
            glutExit();
        }

        void Application::CleanUp()
        {
            while(!s_Controls.empty())
            {
                delete s_Controls.front();
                s_Controls.pop_front();
            }
        }

        const string& Application::Title()
        {
            return s_AppName;
        }

        void Application::Title(const string& title)
        {
            glutSetWindowTitle(title.c_str());
            s_AppName = title;
        }

        Geometry::Size Application::Size()
        {
            if(!s_Running)
                return { (unsigned int)(glutGet(GLUT_INIT_WINDOW_WIDTH)), (unsigned int)(glutGet(GLUT_INIT_WINDOW_HEIGHT)) };
            return { (unsigned int)(glutGet(GLUT_WINDOW_WIDTH)), (unsigned int)(glutGet(GLUT_WINDOW_HEIGHT)) };
        }

        void Application::Size(const Geometry::Size& size)
        {
            glutReshapeWindow(size.Width(), size.Height());
        }

        Geometry::Point Application::Position()
        {
            if(!s_Running)
                return { glutGet(GLUT_INIT_WINDOW_X), glutGet(GLUT_INIT_WINDOW_HEIGHT) };
            return { glutGet(GLUT_WINDOW_X), glutGet(GLUT_WINDOW_Y) };
        }

        void Application::Position(const Geometry::Point& position)
        {
            glutPositionWindow(position.X(), position.Y());
        }

        bool Application::Visible()
        {
            return s_WindowVisible;
        }

        void Application::Visible(bool visible)
        {
            if(visible)
                glutShowWindow();
            else
                glutHideWindow();
        }

        bool Application::Fullscreen()
        {
            return s_Fullscreen;
        }

        void Application::Fullscreen(bool fullscreen)
        {
            if(fullscreen)
            {
                glutFullScreen();
                s_Fullscreen = true;
            }
            else
            {
                glutLeaveFullScreen();
                s_Fullscreen = false;
            }
        }

        template <typename C>
        C& Application::AddControl(C&& control)
        {
            control.Renderer().SetParent(s_UIRenderer);
            control.m_Parent = nullptr;
            control.m_ReshapeHandler.Clear();
            OnResize += control.m_ReshapeHandler;
            control.Reshape(null, null);
            C * ptr = new C(forward(control));
            s_Controls.push_back(ptr);
            return *ptr;
        }

        Control Application::RemoveControl(const Control& control)
        {
            for(auto it = s_Controls.begin(); it != s_Controls.end(); ++it)
            {
                if(**it == control)
                {
                    Control c = move(**it);
                    delete *it;
                    s_Controls.erase(it);
                    c.m_Parent = nullptr;
                    c.m_ReshapeHandler.Clear();
                    c.Renderer().UnsetParent();
                    return move(c);
                }
            }
            return null;
        }

        const list<Control *>& Application::Controls()
        {
            return s_Controls;
        }

        Control& Application::GetControl(unsigned int ID)
        {
            for(auto& control : s_Controls)
                if(control->m_ID == ID)
                    return *control;

            throw runtime_error("No such control.");
        }

        BasicEvent Application::OnDisplay;
        TimePassedEvent Application::OnIdle;
        BasicEvent Application::OnResize;

        void Application::display()
        {
            glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
            glMatrixMode(GL_PROJECTION);

            glPushMatrix();
            glLoadIdentity();
            glTranslated(-1, -1, 0); // Origin 0, 0 in bottom left corner.
            // Width and height equal to size in pixels.
            glScaled(2. / Application::Size().Width(), 2. / Application::Size().Height(), 1);

            OnDisplay(null, null);

            s_UIRenderer.Render(null, null);

            glPopMatrix();

            glutSwapBuffers();
        }

        void Application::overlay_display()
        {}

        void Application::idle()
        {
            glutPostRedisplay();
            int time = glutGet(GLUT_ELAPSED_TIME);

            OnIdle(null, TimePassedEventArgs(time - s_LastTime));

            s_LastTime = time;
        }

        void Application::resize(int width, int height)
        {
            glViewport(0, 0, width, height);
            OnResize(null, null);
        }

        void Application::visibility(int state)
        {}

        string Application::s_AppName;
        int Application::s_AppWin;
        bool Application::s_WindowVisible;
        bool Application::s_Fullscreen;
        int Application::s_LastTime;
        Rendering::PointRenderer Application::s_UIRenderer = { {}, 0x0000_argb_short };
        list<Control *> Application::s_Controls;
        bool Application::s_Running = false;
    }
}