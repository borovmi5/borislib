#include <Controls/MouseEventArgs.h>

#include <Geometry/Point.h>
#include <Controls.h>

using namespace std;
using namespace Boris::Controls;
using namespace Boris::Geometry;

namespace Boris
{
    namespace Events
    {
        MouseEventArgs::MouseEventArgs(std::nullptr_t) :
            EventArgs(null),
            m_Position(),
            m_Button(),
            m_State()
        {}

        MouseEventArgs::MouseEventArgs(const Point& pos) :
            m_Position(pos),
            m_Button(MouseButton::UNKNOWN),
            m_State(false)
        {}

        MouseEventArgs::MouseEventArgs(const Point& pos, const MouseButton& button, bool state) :
                m_Position(pos),
                m_Button(button),
                m_State(state)
        {}

        const Geometry::Point& MouseEventArgs::Position() const
        {
            return m_Position;
        }

        pair<MouseButton, bool> MouseEventArgs::Button() const
        {
            return { m_Button, m_State };
        }
    }
}