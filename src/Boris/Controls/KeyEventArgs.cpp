#include <Controls.h>

using namespace Boris::Controls;

namespace Boris
{
    namespace Events
    {
        KeyEventArgs::KeyEventArgs(std::nullptr_t) :
            EventArgs(null),
            m_Char('\0'),
            m_Key(Controls::Key::UNKNOWN),
            m_Modifiers(0x0)
        {}

        KeyEventArgs::KeyEventArgs(unsigned char keyChar, Controls::Key keyCode, int modifiers) :
            EventArgs(),
            m_Char(keyChar),
            m_Key(keyCode),
            m_Modifiers(modifiers)
        {}

        Key KeyEventArgs::Key() const
        {
            return m_Key;
        }

        char KeyEventArgs::KeyChar() const
        {
            return m_Char;
        }

        bool KeyEventArgs::Shift() const
        {
            return bool(m_Modifiers & 0x1);
        }

        bool KeyEventArgs::Control() const
        {
            return bool(m_Modifiers & 0x2);
        }

        bool KeyEventArgs::Alt() const
        {
            return bool(m_Modifiers & 0x4);
        }
    }
}