#include <Core.h>
#include <Controls.h>

#include <GL/freeglut.h>

using namespace Boris::Events;

namespace Boris
{
    namespace Controls
    {
        bool Keys::IsDown(Key key)
        {
            return s_Keys[(unsigned char)(key)];
        }

        bool Keys::IsUp(Key key)
        {
            return !s_Keys[(unsigned char)(key)];
        }

        void Keys::KeyDown(unsigned char c, int x, int y)
        {
            char keyChar = c;
            int mods = glutGetModifiers();

            if(mods & GLUT_ACTIVE_CTRL)
            {
                if(c >= 1 && c < 30)
                    c += 'A' - 1;
            }

            Key code = Key::UNKNOWN;
            switch(c)
            {
                case '0':
                case ')':
                    code = Key::_0;
                    break;
                case '1':
                case '!':
                    code = Key::_1;
                    break;
                case '2':
                case '@':
                    code = Key::_2;
                    break;
                case '3':
                case '#':
                    code = Key::_3;
                    break;
                case '4':
                case '$':
                    code = Key::_4;
                    break;
                case '5':
                case '%':
                    code = Key::_5;
                    break;
                case '6':
                case '^':
                    code = Key::_6;
                    break;
                case '7':
                case '&':
                    code = Key::_7;
                    break;
                case '8':
                case '*':
                    code = Key::_8;
                    break;
                case '9':
                case '(':
                    code = Key::_9;
                    break;
                case 'a':
                case 'A':
                    code = Key::A;
                    break;
                case 'b':
                case 'B':
                    code = Key::B;
                    break;
                case 'c':
                case 'C':
                    code = Key::C;
                    break;
                case 'd':
                case 'D':
                    code = Key::D;
                    break;
                case 'e':
                case 'E':
                    code = Key::E;
                    break;
                case 'f':
                case 'F':
                    code = Key::F;
                    break;
                case 'g':
                case 'G':
                    code = Key::G;
                    break;
                case 'h':
                case 'H':
                    code = Key::H;
                    break;
                case 'i':
                case 'I':
                    code = Key::I;
                    break;
                case 'j':
                case 'J':
                    code = Key::J;
                    break;
                case 'k':
                case 'K':
                    code = Key::K;
                    break;
                case 'l':
                case 'L':
                    code = Key::L;
                    break;
                case 'm':
                case 'M':
                    code = Key::M;
                    break;
                case 'n':
                case 'N':
                    code = Key::N;
                    break;
                case 'o':
                case 'O':
                    code = Key::O;
                    break;
                case 'p':
                case 'P':
                    code = Key::P;
                    break;
                case 'q':
                case 'Q':
                    code = Key::Q;
                    break;
                case 'r':
                case 'R':
                    code = Key::R;
                    break;
                case 's':
                case 'S':
                    code = Key::S;
                    break;
                case 't':
                case 'T':
                    code = Key::T;
                    break;
                case 'u':
                case 'U':
                    code = Key::U;
                    break;
                case 'v':
                case 'V':
                    code = Key::V;
                    break;
                case 'w':
                case 'W':
                    code = Key::W;
                    break;
                case 'x':
                case 'X':
                    code = Key::X;
                    break;
                case 'y':
                case 'Y':
                    code = Key::Y;
                    break;
                case 'z':
                case 'Z':
                    code = Key::Z;
                    break;
                case '[':
                case '{':
                    code = Key::LBRACK;
                    break;
                case '\\':
                case '|':
                    code = Key::BACKSLASH;
                    break;
                case ']':
                case '}':
                    code = Key::RBRACK;
                    break;
                case ',':
                case '<':
                    code = Key::COMA;
                    break;
                case '-':
                case '_':
                    code = Key::DASH;
                    break;
                case '.':
                case '>':
                    code = Key::DOT;
                    break;
                case '/':
                case '?':
                    code = Key::SLASH;
                    break;
                case ';':
                case ':':
                    code = Key::SEMI;
                    break;
                case '\'':
                case '\"':
                    code = Key::QUOT;
                    break;
                case '=':
                case '+':
                    code = Key::EQUAL;
                    break;
                case '`':
                case '~':
                    code = Key::BACKTICK;
                    break;
                case ' ':
                    code = Key::SPACE;
                    break;
                case '\t':
                    code = Key::TAB;
                    break;
                case '\r':
                    code = Key::ENTER;
                    break;
                case '\x1B':
                    code = Key::ESC;
                    break;
                case '\x7F':
                    code = Key::DELETE;
                    break;
                case '\x08':
                    code = Key::BACKSPACE;
                    break;
                default:
                    code = Key::UNKNOWN;
                    break;
            }
            s_Keys[(unsigned char)(code)] = true;
            OnKeyDown(null, KeyEventArgs(keyChar, code, mods));
        }

        void Keys::KeyUp(unsigned char c, int x, int y)
        {
            char keyChar = c;
            int mods = glutGetModifiers();

            if(mods & GLUT_ACTIVE_CTRL)
            {
                if(c >= 1 && c < 30)
                    c += 'A' - 1;
            }

            Key code = Key::UNKNOWN;
            switch(c)
            {
                case '0':
                case ')':
                    code = Key::_0;
                    break;
                case '1':
                case '!':
                    code = Key::_1;
                    break;
                case '2':
                case '@':
                    code = Key::_2;
                    break;
                case '3':
                case '#':
                    code = Key::_3;
                    break;
                case '4':
                case '$':
                    code = Key::_4;
                    break;
                case '5':
                case '%':
                    code = Key::_5;
                    break;
                case '6':
                case '^':
                    code = Key::_6;
                    break;
                case '7':
                case '&':
                    code = Key::_7;
                    break;
                case '8':
                case '*':
                    code = Key::_8;
                    break;
                case '9':
                case '(':
                    code = Key::_9;
                    break;
                case 'a':
                case 'A':
                    code = Key::A;
                    break;
                case 'b':
                case 'B':
                    code = Key::B;
                    break;
                case 'c':
                case 'C':
                    code = Key::C;
                    break;
                case 'd':
                case 'D':
                    code = Key::D;
                    break;
                case 'e':
                case 'E':
                    code = Key::E;
                    break;
                case 'f':
                case 'F':
                    code = Key::F;
                    break;
                case 'g':
                case 'G':
                    code = Key::G;
                    break;
                case 'h':
                case 'H':
                    code = Key::H;
                    break;
                case 'i':
                case 'I':
                    code = Key::I;
                    break;
                case 'j':
                case 'J':
                    code = Key::J;
                    break;
                case 'k':
                case 'K':
                    code = Key::K;
                    break;
                case 'l':
                case 'L':
                    code = Key::L;
                    break;
                case 'm':
                case 'M':
                    code = Key::M;
                    break;
                case 'n':
                case 'N':
                    code = Key::N;
                    break;
                case 'o':
                case 'O':
                    code = Key::O;
                    break;
                case 'p':
                case 'P':
                    code = Key::P;
                    break;
                case 'q':
                case 'Q':
                    code = Key::Q;
                    break;
                case 'r':
                case 'R':
                    code = Key::R;
                    break;
                case 's':
                case 'S':
                    code = Key::S;
                    break;
                case 't':
                case 'T':
                    code = Key::T;
                    break;
                case 'u':
                case 'U':
                    code = Key::U;
                    break;
                case 'v':
                case 'V':
                    code = Key::V;
                    break;
                case 'w':
                case 'W':
                    code = Key::W;
                    break;
                case 'x':
                case 'X':
                    code = Key::X;
                    break;
                case 'y':
                case 'Y':
                    code = Key::Y;
                    break;
                case 'z':
                case 'Z':
                    code = Key::Z;
                    break;
                case '[':
                case '{':
                    code = Key::LBRACK;
                    break;
                case '\\':
                case '|':
                    code = Key::BACKSLASH;
                    break;
                case ']':
                case '}':
                    code = Key::RBRACK;
                    break;
                case ',':
                case '<':
                    code = Key::COMA;
                    break;
                case '-':
                case '_':
                    code = Key::DASH;
                    break;
                case '.':
                case '>':
                    code = Key::DOT;
                    break;
                case '/':
                case '?':
                    code = Key::SLASH;
                    break;
                case ';':
                case ':':
                    code = Key::SEMI;
                    break;
                case '\'':
                case '\"':
                    code = Key::QUOT;
                    break;
                case '=':
                case '+':
                    code = Key::EQUAL;
                    break;
                case '`':
                case '~':
                    code = Key::BACKTICK;
                    break;
                case ' ':
                    code = Key::SPACE;
                    break;
                case '\t':
                    code = Key::TAB;
                    break;
                case '\r':
                    code = Key::ENTER;
                    break;
                case '\x1B':
                    code = Key::ESC;
                    break;
                case '\x7F':
                    code = Key::DELETE;
                    break;
                case '\x08':
                    code = Key::BACKSPACE;
                    break;
                default:
                    code = Key::UNKNOWN;
                    break;
            }
            s_Keys[(unsigned char)(code)] = false;
            OnKeyUp(null, KeyEventArgs(keyChar, code, mods));
            if( code != Key::DELETE
                && code != Key::ESC
                && code != Key::UNKNOWN
                && code != Key::TAB
                && !bool(mods & GLUT_ACTIVE_CTRL))
                OnKeyPress(null, KeyEventArgs(keyChar, code, mods));
        }

        void Keys::SpecialDown(int c, int x, int y)
        {
            char keyChar = '\0';
            int mods = glutGetModifiers();

            Key code = Key::UNKNOWN;
            switch(c)
            {
                case GLUT_KEY_F1:
                case GLUT_KEY_F2:
                case GLUT_KEY_F3:
                case GLUT_KEY_F4:
                case GLUT_KEY_F5:
                case GLUT_KEY_F6:
                case GLUT_KEY_F7:
                case GLUT_KEY_F8:
                case GLUT_KEY_F9:
                case GLUT_KEY_F10:
                case GLUT_KEY_F11:
                case GLUT_KEY_F12:
                    code = Key(c - GLUT_KEY_F1 + int(Key::F1));
                    break;
                case GLUT_KEY_LEFT:
                case GLUT_KEY_UP:
                case GLUT_KEY_RIGHT:
                case GLUT_KEY_DOWN:
                case GLUT_KEY_PAGE_UP:
                case GLUT_KEY_PAGE_DOWN:
                case GLUT_KEY_HOME:
                case GLUT_KEY_END:
                case GLUT_KEY_INSERT:
                    code = Key(c - GLUT_KEY_LEFT + int(Key::LEFT));
                    break;
                case 109:
                    code = Key::NUM_LOCK;
                    break;
                case 112:
                    code = Key::LSHIFT;
                    break;
                case 113:
                    code = Key::RSHIFT;
                    break;
                case 114:
                    code = Key::LCTRL;
                    break;
                case 115:
                    code = Key::RCTRL;
                    break;
                case 116:
                    code = Key::LALT;
                    break;
                case 117:
                    code = Key::RALT;
                    break;
                default:
                    code = Key::UNKNOWN;
                    break;
            }
            s_Keys[(unsigned char)(code)] = true;
            OnKeyDown(null, KeyEventArgs(keyChar, code, mods));
        }

        void Keys::SpecialUp(int c, int x, int y)
        {
            char keyChar = '\0';
            int mods = glutGetModifiers();

            Key code = Key::UNKNOWN;
            switch(c)
            {
                case GLUT_KEY_F1:
                case GLUT_KEY_F2:
                case GLUT_KEY_F3:
                case GLUT_KEY_F4:
                case GLUT_KEY_F5:
                case GLUT_KEY_F6:
                case GLUT_KEY_F7:
                case GLUT_KEY_F8:
                case GLUT_KEY_F9:
                case GLUT_KEY_F10:
                case GLUT_KEY_F11:
                case GLUT_KEY_F12:
                    code = Key(c - GLUT_KEY_F1 + int(Key::F1));
                    break;
                case GLUT_KEY_LEFT:
                case GLUT_KEY_UP:
                case GLUT_KEY_RIGHT:
                case GLUT_KEY_DOWN:
                case GLUT_KEY_PAGE_UP:
                case GLUT_KEY_PAGE_DOWN:
                case GLUT_KEY_HOME:
                case GLUT_KEY_END:
                case GLUT_KEY_INSERT:
                    code = Key(c - GLUT_KEY_LEFT + int(Key::LEFT));
                    break;
                case 109:
                    code = Key::NUM_LOCK;
                    break;
                case 112:
                    code = Key::LSHIFT;
                    break;
                case 113:
                    code = Key::RSHIFT;
                    break;
                case 114:
                    code = Key::LCTRL;
                    break;
                case 115:
                    code = Key::RCTRL;
                    break;
                case 116:
                    code = Key::LALT;
                    break;
                case 117:
                    code = Key::RALT;
                    break;
                default:
                    code = Key::UNKNOWN;
                    break;
            }
            s_Keys[(unsigned char)(code)] = false;
            OnKeyUp(null, KeyEventArgs(keyChar, code, mods));
        }

        bool Keys::s_Keys[UINT8_MAX];

        Events::KeyEvent Keys::OnKeyDown;
        Events::KeyEvent Keys::OnKeyUp;
        Events::KeyEvent Keys::OnKeyPress;
    }
}