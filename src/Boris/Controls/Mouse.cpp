#include <Controls.h>
#include <Core.h>
#include <Geometry/Point.h>
#include <Utility/Timer.h>

#include <GL/freeglut.h>

using namespace std;
using namespace Boris::Events;
using namespace Boris::Geometry;
using namespace Boris::Utility;

namespace Boris
{
    namespace Controls
    {
        bool Mouse::IsPressed(Controls::MouseButton button)
        {
            return s_Buttons[static_cast<int>(button)];
        }

        bool Mouse::IsReleased(Controls::MouseButton button)
        {
            return !s_Buttons[static_cast<int>(button)];
        }

        void Mouse::SetCursor(Controls::CursorType cursor)
        {
            glutSetCursor(static_cast<int>(cursor));
        }

        void Mouse::InitTimer()
        {
            s_Timer.OnTimeElapsed -= s_TimerHandler;
            s_Timer.OnTimeElapsed += s_TimerHandler;
        }

        void Mouse::MouseButton(int button, int state, int x, int y)
        {
            ++button;
            if(button > 9 || button < 0)
                button = 0;
            auto b = Controls::MouseButton(button);
            auto s = !bool(state);

            s_Buttons[button] = s;
            s_Position = { x, int(UI::Application::Size().Height()) - y };

            if(b != Controls::MouseButton::SCR_UP
               && b != Controls::MouseButton::SCR_DN)
            {
                if(s)
                {
                    OnMouseDown(null, MouseEventArgs(s_Position, b, s));
                    if((glutGet(GLUT_ELAPSED_TIME) - s_Time > 500) || s_Last != b || s_DoubleClicked)
                    {
                        OnMouseClick(null, MouseEventArgs(s_Position, b, s));
                        s_DoubleClicked = false;
                    }
                    else
                    {
                        OnMouseDoubleClick(null, MouseEventArgs(s_Position, b, s));
                        s_DoubleClicked = true;
                    }
                }
                else
                {
                    if(b == Controls::MouseButton::LMB && s_Drag)
                    {
                        s_Drag = false;
                        OnMouseDrop(null, MouseEventArgs(s_Position));
                    }
                    OnMouseUp(null, MouseEventArgs(s_Position, b, s));
                }

                s_Last = b;
                s_Time = glutGet(GLUT_ELAPSED_TIME);
            }
            else
            {
                if(b == Controls::MouseButton::SCR_UP)
                    OnScrollUp(null, MouseEventArgs(s_Position));
                else
                    OnScrollDown(null, MouseEventArgs(s_Position));
            }
        }

        void Mouse::MouseMove(int x, int y)
        {
            s_Position = { x, int(UI::Application::Size().Height()) - y };
            s_Timer.Restart();

            if(s_Buttons[int(Controls::MouseButton::LMB)] && !s_Drag)
            {
                s_Drag = true;
                OnMouseDrag(null, MouseEventArgs(s_Position));
            }

            OnMouseMove(null, MouseEventArgs(s_Position));
        }

        void Mouse::MouseEntry(int state)
        {
            if(state == GLUT_ENTERED)
            {
                s_Timer.Start();
                OnMouseEnter(null, null);
            }
            else
            {
                s_Timer.Stop();
                OnMouseLeave(null, null);
            }
        }

        void Mouse::MouseHover(const Boris::Object& sender, const Boris::Events::EventArgs& e)
        {
            OnMouseHover(null, MouseEventArgs(s_Position));
        }

        Point Mouse::s_Position = { 0, 0 };
        bool Mouse::s_Buttons[10];
        Controls::MouseButton Mouse::s_Last = Controls::MouseButton::UNKNOWN;
        int Mouse::s_Time = 0;
        bool Mouse::s_DoubleClicked = false;
        bool Mouse::s_Drag = false;

        Timer Mouse::s_Timer(250, Timer::TimerType::TIMER);
        BasicEventHandler Mouse::s_TimerHandler(Mouse::MouseHover);

        MouseEvent Mouse::OnMouseDown;
        MouseEvent Mouse::OnMouseUp;
        MouseEvent Mouse::OnMouseClick;
        MouseEvent Mouse::OnMouseDoubleClick;
        BasicEvent Mouse::OnMouseEnter;
        MouseEvent Mouse::OnMouseMove;
        MouseEvent Mouse::OnMouseHover;
        BasicEvent Mouse::OnMouseLeave;
        MouseEvent Mouse::OnScrollUp;
        MouseEvent Mouse::OnScrollDown;
        MouseEvent Mouse::OnMouseDrag;
        MouseEvent Mouse::OnMouseDrop;
    }
}