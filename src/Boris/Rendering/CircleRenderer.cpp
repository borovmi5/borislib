#include <Rendering.h>
#include <Geometry.h>
#include <Core.h>
#include <Graphics.h>

#include <GL/freeglut.h>

#include <cmath>

using namespace std;
using namespace Boris::Geometry;
using namespace Boris::Graphics;

namespace Boris
{
    namespace Rendering
    {
        CircleRenderer::CircleRenderer(const FPoint& position, double radius, const FPoint& offset) :
                Renderer(position, offset),
                m_Circle(offset, radius)
        {
            m_OutlineType = LineType::BLANK;
        }

        CircleRenderer::CircleRenderer(const FPoint& position, double radius, const Graphics::Color& color, const FPoint& offset) :
                Renderer(position, offset),
                m_Circle(offset, radius)
        {
            m_Color = color;
            m_OutlineType = LineType::BLANK;
        }

        CircleRenderer::CircleRenderer(const FPoint& position, double radius, const Graphics::Color& color, const Graphics::Color& outline, double outline_width, const Graphics::LineType& type, const FPoint& offset) :
                Renderer(position, offset),
                m_Circle(offset, radius)
        {
            m_Color = color;
            m_OutlineType = type;
            m_OutlineWidth = outline_width;
            m_OutlineColor = outline;
        }

        CircleRenderer::CircleRenderer(nullptr_t) :
                Renderer(null),
                m_Circle(null)
        {}

        void CircleRenderer::Render(const Object& sender, const Events::EventArgs& e) const
        {
            if(m_Null)
                throw NullException();

            glPushMatrix();
            glTranslated(m_Position.X(), m_Position.Y(), 0);
            glRotated(m_Rotation, 0, 0, 1);
            if(m_Color != Colors::Transparent)
            {
                glBegin(GL_TRIANGLE_FAN);
                glColor(m_Color);
                for(int i = 0; i < s_CircleSlices; ++i)
                    glVertex(m_Circle.Radius() * Vector2{ sin(2. * M_PI * i / s_CircleSlices), cos(2. * M_PI * i / s_CircleSlices) } + Vector2(m_Circle.Center()) + Vector2{ m_Circle.Radius(), m_Circle.Radius() });
                glEnd();
            }
            if(m_OutlineType != Graphics::LineType::BLANK && m_OutlineWidth > 0 && m_OutlineColor != Colors::Transparent)
            {
                glLineWidth(m_OutlineWidth);
                glLineStipple(int(m_OutlineWidth), uint16_t(m_OutlineType));
                glEnable(GL_LINE_STIPPLE);
                glBegin(GL_LINE_LOOP);
                glColor(m_OutlineColor);
                for(int i = 0; i < s_CircleSlices; ++i)
                    glVertex(m_Circle.Radius() * Vector2{ sin(2. * M_PI * i / s_CircleSlices), cos(2. * M_PI * i / s_CircleSlices) } + Vector2(m_Circle.Center()) + Vector2{ m_Circle.Radius(), m_Circle.Radius() });
                glEnd();
                glDisable(GL_LINE_STIPPLE);
            }

            double scale_x = m_InnerSize.Width() > 0 ? 1. / m_InnerSize.Width() : 1.;
            double scale_y = m_InnerSize.Height() > 0 ? 1. / m_InnerSize.Height() : 1.;

            glTranslated(m_InnerOffset.X(), m_InnerOffset.Y(), 0);
            glScaled(scale_x, scale_y, 1);

            OnRender(*this, null);

            glPopMatrix();
        }

        void CircleRenderer::Offset(const FPoint& offset)
        {
            if(m_Null)
                throw NullException();
            m_Circle.Center() = offset;
            m_Offset = offset;
        }

        double CircleRenderer::Radius()
        {
            return m_Circle.Radius();
        }

        void CircleRenderer::Radius(double radius)
        {
            if(m_Null)
                throw NullException();
            m_Circle.Radius() = radius;
        }
    }
}