#include <Rendering.h>
#include <Geometry.h>
#include <Core.h>
#include <Graphics.h>
#include <cmath>

#include <GL/freeglut.h>

using namespace std;
using namespace Boris::Geometry;
using namespace Boris::Graphics;

namespace Boris
{
    namespace Rendering
    {
        RoundedRectangleRenderer::RoundedRectangleRenderer(const FPoint& position, const FSize& size, double radius, const FPoint& offset) :
                Renderer(position, offset),
                m_RoundedRectangle(offset, size, radius)
        {
            m_OutlineType = LineType::BLANK;
        }

        RoundedRectangleRenderer::RoundedRectangleRenderer(const FPoint& position, const FSize& size, double radius, const Graphics::Color& color, const FPoint& offset) :
                Renderer(position, offset),
                m_RoundedRectangle(offset, size, radius)
        {
            m_Color = color;
            m_OutlineType = LineType::BLANK;
        }

        RoundedRectangleRenderer::RoundedRectangleRenderer(const FPoint& position, const FSize& size, double radius, const Graphics::Color& color, const Graphics::Color& outline, double outline_width, const Graphics::LineType& type, const FPoint& offset) :
                Renderer(position, offset),
                m_RoundedRectangle(offset, size, radius)
        {
            m_Color = color;
            m_OutlineType = type;
            m_OutlineWidth = outline_width;
            m_OutlineColor = outline;
        }

        RoundedRectangleRenderer::RoundedRectangleRenderer(nullptr_t) :
                Renderer(null),
                m_RoundedRectangle(null)
        {}

        void RoundedRectangleRenderer::Render(const Object& sender, const Events::EventArgs& e) const
        {
            if(m_Null)
                throw NullException();

            double realRadius = min(min(m_RoundedRectangle.Size().Width(), m_RoundedRectangle.Size().Height()) / 2., m_RoundedRectangle.Radius());
            if(m_Color != Colors::Transparent)
            {
                glPushMatrix();
                glTranslated(m_Position.X(), m_Position.Y(), 0);
                glRotated(m_Rotation, 0, 0, 1);
                glBegin(GL_TRIANGLE_FAN);
                    glColor(m_Color);
                    glVertex(Vector2(m_RoundedRectangle.UpperRight()) + Vector2{ -realRadius, 0 });
                    for(int i = 0; i < s_CircleSlices / 4; ++i)
                        glVertex(Vector2(m_RoundedRectangle.UpperRight()) + Vector2{-realRadius, realRadius} + realRadius * Vector2{ sin( M_PI / 2. * i / s_CircleSlices * 4.), -cos(M_PI / 2. * i / s_CircleSlices * 4.) });
                    glVertex(Vector2(m_RoundedRectangle.UpperRight()) + Vector2{ 0, realRadius });
                    glVertex(Vector2(m_RoundedRectangle.LowerRight()) + Vector2{ 0, -realRadius });
                    for(int i = s_CircleSlices / 4 - 1; i >= 0; --i)
                        glVertex(Vector2(m_RoundedRectangle.LowerRight()) + Vector2{-realRadius, -realRadius} + realRadius * Vector2{ sin( M_PI / 2. * i / s_CircleSlices * 4.), cos(M_PI / 2. * i / s_CircleSlices * 4.) });
                    glVertex(Vector2(m_RoundedRectangle.LowerRight()) + Vector2{ -realRadius, 0 });
                    glVertex(Vector2(m_RoundedRectangle.LowerLeft()) + Vector2{ realRadius, 0 });
                    for(int i = 0; i < s_CircleSlices / 4; ++i)
                        glVertex(Vector2(m_RoundedRectangle.LowerLeft()) + Vector2{realRadius, -realRadius} + realRadius * Vector2{ -sin( M_PI / 2. * i / s_CircleSlices * 4.), cos(M_PI / 2. * i / s_CircleSlices * 4.) });
                    glVertex(Vector2(m_RoundedRectangle.LowerLeft()) + Vector2{ 0, -realRadius });
                    glVertex(Vector2(m_RoundedRectangle.UpperLeft()) + Vector2{ 0, realRadius });
                    for(int i = s_CircleSlices / 4 - 1; i >= 0; --i)
                        glVertex(Vector2(m_RoundedRectangle.UpperLeft()) + Vector2{realRadius, realRadius} + realRadius * Vector2{ -sin( M_PI / 2. * i / s_CircleSlices * 4.), -cos(M_PI / 2. * i / s_CircleSlices * 4.) });
                    glVertex(Vector2(m_RoundedRectangle.UpperLeft()) + Vector2{ realRadius, 0 });
                glEnd();
            }
            if(m_OutlineType != Graphics::LineType::BLANK && m_OutlineWidth > 0 && m_OutlineColor != Colors::Transparent)
            {
                glLineWidth(m_OutlineWidth);
                glLineStipple(int(m_OutlineWidth), uint16_t(m_OutlineType));
                glEnable(GL_LINE_STIPPLE);
                glBegin(GL_LINE_LOOP);
                    glColor(m_OutlineColor);
                    glVertex(Vector2(m_RoundedRectangle.UpperRight()) + Vector2{ -realRadius, 0 });
                    for(int i = 0; i < s_CircleSlices / 4; ++i)
                        glVertex(Vector2(m_RoundedRectangle.UpperRight()) + Vector2{-realRadius, realRadius} + realRadius * Vector2{ sin( M_PI / 2. * i / s_CircleSlices * 4.), -cos(M_PI / 2. * i / s_CircleSlices * 4.) });
                    glVertex(Vector2(m_RoundedRectangle.UpperRight()) + Vector2{ 0, realRadius });
                    glVertex(Vector2(m_RoundedRectangle.LowerRight()) + Vector2{ 0, -realRadius });
                    for(int i = s_CircleSlices / 4 - 1; i >= 0; --i)
                        glVertex(Vector2(m_RoundedRectangle.LowerRight()) + Vector2{-realRadius, -realRadius} + realRadius * Vector2{ sin( M_PI / 2. * i / s_CircleSlices * 4.), cos(M_PI / 2. * i / s_CircleSlices * 4.) });
                    glVertex(Vector2(m_RoundedRectangle.LowerRight()) + Vector2{ -realRadius, 0 });
                    glVertex(Vector2(m_RoundedRectangle.LowerLeft()) + Vector2{ realRadius, 0 });
                    for(int i = 0; i < s_CircleSlices / 4; ++i)
                        glVertex(Vector2(m_RoundedRectangle.LowerLeft()) + Vector2{realRadius, -realRadius} + realRadius * Vector2{ -sin( M_PI / 2. * i / s_CircleSlices * 4.), cos(M_PI / 2. * i / s_CircleSlices * 4.) });
                    glVertex(Vector2(m_RoundedRectangle.LowerLeft()) + Vector2{ 0, -realRadius });
                    glVertex(Vector2(m_RoundedRectangle.UpperLeft()) + Vector2{ 0, realRadius });
                    for(int i = s_CircleSlices / 4 - 1; i >= 0; --i)
                        glVertex(Vector2(m_RoundedRectangle.UpperLeft()) + Vector2{realRadius, realRadius} + realRadius * Vector2{ -sin( M_PI / 2. * i / s_CircleSlices * 4.), -cos(M_PI / 2. * i / s_CircleSlices * 4.) });
                    glVertex(Vector2(m_RoundedRectangle.UpperLeft()) + Vector2{ realRadius, 0 });
                glEnd();
                glDisable(GL_LINE_STIPPLE);
            }

            double scale_x = m_InnerSize.Width() > 0 ? 1. / m_InnerSize.Width() : 1.;
            double scale_y = m_InnerSize.Height() > 0 ? 1. / m_InnerSize.Height() : 1.;

            glTranslated(m_InnerOffset.X(), m_InnerOffset.Y(), 0);
            glScaled(scale_x, scale_y, 1);

            OnRender(*this, null);

            glPopMatrix();
        }

        void RoundedRectangleRenderer::Offset(const FPoint& offset)
        {
            if(m_Null)
                throw NullException();
            m_RoundedRectangle.Point() = offset;
            m_Offset = offset;
        }

        const FSize& RoundedRectangleRenderer::Size() const
        {
            return m_RoundedRectangle.Size();
        }

        FSize& RoundedRectangleRenderer::Size()
        {
            return m_RoundedRectangle.Size();
        }

        double RoundedRectangleRenderer::Radius() const
        {
            return m_RoundedRectangle.Radius();
        }

        void RoundedRectangleRenderer::Radius(double radius)
        {
            m_RoundedRectangle.Radius(radius);
        }
    }
}