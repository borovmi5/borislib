#include <Rendering.h>
#include <Core.h>
#include <Events.h>
#include <Geometry.h>
#include <Graphics.h>

#include <cmath>

using namespace std;
using namespace Boris::Geometry;
using namespace Boris::Graphics;
using namespace Boris::Events;

namespace Boris
{
    namespace Rendering
    {
        Renderer::Renderer(const FPoint& position, const FPoint& offset, const FSize& innersize, double rotation) :
            Object(),
            m_RenderHandler(&Renderer::Render, this),
            m_Position(position),
            m_Offset(offset),
            m_Color(s_DefaultColor),
            m_OutlineColor(s_DefaultOutlineColor),
            m_OutlineType(s_DefaultOutlineType),
            m_OutlineWidth(s_DefaultOutlineWidth),
            m_InnerSize(innersize),
            m_Rotation(rotation)
        {}

        Renderer::Renderer(nullptr_t) :
            Object(null),
            m_RenderHandler(null),
            m_Position(),
            m_Offset(),
            m_Color(),
            m_OutlineColor(),
            m_OutlineType(),
            m_OutlineWidth(),
            m_InnerSize(),
            m_Rotation()
        {}

        Renderer::Renderer(Renderer&& other) noexcept :
            Object(other),
            m_RenderHandler(move(other.m_RenderHandler)),
            m_Position(other.m_Position),
            m_Offset(other.m_Offset),
            m_Color(other.m_Color),
            m_Texture(move(other.m_Texture)),
            m_OutlineColor(other.m_OutlineColor),
            m_OutlineType(other.m_OutlineType),
            m_OutlineWidth(other.m_OutlineWidth),
            m_InnerSize(other.m_InnerSize),
            m_InnerOffset(other.m_InnerOffset),
            m_Rotation(other.m_Rotation),
            OnRender(move(other.OnRender))
        {
            if(m_RenderHandler != null)
                m_RenderHandler = bind(&Renderer::Render, this, placeholders::_1, placeholders::_2);
        }

        const FPoint& Renderer::Position() const
        {
            return m_Position;
        }

        void Renderer::Position(const FPoint& position)
        {
            m_Position = position;
        }

        const FPoint& Renderer::Offset() const
        {
            if(m_Null)
                throw NullException();
            return m_Offset;
        }

        void Renderer::Offset(const FPoint& offset)
        {
            if(m_Null)
                throw NullException();
            m_Offset = offset;
        }

        const Color& Renderer::Color() const
        {
            if(m_Null)
                throw NullException();
            return m_Color;
        }

        void Renderer::Color(const Graphics::Color& color)
        {
            if(m_Null)
                throw NullException();
            m_Color = color;
        }

        const Graphics::Texture& Renderer::Texture() const
        {
            if(m_Null)
                throw NullException();
            return m_Texture;
        }

        void Renderer::Texture(const Graphics::Texture& texture)
        {
            if(m_Null)
                throw NullException();
            m_Texture = texture;
        }

        void Renderer::ClearTexture()
        {
            m_Texture = null;
        }

        const Color& Renderer::LineColor() const
        {
            if(m_Null)
                throw NullException();
            return m_OutlineColor;
        }

        void Renderer::LineColor(const Graphics::Color& color)
        {
            if(m_Null)
                throw NullException();
            m_OutlineColor = color;
        }

        double Renderer::LineWidth() const
        {
            if(m_Null)
                throw NullException();
            return m_OutlineWidth;
        }

        void Renderer::LineWidth(double width)
        {
            if(m_Null)
                throw NullException();
            m_OutlineWidth = width;
        }

        const Graphics::LineType& Renderer::LineType() const
        {
            if(m_Null)
                throw NullException();
            return m_OutlineType;
        }

        void Renderer::LineType(const Boris::Graphics::LineType& type)
        {
            if(m_Null)
                throw NullException();
            m_OutlineType = type;
        }

        const FSize& Renderer::InnerSize() const
        {
            return m_InnerSize;
        }

        FSize& Renderer::InnerSize()
        {
            return m_InnerSize;
        }

        const FPoint& Renderer::InnerOffset() const
        {
            return m_InnerOffset;
        }

        FPoint& Renderer::InnerOffset()
        {
            return m_InnerOffset;
        }

        const double& Renderer::Rotation() const
        {
            return m_Rotation;
        }

        void Renderer::Rotation(double rotation)
        {
            m_Rotation = fmod(rotation, 2 * M_PI) * 180 / M_PI;
        }

        void Renderer::SetParent(Renderer& renderer)
        {
            if(m_Null)
                throw NullException();
            m_RenderHandler.Clear();
            renderer.OnRender += m_RenderHandler;
        }

        void Renderer::UnsetParent()
        {
            m_RenderHandler.Clear();
        }
    }
}