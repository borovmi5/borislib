#include <Rendering.h>

#include <Geometry.h>
#include <Graphics.h>
#include <Events.h>

#include <GL/freeglut.h>

using namespace std;
using namespace Boris::Geometry;
using namespace Boris::Graphics;
using namespace Boris::Events;

namespace Boris
{
    namespace Rendering
    {
        TextRenderer::TextRenderer(nullptr_t) :
            Renderer(null),
            m_Text(*new string()),
            m_Size(),
            m_Bold(),
            m_Mono()
        {
            delete &m_Text;
        }

        TextRenderer::TextRenderer(const FPoint& position, const string& text, const FPoint& offset) :
                Renderer(position, offset),
                m_Text(text),
                m_Size(16),
                m_Bold(false),
                m_Mono(false)
        {}

        TextRenderer::TextRenderer(const FPoint& position, const string& text, const Graphics::Color& color, double size, bool bold, bool mono, const FPoint& offset) :
            Renderer(position, offset),
            m_Text(text),
            m_Size(size),
            m_Bold(bold),
            m_Mono(mono)
        {
            m_Color = color;
        }

        void TextRenderer::Render(const Object& sender, const EventArgs& e) const
        {
            if(m_Null)
                throw NullException();

            glPushMatrix();
            glTranslated(m_Position.X(), m_Position.Y(), 0);
            glRotated(m_Rotation, 0, 0, 1);
            glPushMatrix();
            glTranslated(m_Offset.X(), m_Offset.Y(), 0);
            double font_scale = m_Size * .01;
            glScaled(font_scale, font_scale, 1);
            if(m_Color != Colors::Transparent)
            {
                glLineWidth(m_Bold ? m_Size / 10. : m_Size / 20.);
                glColor(m_Color);
                glutStrokeString(m_Mono ? GLUT_STROKE_MONO_ROMAN : GLUT_STROKE_ROMAN, (const unsigned char *)(m_Text.c_str()));
            }
            glPopMatrix();

            double scale_x = m_InnerSize.Width() > 0 ? 1. / m_InnerSize.Width() : 1.;
            double scale_y = m_InnerSize.Height() > 0 ? 1. / m_InnerSize.Height() : 1.;

            glTranslated(m_InnerOffset.X(), m_InnerOffset.Y(), 0);
            glScaled(scale_x, scale_y, 1);

            OnRender(*this, null);

            glPopMatrix();
        }

        const string& TextRenderer::Text() const
        {
            if(m_Null)
                throw NullException();
            return m_Text;
        }

        string& TextRenderer::Text()
        {
            if(m_Null)
                throw NullException();
            return m_Text;
        }

        const double& TextRenderer::Size() const
        {
            if(m_Null)
                throw NullException();
            return m_Size;
        }

        double& TextRenderer::Size()
        {
            if(m_Null)
                throw NullException();
            return m_Size;
        }

        const bool& TextRenderer::Mono() const
        {
            if(m_Null)
                throw NullException();
            return m_Mono;
        }

        bool& TextRenderer::Mono()
        {
            if(m_Null)
                throw NullException();
            return m_Mono;
        }

        const bool& TextRenderer::Bold() const
        {
            return m_Bold;
        }

        bool& TextRenderer::Bold()
        {
            return m_Bold;
        }

        double TextRenderer::Width() const
        {
            if(m_Null)
                throw NullException();

            return glutStrokeLengthf(m_Mono ? GLUT_STROKE_MONO_ROMAN : GLUT_STROKE_ROMAN, (const unsigned char *)(m_Text.c_str())) * .01 * m_Size;
        }
    }
}