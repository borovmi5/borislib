#include <Rendering.h>
#include <Geometry.h>
#include <Graphics.h>

using namespace std;
using namespace Boris::Geometry;
using namespace Boris::Graphics;

namespace Boris
{
    namespace Rendering
    {
        PolygonRenderer::PolygonRenderer(std::nullptr_t) :
            Renderer(null),
            m_Polygon(null)
        {}

        PolygonRenderer::PolygonRenderer(const FPoint& position, Polygon poly, const FPoint& offset) :
            Renderer(position, offset),
            m_Polygon(move(poly))
        {
            for(auto& pt : m_Polygon.Points())
                pt = Vector2(pt) + offset;
        }

        PolygonRenderer::PolygonRenderer(const FPoint& position, Polygon poly, const Graphics::Color& color, const FPoint& offset) :
            Renderer(position, offset),
            m_Polygon(move(poly))
        {
            for(auto& pt : m_Polygon.Points())
                pt = Vector2(pt) + offset;
            m_Color = color;
        }

        PolygonRenderer::PolygonRenderer(const FPoint& position, Polygon poly, const Graphics::Color& color, const Graphics::Color& outline, double outline_width, const FPoint& offset) :
            Renderer(position, offset),
            m_Polygon(move(poly))
        {
            for(auto& pt : m_Polygon.Points())
                pt = Vector2(pt) + offset;
            m_Color = color;
            m_OutlineColor = outline;
            m_OutlineWidth = outline_width;
        }

        void PolygonRenderer::Render(const Object& sender, const Events::EventArgs& e) const
        {
            if(m_Null)
                throw NullException();

            glPushMatrix();
            glTranslated(m_Position.X(), m_Position.Y(), 0);
            glRotated(m_Rotation, 0, 0, 1);
            if(m_Color != Colors::Transparent)
            {
                glBegin(GL_TRIANGLE_FAN);
                    glColor(m_Color);
                    for(const auto& pt : m_Polygon.Points())
                        glVertex(pt);
                glEnd();
            }
            if(m_OutlineColor != Colors::Transparent)
            {
                glLineWidth(m_OutlineWidth);
                glLineStipple(int(m_OutlineWidth), uint16_t(m_OutlineType));
                glEnable(GL_LINE_STIPPLE);
                glBegin(GL_TRIANGLE_FAN);
                glColor(m_Color);
                for(const auto& pt : m_Polygon.Points())
                    glVertex(pt);
                glEnd();
                glDisable(GL_LINE_STIPPLE);
            }

            double scale_x = m_InnerSize.Width() > 0 ? 1. / m_InnerSize.Width() : 1.;
            double scale_y = m_InnerSize.Height() > 0 ? 1. / m_InnerSize.Height() : 1.;

            glTranslated(m_InnerOffset.X(), m_InnerOffset.Y(), 0);
            glScaled(scale_x, scale_y, 1);

            OnRender(*this, null);

            glPopMatrix();
        }

        void PolygonRenderer::Offset(const FPoint& offset)
        {
            FPoint dif = Vector2(offset) - m_Offset;
            for(auto& pt : m_Polygon.Points())
                pt = Vector2(pt) + dif;
            m_Offset = offset;
        }
    }
}