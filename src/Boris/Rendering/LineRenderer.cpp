#include <Rendering.h>
#include <Events.h>
#include <Geometry.h>

#include <GL/freeglut.h>

using namespace Boris::Events;
using namespace Boris::Geometry;
using namespace Boris::Graphics;

namespace Boris
{
    namespace Rendering
    {
        LineRenderer::LineRenderer(std::nullptr_t) :
            Renderer(null),
            m_Line(null)
        {}

        LineRenderer::LineRenderer(const FPoint& position, const FPoint& p1, const FPoint& p2, const FPoint& offset) :
            Renderer(position, offset),
            m_Line(Vector2(p1) + Vector2(offset), Vector2(p2) + Vector2(offset))
        {}

        LineRenderer::LineRenderer(const FPoint& position, const FPoint& p1, const FPoint& p2, const Graphics::Color& color, double width, const Graphics::LineType& type, const FPoint& offset) :
            Renderer(position, offset),
            m_Line(Vector2(p1) + Vector2(offset), Vector2(p2) + Vector2(offset))
        {
            m_OutlineColor = color;
            m_OutlineWidth = width;
            m_OutlineType = type;
        }

        void LineRenderer::Offset(const FPoint& offset)
        {
            if(m_Null)
                throw NullException();
            Vector2 diff = Vector2(offset) - Vector2(m_Offset);
            m_Line.P1() = Vector2(m_Line.P1()) + diff;
            m_Line.P2() = Vector2(m_Line.P2()) + diff;
            m_Offset = offset;
        }

        void LineRenderer::Render(const Object& sender, const EventArgs& e) const
        {
            if(m_Null)
                throw NullException();

            glPushMatrix();
            glTranslated(m_Position.X(), m_Position.Y(), 0);
            glRotated(m_Rotation, 0, 0, 1);
            if(m_OutlineType != Graphics::LineType::BLANK && m_OutlineWidth > 0 && m_OutlineColor != Graphics::Colors::Transparent)
            {
                glLineWidth(m_OutlineWidth);
                glLineStipple(int(m_OutlineWidth), uint16_t(m_OutlineType));
                glEnable(GL_LINE_STIPPLE);
                glBegin(GL_LINE_LOOP);
                    glColor(m_OutlineColor);
                    glVertex(m_Line.P1());
                    glVertex(m_Line.P2());
                glEnd();
                glDisable(GL_LINE_STIPPLE);
            }

            double scale_x = m_InnerSize.Width() > 0 ? 1. / m_InnerSize.Width() : 1.;
            double scale_y = m_InnerSize.Height() > 0 ? 1. / m_InnerSize.Height() : 1.;

            glTranslated(m_InnerOffset.X(), m_InnerOffset.Y(), 0);
            glScaled(scale_x, scale_y, 1);

            OnRender(*this, null);

            glPopMatrix();
        }

        FPoint LineRenderer::P1() const
        {
            if(m_Null)
                throw NullException();
            return Vector2(m_Line.P1()) - Vector2(m_Offset);
        }

        void LineRenderer::P1(const FPoint& point)
        {
            if(m_Null)
                throw NullException();
            m_Line.P1() = Vector2(point) + Vector2(m_Offset);
        }

        FPoint LineRenderer::P2() const
        {
            if(m_Null)
                throw NullException();
            return Vector2(m_Line.P2()) - Vector2(m_Offset);
        }

        void LineRenderer::P2(const FPoint& point)
        {
            if(m_Null)
                throw NullException();
            m_Line.P2() = Vector2(point) + Vector2(m_Offset);
        }
    }
}