#include <Rendering.h>
#include <Geometry.h>
#include <Graphics.h>
#include <Core.h>

#include <GL/freeglut.h>

#include <cmath>

using namespace std;
using namespace Boris::Events;
using namespace Boris::Geometry;
using namespace Boris::Graphics;

namespace Boris
{
    namespace Rendering
    {
        PointRenderer::PointRenderer(nullptr_t) :
            Renderer(null)
        {}

        PointRenderer::PointRenderer(const FPoint& position, const Graphics::Color& color, const PointType& type, double size, const FPoint& offset) :
            Renderer(position, offset),
            m_Size(size),
            m_Type(type)
        {
            m_Color = color;
            m_OutlineColor = color;
            m_OutlineWidth = 1;
        }

        void PointRenderer::Render(const Object& sender, const EventArgs& e) const
        {
            glPushMatrix();
            glTranslated(m_Position.X(), m_Position.Y(), 0);
            glRotated(m_Rotation, 0, 0, 1);
            switch(m_Type)
            {
                case PointType::DOT:
                    glPointSize(1);
                    glBegin(GL_POINTS);
                        glColor(m_Color);
                        glVertex(m_Offset);
                    glEnd();
                    break;
                case PointType::CROSS:
                    glBegin(GL_LINES);
                        glColor(m_Color);
                        glVertex({m_Offset.X(), m_Offset.Y() + m_Size / 2.});
                        glVertex({m_Offset.X(), m_Offset.Y() - m_Size / 2.});
                        glVertex({m_Offset.X() + m_Size / 2., m_Offset.Y()});
                        glVertex({m_Offset.X() - m_Size / 2., m_Offset.Y()});
                    glEnd();
                    break;
                case PointType::CIRCLE:
                    glBegin(GL_TRIANGLE_FAN);
                        glColor(m_Color);
                        for(int i = 0; i < s_CircleSlices; ++i)
                            glVertex(Vector2(m_Offset) + (m_Size / 2.) * Vector2{ sin(2. * M_PI * i / s_CircleSlices), cos(2. * M_PI * i / s_CircleSlices) });
                    glEnd();
                    break;
                case PointType::SQUARE:
                    glPointSize(m_Size);
                    glBegin(GL_POINTS);
                        glColor(m_Color);
                        glVertex(m_Offset);
                    glEnd();
                    break;
            }

            double scale_x = m_InnerSize.Width() > 0 ? 1. / m_InnerSize.Width() : 1.;
            double scale_y = m_InnerSize.Height() > 0 ? 1. / m_InnerSize.Height() : 1.;

            glTranslated(m_InnerOffset.X(), m_InnerOffset.Y(), 0);
            glScaled(scale_x, scale_y, 1);

            OnRender(*this, null);

            glPopMatrix();
        }

        double& PointRenderer::Size()
        {
            return m_Size;
        }

        double PointRenderer::Size() const
        {
            return m_Size;
        }

        PointType& PointRenderer::Type()
        {
            return m_Type;
        }

        const PointType& PointRenderer::Type() const
        {
            return m_Type;
        }
    }
}