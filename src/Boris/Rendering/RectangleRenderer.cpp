#include <Rendering.h>
#include <Geometry.h>
#include <Core.h>
#include <Graphics.h>

#include <GL/freeglut.h>

using namespace std;
using namespace Boris::Geometry;
using namespace Boris::Graphics;

namespace Boris
{
    namespace Rendering
    {
        RectangleRenderer::RectangleRenderer(const FPoint& position, const FSize& size, const FPoint& offset) :
            Renderer(position, offset),
            m_Rectangle(offset, size)
        {
            m_OutlineType = LineType::BLANK;
        }

        RectangleRenderer::RectangleRenderer(const FPoint& position, const FSize& size, const Graphics::Color& color, const FPoint& offset) :
            Renderer(position, offset),
            m_Rectangle(offset, size)
        {
            m_Color = color;
            m_OutlineType = LineType::BLANK;
        }

        RectangleRenderer::RectangleRenderer(const FPoint& position, const FSize& size, const Graphics::Color& color, const Graphics::Color& outline, double outline_width, const Graphics::LineType& type, const FPoint& offset) :
            Renderer(position, offset),
            m_Rectangle(offset, size)
        {
            m_Color = color;
            m_OutlineType = type;
            m_OutlineWidth = outline_width;
            m_OutlineColor = outline;
        }

        RectangleRenderer::RectangleRenderer(nullptr_t) :
            Renderer(null),
            m_Rectangle(null)
        {}

        void RectangleRenderer::Render(const Object& sender, const Events::EventArgs& e) const
        {
            if(m_Null)
                throw NullException();

            glPushMatrix();
            glTranslated(m_Position.X(), m_Position.Y(), 0);
            glRotated(m_Rotation, 0, 0, 1);
            if(m_Color != Colors::Transparent)
            {
                vector<FPoint> UV = { {0, 0}, {1, 0}, {1, 1}, {0, 1} };
                if(m_Texture != null)
                {
                    glEnable(GL_TEXTURE_2D);
                    m_Texture.Apply();
                }
                glBegin(GL_TRIANGLE_FAN);
                    glNormal3f(0.0, 0.0, 1.0);
                    glColor(m_Color);
                    auto uv = UV.begin();
                    for(const auto& pt : m_Rectangle.Points())
                    {
                        glUV(*uv++);
                        glVertex(pt);
                    }
                glEnd();
                glDisable(GL_TEXTURE_2D);
            }
            if(m_OutlineType != Graphics::LineType::BLANK && m_OutlineWidth > 0 && m_OutlineColor != Colors::Transparent)
            {
                glLineWidth(m_OutlineWidth);
                glLineStipple(int(m_OutlineWidth), uint16_t(m_OutlineType));
                glEnable(GL_LINE_STIPPLE);
                glBegin(GL_LINE_LOOP);
                    glColor(m_OutlineColor);
                    for(const auto& pt : m_Rectangle.Points())
                        glVertex(pt);
                glEnd();
                glDisable(GL_LINE_STIPPLE);
            }

            double scale_x = m_InnerSize.Width() > 0 ? 1. / m_InnerSize.Width() : 1.;
            double scale_y = m_InnerSize.Height() > 0 ? 1. / m_InnerSize.Height() : 1.;

            glTranslated(m_InnerOffset.X(), m_InnerOffset.Y(), 0);
            glScaled(scale_x, scale_y, 1);

            OnRender(*this, null);

            glPopMatrix();
        }

        void RectangleRenderer::Offset(const FPoint& offset)
        {
            if(m_Null)
                throw NullException();
            m_Rectangle.Point() = offset;
            m_Offset = offset;
        }

        const FSize& RectangleRenderer::Size() const
        {
            return m_Rectangle.Size();
        }

        void RectangleRenderer::Size(const FSize& size)
        {
            if(m_Null)
                throw NullException();
            m_Rectangle.Size() = size;
        }
    }
}