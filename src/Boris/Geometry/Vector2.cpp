#include <Geometry/Vector2.h>
#include <cmath>

using namespace std;

namespace Boris
{
    namespace Geometry
    {
        Vector2::Vector2(double x, double y) :
            Vector<2, double>({x, y})
        {}

        Vector2::Vector2(const Boris::Geometry::Vector<2, double>& vector) :
            Vector<2, double>(vector)
        {}

        Vector2::Vector2(const Boris::Geometry::Matrix<2, 1, double>& matrix) :
            Vector<2, double>(matrix)
        {}

        Vector2 Vector2::FromAbsDir(double abs, double dir)
        {
            return Vector2(abs * cos(dir), abs * sin(dir));
        }

        Vector2 Vector2::operator+(const Vector2& other) const
        {
            return { X() + other.X(), Y() + other.Y() };
        }

        Vector2& Vector2::operator+=(const Vector2& other)
        {
            X() += other.X();
            Y() += other.Y();
            return *this;
        }

        Vector2 Vector2::operator-(const Vector2& other) const
        {
            return { X() - other.X(), Y() - other.Y() };
        }

        Vector2& Vector2::operator-=(const Vector2& other)
        {
            X() -= other.X();
            Y() -= other.Y();
            return *this;
        }

        Vector2 Vector2::operator*(double c) const
        {
            return { X() * c, Y() * c };
        }

        Vector2 operator*(double c, const Vector2& vector)
        {
            return { c * vector.X(), c * vector.Y() };
        }

        Vector2 operator*(const Matrix<2, 2>& matrix, const Vector2& vector)
        {
            return matrix * Matrix<2, 1>(vector);
        }

        Vector2& Vector2::operator*=(double c)
        {
            X() *= c;
            Y() *= c;
            return *this;
        }

        Vector2 Vector2::operator/(double c) const
        {
            return { X() / c, Y() / c };
        }

        Vector2& Vector2::operator/=(double c)
        {
            X() /= c;
            Y() /= c;
            return *this;
        }

        double Vector2::Abs() const
        {
            return sqrt(X() * X() + Y() * Y());
        }

        double Vector2::Dir() const
        {
            return atan2(Y(), X());
        }

        double& Vector2::X()
        {
            return this->m_Matrix[0][0];
        }

        double Vector2::X() const
        {
            return this->m_Matrix[0][0];
        }

        void Vector2::X(double x)
        {
            this->m_Matrix[0][0] = x;
        }

        double& Vector2::Y()
        {
            return this->m_Matrix[1][0];
        }

        double Vector2::Y() const
        {
            return this->m_Matrix[1][0];
        }

        void Vector2::Y(double y)
        {
            this->m_Matrix[1][0] = y;
        }
    }
}