#include <Geometry.h>

using namespace std;

namespace Boris
{
    namespace Geometry
    {
        Rectangle::Rectangle(nullptr_t) :
            Shape(null),
            m_Point(),
            m_Size()
        {}

        Rectangle::Rectangle(FPoint point, FSize size) :
            Shape(),
            m_Point(point),
            m_Size(size)
        {}

        bool Rectangle::operator==(const Rectangle& other) const
        {
            if(m_Null != other.m_Null)
                return false;
            return m_Point == other.m_Point && m_Size == other.m_Size;
        }

        bool Rectangle::operator!=(const Rectangle& other) const
        {
            if(m_Null != other.m_Null)
                return true;
            return m_Point != other.m_Point || m_Size != other.m_Size;
        }

        FPoint& Rectangle::Point()
        {
            if(m_Null)
                throw NullException();
            return m_Point;
        }

        const FPoint& Rectangle::Point() const
        {
            if(m_Null)
                throw NullException();
            return m_Point;
        }

        const FPoint& Rectangle::UpperLeft() const
        {
            if(m_Null)
                throw NullException();
            return m_Point;
        }

        FPoint Rectangle::UpperRight() const
        {
            if(m_Null)
                throw NullException();
            return { m_Point.X() + m_Size.Width(), m_Point.Y() };
        }

        FPoint Rectangle::LowerLeft() const
        {
            if(m_Null)
                throw NullException();
            return { m_Point.X(), m_Point.Y() + m_Size.Height() };
        }

        FPoint Rectangle::LowerRight() const
        {
            if(m_Null)
                throw NullException();
            return { m_Point.X() + m_Size.Width(), m_Point.Y() + m_Size.Height() };
        }

        FSize& Rectangle::Size()
        {
            if(m_Null)
                throw NullException();
            return m_Size;
        }

        const FSize& Rectangle::Size() const
        {
            if(m_Null)
                throw NullException();
            return m_Size;
        }

        double Rectangle::Area()
        {
            if(m_Null)
                throw NullException();
            return m_Size.Area();
        }

        vector<FPoint> Rectangle::Points() const
        {
            if(m_Null)
                throw NullException();
            return { UpperLeft(), UpperRight(), LowerRight(), LowerLeft() };
        }

        bool Rectangle::IntersectionTest(const vector<FPoint>& points) const
        {
            if(m_Null)
                throw NullException();
            for(const auto& point : points)
            {
                if(point.X() >= m_Point.X()
                    && point.Y() >= m_Point.Y()
                    && point.X() <= m_Point.X() + m_Size.Width()
                    && point.Y() <= m_Point.Y() + m_Size.Height())
                    return true;
            }
            return false;
        }
    }
}