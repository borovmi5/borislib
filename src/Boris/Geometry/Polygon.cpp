#include <Geometry.h>

#include <vector>
#include <list>
#include <initializer_list>

using namespace std;

namespace Boris
{
    namespace Geometry
    {
        Polygon::Polygon(std::nullptr_t) :
            Shape(null),
            m_Points()
        {}

        Polygon::Polygon(const vector<FPoint>& points) :
            Shape(),
            m_Points(points)
        {
            if(points.size() < 3)
                throw runtime_error("Polygon must be defined by at least 3 points!");
        }

        Polygon::Polygon(initializer_list<FPoint> points) :
                Shape(),
                m_Points(points)
        {
            if(points.size() < 3)
                throw runtime_error("Polygon must be defined by at least 3 points!");
        }

        bool Polygon::operator==(const Polygon& other) const
        {
            if(m_Null == other.m_Null)
                return true;
            if(m_Points.size() != other.m_Points.size())
                return false;
            auto pt = other.m_Points.begin();
            while(pt != other.m_Points.end())
            {
                if(*pt == *m_Points.begin())
                    break;
                ++pt;
            }
            for(const auto& point : m_Points)
            {
                if(pt == other.m_Points.end())
                    pt = other.m_Points.begin();

                if(point != *pt)
                    return false;

                ++pt;
            }
            return true;
        }

        bool Polygon::operator!=(const Polygon& other) const
        {
            if(m_Null != other.m_Null)
                return true;
            if(m_Points.size() != other.m_Points.size())
                return true;
            auto pt = other.m_Points.begin();
            while(pt != other.m_Points.end())
            {
                if(*pt == *m_Points.begin())
                    break;
                ++pt;
            }
            for(const auto& point : m_Points)
            {
                if(pt == other.m_Points.end())
                    pt = other.m_Points.begin();

                if(point != *pt)
                    return true;

                ++pt;
            }
            return false;
        }

        vector<FPoint> Polygon::Points() const
        {
            if(m_Null)
                throw NullException();
            return m_Points;
        }

        vector<FPoint>& Polygon::Points()
        {
            if(m_Null)
                throw NullException();
            return m_Points;
        }

        const FPoint& Polygon::operator[](unsigned int index) const
        {
            if(m_Null)
                throw NullException();
            if(m_Points.size() <= index)
                throw range_error("Index out of range.");
            auto it = m_Points.begin();
            for(int i = 0; i <= index; ++i)
                ++it;
            return *it;
        }

        FPoint& Polygon::operator[](unsigned int index)
        {
            if(m_Null)
                throw NullException();
            if(m_Points.size() <= index)
                throw range_error("Index out of range.");
            auto it = m_Points.begin();
            for(int i = 0; i <= index; ++i)
                ++it;
            return *it;
        }

        bool Polygon::IntersectionTest(const vector<FPoint>& points) const
        {
            if(m_Null)
                throw NullException();
            for(const auto& point : points)
            {
                int winds = 0;
                auto cur = m_Points.back();
                for(const auto& next : m_Points)
                {
                    if(cur.Y() <= point.Y() && next.Y() > point.Y())
                    {
                        if((next.X() - cur.X()) * (point.Y() - cur.Y()) - (point.X() - cur.X()) * (next.Y() - cur.Y()) > 0)
                            ++winds;
                    }
                    else if(next.Y() <= point.Y())
                    {
                        if((next.X() - cur.X()) * (point.Y() - cur.Y()) - (point.X() - cur.X()) * (next.Y() - cur.Y()) < 0)
                            --winds;
                    }
                }
                if(winds != 0)
                    return true;
            }
            return false;
        }
    }
}