#include <Geometry.h>

namespace Boris
{
    namespace Geometry
    {
        Point::Point() :
            m_X(0),
            m_Y(0)
        {}

        Point::Point(int x, int y) :
            m_X(x),
            m_Y(y)
        {}

        bool Point::operator==(const Point& other) const
        {
            return m_X == other.m_X && m_Y == other.m_Y;
        }

        bool Point::operator!=(const Point& other) const
        {
            return m_X != other.m_X || m_Y != other.m_Y;
        }

        Vector2 Point::Difference(const Point& other)
        {
            return Vector2(m_X - other.m_X, m_Y - other.m_Y);
        }

        double Point::Distance(const Boris::Geometry::Point& other)
        {
            return Vector2(m_X - other.m_X, m_Y - other.m_Y).Abs();
        }

        Point& Point::MoveTo(int x, int y)
        {
            m_X = x;
            m_Y = y;
            return *this;
        }

        Point& Point::MoveBy(int x, int y)
        {
            m_X += x;
            m_Y += y;
            return *this;
        }

        int& Point::X()
        {
            return m_X;
        }

        int Point::X() const
        {
            return m_X;
        }

        void Point::X(int x)
        {
            m_X = x;
        }

        int& Point::Y()
        {
            return m_Y;
        }

        int Point::Y() const
        {
            return m_Y;
        }

        void Point::Y(int y)
        {
            m_Y = y;
        }

        Point::operator Vector2() const
        {
            return Vector2(m_X, m_Y);
        }

        Point::operator FPoint() const
        {
            return { double(m_X), double(m_Y) };
        }
    }
}