#include <Geometry.h>

using namespace std;

namespace Boris
{
    namespace Geometry
    {
        Line::Line(nullptr_t) :
            Shape(null),
            m_P1(),
            m_P2()
        {}

        Line::Line(FPoint p1, FPoint p2) :
            Shape(),
            m_P1(move(p1)),
            m_P2(move(p2))
        {}

        Line::Line(FPoint p1, double length, double direction) :
            Shape(),
            m_P1(move(p1)),
            m_P2()
        {
            Vector2 tmp = Vector2(m_P1) + Vector2::FromAbsDir(length, direction);
            m_P2.X() = tmp.X();
            m_P2.Y() = tmp.Y();
        }

        bool Line::operator==(const Line& other) const
        {
            if(m_Null != other.m_Null)
                return false;
            return (m_P1 == other.m_P1 && m_P2 == other.m_P2) || (m_P1 == other.m_P2 && m_P2 == other.m_P1);
        }

        bool Line::operator!=(const Line& other) const
        {
            if(m_Null != other.m_Null)
                return true;
            return (m_P1 != other.m_P1 || m_P2 != other.m_P2) && (m_P1 != other.m_P2 || m_P2 != other.m_P1);
        }

        FPoint& Line::P1()
        {
            if(m_Null)
                throw NullException();
            return m_P1;
        }

        const FPoint& Line::P1() const
        {
            if(m_Null)
                throw NullException();
            return m_P1;
        }

        FPoint& Line::P2()
        {
            if(m_Null)
                throw NullException();
            return m_P2;
        }

        const FPoint& Line::P2() const
        {
            if(m_Null)
                throw NullException();
            return m_P2;
        }

        double Line::Length() const
        {
            if(m_Null)
                throw NullException();
            return (Vector2(FPoint(m_P1)) - Vector2(FPoint(m_P2))).Abs();
        }

        double Line::Direction() const
        {
            if(m_Null)
                throw NullException();
            return (Vector2(FPoint(m_P1)) - Vector2(FPoint(m_P2))).Dir();
        }

        std::vector<FPoint> Line::Points() const
        {
            if(m_Null)
                throw NullException();
            return { m_P1, m_P2 };
        }

        bool Line::IntersectionTest(const vector<FPoint>& points) const
        {
            if(m_Null)
                throw NullException();
            if(points.empty())
                return false;
            FPoint cur = points.back();
            for(const auto& point : points)
            {
                double denom = (m_P1.X() - m_P2.X()) * (cur.Y() - point.Y()) - (m_P1.Y() - m_P2.Y()) * (cur.X() - point.X());
                double t_num = (m_P1.X() - cur.X()) * (cur.Y() - point.Y()) - (m_P1.Y() - cur.Y()) * (cur.X() - point.X());
                double u_num = (m_P1.X() - m_P2.X()) * (m_P1.Y() - cur.Y()) - (m_P1.Y() - m_P2.Y()) * (m_P1.X() - point.X());
                if(denom == 0 && t_num == 0 && u_num == 0)
                    return true;
                if(denom != 0
                    && t_num / denom >= 0
                    && t_num <= denom
                    && u_num / denom >= 0
                    && u_num <= denom)
                    return true;
                cur = point;
            }
            return false;
        }
    }
}