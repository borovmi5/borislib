#include <Geometry.h>

#include <cmath>
#include <vector>

using namespace std;

namespace Boris
{
    namespace Geometry
    {
        Circle::Circle(std::nullptr_t) :
            Shape(null),
            m_Center(),
            m_Radius()
        {}

        Circle::Circle(FPoint center, double radius) :
            Shape(),
            m_Center(center),
            m_Radius(radius)
        {}

        bool Circle::operator==(const Circle& other) const
        {
            if(m_Null == other.m_Null)
                return true;
            return m_Radius == other.m_Radius && m_Center == other.m_Center;
        }

        bool Circle::operator!=(const Circle& other) const
        {
            if(m_Null != other.m_Null)
                return true;
            return m_Radius != other.m_Radius || m_Center != other.m_Center;
        }

        FPoint& Circle::Center()
        {
            if(m_Null)
                throw NullException();
            return m_Center;
        }

        const FPoint& Circle::Center() const
        {
            if(m_Null)
                throw NullException();
            return m_Center;
        }

        double& Circle::Radius()
        {
            if(m_Null)
                throw NullException();
            return m_Radius;
        }

        const double& Circle::Radius() const
        {
            if(m_Null)
                throw NullException();
            return m_Radius;
        }

        double Circle::Area()
        {
            if(m_Null)
                throw NullException();
            return 2 * m_Radius * M_PI;
        }

        vector<FPoint> Circle::Points() const
        {
            if(m_Null)
                throw NullException();
            return { m_Center,
                     { m_Center.X() + m_Radius, m_Center.Y() },
                     { m_Center.X() - m_Radius, m_Center.Y() },
                     { m_Center.X(), m_Center.Y() + m_Radius },
                     { m_Center.X(), m_Center.Y() - m_Radius }};
        }

        bool Circle::IntersectionTest(const vector<FPoint>& points) const
        {
            if(m_Null)
                throw NullException();
            Vector2 center = Vector2(FPoint(m_Center));
            for(auto point : points)
            {
                if((center - Vector2(point)).Abs() <= m_Radius)
                    return true;
            }
            return false;
        }
    }
}