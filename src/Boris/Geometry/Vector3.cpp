#include <Geometry/Vector3.h>
#include <cmath>

using namespace std;

namespace Boris
{
    namespace Geometry
    {
        Vector3::Vector3(double x, double y, double z) :
                Vector<3, double>({x, y, z})
        {}

        Vector3::Vector3(const Boris::Geometry::Vector<3, double>& vector) :
                Vector<3, double>(vector)
        {}

        Vector3::Vector3(const Boris::Geometry::Matrix<3, 1, double>& matrix) :
                Vector<3, double>(matrix)
        {}

        Vector3 Vector3::FromAbsDir(double abs, Vector2 dir)
        {
            double z = abs * sin(dir.Y());
            abs *= cos(dir.Y());
            double y = sin(dir.X());
            double x = cos(dir.X());
            return { x, y, z };
        }

        Vector3 Vector3::operator+(const Vector3& other) const
        {
            return { X() + other.X(), Y() + other.Y(), Z() + other.Z() };
        }

        Vector3& Vector3::operator+=(const Vector3& other)
        {
            X() += other.X();
            Y() += other.Y();
            Z() += other.Z();
            return *this;
        }

        Vector3 Vector3::operator-(const Vector3& other) const
        {
            return { X() - other.X(), Y() - other.Y(), Z() - other.Z() };
        }

        Vector3& Vector3::operator-=(const Vector3& other)
        {
            X() -= other.X();
            Y() -= other.Y();
            Z() -= other.Z();
            return *this;
        }

        Vector3 Vector3::operator*(double c) const
        {
            return { X() * c, Y() * c, Z() * c };
        }

        Vector3 operator*(double c, const Vector3& vector)
        {
            return { c * vector.X(), c * vector.Y(), c * vector.Z() };
        }

        Vector3 operator*(const Matrix<3, 3>& matrix, const Vector3& vector)
        {
            return matrix * Matrix<3, 1>(vector);
        }

        Vector3& Vector3::operator*=(double c)
        {
            X() *= c;
            Y() *= c;
            Z() *= c;
            return *this;
        }

        Vector3 Vector3::operator/(double c) const
        {
            return { X() / c, Y() / c, Z() / c };
        }

        Vector3& Vector3::operator/=(double c)
        {
            X() /= c;
            Y() /= c;
            Z() /= c;
            return *this;
        }

        double Vector3::Abs() const
        {
            return sqrt(X() * X() + Y() * Y() + Z() * Z());
        }

        Vector2 Vector3::Dir() const
        {
            return { atan2(Y(), X()), atan2(Z(), X()) };
        }

        double& Vector3::X()
        {
            return this->m_Matrix[0][0];
        }

        double Vector3::X() const
        {
            return this->m_Matrix[0][0];
        }

        void Vector3::X(double x)
        {
            this->m_Matrix[0][0] = x;
        }

        double& Vector3::Y()
        {
            return this->m_Matrix[1][0];
        }

        double Vector3::Y() const
        {
            return this->m_Matrix[1][0];
        }

        void Vector3::Y(double y)
        {
            this->m_Matrix[1][0] = y;
        }

        double& Vector3::Z()
        {
            return this->m_Matrix[2][0];
        }

        double Vector3::Z() const
        {
            return this->m_Matrix[2][0];
        }

        void Vector3::Z(double z)
        {
            this->m_Matrix[2][0] = z;
        }
    }
}