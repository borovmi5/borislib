#include <Geometry.h>

namespace Boris
{
    namespace Geometry
    {
        FPoint::FPoint() :
                m_X(0),
                m_Y(0)
        {}

        FPoint::FPoint(const Vector2& vector) :
                m_X(vector.X()),
                m_Y(vector.Y())
        {}

        FPoint::FPoint(double x, double y) :
                m_X(x),
                m_Y(y)
        {}

        bool FPoint::operator==(const FPoint& other) const
        {
            return m_X == other.m_X && m_Y == other.m_Y;
        }

        bool FPoint::operator!=(const FPoint& other) const
        {
            return m_X != other.m_X || m_Y != other.m_Y;
        }

        Vector2 FPoint::Difference(const FPoint& other)
        {
            return Vector2(m_X - other.m_X, m_Y - other.m_Y);
        }

        double FPoint::Distance(const Boris::Geometry::FPoint& other)
        {
            return Vector2(m_X - other.m_X, m_Y - other.m_Y).Abs();
        }

        FPoint& FPoint::MoveTo(double x, double y)
        {
            m_X = x;
            m_Y = y;
            return *this;
        }

        FPoint& FPoint::MoveBy(double x, double y)
        {
            m_X += x;
            m_Y += y;
            return *this;
        }

        double& FPoint::X()
        {
            return m_X;
        }

        double FPoint::X() const
        {
            return m_X;
        }

        void FPoint::X(double x)
        {
            m_X = x;
        }

        double& FPoint::Y()
        {
            return m_Y;
        }

        double FPoint::Y() const
        {
            return m_Y;
        }

        void FPoint::Y(double y)
        {
            m_Y = y;
        }

        FPoint::operator Vector2() const
        {
            return Vector2(m_X, m_Y);
        }

        FPoint::operator Point() const
        {
            return { int(m_X), int(m_Y) };
        }
    }
}