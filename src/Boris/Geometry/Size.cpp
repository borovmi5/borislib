#include <Geometry.h>

namespace Boris
{
    namespace Geometry
    {
        Size::Size() :
                m_Width(0),
                m_Height(0)
        {}

        Size::Size(unsigned int width, unsigned int height) :
                m_Width(width),
                m_Height(height)
        {}

        bool Size::operator==(const Size& other) const
        {
            return m_Width == other.m_Width && m_Height == other.m_Height;
        }

        bool Size::operator!=(const Size& other) const
        {
            return m_Width != other.m_Width || m_Height != other.m_Height;
        }

        unsigned int Size::Area() const
        {
            return m_Height * m_Width;
        }

        Size& Size::Resize(unsigned int width, unsigned int height)
        {
            m_Width = width;
            m_Height = height;
            return *this;
        }

        unsigned int Size::Width() const
        {
            return m_Width;
        }

        void Size::Width(unsigned int width)
        {
            m_Width = width;
        }

        unsigned int Size::Height() const
        {
            return m_Height;
        }

        void Size::Height(unsigned int height)
        {
            m_Height = height;
        }

        Size::operator Vector2() const
        {
            return Vector2(m_Width, m_Height);
        }

        Size::operator FSize() const
        {
            return { double(m_Width), double(m_Height) };
        }
    }
}