#include <Geometry.h>

namespace Boris
{
    namespace Geometry
    {
        FSize::FSize() :
                m_Width(0),
                m_Height(0)
        {}

        FSize::FSize(double width, double height) :
                m_Width(width),
                m_Height(height)
        {
            if(width < 0 || height < 0)
                throw std::runtime_error("Size must not be negative!");
        }

        bool FSize::operator==(const FSize& other) const
        {
            return m_Width == other.m_Width && m_Height == other.m_Height;
        }

        bool FSize::operator!=(const FSize& other) const
        {
            return m_Width != other.m_Width || m_Height != other.m_Height;
        }

        double FSize::Area() const
        {
            return m_Height * m_Width;
        }

        FSize& FSize::Resize(double width, double height)
        {
            m_Width = width;
            m_Height = height;
            return *this;
        }

        double FSize::Width() const
        {
            return m_Width;
        }

        void FSize::Width(double width)
        {
            if(width < 0)
                throw std::runtime_error("Width must not be negative!");
            m_Width = width;
        }

        double FSize::Height() const
        {
            return m_Height;
        }

        void FSize::Height(double height)
        {
            if(height < 0)
                throw std::runtime_error("Height must not be negative!");
            m_Height = height;
        }

        FSize::operator Vector2() const
        {
            return Vector2(m_Width, m_Height);
        }

        FSize::operator Size() const
        {
            return { (unsigned int)(m_Width), (unsigned int)(m_Height) };
        }
    }
}