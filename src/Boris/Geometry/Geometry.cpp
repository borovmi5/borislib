#include <Geometry.h>

#include <GL/freeglut.h>

namespace Boris
{
    namespace Geometry
    {
        void glVertex(const Vector2& vector)
        {
            glVertex2d(vector.X(), vector.Y());
        }

        void glUV(const Vector2& vector)
        {
            glTexCoord2d(vector.X(), vector.Y());
        }
    }
}