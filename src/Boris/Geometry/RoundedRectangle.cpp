#include <Geometry.h>

using namespace std;

namespace Boris
{
    namespace Geometry
    {
        RoundedRectangle::RoundedRectangle(nullptr_t) :
                Shape(null),
                m_Point(),
                m_Size(),
                m_Radius()
        {}

        RoundedRectangle::RoundedRectangle(FPoint point, FSize size, double radius) :
                Shape(),
                m_Point(point),
                m_Size(size),
                m_Radius(max(radius, 0.))
        {}

        bool RoundedRectangle::operator==(const RoundedRectangle& other) const
        {
            if(m_Null != other.m_Null)
                return false;
            return m_Point == other.m_Point && m_Size == other.m_Size && m_Radius == other.m_Radius;
        }

        bool RoundedRectangle::operator!=(const RoundedRectangle& other) const
        {
            if(m_Null != other.m_Null)
                return true;
            return m_Point != other.m_Point || m_Size != other.m_Size || m_Radius != other.m_Radius;
        }

        FPoint& RoundedRectangle::Point()
        {
            if(m_Null)
                throw NullException();
            return m_Point;
        }

        const FPoint& RoundedRectangle::Point() const
        {
            if(m_Null)
                throw NullException();
            return m_Point;
        }

        const FPoint& RoundedRectangle::UpperLeft() const
        {
            if(m_Null)
                throw NullException();
            return m_Point;
        }

        FPoint RoundedRectangle::UpperRight() const
        {
            if(m_Null)
                throw NullException();
            return { m_Point.X() + m_Size.Width(), m_Point.Y() };
        }

        FPoint RoundedRectangle::LowerLeft() const
        {
            if(m_Null)
                throw NullException();
            return { m_Point.X(), m_Point.Y() + m_Size.Height() };
        }

        FPoint RoundedRectangle::LowerRight() const
        {
            if(m_Null)
                throw NullException();
            return { m_Point.X() + m_Size.Width(), m_Point.Y() + m_Size.Height() };
        }

        FSize& RoundedRectangle::Size()
        {
            if(m_Null)
                throw NullException();
            return m_Size;
        }

        const FSize& RoundedRectangle::Size() const
        {
            if(m_Null)
                throw NullException();
            return m_Size;
        }

        void RoundedRectangle::Radius(double radius)
        {
            m_Radius = max(radius, 0.);
        }

        double RoundedRectangle::Radius() const
        {
            return m_Radius;
        }

        double RoundedRectangle::ActualRadius() const
        {
            return clamp(m_Radius, 0., min(m_Size.Width(), m_Size.Height())/2.);
        }

        double RoundedRectangle::Area()
        {
            if(m_Null)
                throw NullException();
            return m_Size.Area();
        }

        vector<FPoint> RoundedRectangle::Points() const
        {
            if(m_Null)
                throw NullException();
            return { UpperLeft(), UpperRight(), LowerRight(), LowerLeft() };
        }

        bool RoundedRectangle::IntersectionTest(const vector<FPoint>& points) const
        {
            if(m_Null)
                throw NullException();
            for(const auto& point : points)
            {
                if(point.X() >= m_Point.X()
                   && point.Y() >= m_Point.Y()
                   && point.X() <= m_Point.X() + m_Size.Width()
                   && point.Y() <= m_Point.Y() + m_Size.Height())
                    return true;
            }
            return false;
        }
    }
}