#include <Utility/Random.h>

#include <chrono>
#include <random>
#include <stdexcept>

using namespace std;
using namespace std::chrono;

namespace Boris
{
    namespace Utility
    {
        Random::Random() :
                m_Int(0, UINT64_MAX),
                m_Real(0, 1)
        {
            uint64_t time = high_resolution_clock::now().time_since_epoch().count();
            seed_seq seed{uint32_t(time & 0xFFFFFFFF), uint32_t(time >> 32)};
            m_Generator.seed(seed);
        }

        Random::Random(uint64_t s) :
                m_Int(0, UINT64_MAX),
                m_Real(0, 1)
        {
            seed_seq seed{uint32_t(s & 0xFFFFFFFF), uint32_t(s >> 32)};
            m_Generator.seed(seed);
        }

        template <>
        char Random::Next<char>()
        {
            return char(m_Int(m_Generator));
        }
        template <>
        char Random::Next<char>(char min, char max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return char(m_Int(m_Generator) % (max - min)) + min;
        }

        template <>
        int8_t Random::Next<int8_t>()
        {
            return int8_t(m_Int(m_Generator));
        }
        template <>
        int8_t Random::Next<int8_t>(int8_t min, int8_t max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return int8_t(m_Int(m_Generator) % (max - min)) + min;
        }

        template <>
        uint8_t Random::Next<uint8_t>()
        {
            return uint8_t(m_Int(m_Generator));
        }
        template <>
        uint8_t Random::Next<uint8_t>(uint8_t min, uint8_t max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return uint8_t(m_Int(m_Generator) % (max - min)) + min;
        }

        template <>
        int16_t Random::Next<int16_t>()
        {
            return int16_t(m_Int(m_Generator));
        }
        template <>
        int16_t Random::Next<int16_t>(int16_t min, int16_t max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return int16_t(m_Int(m_Generator) % (max - min)) + min;
        }

        template <>
        uint16_t Random::Next<uint16_t>()
        {
            return uint16_t(m_Int(m_Generator));
        }
        template <>
        uint16_t Random::Next<uint16_t>(uint16_t min, uint16_t max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return uint16_t(m_Int(m_Generator) % (max - min)) + min;
        }

        template <>
        int32_t Random::Next<int32_t>()
        {
            return int32_t(m_Int(m_Generator));
        }
        template <>
        int32_t Random::Next<int32_t>(int32_t min, int32_t max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return int32_t(m_Int(m_Generator) % (max - min)) + min;
        }

        template <>
        uint32_t Random::Next<uint32_t>()
        {
            return uint32_t(m_Int(m_Generator));
        }
        template <>
        uint32_t Random::Next<uint32_t>(uint32_t min, uint32_t max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return uint32_t(m_Int(m_Generator) % (max - min)) + min;
        }

        template <>
        int64_t Random::Next<int64_t>()
        {
            return int64_t(m_Int(m_Generator));
        }
        template <>
        int64_t Random::Next<int64_t>(int64_t min, int64_t max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return int64_t(m_Int(m_Generator) % (max - min)) + min;
        }

        template <>
        uint64_t Random::Next<uint64_t>()
        {
            return m_Int(m_Generator);
        }
        template <>
        uint64_t Random::Next<uint64_t>(uint64_t min, uint64_t max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return m_Int(m_Generator) % (max - min) + min;
        }

        template <>
        float Random::Next<float>()
        {
            return float(m_Real(m_Generator));
        }
        template<>
        float Random::Next<float>(float min, float max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return float(m_Real(m_Generator)) * (max - min) + min;
        }

        template <>
        double Random::Next<double>()
        {
            return m_Real(m_Generator);
        }
        template<>
        double Random::Next<double>(double min, double max)
        {
            if(min > max)
                throw runtime_error("Random min cannot be greater than max!");
            return m_Real(m_Generator) * (max - min) + min;
        }
    }
}