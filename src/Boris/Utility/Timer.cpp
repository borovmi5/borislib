#include <Utility/Timer.h>
#include <Events.h>
#include <Core/Object.h>

#include <functional>
#include <chrono>
#include <iostream>

using namespace std;
using namespace std::chrono;

namespace Boris
{
    namespace Utility
    {
        Timer::Timer(std::nullptr_t) :
            Object(null),
            OnTimeElapsed(),
            m_Type(),
            m_WaitThread(),
            m_Milliseconds(),
            m_Start(),
            m_Stop(),
            m_Started()
        {}

        Timer::Timer(unsigned int msec, TimerType type) :
            Object(),
            OnTimeElapsed(),
            m_Type(type),
            m_WaitThread(),
            m_Milliseconds(msec),
            m_Start(),
            m_Stop(true),
            m_Started(false)
        {}

        unsigned int Timer::Milliseconds() const
        {
            if(m_Null)
                throw NullException();
            return m_Milliseconds;
        }

        unsigned int& Timer::Milliseconds()
        {
            if(m_Null)
                throw NullException();
            return m_Milliseconds;
        }

        unsigned int Timer::Remaining() const
        {
            if(m_Null)
                throw NullException();
            if(!m_Started)
                return 0;
            return m_Milliseconds - duration_cast<milliseconds>(high_resolution_clock::now() - m_Start).count();
        }

        Timer::TimerType Timer::Type() const
        {
            if(m_Null)
                throw NullException();
            return m_Type;
        }

        Timer::TimerType& Timer::Type()
        {
            if(m_Null)
                throw NullException();
            return m_Type;
        }

        void Timer::Start()
        {
            if(m_Null)
                throw NullException();
            if(m_Started)
                return;
            m_Stop = false;
            m_Start = high_resolution_clock::now();
            m_Started = true;
            m_WaitThread = std::async(std::launch::async, std::bind(&Timer::Wait, this));
        }

        void Timer::Restart()
        {
            if(m_Null)
                throw NullException();
            if(m_Started)
            {
                Stop();
                m_WaitThread.get();
            }
            Start();
        }

        void Timer::Stop()
        {
            if(m_Null)
                throw NullException();
            m_Stop = true;
        }

        void Timer::Wait()
        {
            while(true)
            {
                while(( duration_cast<milliseconds>(high_resolution_clock::now() - m_Start).count() <
                        m_Milliseconds ) && !m_Stop)
                    std::this_thread::sleep_for(duration<unsigned int, milli>(1));
                if(!m_Stop)
                {
                    m_Start = high_resolution_clock::now();
                    OnTimeElapsed(*this, null);

                    if(m_Type != TimerType::INTERVAL)
                    {
                        m_Started = false;
                        break;
                    }
                }
                else
                {
                    m_Started = false;
                    break;
                }
            }
        }
    }
}