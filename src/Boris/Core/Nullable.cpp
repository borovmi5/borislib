#include <Core/Nullable.h>

#include <cstddef>

namespace Boris
{
    Nullable::Nullable() :
        m_Null(false)
    {}

    Nullable::Nullable(std::nullptr_t) :
        m_Null(true)
    {}

    bool Nullable::operator==(std::nullptr_t) const
    {
        return m_Null;
    }

    bool Nullable::operator!=(std::nullptr_t) const
    {
        return !m_Null;
    }

    bool Nullable::IsNull()
    {
        return m_Null;
    }
}