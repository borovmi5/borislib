#include <Core/Object.h>

#include <Events.h>

using namespace std;

using namespace Boris::Events;

namespace Boris
{
    Object::Object(std::nullptr_t) :
        Nullable(null),
        OnDestruct()
    {}

    Object::Object(const Object& other) :
        Nullable(other),
        OnDestruct()
    {}

    Object::~Object()
    {
        OnDestruct(*this, null);
    }
}