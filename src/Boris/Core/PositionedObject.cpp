#include <Core.h>
#include <Events.h>
#include <Geometry.h>

using namespace std;
using namespace Boris::Geometry;
using namespace Boris::Events;

namespace Boris
{
    PositionedObject::PositionedObject(nullptr_t) :
        Object(null),
        m_Position(),
        m_Rotation()
    {}

    PositionedObject::PositionedObject(const FPoint& position, double rotation) :
        Object(),
        m_Position(position),
        m_Rotation(rotation)
    {}

    const FPoint& PositionedObject::Position() const
    {
        return m_Position;
    }

    void PositionedObject::Position(const FPoint& position)
    {
        m_Position = position;
    }

    void PositionedObject::Move(const Vector2& difference)
    {
        m_Position.X() += difference.X();
        m_Position.Y() += difference.Y();
    }

    double PositionedObject::Rotation() const
    {
        return m_Rotation;
    }

    void PositionedObject::Rotation(double rotation)
    {
        double max = M_PI * 2;
        while(rotation > max)
            rotation -= max;
        while(rotation < 0)
            rotation += max;
        m_Rotation = rotation;
    }

    void PositionedObject::Rotate(double angle)
    {
        double max = M_PI * 2;
        angle += m_Rotation;
        while(angle > max)
            angle -= max;
        while(angle < 0)
            angle += max;
        m_Rotation = angle;
    }
}