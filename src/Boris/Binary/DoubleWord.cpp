#include <Binary.h>

using namespace std;

namespace Boris
{
    namespace Binary
    {
        DoubleWord::DoubleWord(uint32_t value) :
                m_Value(value)
        {}

        DoubleWord::DoubleWord(array<bit, 32> bits) :
                m_Value()
        {
            for(uint8_t i = 0; i < 32; ++i)
                m_Value |= uint32_t(bits[i] << i);
        }

        DoubleWord::DoubleWord(array<byte, 4> bytes) :
                m_Value()
        {
            for(uint8_t i = 0; i < 4; ++i)
                for(uint8_t j = 0; j < 8; ++j)
                    m_Value |= uint32_t(bytes[i].GetBit(j) << (j + i * 8u));
        }

        DoubleWord::DoubleWord(array<word, 2> words) :
                m_Value()
        {
            for(uint8_t i = 0; i < 2; ++i)
                for(uint8_t j = 0; j < 16; ++j)
                    m_Value |= uint32_t(words[i].GetBit(j) << (j + i * 16u));
        }

        bool DoubleWord::operator==(const DoubleWord& other) const
        {
            return m_Value == other.m_Value;
        }

        bool DoubleWord::operator!=(const DoubleWord& other) const
        {
            return m_Value != other.m_Value;
        }

        DoubleWord DoubleWord::operator&(const DoubleWord& other) const
        {
            return m_Value & other.m_Value;
        }

        DoubleWord& DoubleWord::operator&=(const DoubleWord& other)
        {
            m_Value &= other.m_Value;
            return *this;
        }

        DoubleWord DoubleWord::operator|(const DoubleWord& other) const
        {
            return m_Value | other.m_Value;
        }

        DoubleWord& DoubleWord::operator|=(const DoubleWord& other)
        {
            m_Value |= other.m_Value;
            return *this;
        }

        DoubleWord DoubleWord::operator^(const DoubleWord& other) const
        {
            return m_Value ^ other.m_Value;
        }

        DoubleWord& DoubleWord::operator^=(const DoubleWord& other)
        {
            m_Value ^= other.m_Value;
            return *this;
        }

        DoubleWord DoubleWord::operator>>(unsigned int count) const
        {
            return m_Value >> count;
        }

        DoubleWord& DoubleWord::operator>>=(unsigned int count)
        {
            m_Value >>= count;
            return *this;
        }

        DoubleWord DoubleWord::operator<<(unsigned int count) const
        {
            return m_Value << count;
        }

        DoubleWord& DoubleWord::operator<<=(unsigned int count)
        {
            m_Value <<= count;
            return *this;
        }

        DoubleWord::operator uint32_t() const
        {
            return m_Value;
        }

        array<bit, 32> DoubleWord::ToBits() const
        {
            array<bit, 32> bits;
            uint8_t idx = 0;
            for(uint32_t mask = 0x00000001; mask != 0x00000000; mask <<= 1u)
                bits[idx++] = m_Value & mask;

            return bits;
        }

        array<byte, 4> DoubleWord::ToBytes() const
        {
            array<byte, 4> bytes;
            uint8_t idx = 0;
            for(uint32_t mask = 0x000000FF; mask != 0x00000000; mask <<= 8u)
            {
                bytes[idx] = (m_Value & mask) >> (idx * 8u);
                ++idx;
            }

            return bytes;
        }

        array<word, 2> DoubleWord::ToWords() const
        {
            array<word, 2> words;
            uint8_t idx = 0;
            for(uint32_t mask = 0x0000FFFF; mask != 0x00000000; mask <<= 16u)
            {
                words[idx] = (m_Value & mask) >> (idx * 16u);
                ++idx;
            }

            return words;
        }

        bit DoubleWord::GetBit(uint8_t index) const
        {
            if(index >= 32)
                throw runtime_error("Double word has 32 bits only!");
            return m_Value & (0x00000001u << index);
        }

        DoubleWord& DoubleWord::SetBit(uint8_t index, const bit& value)
        {
            if(index >= 32)
                throw runtime_error("Double word has 32 bits only!");
            if(value)
                m_Value |= 0x00000001u << index;
            else
                m_Value &= ~(0x00000001u << index);

            return *this;
        }

        DoubleWord& DoubleWord::ClearBit(uint8_t index)
        {
            if(index >= 32)
                throw runtime_error("Double word has 32 bits only!");
            m_Value &= ~(0x00000001u << index);
            return *this;
        }

        byte DoubleWord::GetByte(uint8_t index) const
        {
            if(index >= 4)
                throw runtime_error("Double word has 4 bytes only!");
            return (m_Value & (0x000000FFu << (index * 8u))) >> (index * 8u);
        }

        DoubleWord& DoubleWord::SetByte(uint8_t index, const byte& value)
        {
            if(index >= 4)
                throw runtime_error("Double word has 4 bytes only!");
            m_Value &= ~(0x000000FFu << (index * 8u));
            m_Value |= uint32_t(value << (index * 8u));

            return *this;
        }

        DoubleWord& DoubleWord::ClearByte(uint8_t index)
        {
            if(index >= 4)
                throw runtime_error("Double word has 4 bytes only!");
            m_Value &= ~(0x000000FFu << (index * 8u));

            return *this;
        }

        word DoubleWord::GetWord(uint8_t index) const
        {
            if(index >= 2)
                throw runtime_error("Double word has 2 words only!");
            return (m_Value & (0x0000FFFFu << (index * 16u))) >> (index * 16u);
        }

        DoubleWord& DoubleWord::SetWord(uint8_t index, const word& value)
        {
            if(index >= 2)
                throw runtime_error("Double word has 2 words only!");
            m_Value &= ~(0x0000FFFFu << (index * 16u));
            m_Value |= uint32_t(value << (index * 16u));

            return *this;
        }

        DoubleWord& DoubleWord::ClearWord(uint8_t index)
        {
            if(index >= 2)
                throw runtime_error("Double word has 2 words only!");
            m_Value &= ~(0x0000FFFFu << (index * 16u));

            return *this;
        }
    }
}