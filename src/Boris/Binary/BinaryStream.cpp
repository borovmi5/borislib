#include <Binary.h>

#include <algorithm>

using namespace std;

namespace Boris
{
    namespace Binary
    {
        BinaryStream::BinaryStream() :
            fstream(),
            m_Flags()
        {}

        BinaryStream::BinaryStream(BinaryStream&& other) noexcept :
            fstream(),
            m_Endian(other.m_Endian),
            m_InBuffer(other.m_InBuffer),
            m_OutBuffer(other.m_OutBuffer),
            m_InPointer(other.m_OutPointer),
            m_ForceNewIn(other.m_ForceNewIn),
            m_ForceNewOut(other.m_ForceNewOut),
            m_FilePath(std::move(other.m_FilePath)),
            m_Flags(other.m_Flags)
        {
            if(other.is_open())
                open(m_FilePath, m_Flags);
            seekg(other.tellg());
            seekp(other.tellp());
        }

        BinaryStream& BinaryStream::operator=(BinaryStream&& other) noexcept
        {
            if(is_open())
                close();
            m_Endian = other.m_Endian;
            m_InBuffer = other.m_InBuffer;
            m_OutBuffer = other.m_OutBuffer;
            m_InPointer = other.m_InPointer;
            m_OutPointer = other.m_OutPointer;
            m_ForceNewIn = other.m_ForceNewOut;
            m_FilePath = std::move(other.m_FilePath);
            m_Flags = other.m_Flags;
            if(other.is_open())
                open(m_FilePath, m_Flags);
            seekg(other.tellg());
            seekp(other.tellp());

            return *this;
        }

        void BinaryStream::open(const string& filepath, ios_base::openmode mode)
        {
            if(is_open())
                close();
            fstream::open(filepath, mode | ios_base::binary);
            m_FilePath = filepath;
            m_Flags = mode;
            m_InBuffer = 0;
            m_OutBuffer = 0;
            m_InPointer = 8;
            m_OutPointer = 0;
            m_ForceNewIn = false;
            m_ForceNewOut = false;
        }

        void BinaryStream::close()
        {
            if(m_OutPointer > 0)
                put(char(m_OutBuffer));
            fstream::close();
        }

        BinaryStream & BinaryStream::seekg(pos_type pos)
        {
            m_InPointer = 8;
            fstream::seekg(pos);
            return *this;
        }

        BinaryStream & BinaryStream::seekg(off_type off, ios_base::seekdir dir)
        {
            m_InPointer = 8;
            fstream::seekg(off, dir);
            return *this;
        }

        BinaryStream & BinaryStream::seekp(pos_type pos)
        {
            if(m_OutPointer > 0)
            {
                m_OutPointer = 0;
                put(char(m_OutBuffer));
                m_OutBuffer = 0;
            }
            fstream::seekp(pos);
            return *this;
        }

        BinaryStream & BinaryStream::seekp(off_type off, ios_base::seekdir dir)
        {
            if(m_OutPointer > 0)
            {
                m_OutPointer = 0;
                put(char(m_OutBuffer));
                m_OutBuffer = 0;
            }
            fstream::seekp(off, dir);
            return *this;
        }

        Endian BinaryStream::Endianness() const
        {
            return m_Endian;
        }

        void BinaryStream::Endianness(Boris::Binary::Endian endian)
        {
            m_Endian = endian;
        }

        bit BinaryStream::GetBit(bool force_new)
        {
            if(m_InPointer >= 8 || m_ForceNewIn || force_new)
            {
                m_InPointer = 0;
                m_ForceNewIn = false;
                m_InBuffer = byte(get());
            }
            return m_InBuffer.GetBit(7 - m_InPointer++);
        }

        byte BinaryStream::GetByte()
        {
            m_ForceNewIn = true;
            return get();
        }

        word BinaryStream::GetWord()
        {
            m_ForceNewIn = true;
            array<byte, 2> bytes;
            for(byte& b : bytes)
                b = get();
            if(m_Endian == Endian::BIG)
                reverse(bytes.begin(), bytes.end());
            return word(bytes);
        }

        dword BinaryStream::GetDWord()
        {
            m_ForceNewIn = true;
            array<byte, 4> bytes;
            for(byte& b : bytes)
                b = get();
            if(m_Endian == Endian::BIG)
                reverse(bytes.begin(), bytes.end());
            return dword(bytes);
        }

        qword BinaryStream::GetQWord()
        {
            m_ForceNewIn = true;
            array<byte, 8> bytes;
            for(byte& b : bytes)
                b = get();
            if(m_Endian == Endian::BIG)
                reverse(bytes.begin(), bytes.end());
            return qword(bytes);
        }

        bit BinaryStream::PeekBit(bool force_new)
        {
            if(m_InPointer >= 8 || m_ForceNewIn || force_new)
                return byte(peek()).GetBit(0);
            return m_InBuffer.GetBit(m_InPointer);
        }

        byte BinaryStream::PeekByte()
        {
            return peek();
        }

        word BinaryStream::PeekWord()
        {
            array<byte, 2> bytes;
            for(byte& b : bytes)
                b = get();
            for(byte& b : bytes)
                unget();
            if(m_Endian == Endian::BIG)
                reverse(bytes.begin(), bytes.end());
            return word(bytes);
        }

        dword BinaryStream::PeekDWord()
        {
            array<byte, 4> bytes;
            for(byte& b : bytes)
                b = get();
            for(byte& b : bytes)
                unget();
            if(m_Endian == Endian::BIG)
                reverse(bytes.begin(), bytes.end());
            return dword(bytes);
        }

        qword BinaryStream::PeekQWord()
        {
            array<byte, 8> bytes;
            for(byte& b : bytes)
                b = get();
            for(byte& b : bytes)
                unget();
            if(m_Endian == Endian::BIG)
                reverse(bytes.begin(), bytes.end());
            return qword(bytes);
        }

        void BinaryStream::WriteBit(bit b, bool force_new)
        {
            if(m_OutPointer >= 8 || m_ForceNewOut || force_new)
            {
                m_OutPointer = 0;
                m_ForceNewOut = false;
                put(m_OutBuffer);
                m_OutBuffer = 0;
            }
            m_OutBuffer.SetBit(7 - m_OutPointer++, b);
        }

        void BinaryStream::WriteByte(byte B)
        {
            if(m_OutPointer > 0)
            {
                m_OutPointer = 0;
                m_ForceNewOut = false;
                put(m_OutBuffer);
                m_OutBuffer = 0;
            }

            put(B);
        }

        void BinaryStream::WriteWord(word w)
        {
            if(m_OutPointer > 0)
            {
                m_OutPointer = 0;
                m_ForceNewOut = false;
                put(m_OutBuffer);
                m_OutBuffer = 0;
            }

            array<byte, 2> bytes = w.ToBytes();
            if(m_Endian == Endian::BIG)
                reverse(bytes.begin(), bytes.end());
            for(const byte& b : bytes)
                put(b);
        }

        void BinaryStream::WriteDWord(dword d)
        {
            if(m_OutPointer > 0)
            {
                m_OutPointer = 0;
                m_ForceNewOut = false;
                put(m_OutBuffer);
                m_OutBuffer = 0;
            }

            array<byte, 4> bytes = d.ToBytes();
            if(m_Endian == Endian::BIG)
                reverse(bytes.begin(), bytes.end());
            for(const byte& b : bytes)
                put(b);
        }

        void BinaryStream::WriteQWord(qword q)
        {
            if(m_OutPointer > 0)
            {
                m_OutPointer = 0;
                m_ForceNewOut = false;
                put(m_OutBuffer);
                m_OutBuffer = 0;
            }

            array<byte, 8> bytes = q.ToBytes();
            if(m_Endian == Endian::BIG)
                reverse(bytes.begin(), bytes.end());
            for(const byte& b : bytes)
                put(b);
        }

        qword BinaryStream::Checksum()
        {
            pos_type cur = fstream::tellg();
            fstream::seekg(fstream::beg);
            qword check = 0;
            while(true)
            {
                qword in = GetQWord();
                if(eof())
                    break;
                check ^= in;
            }
            fstream::clear();
            fstream::seekg(cur);
            cur = fstream::tellg();
            return check;
        }

        void BinaryStream::Flush()
        {
            if(m_OutPointer > 0)
                put(m_OutBuffer);
            m_ForceNewOut = false;
            m_OutPointer = 0;
            m_OutBuffer = 0;
        }

        bstream& operator>>(bstream& stream, bit& b)
        {
            b = stream.GetBit();
            return stream;
        }

        bstream& operator>>(bstream& stream, byte& B)
        {
            B = stream.GetByte();
            return stream;
        }

        bstream& operator>>(bstream& stream, word& w)
        {
            w = stream.GetWord();
            return stream;
        }

        bstream& operator>>(bstream& stream, dword& d)
        {
            d = stream.GetDWord();
            return stream;
        }

        bstream& operator>>(bstream& stream, qword& q)
        {
            q = stream.GetQWord();
            return stream;
        }

        bstream& operator<<(bstream& stream, const bit& b)
        {
            stream.WriteBit(b);
            return stream;
        }

        bstream& operator<<(bstream& stream, const byte& B)
        {
            stream.WriteByte(B);
            return stream;
        }

        bstream& operator<<(bstream& stream, const word& w)
        {
            stream.WriteWord(w);
            return stream;
        }

        bstream& operator<<(bstream& stream, const dword& d)
        {
            stream.WriteDWord(d);
            return stream;
        }

        bstream& operator<<(bstream& stream, const qword& q)
        {
            stream.WriteQWord(q);
            return stream;
        }
    }
}