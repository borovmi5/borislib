#include <Binary.h>

using namespace std;

namespace Boris
{
    namespace Binary
    {
        QuadWord::QuadWord(uint64_t value) :
                m_Value(value)
        {}

        QuadWord::QuadWord(array<bit, 64> bits) :
                m_Value()
        {
            for(uint8_t i = 0; i < 64; ++i)
                m_Value |= uint64_t(bits[i]) << i;
        }

        QuadWord::QuadWord(array<byte, 8> bytes) :
                m_Value()
        {
            for(uint8_t i = 0; i < 8; ++i)
                for(uint8_t j = 0; j < 8; ++j)
                    m_Value |= uint64_t(bytes[i].GetBit(j)) << (j + i * 8u);
        }

        QuadWord::QuadWord(array<word, 4> words) :
                m_Value()
        {
            for(uint8_t i = 0; i < 4; ++i)
                for(uint8_t j = 0; j < 16; ++j)
                    m_Value |= uint64_t(words[i].GetBit(j)) << (j + i * 16u);
        }

        QuadWord::QuadWord(array<dword, 2> dwords) :
                m_Value()
        {
            for(uint8_t i = 0; i < 2; ++i)
                for(uint8_t j = 0; j < 32; ++j)
                    m_Value |= uint64_t(dwords[i].GetBit(j)) << (j + i * 32u);
        }

        bool QuadWord::operator==(const QuadWord& other) const
        {
            return m_Value == other.m_Value;
        }

        bool QuadWord::operator!=(const QuadWord& other) const
        {
            return m_Value != other.m_Value;
        }

        QuadWord QuadWord::operator&(const QuadWord& other) const
        {
            return m_Value & other.m_Value;
        }

        QuadWord& QuadWord::operator&=(const QuadWord& other)
        {
            m_Value &= other.m_Value;
            return *this;
        }

        QuadWord QuadWord::operator|(const QuadWord& other) const
        {
            return m_Value | other.m_Value;
        }

        QuadWord& QuadWord::operator|=(const QuadWord& other)
        {
            m_Value |= other.m_Value;
            return *this;
        }

        QuadWord QuadWord::operator^(const QuadWord& other) const
        {
            return m_Value ^ other.m_Value;
        }

        QuadWord& QuadWord::operator^=(const QuadWord& other)
        {
            m_Value ^= other.m_Value;
            return *this;
        }

        QuadWord QuadWord::operator>>(unsigned int count) const
        {
            return m_Value >> count;
        }

        QuadWord& QuadWord::operator>>=(unsigned int count)
        {
            m_Value >>= count;
            return *this;
        }

        QuadWord QuadWord::operator<<(unsigned int count) const
        {
            return m_Value << count;
        }

        QuadWord& QuadWord::operator<<=(unsigned int count)
        {
            m_Value <<= count;
            return *this;
        }

        QuadWord::operator uint64_t() const
        {
            return m_Value;
        }

        array<bit, 64> QuadWord::ToBits() const
        {
            array<bit, 64> bits;
            uint8_t idx = 0;
            for(uint64_t mask = 0x0000000000000001; mask != 0x0000000000000000; mask <<= 1u)
                bits[idx++] = m_Value & mask;

            return bits;
        }

        array<byte, 8> QuadWord::ToBytes() const
        {
            array<byte, 8> bytes;
            uint8_t idx = 0;
            for(uint64_t mask = 0x00000000000000FF; mask != 0x0000000000000000; mask <<= 8u)
            {
                bytes[idx] = (m_Value & mask) >> (idx * 8u);
                ++idx;
            }

            return bytes;
        }

        array<word, 4> QuadWord::ToWords() const
        {
            array<word, 4> words;
            uint8_t idx = 0;
            for(uint64_t mask = 0x000000000000FFFF; mask != 0x0000000000000000; mask <<= 16u)
            {
                words[idx] = (m_Value & mask) >> (idx * 16u);
                ++idx;
            }

            return words;
        }

        array<dword, 2> QuadWord::ToDoubleWords() const
        {
            array<dword, 2> dwords;
            uint8_t idx = 0;
            for(uint64_t mask = 0x00000000FFFFFFFF; mask != 0x0000000000000000; mask <<= 32u)
            {
                dwords[idx] = (m_Value & mask) >> (idx * 32u);
                ++idx;
            }

            return dwords;
        }

        bit QuadWord::GetBit(uint8_t index) const
        {
            if(index >= 64)
                throw runtime_error("Quad word has 64 bits only!");
            return m_Value & (0x0000000000000001u << index);
        }

        QuadWord& QuadWord::SetBit(uint8_t index, const bit& value)
        {
            if(index >= 64)
                throw runtime_error("Quad word has 64 bits only!");
            if(value)
                m_Value |= 0x0000000000000001u << index;
            else
                m_Value &= ~(0x0000000000000001u << index);

            return *this;
        }

        QuadWord& QuadWord::ClearBit(uint8_t index)
        {
            if(index >= 64)
                throw runtime_error("Quad word has 64 bits only!");
            m_Value &= ~(0x0000000000000001u << index);
            return *this;
        }

        byte QuadWord::GetByte(uint8_t index) const
        {
            if(index >= 8)
                throw runtime_error("Quad word has 8 bytes only!");
            return (m_Value & (0x00000000000000FFu << (index * 8u))) >> (index * 8u);
        }

        QuadWord& QuadWord::SetByte(uint8_t index, const byte& value)
        {
            if(index >= 8)
                throw runtime_error("Quad word has 8 bytes only!");
            m_Value &= ~(0x00000000000000FFu << (index * 8u));
            m_Value |= uint64_t(value << (index * 8u));

            return *this;
        }

        QuadWord& QuadWord::ClearByte(uint8_t index)
        {
            if(index >= 8)
                throw runtime_error("Quad word has 8 bytes only!");
            m_Value &= ~(0x00000000000000FFu << (index * 8u));

            return *this;
        }

        word QuadWord::GetWord(uint8_t index) const
        {
            if(index >= 4)
                throw runtime_error("Quad word has 4 words only!");
            return (m_Value & (0x000000000000FFFFu << (index * 16u))) >> (index * 16u);
        }

        QuadWord& QuadWord::SetWord(uint8_t index, const word& value)
        {
            if(index >= 4)
                throw runtime_error("Quad word has 4 words only!");
            m_Value &= ~(0x000000000000FFFFu << (index * 16u));
            m_Value |= uint64_t(value << (index * 16u));

            return *this;
        }

        QuadWord& QuadWord::ClearWord(uint8_t index)
        {
            if(index >= 4)
                throw runtime_error("Quad word has 4 words only!");
            m_Value &= ~(0x000000000000FFFFu << (index * 16u));

            return *this;
        }

        dword QuadWord::GetDoubleWord(uint8_t index) const
        {
            if(index >= 2)
                throw runtime_error("Quad word has 2 double words only!");
            return (m_Value & (0x00000000FFFFFFFFu << (index * 32u))) >> (index * 32u);
        }

        QuadWord& QuadWord::SetDoubleWord(uint8_t index, const dword& value)
        {
            if(index >= 2)
                throw runtime_error("Quad word has 2 double words only!");
            m_Value &= ~(0x00000000FFFFFFFFu << (index * 32u));
            m_Value |= uint64_t(value << (index * 32u));

            return *this;
        }

        QuadWord& QuadWord::ClearDoubleWord(uint8_t index)
        {
            if(index >= 2)
                throw runtime_error("Quad word has 2 double words only!");
            m_Value &= ~(0x00000000FFFFFFFFu << (index * 32u));

            return *this;
        }
    }
}