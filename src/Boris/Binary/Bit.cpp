#include <Binary.h>

namespace Boris
{
    namespace Binary
    {
        Bit::Bit(bool value) :
            m_Value(value)
        {}

        Bit::Bit(uint8_t value) :
            m_Value(value != 0)
        {}

        bool Bit::operator==(const Bit& other) const
        {
            return m_Value == other.m_Value;
        }

        bool Bit::operator!=(const Bit& other) const
        {
            return m_Value != other.m_Value;
        }

        Bit Bit::operator&(const Bit& other) const
        {
            return m_Value & other.m_Value;
        }

        Bit& Bit::operator&=(const Bit& other)
        {
            m_Value &= other.m_Value;
            return *this;
        }

        Bit Bit::operator|(const Bit& other) const
        {
            return m_Value | other.m_Value;
        }

        Bit& Bit::operator|=(const Bit& other)
        {
            m_Value |= other.m_Value;
            return *this;
        }

        Bit Bit::operator^(const Bit& other) const
        {
            return m_Value ^ other.m_Value;
        }

        Bit& Bit::operator^=(const Bit& other)
        {
            m_Value ^= other.m_Value;
            return *this;
        }

        Bit::operator bool() const
        {
            return m_Value;
        }

        Bit::operator uint8_t() const
        {
            return m_Value ? 0x01 : 0x00;
        }
    }
}