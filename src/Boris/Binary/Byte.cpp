#include <Binary.h>

using namespace std;

namespace Boris
{
    namespace Binary
    {
        Byte::Byte(uint8_t value) :
                m_Value(value)
        {}

        Byte::Byte(array<bit, 8> bits) :
            m_Value()
        {
            for(uint8_t i = 0; i < 8; ++i)
                m_Value |= uint8_t(bits[i] << i);
        }

        bool Byte::operator==(const Byte& other) const
        {
            return m_Value == other.m_Value;
        }

        bool Byte::operator!=(const Byte& other) const
        {
            return m_Value != other.m_Value;
        }

        Byte Byte::operator&(const Byte& other) const
        {
            return m_Value & other.m_Value;
        }

        Byte& Byte::operator&=(const Byte& other)
        {
            m_Value &= other.m_Value;
            return *this;
        }

        Byte Byte::operator|(const Byte& other) const
        {
            return m_Value | other.m_Value;
        }

        Byte& Byte::operator|=(const Byte& other)
        {
            m_Value |= other.m_Value;
            return *this;
        }

        Byte Byte::operator^(const Byte& other) const
        {
            return m_Value ^ other.m_Value;
        }

        Byte& Byte::operator^=(const Byte& other)
        {
            m_Value ^= other.m_Value;
            return *this;
        }

        Byte::operator uint8_t() const
        {
            return m_Value;
        }

        Byte Byte::operator>>(unsigned int count) const
        {
            return m_Value >> count;
        }

        Byte& Byte::operator>>=(unsigned int count)
        {
            m_Value >>= count;
            return *this;
        }

        Byte Byte::operator<<(unsigned int count) const
        {
            return m_Value << count;
        }

        Byte& Byte::operator<<=(unsigned int count)
        {
            m_Value <<= count;
            return *this;
        }

        array<bit, 8> Byte::ToBits() const
        {
            array<bit, 8> bits;
            uint8_t idx = 0;
            for(uint8_t mask = 0x01; mask != 0x00; mask <<= 1u)
                bits[idx++] = m_Value & mask;

            return bits;
        }

        bit Byte::GetBit(uint8_t index) const
        {
            if(index >= 8)
                throw runtime_error("Byte has 8 bits only!");
            return m_Value & (0x01u << index);
        }

        Byte& Byte::SetBit(uint8_t index, const bit& value)
        {
            if(index >= 8)
                throw runtime_error("Byte has 8 bits only!");
            if(value)
                m_Value |= 0x01u << index;
            else
                m_Value &= ~(0x01u << index);

            return *this;
        }

        Byte& Byte::ClearBit(uint8_t index)
        {
            if(index >= 8)
                throw runtime_error("Byte has 8 bits only!");
            m_Value &= ~(0x01u << index);

            return *this;
        }
    }
}