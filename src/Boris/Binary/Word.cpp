#include <Binary.h>

using namespace std;

namespace Boris
{
    namespace Binary
    {
        Word::Word(uint16_t value) :
                m_Value(value)
        {}

        Word::Word(array<bit, 16> bits) :
                m_Value()
        {
            for(uint8_t i = 0; i < 16; ++i)
                m_Value |= uint16_t(bits[i] << i);
        }

        Word::Word(array<byte, 2> bytes) :
                m_Value()
        {
            for(uint8_t i = 0; i < 2; ++i)
                for(uint8_t j = 0; j < 8; ++j)
                    m_Value |= uint16_t(bytes[i].GetBit(j) << (j + i * 8u));
        }

        bool Word::operator==(const Word& other) const
        {
            return m_Value == other.m_Value;
        }

        bool Word::operator!=(const Word& other) const
        {
            return m_Value != other.m_Value;
        }

        Word Word::operator&(const Word& other) const
        {
            return m_Value & other.m_Value;
        }

        Word& Word::operator&=(const Word& other)
        {
            m_Value &= other.m_Value;
            return *this;
        }

        Word Word::operator|(const Word& other) const
        {
            return m_Value | other.m_Value;
        }

        Word& Word::operator|=(const Word& other)
        {
            m_Value |= other.m_Value;
            return *this;
        }

        Word Word::operator^(const Word& other) const
        {
            return m_Value ^ other.m_Value;
        }

        Word& Word::operator^=(const Word& other)
        {
            m_Value ^= other.m_Value;
            return *this;
        }

        Word Word::operator>>(unsigned int count) const
        {
            return m_Value >> count;
        }

        Word& Word::operator>>=(unsigned int count)
        {
            m_Value >>= count;
            return *this;
        }

        Word Word::operator<<(unsigned int count) const
        {
            return m_Value << count;
        }

        Word& Word::operator<<=(unsigned int count)
        {
            m_Value <<= count;
            return *this;
        }

        Word::operator uint16_t() const
        {
            return m_Value;
        }

        array<bit, 16> Word::ToBits() const
        {
            array<bit, 16> bits;
            uint8_t idx = 0;
            for(uint16_t mask = 0x0001; mask != 0x0000; mask <<= 1u)
                bits[idx++] = m_Value & mask;

            return bits;
        }

        array<byte, 2> Word::ToBytes() const
        {
            array<byte, 2> bytes;
            uint8_t idx = 0;
            for(uint16_t mask = 0x00FF; mask != 0x0000; mask <<= 8u)
            {
                bytes[idx] = uint16_t(m_Value & mask) >> (idx * 8u);
                ++idx;
            }

            return bytes;
        }

        bit Word::GetBit(uint8_t index) const
        {
            if(index >= 16)
                throw runtime_error("Word has 16 bits only!");
            return m_Value & (0x0001u << index);
        }

        Word& Word::SetBit(uint8_t index, const bit& value)
        {
            if(index >= 16)
                throw runtime_error("Word has 16 bits only!");
            if(value)
                m_Value |= 0x0001u << index;
            else
                m_Value &= ~(0x0001u << index);

            return *this;
        }

        Word& Word::ClearBit(uint8_t index)
        {
            if(index >= 16)
                throw runtime_error("Word has 16 bits only!");
            m_Value &= ~(0x0001u << index);

            return *this;
        }

        byte Word::GetByte(uint8_t index) const
        {
            if(index >= 2)
                throw runtime_error("Word has 2 bytes only!");
            return (m_Value & (0x00FFu << (index * 8u))) >> (index * 8u);
        }

        Word& Word::SetByte(uint8_t index, const byte& value)
        {
            if(index >= 2)
                throw runtime_error("Word has 2 bytes only!");
            m_Value &= ~(0x00FFu << (index * 8u));
            m_Value |= uint16_t(value << (index * 8u));

            return *this;
        }

        Word& Word::ClearByte(uint8_t index)
        {
            if(index >= 2)
                throw runtime_error("Word has 2 bytes only!");
            m_Value &= ~(0x00FFu << (index * 8u));

            return *this;
        }
    }
}