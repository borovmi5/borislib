#include <Graphics.h>
#include <Binary.h>

#define ERROR_FILE_CORRUPTED runtime_error("File corrupted.")

using namespace std;
using namespace Boris::Binary;
using namespace Boris::Geometry;

namespace Boris
{
    namespace Graphics
    {
        BMP::BMP(const std::string& filepath) :
            m_BitDepth(),
            m_ColorType(),
            m_ImageData(nullptr),
            m_DataOffset()
        {
            try
            {
                // Open stream
                BinaryStream file;
                file.open(filepath, ios_base::in);
                if(!file.is_open())
                    throw runtime_error("File cannot be read.");

                if(file.GetWord() != word(0x4d42))
                    throw runtime_error("Not BMP file.");

                uint32_t dataOffset = ReadHeader(file);

                ReadDIB(file);
                ReadColorTable(file);

                file.seekg(dataOffset, BinaryStream::beg);
                ReadData(file);

                ReadICC(file);

                // Close stream
                file.close();
            }
            catch(exception& e)
            {
                throw runtime_error("BMP: File " + filepath + " could not be opened. Reason: " + e.what());
            }
        }

        BMP::~BMP()
        {
            delete [] m_ImageData;
        }

        bool BMP::IsBMP(const string& filepath)
        {
            BinaryStream file;
            file.open(filepath, ios_base::in);
            if(!file.is_open())
                throw runtime_error("BMP: File cannot be read.");

            bool result = file.GetWord() == word(0x4d42);
            file.close();
            return result;
        }

        bool BMP::Alpha() const
        {
            return m_ColorType == ColorType::BGRA;
        }

        const Size& BMP::Size() const
        {
            return m_Size;
        }

        const Size& BMP::PPU() const
        {
            return m_PPU;
        }

        const uint8_t * BMP::Data() const
        {
            return m_ImageData;
        }

        uint32_t BMP::ReadHeader(BinaryStream& stream)
        {
            dword filesize, reserved, dataOffset;
            stream >> filesize >> reserved >> dataOffset;

            return dataOffset;
        }

        void BMP::ReadDIB(BinaryStream& stream)
        {
            dword headerSize;
            stream >> headerSize;
            auto headerType = static_cast<HeaderType>(uint32_t(headerSize));

            if(headerType == HeaderType::OS22XBITMAPHEADER || headerType == HeaderType::OS22XBITMAPHEADER_SHORT)
                throw runtime_error("OS/2 header not supported.");
            if(headerType == HeaderType::BITMAPV2INFOHEADER || headerType == HeaderType::BITMAPV3INFOHEADER)
                throw runtime_error("Adobe header not supported.");

            // BMP core header
            dword width, height;
            word planes, bitCount;

            // Size
            stream >> width >> height;
            m_Size = { width, height };

            // Number of color planes (=1)
            stream >> planes;
            if(planes != word(0x01))
                throw ERROR_FILE_CORRUPTED;

            // Bits per pixel
            stream >> bitCount;

            if(bitCount == word(0x18))
                m_ColorType = ColorType::BGR;
            else if(bitCount == word(0x20))
                m_ColorType = ColorType::BGRA;
            else
                throw runtime_error("Color type not supported.");
            // TODO: Add support to other formats.
            m_BitDepth = 8;

            if(headerType == HeaderType::BITMAPCOREHEADER)
                return;

            dword compression, sizeImage, ppuX, ppuY;
            dword clrUsed, clrImportant;

            stream >> compression >> sizeImage;
            stream >> ppuX >> ppuY;
            m_PPU = { ppuX, ppuY };
            stream >> clrUsed >> clrImportant;

            if(headerType == HeaderType::BITMAPINFOHEADER)
                return;

            dword redMask, greenMask, blueMask, alphaMask;
            dword csType;
            array<dword, 9> endpoints;
            dword gammaRed, gammaGreen, gammaBlue;

            stream >> redMask >> greenMask >> blueMask >> alphaMask;
            stream >> csType;
            for(auto& e : endpoints)
                stream >> e;
            stream >> gammaRed >> gammaGreen >> gammaBlue;

            if(headerType == HeaderType::BITMAPV4HEADER)
                return;

            dword intent, profileData, profileSize, reserved;
            stream >> intent;
            stream >> profileData >> profileSize;
            stream >> reserved;
        }

        void BMP::ReadColorTable(BinaryStream& stream)
        {
            // Not implemented yet. May be implemented in the future.
        }

        void BMP::ReadData(BinaryStream& stream)
        {
            unsigned int channelCount = Alpha() ? 4 : 3;
            unsigned int rem = m_Size.Area();

            m_ImageData = new uint8_t[rem * channelCount];

            unsigned int permutation [] = {
                    2, 1, 0, 3
            };

            byte data;

            while(rem--)
            {
                for(int channel = 0; channel < channelCount; ++channel)
                {
                    stream >> data;
                    m_ImageData[m_DataOffset++ - channel + permutation[channel]] = data;
                }
            }
        }

        void BMP::ReadICC(Boris::Binary::BinaryStream& stream)
        {
            // Not implemented yet.
        }
    }
}