#include <Graphics.h>

#include <GL/freeglut.h>

namespace Boris
{
    namespace Graphics
    {
        void glColor(const Color& color)
        {
            glColor4ub(color.Red(), color.Green(), color.Blue(), color.Alpha());
        }
    }
}