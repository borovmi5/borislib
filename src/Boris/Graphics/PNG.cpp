#include <Graphics.h>
#include <Binary.h>

#include <map>
#include <cstring>

#define ERROR_FILE_CORRUPTED runtime_error("File corrupted.")

using namespace std;
using namespace Boris::Binary;
using namespace Boris::Geometry;

namespace Boris
{
    namespace Graphics
    {
        // TODO: Implement other chunks

        // Convert chunk name to signature
        constexpr uint32_t ChunkCode(const char code[5])
        {
            return (code[0] << 24u) | (code[1] << 16u) | (code[2] << 8u) | code[3];
        }

        // Chunk types.
        // Available only in this file!
        enum ChunkType {
            IHDR = ChunkCode("IHDR"),
            PLTE = ChunkCode("PLTE"),
            IDAT = ChunkCode("IDAT"),
            IEND = ChunkCode("IEND"),
            cHRM = ChunkCode("cHRM"),
            gAMA = ChunkCode("gAMA"),
            iCCP = ChunkCode("iCCP"),
            sBIT = ChunkCode("sBIT"),
            sRGB = ChunkCode("sRGB"),
            bKGD = ChunkCode("bKGD"),
            hIST = ChunkCode("hIST"),
            tRNS = ChunkCode("tRNS"),
            pHYs = ChunkCode("pHYs"),
            sPLT = ChunkCode("sPLT"),
            tIME = ChunkCode("tIME"),
            iTXt = ChunkCode("iTXt"),
            tEXt = ChunkCode("tEXt"),
            zTXt = ChunkCode("zTXt")
        };

        PNG::PNG(const std::string& filepath) :
            m_BitDepth(),
            m_ColorType(),
            m_InterlaceMethod(),
            m_AlphaPalette(),
            m_Unit(),
            m_BufferTop(),
            m_BufferTopBits(),
            m_LastModified(),
            m_ImageData(nullptr),
            m_DataOffset(),
            m_DataSize()
        {
            try
            {
                // Init CRC
                if(!s_CRCInitialized)
                    InitCRC();

                // Open stream
                BinaryStream file;
                file.open(filepath, ios_base::in);
                if(!file.is_open())
                    throw runtime_error("File cannot be read.");

                file.Endianness(Endian::BIG);

                // PNG signature
                qword header;
                file >> header;
                if(header != qword(0x89504e470d0a1a0a))
                    throw runtime_error("Not PNG file.");

                // Chunks
                dword length;
                dword chunkType;
                std::map<ChunkType, bool> appearance;

                // Header chunk
                file >> length >> chunkType;
                if(IHDR != chunkType)
                    throw runtime_error("Missing header.");
                ReadIHDR(file, length);
                appearance[IHDR] = true;

                while(!appearance[IEND])
                {
                    file >> length >> chunkType;
                    if(file.eof())
                        break;
                    switch(chunkType)
                    {
                        case PLTE:
                            if(m_ColorType == ColorType::GRAYSCALE
                               || m_ColorType == ColorType::GRAYSCALE_ALPHA
                               || appearance[PLTE]
                               || appearance[IDAT])
                                throw ERROR_FILE_CORRUPTED;
                            ReadPLTE(file, length);
                            appearance[PLTE] = true;
                            break;
                        case IDAT:
                            ReadIDAT(file, length);
                            appearance[IDAT] = true;
                            break;
                        case IEND:
                            ReadIEND(file, length);
                            appearance[IEND] = true;
                            break;
                        case cHRM:
                            if(appearance[cHRM]
                               || appearance[IDAT]
                               || appearance[PLTE])
                                throw ERROR_FILE_CORRUPTED;
                            ReadcHRM(file, length);
                            appearance[cHRM] = true;
                            break;
                        case gAMA:
                            if(appearance[gAMA]
                               || appearance[IDAT]
                               || appearance[PLTE])
                                throw ERROR_FILE_CORRUPTED;
                            ReadgAMA(file, length);
                            appearance[gAMA] = true;
                            break;
                        case iCCP:
                            if(appearance[iCCP]
                               || appearance[IDAT]
                               || appearance[PLTE]
                               || appearance[sRGB])
                                throw ERROR_FILE_CORRUPTED;
                            ReadiCCP(file, length);
                            appearance[iCCP] = true;
                            break;
                        case sBIT:
                            if(appearance[sBIT]
                               || appearance[IDAT]
                               || appearance[PLTE])
                                throw ERROR_FILE_CORRUPTED;
                            ReadsBIT(file, length);
                            appearance[sBIT] = true;
                            break;
                        case sRGB:
                            if(appearance[sRGB]
                               || appearance[IDAT]
                               || appearance[PLTE]
                               || appearance[iCCP])
                                throw ERROR_FILE_CORRUPTED;
                            ReadsRGB(file, length);
                            appearance[sRGB] = true;
                            break;
                        case bKGD:
                            if(appearance[bKGD]
                               || appearance[IDAT])
                                throw ERROR_FILE_CORRUPTED;
                            ReadbKGD(file, length);
                            appearance[bKGD] = true;
                            break;
                        case hIST:
                            if(appearance[hIST]
                               || appearance[IDAT]
                               || !appearance[PLTE])
                                throw ERROR_FILE_CORRUPTED;
                            ReadhIST(file, length);
                            appearance[hIST] = true;
                            break;
                        case tRNS:
                            if(m_ColorType == ColorType::GRAYSCALE_ALPHA
                               || m_ColorType == ColorType::TRUECOLOR_ALPHA
                               || appearance[tRNS]
                               || appearance[IDAT]
                               || appearance[PLTE])
                                throw ERROR_FILE_CORRUPTED;
                            ReadtRNS(file, length);
                            appearance[tRNS] = true;
                            break;
                        case pHYs:
                            if(appearance[pHYs]
                               || appearance[IDAT])
                                throw ERROR_FILE_CORRUPTED;
                            ReadpHYs(file, length);
                            appearance[pHYs] = true;
                            break;
                        case sPLT:
                            if(appearance[IDAT])
                                throw ERROR_FILE_CORRUPTED;
                            ReadsPLT(file, length);
                            appearance[sPLT] = true;
                            break;
                        case tIME:
                            if(appearance[tIME])
                                throw ERROR_FILE_CORRUPTED;
                            ReadtIME(file, length);
                            appearance[tIME] = true;
                            break;
                        case iTXt:
                            ReadiTXt(file, length);
                            appearance[iTXt] = true;
                            break;
                        case tEXt:
                            ReadtEXt(file, length);
                            appearance[tEXt] = true;
                            break;
                        case zTXt:
                            ReadzTXt(file, length);
                            appearance[zTXt] = true;
                            break;
                        default:
                            throw ERROR_FILE_CORRUPTED;
                    }
                }

                // Check PLTE is present in file if is INDEXED
                if(!appearance[PLTE] && m_ColorType == ColorType::INDEXED)
                    throw runtime_error("Indexed PNG has no palette defined.");
                if(m_Buffer.empty())
                    throw ERROR_FILE_CORRUPTED;

                // W x H x Channels + H (for filter types)
                m_DataSize = m_Size.Area() * ( Alpha() ? 4 : 3 ) + m_Size.Height();
                m_ImageData = new uint8_t[m_DataSize];

                try
                {
                    Decompress();
                }
                catch(exception& e)
                {
                    throw runtime_error(string("zlib: ") + e.what());
                }
                Reconstruct();
                Flip();

                // Close stream
                file.close();
            }
            catch(exception& e)
            {
                throw runtime_error("PNG: File " + filepath + " could not be opened. Reason: " + e.what());
            }
        }

        PNG::~PNG()
        {
            delete [] m_ImageData;
        }

        bool PNG::IsPNG(const string& filepath)
        {
            BinaryStream file;
            file.open(filepath, ios_base::in);
            if(!file.is_open())
                throw runtime_error("PNG: File cannot be read.");

            file.Endianness(Endian::BIG);
            bool result = file.GetQWord() == qword(0x89504e470d0a1a0a);
            file.close();
            return result;
        }

        bool PNG::Alpha() const
        {
            return m_ColorType == ColorType::GRAYSCALE_ALPHA || m_ColorType == ColorType::TRUECOLOR_ALPHA || m_AlphaPalette;
        }

        bool PNG::Grayscale() const
        {
            return m_ColorType == ColorType::GRAYSCALE || m_ColorType == ColorType::GRAYSCALE_ALPHA;
        }

        const Size& PNG::Size() const
        {
            return m_Size;
        }

        const Size& PNG::PPU() const
        {
            return m_PPU;
        }

        bool PNG::PPUInMetres() const
        {
            return m_Unit == UnitType::METRE;
        }

        time_t PNG::LastModified() const
        {
            return m_LastModified;
        }

        const uint8_t* PNG::Data() const
        {
            return m_ImageData;
        }

        void PNG::InitCRC()
        {
            uint64_t CRCseed = 0xedb88320;
            for(uint16_t b = 0x00; b < 0x100; ++b)
            {
                uint64_t CRC = b;
                for(uint8_t bit = 0; bit < 8; ++bit)
                {
                    if(CRC & 1u)
                        CRC = CRCseed ^ (CRC >> 1u);
                    else
                        CRC >>= 1u;
                }

                s_CRCTable[b] = CRC;
            }

            s_CRCInitialized = true;
        }

        dword PNG::CRC(byte value, dword crc)
        {
            crc = ~crc;
            crc = s_CRCTable[byte(crc ^ value)] ^ (crc >> 8u);
            return ~crc;
        }

        dword PNG::CRC(const word& value, dword crc)
        {
            crc = ~crc;
            array<byte, 2> bytes = value.ToBytes();
            for(size_t i = 2; i > 0; --i)
                crc = s_CRCTable[byte(crc ^ bytes[i - 1])] ^ (crc >> 8u);
            return ~crc;
        }

        dword PNG::CRC(const dword& value, dword crc)
        {
            crc = ~crc;
            array<byte, 4> bytes = value.ToBytes();
            for(size_t i = 4; i > 0; --i)
                crc = s_CRCTable[byte(crc ^ bytes[i - 1])] ^ (crc >> 8u);
            return ~crc;
        }

        dword PNG::CRC(const qword& value, dword crc)
        {
            crc = ~crc;
            array<byte, 8> bytes = value.ToBytes();
            for(size_t i = 8; i > 0; --i)
                crc = s_CRCTable[byte(crc ^ bytes[i - 1])] ^ (crc >> 8u);
            return ~crc;
        }

        void PNG::ReadIHDR(BinaryStream& stream, uint32_t length)
        {
            if(length != 13)
                throw ERROR_FILE_CORRUPTED;
            dword header = IHDR;
            dword crc = CRC(header);

            dword width, height;
            byte depth, color, compression, filter, interlace;
            dword checksum;

            stream >> width >> height; // Read size
            crc = CRC(height, CRC(width, crc));

            stream >> depth >> color; // Read color info
            crc = CRC(color, CRC(depth, crc));

            stream >> compression >> filter >> interlace; // Read compression info
            crc = CRC(interlace, CRC(filter, CRC(compression, crc)));

            stream >> checksum; // Read checksum
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;

            m_Size = { width, height };

            m_BitDepth = depth;
            switch(color)
            {
                case static_cast<uint8_t>(ColorType::GRAYSCALE):
                    if(m_BitDepth != 0x01
                       && m_BitDepth != 0x02
                       && m_BitDepth != 0x04
                       && m_BitDepth != 0x08
                       && m_BitDepth != 0x10)
                        throw runtime_error("Restricted bit depth used.");
                    m_ColorType = static_cast<ColorType>(uint8_t(color));
                    break;
                case static_cast<uint8_t>(ColorType::TRUECOLOR):
                case static_cast<uint8_t>(ColorType::GRAYSCALE_ALPHA):
                case static_cast<uint8_t>(ColorType::TRUECOLOR_ALPHA):
                    if(m_BitDepth != 0x08
                       && m_BitDepth != 0x10)
                        throw runtime_error("Restricted bit depth used.");
                    m_ColorType = static_cast<ColorType>(uint8_t(color));
                    break;
                case static_cast<uint8_t>(ColorType::INDEXED):
                    if(m_BitDepth != 0x01
                       && m_BitDepth != 0x02
                       && m_BitDepth != 0x04
                       && m_BitDepth != 0x08)
                        throw runtime_error("Restricted bit depth used.");
                    m_ColorType = static_cast<ColorType>(uint8_t(color));
                    break;
                default:
                    throw runtime_error("Unknown color type.");
            }

            if(m_ColorType != ColorType::TRUECOLOR && m_ColorType != ColorType::TRUECOLOR_ALPHA)
                throw runtime_error("Color type not supported");

            if(0x00 != compression)
                throw runtime_error("Compression method not supported.");
            if(0x00 != filter)
                throw runtime_error("Filter method not supported.");

            switch(interlace)
            {
                case static_cast<uint8_t>(InterlaceMethod::NONE):
                    m_InterlaceMethod = static_cast<InterlaceMethod>(uint8_t(interlace));
                    break;
                case static_cast<uint8_t>(InterlaceMethod::ADAM7):
                default:
                    throw runtime_error("Interlace method not supported.");
            }
        }

        void PNG::ReadPLTE(BinaryStream& stream, uint32_t length)
        {
            if(length > 256 * 3 || length % 3 || length < 3)
                throw runtime_error("Invalid palette length.");

            dword header = PLTE;
            dword crc = CRC(header);
            byte r, g, b;
            while(length)
            {
                stream >> r >> g >> b;
                crc = CRC(b, CRC(g, CRC(r, crc)));
                m_Palette.emplace_back(Color(r, g, b));
                length /= 3;
            }
            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadIDAT(BinaryStream& stream, uint32_t length)
        {
            dword header = IDAT;
            dword crc = CRC(header);

            byte data;

            while(length--)
            {
                stream >> data;
                m_Buffer.push_back(data);
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadIEND(BinaryStream& stream, uint32_t length)
        {
            if(length != 0x00)
                throw ERROR_FILE_CORRUPTED;
            dword header = IEND;
            dword crc = CRC(header);
            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadcHRM(BinaryStream& stream, uint32_t length)
        {
            dword header = cHRM;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadgAMA(BinaryStream& stream, uint32_t length)
        {
            dword header = gAMA;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadiCCP(BinaryStream& stream, uint32_t length)
        {
            dword header = iCCP;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadsBIT(BinaryStream& stream, uint32_t length)
        {
            dword header = sBIT;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadsRGB(BinaryStream& stream, uint32_t length)
        {
            dword header = sRGB;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadbKGD(BinaryStream& stream, uint32_t length)
        {
            dword header = bKGD;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadhIST(BinaryStream& stream, uint32_t length)
        {
            dword header = hIST;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadtRNS(BinaryStream& stream, uint32_t length)
        {
            dword header = tRNS;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadpHYs(BinaryStream& stream, uint32_t length)
        {
            if(length != 9)
                throw ERROR_FILE_CORRUPTED;
            dword header = pHYs;
            dword crc = CRC(header);

            dword x, y;
            byte unit;

            stream >> x >> y;
            crc = CRC(y, CRC(x, crc));

            stream >> unit;
            crc = CRC(unit, crc);

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;

            m_PPU = { x, y };
            switch(unit)
            {
                case static_cast<uint8_t>(UnitType::UNKNOWN):
                case static_cast<uint8_t>(UnitType::METRE):
                    m_Unit = static_cast<UnitType>(uint8_t(unit));
                    break;
                default:
                    throw runtime_error("Unsupported unit.");
            }
        }

        void PNG::ReadsPLT(BinaryStream& stream, uint32_t length)
        {
            dword header = sPLT;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadtIME(BinaryStream& stream, uint32_t length)
        {
            if(length != 7)
                throw ERROR_FILE_CORRUPTED;
            dword header = tIME;
            dword crc = CRC(header);

            word year;
            byte month, day, hour, minute, second;

            stream >> year >> month >> day;
            crc = CRC(day, CRC(month, CRC(year, crc)));

            stream >> hour >> minute >> second;
            crc = CRC(second, CRC(minute, CRC(hour, crc)));

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;

            tm t {
                .tm_sec = uint16_t(second),
                .tm_min = uint16_t(minute),
                .tm_hour = uint16_t(hour),
                .tm_mday = uint16_t(day),
                .tm_mon = uint16_t(month) - 1,
                .tm_year = uint16_t(year) - 1900,
            };

            m_LastModified = mktime(&t);
        }

        void PNG::ReadiTXt(BinaryStream& stream, uint32_t length)
        {
            dword header = iTXt;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadtEXt(BinaryStream& stream, uint32_t length)
        {
            dword header = tEXt;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        void PNG::ReadzTXt(BinaryStream& stream, uint32_t length)
        {
            dword header = zTXt;
            dword crc = CRC(header);

            // Not implemented. Ignored in current version.
            byte data;
            while(length--)
            {
                stream >> data;
                crc = CRC(data, crc);
            }

            dword checksum;
            stream >> checksum;
            if(checksum != crc)
                throw ERROR_FILE_CORRUPTED;
        }

        //
        //  Functions for Huffman decompression are modified
        //  versions of:
        //  https://github.com/madler/zlib/blob/master/contrib/puff/puff.c
        //

        void PNG::Decompress()
        {
            // Compression method and flags
            byte CMF = m_Buffer.front();
            m_Buffer.pop_front();

            uint16_t windowSize = 0x0001; // zlib window size.

            if((0x0Fu & CMF) != 0x08u)
                throw runtime_error("Compression method not supported.");

            for(uint8_t i = ((0xF0u & CMF) >> 4u) + 8u; i; --i)
                windowSize <<= 1u;

            byte FLG = m_Buffer.front();
            m_Buffer.pop_front();

            word CHECK({ FLG, CMF });
            if(CHECK % 31)
                throw ERROR_FILE_CORRUPTED;

            if(FLG.GetBit(5))
                throw runtime_error("Dictionary not supported.");

            // FLEVEL not needed
            // DICTID forbidden
            // ADLER-32 extraction
            array<byte, 4> ADLER32_bytes;
            for(size_t idx = 0u; idx < 4u; ++idx)
            {
                ADLER32_bytes[idx] = m_Buffer.back();
                m_Buffer.pop_back();
            }
            dword ADLER32(ADLER32_bytes);

            // Data
            m_BufferTop = 0;
            m_BufferTopBits = 0;

            enum BlockType : uint8_t
            {
                NO_COMPRESS = 0x00,
                FIXED_HUFFMAN = 0x01,
                DYNAMIC_HUFFMAN = 0x02,
                ERROR = 0x03
            };
            bool lastBlock;
            int dyn_ct = 0;
            do
            {
                lastBlock = bool(PopBits(1));
                switch(PopBits(2))
                {
                    case NO_COMPRESS:
                        DecompressStatic();
                        break;
                    case FIXED_HUFFMAN:
                        DecompressFixed();
                        break;
                    case DYNAMIC_HUFFMAN:
                        ++dyn_ct;
                        DecompressDynamic();
                        break;
                    default:
                        throw ERROR_FILE_CORRUPTED;
                }

            } while(!lastBlock);

            uint16_t sumA = 1;
            uint16_t sumB = 0;

            // TODO: ADLER-32 Check
        }

        qword PNG::PopBits(unsigned int count)
        {
            if(count >= 64)
                throw range_error("Bit limit exceeded.");
            qword value = uint64_t(m_BufferTop);
            while(m_BufferTopBits < count)
            {
                if(m_Buffer.empty())
                    throw ERROR_FILE_CORRUPTED;
                value |= uint64_t(m_Buffer.front()) << m_BufferTopBits;
                m_Buffer.pop_front();
                m_BufferTopBits += 8;
            }

            m_BufferTop = uint8_t(value >> count);
            m_BufferTopBits -= count;

            return value & qword((1u << count) - 1u);
        }

        void PNG::DecompressStatic()
        {
            // throw away byte leftover
            m_BufferTop = 0x00;
            m_BufferTopBits = 0;

            if(m_Buffer.size() < 4)
                throw ERROR_FILE_CORRUPTED;

            word len({ m_Buffer[1], m_Buffer[0] });
            word nlen({ m_Buffer[3], m_Buffer[2] });
            for(int i = 4; i; --i)
                m_Buffer.pop_front();

            if(m_Buffer.size() < len || uint16_t(len) != ~nlen)
                throw ERROR_FILE_CORRUPTED;

            for(uint16_t i = len; i; --i)
            {
                if(m_DataOffset >= m_DataSize)
                    throw ERROR_FILE_CORRUPTED;
                m_ImageData[m_DataOffset++] = m_Buffer.front();
                m_Buffer.pop_front();
            }
        }

        vector<vector<uint16_t>> PNG::BuildHuffman(const vector<uint16_t>& lengths) const
        {
            // Init
            vector<vector<uint16_t>> codes(16);

            for(uint16_t symbol = 0; symbol < lengths.size(); ++symbol)
                codes[lengths[symbol]].push_back(symbol);

            // Check code validity
            if(codes[0].size() == lengths.size())
                throw ERROR_FILE_CORRUPTED;

            uint16_t left = 1;
            for(auto count = ++codes.begin(); count != codes.end(); ++count)
            {
                left <<= 1u;
                if(left < count->size())
                    throw ERROR_FILE_CORRUPTED;
                left -= count->size();
            }

            return codes;
        }

        void PNG::DecompressFixed()
        {
            if(!s_FixedHuffmanInitialized)
            {
                // 288 fixed Huffman literal/length codes defined
                vector<uint16_t> llLengths;
                llLengths.reserve(288);

                uint16_t symbol = 0;
                for(; symbol < 144; ++symbol)
                    llLengths.push_back(8);
                for(; symbol < 256; ++symbol)
                    llLengths.push_back(9);
                for(; symbol < 280; ++symbol)
                    llLengths.push_back(7);
                for(; symbol < 288; ++symbol)
                    llLengths.push_back(8);
                s_FixedHuffmanLL = BuildHuffman(llLengths);

                // 30 fixed Huffman distance codes defined
                vector<uint16_t> dLengths;
                dLengths.reserve(30);

                symbol = 0;
                for(; symbol < 30; ++symbol)
                    dLengths.push_back(5);
                s_FixedHuffmanD = BuildHuffman(dLengths);

                s_FixedHuffmanInitialized = true;
            }

            DecompressHuffman(s_FixedHuffmanLL, s_FixedHuffmanD);
        }

        void PNG::DecompressDynamic()
        {
            // Read Huffman codes header HLIT, HDIST, HCLEN
            uint16_t llCodeCount, dCodeCount, codeLengthCount;

            // See https://www.ietf.org/rfc/rfc1951.txt
            // 3.2.7. Compression with dynamic Huffman codes (BTYPE=10)
            llCodeCount = PopBits(5) + 257;
            dCodeCount = PopBits(5) + 1;
            codeLengthCount = PopBits(4) + 4;

            if(dCodeCount > 30)
                throw ERROR_FILE_CORRUPTED;

            // Read code lengths codes :)
            uint8_t permutation[] = {
                    16, 17, 18,  0,  8,  7,  9,
                     6, 10,  5, 11,  4, 12,  3,
                    13,  2, 14,  1, 15
            };

            vector<uint16_t> clLengths(19);

            for(uint8_t idx = 0; idx < codeLengthCount; ++idx)
                clLengths[permutation[idx]] = PopBits(3);

            auto clCodes = BuildHuffman(clLengths);

            // Read code lengths
            vector<uint16_t> llLengths(llCodeCount + dCodeCount);

            for(uint16_t idx = 0; idx < llLengths.size();)
            {
                uint16_t symbol = Decode(clCodes);
                if(symbol < 16)
                    llLengths[idx++] = symbol;
                else
                {
                    uint16_t repeat_count;
                    uint16_t length = 0;
                    if(symbol == 16)
                    {
                        if(idx == 0)
                            throw ERROR_FILE_CORRUPTED;
                        length = llLengths[idx - 1];
                        repeat_count = 0x03 + PopBits(2);
                    }
                    else if(symbol == 17)
                        repeat_count = 0x03 + PopBits(3);
                    else
                        repeat_count = 0x0B + PopBits(7);
                    if(idx + repeat_count > llLengths.size())
                        throw ERROR_FILE_CORRUPTED;
                    while(repeat_count--)
                        llLengths[idx++] = length;
                }
            }

            // Check EOB
            if(!llLengths[0x100])
                throw ERROR_FILE_CORRUPTED;

            vector<uint16_t> dLengths(dCodeCount);

            memcpy(dLengths.data(), llLengths.data() + llCodeCount, sizeof(uint16_t) * dCodeCount);

            llLengths.resize(llCodeCount);

            auto llCodes = BuildHuffman(llLengths);
            auto dCodes = BuildHuffman(dLengths);

            DecompressHuffman(llCodes, dCodes);
        }

        void PNG::DecompressHuffman(const vector<vector<uint16_t>>& llCodes, const vector<vector<uint16_t>>& dCodes)
        {
            // Init
            // Length codes size base.
            uint16_t lCodesBase[] = {
                      3,   4,   5,   6,   7,   8,   9,  10,
                     11,  13,  15,  17,  19,  23,  27,  31,
                     35,  43,  51,  59,  67,  83,  99, 115,
                    131, 163, 195, 227, 258
            };
            // Length codes extra bits.
            uint16_t lCodesExtra[] = {
                    0, 0, 0, 0, 0, 0, 0, 0,
                    1, 1, 1, 1, 2, 2, 2, 2,
                    3, 3, 3, 3, 4, 4, 4, 4,
                    5, 5, 5, 5, 0
            };
            // Distance codes offset base.
            uint16_t dCodesBase[] = {
                        1,     2,     3,     4,     5,     7,     9,    13,
                       17,    25,    33,    49,    65,    97,   129,   193,
                      257,   385,   513,   769,  1025,  1537,  2049,  3073,
                     4097,  6145,  8193, 12289, 16385, 24577
            };
            uint16_t dCodesExtra[] = {
                     0,  0,  0,  0,  1,  1,  2,  2,
                     3,  3,  4,  4,  5,  5,  6,  6,
                     7,  7,  8,  8,  9,  9, 10, 10,
                    11, 11, 12, 12, 13, 13
            };

            uint16_t symbol;
            while(true)
            {
                symbol = Decode(llCodes);
                if(symbol < 256)
                {
                    if(m_DataOffset >= m_DataSize)
                        throw ERROR_FILE_CORRUPTED;
                    m_ImageData[m_DataOffset++] = symbol;
                }
                else if(symbol == 256)
                    break;
                else
                {
                    symbol -= 257;
                    uint16_t length = lCodesBase[symbol] + PopBits(lCodesExtra[symbol]);

                    symbol = Decode(dCodes);
                    uint16_t distance = dCodesBase[symbol] + PopBits(dCodesExtra[symbol]);

                    if(distance > m_DataOffset || m_DataOffset + length > m_DataSize)
                        throw ERROR_FILE_CORRUPTED;

                    while(length--)
                    {
                        m_ImageData[m_DataOffset] = m_ImageData[m_DataOffset - distance];
                        ++m_DataOffset;
                    }
                }
            }
        }

        uint16_t PNG::Decode(const vector<vector<uint16_t>>& huffman)
        {
            uint16_t symbol = 0x0000;
            uint16_t first = 0x0000; // First symbol of this length

            for(uint16_t len = 1; len < 16u; ++len)
            {
                symbol |= PopBits(1);
                if(symbol < first + huffman[len].size())
                    return huffman[len][symbol - first];

                first += huffman[len].size();
                first <<= 1u;
                symbol <<= 1u;
            }
            throw ERROR_FILE_CORRUPTED;
        }

        uint8_t PNG::Paeth(uint8_t A, uint8_t B, uint8_t C)
        {
            int p = A + B - C;
            unsigned int pa = abs(p - A);
            unsigned int pb = abs(p - B);
            unsigned int pc = abs(p - C);
            if(pa <= pb && pa <= pc)
                return A;
            else if(pb <= pc)
                return B;
            else
                return C;
        }

        void PNG::Reconstruct()
        {
            m_DataOffset = 0;
            unsigned int imageOffset = 0;
            FilterType filterType = FilterType::NONE;
            unsigned int lineSize = m_Size.Width() * (Alpha() ? 4 : 3); // Bytes per scanline.
            unsigned int channelCount = Alpha() ? 4 : 3;
            uint8_t * X = m_ImageData;
            uint8_t * R = m_ImageData;
            uint8_t * A = m_ImageData - channelCount;
            uint8_t * B = m_ImageData - lineSize;
            uint8_t * C = m_ImageData - lineSize - channelCount;

            for(unsigned int y = 0; y < m_Size.Height(); ++y)
            {
                filterType = static_cast<FilterType>(*X++);
                for(unsigned int x = 0; x < lineSize; ++x)
                {
                    switch(filterType)
                    {
                        case FilterType::NONE:
                            *R++ = *X++;
                            break;
                        case FilterType::SUB:
                            *R++ = *X++ + (x / channelCount ? *A : 0);
                            break;
                        case FilterType::UP:
                            *R++ = *X++ + (y ? *B : 0);
                            break;
                        case FilterType::AVERAGE:
                            *R++ = *X++ + ((x / channelCount ? *A : 0) + (y ? *B : 0)) / 2;
                            break;
                        case FilterType::PAETH:
                            *R++ = *X++ + Paeth((x / channelCount ? *A : 0), (y ? *B : 0), ((x / channelCount) && y ? *C : 0) );
                            break;
                    }
                    ++A;
                    ++B;
                    ++C;
                }
            }
        }

        void PNG::Flip()
        {
            unsigned int lineSize = m_Size.Width() * (Alpha() ? 4 : 3);
            auto * swapSpace = new uint8_t[lineSize];
            for(int y = 0; y < m_Size.Height() / 2; ++y)
            {
                memcpy(swapSpace, m_ImageData + y * lineSize, lineSize);
                memcpy(m_ImageData + y * lineSize, m_ImageData + (m_Size.Height() - y - 1) * lineSize, lineSize);
                memcpy(m_ImageData + (m_Size.Height() - y - 1) * lineSize, swapSpace, lineSize);
            }
        }

        dword PNG::s_CRCTable[0x100];
        bool PNG::s_CRCInitialized = false;

        vector<vector<uint16_t>> PNG::s_FixedHuffmanLL;
        vector<vector<uint16_t>> PNG::s_FixedHuffmanD;
        bool PNG::s_FixedHuffmanInitialized = false;
    }
}