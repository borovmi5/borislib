#include <Graphics/Color.h>
#include <Core.h>

#include <regex>
#include <cmath>
#include <algorithm>

namespace Boris
{
    namespace Graphics
    {
        Color::Color() :
            m_Red(0),
            m_Green(0),
            m_Blue(0),
            m_Alpha(255)
        {}

        Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) :
            m_Red(r),
            m_Green(g),
            m_Blue(b),
            m_Alpha(a)
        {}

        Color::Color(double r, double g, double b, double a) :
            m_Red(uint8_t(clamp(r, 0., 1.) * 255)),
            m_Green(uint8_t(clamp(g, 0., 1.) * 255)),
            m_Blue(uint8_t(clamp(b, 0., 1.) * 255)),
            m_Alpha(uint8_t(clamp(a, 0., 1.) * 255))
        {}

        Color::Color(const char * hexcode) :
            m_Red(0),
            m_Green(0),
            m_Blue(0),
            m_Alpha(255)
        {
            if(hexcode[0] != '#')
                throw std::runtime_error("Hex color code has to start with #!");
            if(regex_match(hexcode, std::regex("^[#][0-9a-fA-F]{3}$")))
            {
                m_Red = hex_to_byte(hexcode[1]);
                m_Red = (m_Red << 4) + m_Red;
                m_Green = hex_to_byte(hexcode[2]);
                m_Green = (m_Green << 4) + m_Green;
                m_Blue = hex_to_byte(hexcode[3]);
                m_Blue = (m_Blue << 4) + m_Blue;
                m_Alpha = 255;
            }
            else if(regex_match(hexcode, std::regex("^[#][0-9a-fA-F]{4}$")))
            {
                m_Red = hex_to_byte(hexcode[2]);
                m_Red = (m_Red << 4) + m_Red;
                m_Green = hex_to_byte(hexcode[3]);
                m_Green = (m_Green << 4) + m_Green;
                m_Blue = hex_to_byte(hexcode[4]);
                m_Blue = (m_Blue << 4) + m_Blue;
                m_Alpha = hex_to_byte(hexcode[1]);
                m_Alpha = (m_Alpha << 4) + m_Alpha;
            }
            else if(regex_match(hexcode, std::regex("^[#][0-9a-fA-F]{6}$")))
            {
                char buf[2];
                std::memcpy(buf, hexcode + 1, 2);
                m_Red = hex_to_byte(buf);
                std::memcpy(buf, hexcode + 3, 2);
                m_Green = hex_to_byte(buf);
                std::memcpy(buf, hexcode + 5, 2);
                m_Blue = hex_to_byte(buf);
                m_Alpha = 255;
            }
            else if(regex_match(hexcode, std::regex("^[#][0-9a-fA-F]{8}$")))
            {
                char buf[2];
                std::memcpy(buf, hexcode + 3, 2);
                m_Red = hex_to_byte(buf);
                std::memcpy(buf, hexcode + 5, 2);
                m_Green = hex_to_byte(buf);
                std::memcpy(buf, hexcode + 7, 2);
                m_Blue = hex_to_byte(buf);
                std::memcpy(buf, hexcode + 1, 2);
                m_Alpha = hex_to_byte(buf);
            }
            else
                throw std::runtime_error("Invalid Hex color code!");
        }

        Color Color::FromHSV(double h, double s, double v, double a)
        {
            h -= int(h / 360) * 360;
            s = clamp(s, 0., 1.);
            v = clamp(v, 0., 1.);
            double c = v * s;
            double x = c * (1 - fabs(fmod(h/60, 2) - 1));
            double m = v - c;
            double tmp [3];
            if(h < 60)
            {
                tmp[0] = c;
                tmp[1] = x;
                tmp[2] = 0;
            }
            else if (h < 120)
            {
                tmp[0] = x;
                tmp[1] = c;
                tmp[2] = 0;
            }
            else if (h < 180)
            {
                tmp[0] = 0;
                tmp[1] = c;
                tmp[2] = x;
            }
            else if (h < 240)
            {
                tmp[0] = 0;
                tmp[1] = x;
                tmp[2] = c;
            }
            else if (h < 300)
            {
                tmp[0] = x;
                tmp[1] = 0;
                tmp[2] = c;
            }
            else
            {
                tmp[0] = c;
                tmp[1] = 0;
                tmp[2] = x;
            }

            uint8_t red = uint8_t(255 * tmp[0] + m);
            uint8_t green = uint8_t(255 * tmp[1] + m);
            uint8_t blue = uint8_t(255 * tmp[2] + m);
            uint8_t alpha = uint8_t(clamp(a, 0., 1.) * 255);

            return {red, green, blue, alpha};
        }

        bool Color::operator==(const Color& other) const
        {
            return m_Red == other.m_Red && m_Green == other.m_Green && m_Blue == other.m_Blue && m_Alpha == other.m_Alpha;
        }

        bool Color::operator!=(const Color& other) const
        {
            return m_Red != other.m_Red || m_Green != other.m_Green || m_Blue != other.m_Blue || m_Alpha != other.m_Alpha;
        }

        uint8_t Color::Red() const
        {
            return m_Red;
        }

        Color & Color::Red(uint8_t r)
        {
            m_Red = r;
            return *this;
        }

        Color& Color::Red(double r)
        {
            m_Red = uint8_t(clamp(r, 0., 1.) * 255);
            return *this;
        }

        uint8_t Color::Green() const
        {
            return m_Green;
        }

        Color & Color::Green(uint8_t g)
        {
            m_Green = g;
            return *this;
        }

        Color& Color::Green(double g)
        {
            m_Green = uint8_t(clamp(g, 0., 1.) * 255);
            return *this;
        }

        uint8_t Color::Blue() const
        {
            return m_Blue;
        }

        Color & Color::Blue(uint8_t b)
        {
            m_Blue = b;
            return *this;
        }

        Color& Color::Blue(double b)
        {
            m_Blue = uint8_t(clamp(b, 0., 1.) * 255);
            return *this;
        }

        uint8_t Color::Alpha() const
        {
            return m_Alpha;
        }

        Color & Color::Alpha(uint8_t a)
        {
            m_Alpha = a;
            return *this;
        }

        Color& Color::Alpha(double a)
        {
            m_Alpha = uint8_t(clamp(a, 0., 1.) * 255);
            return *this;
        }

        double Color::Hue() const
        {
            auto r = double(m_Red / 255.);
            auto g = double(m_Green / 255.);
            auto b = double(m_Blue / 255.);
            double max = std::max(std::max(r, g), b);
            double min = std::min(std::min(r, g), b);
            double delta = max - min;
            if(delta == 0)
                return 0.;
            else if(max == r)
                return 60. * fmod((g - b)/delta, 6.);
            else if(max == g)
                return 60. * ((b - r)/delta + 2);
            else
                return 60. * ((r - g)/delta + 4);
        }

        Color& Color::Hue(double h)
        {
            h -= int(h / 360) * 360;
            double s = Saturation();
            double v = Value();
            double c = v * s;
            double x = c * (1 - fabs(fmod(h/60, 2) - 1));
            double m = v - c;
            double tmp [3];
            if(h < 60)
            {
                tmp[0] = c;
                tmp[1] = x;
                tmp[2] = 0;
            }
            else if (h < 120)
            {
                tmp[0] = x;
                tmp[1] = c;
                tmp[2] = 0;
            }
            else if (h < 180)
            {
                tmp[0] = 0;
                tmp[1] = c;
                tmp[2] = x;
            }
            else if (h < 240)
            {
                tmp[0] = 0;
                tmp[1] = x;
                tmp[2] = c;
            }
            else if (h < 300)
            {
                tmp[0] = x;
                tmp[1] = 0;
                tmp[2] = c;
            }
            else
            {
                tmp[0] = c;
                tmp[1] = 0;
                tmp[2] = x;
            }

            m_Red = uint8_t(255 * tmp[0] + m);
            m_Green = uint8_t(255 * tmp[1] + m);
            m_Blue = uint8_t(255 * tmp[2] + m);

            return *this;
        }

        double Color::Saturation() const
        {
            auto r = double(m_Red / 255.);
            auto g = double(m_Green / 255.);
            auto b = double(m_Blue / 255.);
            double max = std::max(std::max(r, g), b);
            double min = std::min(std::min(r, g), b);
            double delta = max - min;
            if(max == 0)
                return 0.;
            else
                return delta / max;
        }

        Color& Color::Saturation(double s)
        {
            double h = Hue();
            s = clamp(s, 0., 1.);
            double v = Value();
            double c = v * s;
            double x = c * (1 - fabs(fmod(h/60, 2) - 1));
            double m = v - c;
            double tmp [3];
            if(h < 60)
            {
                tmp[0] = c;
                tmp[1] = x;
                tmp[2] = 0;
            }
            else if (h < 120)
            {
                tmp[0] = x;
                tmp[1] = c;
                tmp[2] = 0;
            }
            else if (h < 180)
            {
                tmp[0] = 0;
                tmp[1] = c;
                tmp[2] = x;
            }
            else if (h < 240)
            {
                tmp[0] = 0;
                tmp[1] = x;
                tmp[2] = c;
            }
            else if (h < 300)
            {
                tmp[0] = x;
                tmp[1] = 0;
                tmp[2] = c;
            }
            else
            {
                tmp[0] = c;
                tmp[1] = 0;
                tmp[2] = x;
            }

            m_Red = uint8_t(255 * tmp[0] + m);
            m_Green = uint8_t(255 * tmp[1] + m);
            m_Blue = uint8_t(255 * tmp[2] + m);

            return *this;
        }

        double Color::Value() const
        {
            auto r = double(m_Red / 255.);
            auto g = double(m_Green / 255.);
            auto b = double(m_Blue / 255.);
            double max = std::max(std::max(r, g), b);
            return max;
        }

        Color& Color::Value(double v)
        {
            double h = Hue();
            double s = Saturation();
            v = clamp(v, 0., 1.);
            double c = v * s;
            double x = c * (1 - fabs(fmod(h/60, 2) - 1));
            double m = v - c;
            double tmp [3];
            if(h < 60)
            {
                tmp[0] = c;
                tmp[1] = x;
                tmp[2] = 0;
            }
            else if (h < 120)
            {
                tmp[0] = x;
                tmp[1] = c;
                tmp[2] = 0;
            }
            else if (h < 180)
            {
                tmp[0] = 0;
                tmp[1] = c;
                tmp[2] = x;
            }
            else if (h < 240)
            {
                tmp[0] = 0;
                tmp[1] = x;
                tmp[2] = c;
            }
            else if (h < 300)
            {
                tmp[0] = x;
                tmp[1] = 0;
                tmp[2] = c;
            }
            else
            {
                tmp[0] = c;
                tmp[1] = 0;
                tmp[2] = x;
            }

            m_Red = uint8_t(255 * tmp[0] + m);
            m_Green = uint8_t(255 * tmp[1] + m);
            m_Blue = uint8_t(255 * tmp[2] + m);

            return *this;
        }

        Color Color::Mix(const Boris::Graphics::Color& other, double ratio) const
        {
            ratio = clamp(ratio, 0., 1.);
            uint8_t red = std::max(m_Red - other.m_Red, other.m_Red - m_Red);
            uint8_t green = std::max(m_Green - other.m_Green, other.m_Green - m_Green);
            uint8_t blue = std::max(m_Blue - other.m_Blue, other.m_Blue - m_Blue);
            uint8_t alpha = std::max(m_Alpha - other.m_Alpha, other.m_Alpha - m_Alpha);

            red = uint8_t(red * ratio);
            green = uint8_t(green * ratio);
            blue = uint8_t(blue * ratio);
            alpha = uint8_t(alpha * ratio);

            red = m_Red + (m_Red > other.m_Red ? -red : red);
            green = m_Green + (m_Green > other.m_Green ? -green : green);
            blue = m_Blue + (m_Blue > other.m_Blue ? -blue : blue);
            alpha = m_Alpha + (m_Alpha > other.m_Alpha ? -alpha : alpha);

            return {red, green, blue, alpha};
        }

        void Color::RGB(uint8_t r, uint8_t g, uint8_t b)
        {
            m_Red = r;
            m_Green = g;
            m_Blue = b;
        }

        void Color::RGB(double r, double g, double b)
        {
            m_Red = uint8_t(clamp(r, 0., 1.) * 255);
            m_Green = uint8_t(clamp(g, 0., 1.) * 255);
            m_Blue = uint8_t(clamp(b, 0., 1.) * 255);
        }

        void Color::RGBA(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
        {
            m_Red = r;
            m_Green = g;
            m_Blue = b;
            m_Alpha = a;
        }

        void Color::RGBA(double r, double g, double b, double a)
        {
            m_Red = uint8_t(clamp(r, 0., 1.) * 255);
            m_Green = uint8_t(clamp(g, 0., 1.) * 255);
            m_Blue = uint8_t(clamp(b, 0., 1.) * 255);
            m_Alpha = uint8_t(clamp(a, 0., 1.) * 255);
        }

        void Color::HSV(double h, double s, double v, double a)
        {
            h -= int(h / 360) * 360;
            s = clamp(s, 0., 1.);
            v = clamp(v, 0., 1.);
            double c = v * s;
            double x = c * (1 - fabs(fmod(h/60, 2) - 1));
            double m = v - c;
            double tmp [3];
            if(h < 60)
            {
                tmp[0] = c;
                tmp[1] = x;
                tmp[2] = 0;
            }
            else if (h < 120)
            {
                tmp[0] = x;
                tmp[1] = c;
                tmp[2] = 0;
            }
            else if (h < 180)
            {
                tmp[0] = 0;
                tmp[1] = c;
                tmp[2] = x;
            }
            else if (h < 240)
            {
                tmp[0] = 0;
                tmp[1] = x;
                tmp[2] = c;
            }
            else if (h < 300)
            {
                tmp[0] = x;
                tmp[1] = 0;
                tmp[2] = c;
            }
            else
            {
                tmp[0] = c;
                tmp[1] = 0;
                tmp[2] = x;
            }

            m_Red = uint8_t(255 * tmp[0] + m);
            m_Green = uint8_t(255 * tmp[1] + m);
            m_Blue = uint8_t(255 * tmp[2] + m);
            m_Alpha = uint8_t(clamp(a, 0., 1.) * 255);
        }

        Color operator""_rgb(unsigned long long rgb)
        {
            assert(rgb <= 0xFFFFFF);
            return { uint8_t((rgb & 0x00FF0000u) >> 16u), uint8_t((rgb & 0x0000FF00u) >> 8u), uint8_t((rgb & 0x000000FFu)) };
        }

        Color operator""_argb(unsigned long long argb)
        {
            assert(argb <= 0xFFFFFFFF);
            return { uint8_t((argb & 0x00FF0000u) >> 16u), uint8_t((argb & 0x0000FF00u) >> 8u), uint8_t((argb & 0x000000FFu)), uint8_t((argb & 0xFF000000u) >> 24u) };
        }

        Color operator""_rgb_short(unsigned long long rgb)
        {
            assert(rgb <= 0xFFF);
            return { uint8_t(((rgb & 0x0F00u) >> 8u) | ((rgb & 0x0F00u) >> 4u)), uint8_t(((rgb & 0x00F0u) >> 4u) | ((rgb & 0x00F0u))), uint8_t(((rgb & 0x000Fu)) | ((rgb & 0x000Fu) << 4u)) };
        }

        Color operator""_argb_short(unsigned long long argb)
        {
            assert(argb <= 0xFFFF);
            return { uint8_t(((argb & 0x0F00u) >> 8u) | ((argb & 0x0F00u) >> 4u)), uint8_t(((argb & 0x00F0u) >> 4u) | ((argb & 0x00F0u))), uint8_t(((argb & 0x000Fu)) | ((argb & 0x000Fu) << 4u)), uint8_t(((argb & 0xF000u) >> 12u) | ((argb & 0xF000u) >> 8u)) };
        }
    }
}