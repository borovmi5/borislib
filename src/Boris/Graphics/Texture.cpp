#include <Core.h>
#include <Graphics.h>
#include <Geometry.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>

using namespace std;
using namespace Boris::Geometry;

namespace Boris
{
    namespace Graphics
    {
        Texture::Texture(std::nullptr_t) :
            Object(null),
            m_TextureID(0),
            m_TextureCount(nullptr),
            m_Size(),
            m_Alpha()
        {}

        Texture::Texture(const std::string& texture_path, bool filter) :
            Object(),
            m_TextureID(),
            m_TextureCount(nullptr),
            m_Size(),
            m_Alpha()
        {
            const uint8_t * imageData;
            BMP * bmp = nullptr;
            PNG * png = nullptr;
            try
            {
            if(BMP::IsBMP(texture_path))
            {
                bmp = new BMP(texture_path);
                imageData = bmp->Data();
                m_Size = bmp->Size();
                m_Alpha = bmp->Alpha();
            }
            else if(PNG::IsPNG(texture_path))
            {
                png = new PNG(texture_path);
                imageData = png->Data();
                m_Size = png->Size();
                m_Alpha = png->Alpha();
            }
            else
                throw runtime_error("Unsupported texture format.");
            }
            catch(exception& e)
            {
                delete bmp;
                delete png;
                throw e;
            }

            m_TextureCount = new unsigned int(1);

            glGenTextures(1, &m_TextureID);
            glBindTexture(GL_TEXTURE_2D, m_TextureID);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_Size.Width(), m_Size.Height(), 0, m_Alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, imageData);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter ? GL_LINEAR : GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter ? GL_LINEAR : GL_NEAREST);

            delete bmp;
            delete png;
        }

        Texture::Texture(const Image& image, bool filter) :
            m_TextureID(),
            m_TextureCount(nullptr),
            m_Size(image.Size()),
            m_Alpha(true)
        {

            m_TextureCount = new unsigned int(1);

            glGenTextures(1, &m_TextureID);
            glBindTexture(GL_TEXTURE_2D, m_TextureID);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_Size.Width(), m_Size.Height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, image.Data());
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter ? GL_LINEAR : GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter ? GL_LINEAR : GL_NEAREST);
        }

        Texture::Texture(const Texture& other) :
            Object(other),
            m_TextureID(other.m_TextureID),
            m_TextureCount(other.m_TextureCount),
            m_Size(other.m_Size),
            m_Alpha(other.m_Alpha)
        {
            if(!m_Null)
                (*m_TextureCount)++;
        }

        Texture::Texture(Texture&& other) noexcept :
                Object(other),
                m_TextureID(other.m_TextureID),
                m_TextureCount(other.m_TextureCount),
                m_Size(other.m_Size),
                m_Alpha(other.m_Alpha)
        {
            if(!m_Null)
                (*m_TextureCount)++;
        }

        Texture::~Texture()
        {
            if(!m_Null)
            {
                ( *m_TextureCount )--;
                if(*m_TextureCount < 1)
                {
                    glDeleteTextures(1, &m_TextureID);
                    delete m_TextureCount;
                    m_TextureCount = nullptr;
                }
            }
        }

        Texture& Texture::operator=(const Texture& other)
        {
            if(this == &other)
                return *this;

            if(!m_Null)
            {
                ( *m_TextureCount )--;
                if(*m_TextureCount < 1)
                {
                    glDeleteTextures(1, &m_TextureID);
                    delete m_TextureCount;
                    m_TextureCount = nullptr;
                }
            }

            m_TextureID = other.m_TextureID;
            m_TextureCount = other.m_TextureCount;
            m_Size = other.m_Size;
            m_Alpha = other.m_Alpha;
            m_Null = other.m_Null;

            if(!m_Null)
                (*m_TextureCount)++;

            return *this;
        }

        Texture& Texture::operator=(Texture&& other) noexcept
        {
            if(this == &other)
                return *this;

            if(!m_Null)
            {
                ( *m_TextureCount )--;
                if(*m_TextureCount < 1)
                {
                    glDeleteTextures(1, &m_TextureID);
                    delete m_TextureCount;
                    m_TextureCount = nullptr;
                }
            }

            m_TextureID = other.m_TextureID;
            m_TextureCount = other.m_TextureCount;
            m_Size = other.m_Size;
            m_Alpha = other.m_Alpha;
            m_Null = other.m_Null;

            if(!m_Null)
                (*m_TextureCount)++;

            return *this;
        }

        void Texture::Apply() const
        {
            if(m_Null)
                throw NullException();
            glBindTexture(GL_TEXTURE_2D, m_TextureID);
        }

        const Size& Texture::Size() const
        {
            if(m_Null)
                throw NullException();
            return m_Size;
        }

        const bool& Texture::Alpha() const
        {
            if(m_Null)
                throw NullException();
            return m_Alpha;
        }
    }
}