#include <Graphics.h>
#include <Geometry.h>

#include <vector>

using namespace std;
using namespace Boris::Geometry;

namespace Boris
{
    namespace Graphics
    {
        Image::Image() :
            m_ImageData(),
            m_Size(),
            m_Background(Colors::White)
        {}

        Image::Image(const class Size& size, const Color& background) :
            m_ImageData(size.Area() * 4),
            m_Size(size),
            m_Background(background)
        {
            for(int y = 0; y < size.Height(); ++y)
                for(int x = 0; x < size.Width(); ++x)
                {
                    m_ImageData[4 * (y * size.Width() + x) + 0] = background.Red();
                    m_ImageData[4 * (y * size.Width() + x) + 1] = background.Green();
                    m_ImageData[4 * (y * size.Width() + x) + 2] = background.Blue();
                    m_ImageData[4 * (y * size.Width() + x) + 3] = background.Alpha();
                }
        }

        Color Image::GetPixel(const Point& pixel) const
        {
            if(pixel.X() < 0
               || pixel.X() >= m_Size.Width()
               || pixel.Y() < 0
               || pixel.Y() >= m_Size.Height())
                throw range_error("Pixel out of image range.");

            return {
                m_ImageData[4 * (pixel.Y() * m_Size.Width() + pixel.X()) + 0],
                m_ImageData[4 * (pixel.Y() * m_Size.Width() + pixel.X()) + 1],
                m_ImageData[4 * (pixel.Y() * m_Size.Width() + pixel.X()) + 2],
                m_ImageData[4 * (pixel.Y() * m_Size.Width() + pixel.X()) + 3]
            };
        }

        void Image::SetPixel(const Point& pixel, const Color& color)
        {
            if(pixel.X() < 0
               || pixel.X() >= m_Size.Width()
               || pixel.Y() < 0
               || pixel.Y() >= m_Size.Height())
                throw range_error("Pixel out of image range.");

            m_ImageData[4 * ((m_Size.Height() - pixel.Y() - 1) * m_Size.Width() + pixel.X()) + 0] = color.Red();
            m_ImageData[4 * ((m_Size.Height() - pixel.Y() - 1) * m_Size.Width() + pixel.X()) + 1] = color.Green();
            m_ImageData[4 * ((m_Size.Height() - pixel.Y() - 1) * m_Size.Width() + pixel.X()) + 2] = color.Blue();
            m_ImageData[4 * ((m_Size.Height() - pixel.Y() - 1) * m_Size.Width() + pixel.X()) + 3] = color.Alpha();
        }

        void Image::SetRectangle(const Point& point, const class Size& size, const Color& color)
        {
            if(point.X() < 0
               || point.X() >= m_Size.Width()
               || point.Y() < 0
               || point.Y() >= m_Size.Height())
                throw range_error("Pixel out of image range.");

            for(int y = point.Y(); y < min(point.Y() + size.Height(), m_Size.Height()); ++y)
                for(int x = point.X(); x < min(point.X() + size.Width(), m_Size.Width()); ++x)
                {
                    m_ImageData[4 * ((m_Size.Height() - y - 1) * m_Size.Width() + x) + 0] = color.Red();
                    m_ImageData[4 * ((m_Size.Height() - y - 1) * m_Size.Width() + x) + 1] = color.Green();
                    m_ImageData[4 * ((m_Size.Height() - y - 1) * m_Size.Width() + x) + 2] = color.Blue();
                    m_ImageData[4 * ((m_Size.Height() - y - 1) * m_Size.Width() + x) + 3] = color.Alpha();
                }
        }

        const Size& Image::Size() const
        {
            return m_Size;
        }

        void Image::Resize(const class Size& size)
        {
            if(size.Width() < m_Size.Width())
            {
                for(int y = 0; y < m_Size.Height(); ++y)
                {
                    auto * src = m_ImageData.data() + y * m_Size.Width() * 4;
                    auto * dst = m_ImageData.data() + y * size.Width() * 4;

                    for(int x = 4 * size.Width(); x; --x)
                        *dst++ = *src++;
                }
                m_ImageData.resize(size.Width() * m_Size.Height() * 4);
            }
            else if(size.Width() > m_Size.Width())
            {
                m_ImageData.resize(size.Width() * m_Size.Height() * 4);
                for(int y = m_Size.Height() - 1; y; --y)
                {
                    auto * src = m_ImageData.data() + (y + 1) * m_Size.Width() * 4 - 1;
                    auto * dst = m_ImageData.data() + (y + 1) * size.Width() * 4 - 1;

                    for(int x = size.Width() - m_Size.Width(); x; --x)
                    {
                        *dst-- = m_Background.Alpha();
                        *dst-- = m_Background.Blue();
                        *dst-- = m_Background.Green();
                        *dst-- = m_Background.Red();
                    }
                    for(int x = 4 * m_Size.Width(); x; --x)
                        *dst-- = *src--;
                }
            }
            m_Size.Width(size.Width());

            if(size.Height() < m_Size.Height())
            {
                auto * src = m_ImageData.data() + (m_Size.Height() - size.Height()) * m_Size.Width() * 4;
                auto * dst = m_ImageData.data();
                for(int x = size.Area() * 4; x; --x)
                    *dst++ = *src++;
                m_ImageData.resize(size.Area() * 4);
            }
            else if(size.Height() > m_Size.Height())
            {
                m_ImageData.resize(size.Area() * 4);

                auto * src = m_ImageData.data() + m_Size.Area() * 4 - 1;
                auto * dst = m_ImageData.data() + size.Area() * 4 - 1;

                for(int x = m_Size.Area(); x; --x)
                {
                    *dst-- = *src;
                    *src-- = m_Background.Alpha();
                    *dst-- = *src;
                    *src-- = m_Background.Blue();
                    *dst-- = *src;
                    *src-- = m_Background.Green();
                    *dst-- = *src;
                    *src-- = m_Background.Red();
                }
            }
            m_Size.Height(size.Height());
        }

        const uint8_t * Image::Data() const
        {
            return m_ImageData.data();
        }
    }
}