#include <MapConvertor.h>
#include <Binary.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>

#define EOF_TEST(stream)\
    if(stream.eof())\
        throw runtime_error("Unexpected EOF")

#define UNEXPECTED_SYMB\
    throw runtime_error("Unexpected symbol.")

#define TOO_LONG\
    throw runtime_error("Line too long.")

using namespace std;
using namespace Boris::Binary;

namespace TowerAttack
{
    namespace Map
    {
        void ReadCommentBlock(fstream& stream)
        {
            char buffer[TAMC_BUFFER_SIZE];
            char * sync = nullptr;
            int offset = 0;

            while(true)
            {
                stream.getline(buffer, TAMC_BUFFER_SIZE);
                if(strlen(buffer) >= TAMC_BUFFER_SIZE - 1)
                    TOO_LONG;
                sync = strchr(buffer, TAMC_SYNC_MARK);
                if(sync)
                {
                    break;
                }
                if(stream.eof())
                    return;
            }
            stream.seekg(-strlen(buffer) - 1, fstream::cur);
        }

        void TxtToMap(const char * source, const char * dest)
        {
            // Open files
            fstream in;
            BinaryStream out;
            in.open(source, ios_base::in);
            if(!in.is_open())
                throw runtime_error("Source file could not be opened.");
            out.open(dest, ios_base::out);
            if(!out.is_open())
                throw runtime_error("Source file could not be opened");

            unsigned int width = 0, height = 0, offset = 0;
            vector<unsigned int> entrances;
            unsigned int exit = -1;
            fstream::pos_type dataStart;
            char buffer[TAMC_BUFFER_SIZE];
            char * c;

            // Skip upper comment block;
            ReadCommentBlock(in);
            EOF_TEST(in);

            // Read upper sync marks
            in.getline(buffer, TAMC_BUFFER_SIZE);
            if(strlen(buffer) >= TAMC_BUFFER_SIZE - 1)
                TOO_LONG;
            EOF_TEST(in);
            c = strchr(buffer, TAMC_SYNC_MARK);
            offset = c - buffer;
            c = strrchr(buffer, TAMC_SYNC_MARK);
            if(c - buffer == offset)
                throw runtime_error("Missing sync mark.");
            for(char * i = buffer + offset + 1; i < c; ++i)
                if(*i != ' ')
                    UNEXPECTED_SYMB;
            for(char * i = c + 1; i < buffer + strlen(buffer); ++i)
            {
                if(*i == TAMC_COMMENT)
                    break;
                if(*i != ' ')
                    UNEXPECTED_SYMB;
            }
            width = c - buffer - offset - 1;
            if(width < 1)
                throw runtime_error("Width must be > 0.");

            dataStart = in.tellg();

            // Read map
            while(true)
            {
                in.getline(buffer, TAMC_BUFFER_SIZE);
                if(strlen(buffer) >= TAMC_BUFFER_SIZE - 1)
                    TOO_LONG;
                if(strchr(buffer, TAMC_SYNC_MARK))
                    break;

                EOF_TEST(in);

                // Line start empty
                for(char * i = buffer; i < buffer + offset; ++i)
                    if(*i != ' ')
                        UNEXPECTED_SYMB;

                if(buffer[offset] == TAMC_EXIT)
                    entrances.push_back(height);
                else if(buffer[offset] != ' ')
                    UNEXPECTED_SYMB;

                // Check valid tiles
                for(char * i = buffer + offset + 1; i < buffer + offset + width + 1; ++i)
                    if(*i != TAMC_PATH && *i != TAMC_FREE)
                        UNEXPECTED_SYMB;

                // Check exit
                if(buffer[offset + width + 1] == TAMC_EXIT)
                {
                    if(exit != -1)
                        throw runtime_error("Multiple exits not allowed");
                    else
                        exit = height;
                }
                else if (buffer[offset + width + 1] != ' ')
                    UNEXPECTED_SYMB;

                for(char * i = c + 1; i < buffer + strlen(buffer); ++i)
                {
                    if(*i == TAMC_COMMENT)
                        break;
                    if(*i != ' ')
                        UNEXPECTED_SYMB;
                }

                ++height;
            }

            if(height < 1)
                throw runtime_error("Height must be > 0.");

            // Read lower sync marks
            c = strchr(buffer, TAMC_SYNC_MARK);
            if(offset != c - buffer)
                UNEXPECTED_SYMB;
            c = strrchr(buffer, TAMC_SYNC_MARK);
            if(c - buffer == offset)
                throw runtime_error("Missing sync mark.");
            for(char * i = buffer + offset + 1; i < c; ++i)
                if(*i != ' ')
                    UNEXPECTED_SYMB;
            for(char * i = c + 1; i < buffer + strlen(buffer); ++i)
            {
                if(*i == TAMC_COMMENT)
                    break;
                if(*i != ' ')
                    UNEXPECTED_SYMB;
            }
            if(width != c - buffer - offset - 1)
                UNEXPECTED_SYMB;

            // Read lower comment block
            ReadCommentBlock(in);
            if(!in.eof())
                UNEXPECTED_SYMB;

            in.seekg(dataStart);

            out << dword(0x504d4154); // Header
            out << dword(); // Reserve
            out << word(width); // Width
            out << word(height); // Height
            out << word(entrances.size()); // # entrances
            out << word(exit); // Exit y-coord
            for(auto e : entrances)
                out << word(e); // Entrance list
            for(int y = 0; y < height; ++y)
            {
                in.getline(buffer, TAMC_BUFFER_SIZE);
                for(int x = offset + 1; x < offset + 1 + width; ++x)
                    out << bit(buffer[x] == TAMC_PATH);
            }
            out.close();
            out.open(dest, ios_base::in | ios_base::out);
            out << out.Checksum();
        }
    }
}

int main(int argc, char * argv[])
{
    if(argc != 3)
    {
        cerr << "ERROR: 2 arguments expected, got " << argc - 1 << "." << endl
             << "Usage: ./MapConvertor <source> <destination>" << endl;
        return -1;
    }
    try
    {
        TowerAttack::Map::TxtToMap(argv[1], argv[2]);
    }
    catch (exception& e)
    {
        cerr << "ERROR: Conversion failed." << endl
             << e.what() << endl;
        return -1;
    }
    catch (...)
    {
        cerr << "ERROR: Conversion failed." << endl;
        return -2;
    }

    cout << "Done." << endl;

    return 0;
}